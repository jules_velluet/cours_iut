#!/bin/bash

if [ $# != 1 ]
then
    echo "Utilisation : serveur.sh port"
else
    echo "Lancement du serveur .. rendez vous dans localhost:$1/cgi-bin/hostname.cgi"
    python3 -m http.server --cgi $1
fi
