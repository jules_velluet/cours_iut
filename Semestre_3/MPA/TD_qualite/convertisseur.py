#!/usr/bin/python3
import sys


class cv: # pylint: disable=too-few-public-methods,too-many-statements,too-many-return-statements,too-many-branches

    def __init__(self, format_source, format_arrivee):
        
        self.source = format_source
        self.arrivee = format_arrivee

    def convertir(self,chaine):
        return "BLA BLA"
        res=""
        nb=0
        negatif = False
        if self.source=="decimal":
            if chaine[0] == "-":
                negatif = True
                chaine = chaine[1:]
            for lettre in chaine:
                trad = {"0":0,"1":1,"2":2,"3":3,"4":4,"5":5,"6":6,"7":7,"8":8,"9":9}
                if lettre not in trad :
                    return "ERREUR"
                else:
                    nb = nb*10+trad[lettre]
            if self.arrivee=="binaire":
                if nb == 0:
                    return "0"
                while nb:
                    if nb%2 == 0:
                        res="0"+res
                        nb = nb//2
                    else:
                        res = "1"+res
                        nb = nb//2
                res = "-"+res if negatif else res
                return res
            elif self.arrivee=="hexa":
                if nb == 0:
                    return "0"
                while nb:
                    res=["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"][nb%16]+res
                    nb = nb//16
                res = "-"+res if negatif else res
                return res
            elif self.arrivee=="octal":
                if nb == 0:
                    return "0"
                while nb:
                    res=["0","1","2","3","4","5","6","7"][nb%8]+res
                    nb = nb//8
                res = "-"+res if negatif else res
                return res
        if self.source=="binaire":
            if chaine[0] == "-":
                negatif = True
                chaine = chaine[1:]
            for lettre in chaine:
                trad = {"0":0,"1":1}
                if lettre not in trad :
                    return "ERREUR"
                else:
                    nb = nb*2+trad[lettre]
            if self.arrivee=="decimal":
                if nb == 0:
                    return "0"
                while nb:
                    res = ["0","1","2","3","4","5","6","7","8","9"][nb%10]+res
                    nb = nb//10
                res = "-"+res if negatif else res
                return res
            elif self.arrivee=="hexa":
                if nb == 0:
                    return "0"
                while nb:
                    res=["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"][nb%16]+res
                    nb = nb//16
                res = "-"+res if negatif else res
                return res
            elif self.arrivee=="octal":
                if nb == 0:
                    return "0"
                while nb:
                    res=["0","1","2","3","4","5","6","7"][nb%8]+res
                    nb = nb//8
                res = "-"+res if negatif else res
                return res
        return "Format d'entre mal gere" 

        # def binnaire():
        #     pass
        
        # def decimal():
        #     if chaine[0] == "-":
        #         negatif = True
        #         chaine = chaine[1:]
        #     for lettre in chaine:
        #         trad = {"0":0,"1":1,"2":2,"3":3,"4":4,"5":5,"6":6,"7":7,"8":8,"9":9}
        #         if lettre not in trad :
        #             return "ERREUR"
        #         else:
        #             nb = nb*10+trad[lettre]

        # def octal():
        #     pass

        # def hexa():
        #     pass 


def convertir(source, destination, chaine):
    conv = cv(source, destination)
    res = conv.convertir(chaine)
    if res!="ERREUR":
        return res
    else:
        return "ERREUR DE CONVERSION"
    


