drop table PERSONNE cascade constraints;
drop table MEDECIN cascade constraints;
drop table PATIENT cascade constraints;
drop table PHARMACIEN cascade constraints;
drop table MEDICAMENT cascade constraints;
drop table PRINCEPS cascade constraints;
drop table PHARMACIE cascade constraints;
drop table VILLE cascade constraints;
drop table PRESCRIPTION cascade constraints;
drop table LOT cascade constraints;
drop table COMPAGNIE cascade constraints;
drop table PRIX cascade constraints;

create table PERSONNE
(numSS Varchar2(30),
nomP Varchar2(30),
prenomP Varchar2(30),
CP int,
dDNP date,
constraint KPersonne primary key (numSS));

create table MEDECIN
(numMed Varchar2(30),
specialiste Varchar2(30),
nbAnnee int,
numTelMed int,
constraint kMedecin primary key (numMed));

create table PATIENT
(numPat Varchar2(30),
numMed Varchar2(30),
constraint kPatient primary key (numPat),
foreign key (numPat) references PERSONNE(numSS),
foreign key (numMed) references MEDECIN(numMed));

create table PHARMACIEN
(numPha Varchar2(30),
categorie Varchar2(30),
constraint kPharmacien primary key (numPha),
foreign key (numPha) references PERSONNE(numSS)
);

create table MEDICAMENT
(nomMedi Varchar2(30),
nomCompPharma Varchar2(30),
dosage int,
forme Varchar2(30),
nomPrincipeActif Varchar2(30),
constraint kMedicament primary key (nomMedi, nomCompPharma, dosage, forme)
);

create table PRINCEPS
(nomPrinceps Varchar2(30),
nomCompPharma Varchar2(30),
nomGenerique Varchar2(30),
constraint kPRINCEPS primary key (nomPrinceps));
-- foreign key (nomPrinceps) references MEDICAMENT(nomMedi)

create table VILLE
(nomVille Varchar2(30),
departement int,
nbhab int,
nbPharmacie int,
constraint kVILLE primary key (nomVille, departement));

create table PHARMACIE
(numPharmacie Varchar2(30),
numPha Varchar2(30),
nomPharmacie Varchar2(30),
nomVille Varchar2(30),
departement int,
adresse Varchar2(30),
numTel int,
constraint kPHARMACIE primary key (numPharmacie),
foreign key (numPha) references PHARMACIEN(numPha),
foreign key (nomVille,departement) references VILLE(nomVille,departement));


create table LOT
(numLot int,
nomMedi Varchar2(30),
nomCompPharma Varchar2(30),
dosage int,
forme Varchar2(30),
dateValidite date,
constraint KLOT primary key (numLot),
foreign key (nomMedi,dosage,forme, nomCompPharma)references MEDICAMENT(nomMedi,dosage,forme,nomCompPharma));

create table PRESCRIPTION
(datePresc date,
numPat Varchar2(30),
numMed Varchar2(30),
nomPrincActif Varchar2(30),
dosage int,
forme Varchar2(30),
nbPriseParJour int,
dateAchat date,
numPharmacie Varchar2(30),
numLot int,
constraint kPRESCRIPTION primary key (datePresc, numPat, numMed, nomPrincActif, dosage, forme),
foreign key (numPharmacie) references PHARMACIE(numPharmacie),
foreign key (numLot) references LOT(numLot),
foreign key (numPat) references PATIENT(numPat),
foreign key (numMed) references MEDECIN(numMed));



create table COMPAGNIE
(nomCompPharma Varchar2(30),
telCompPharma int,
webComPharma Varchar2(40),
constraint KCOMPAGNIE primary key (nomCompPharma));

create table PRIX
(numPharmacie Varchar2(30),
nomMedi Varchar2(30),
nomCompPharma Varchar2(30),
dosage int,
forme Varchar2(30),
prix float,
constraint KPRIX primary key (numPharmacie, nomMedi, nomCompPharma, dosage, forme),
foreign key (numPharmacie) references PHARMACIE(numPharmacie),
foreign key (nomMedi,dosage,forme, nomCompPharma)references MEDICAMENT(nomMedi,dosage,forme, nomCompPharma));