
insert into PERSONNE values ('1','velluet','jules', 45150, TO_DATE('18/12/3002', 'DD/MM/YYYY'));
insert into PERSONNE values ('2','soares','julien', 45150, TO_DATE('18/12/3002', 'DD/MM/YYYY'));
insert into PERSONNE values ('3','odiot','nicolas', 45150, TO_DATE('18/12/3002', 'DD/MM/YYYY'));

insert into COMPAGNIE values ('sanofi', 0238593187, 'www.bienvenuchezlesbisounours.fr');

insert into MEDECIN values ('1','cardiologue', 5, 0238593187);

insert into VILLE values ('odiotvillage', 45, 2, 1);

insert into PATIENT values ('2', '1');

insert into PHARMACIEN values ('3', '1');

insert into MEDICAMENT values ('doliprane', 'sanofi', 12, 'comprime', 'paracetamol');

insert into PRINCEPS values ('paracetamol', 'delfarme', 'dolidrôle');

insert into LOT values (1, 'doliprane', 'sanofi', 12, 'comprime', '12:12:3030');

insert into PHARMACIE values ('1', '3', 'odiotpharmacie', 'odiotvillage', 45, '14 rue kingOdiot', 0238541256);

insert into PRIX values ('1', 'doliprane', 'sanofi', 12, 'comprime', 15);

insert into PRESCRIPTION values ('10:12:3030', '2', '1', 'paracetamol', 12, 'comprime', 1, '18:12:3030', '1', 1);



