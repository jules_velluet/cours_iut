<?php 
$file_db = new PDO('sqlite:contacts.sqlite3');
// Gerer le niveau des erreurs rapportees
$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
$id = (string) "De Guillemets";
$query = "SELECT * from contacts WHERE nom = :id";
$stmt = $file_db->prepare($query);
$stmt->bindParam(":name", $id);
$stmt->execute();
$pers = $stmt->fetch(PDO::FETCH_ASSOC);
echo "Found: $pers[prenom] $pers[nom]";
?>