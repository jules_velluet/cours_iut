<?php
$questions=[
    array(
        "name" => "ultime",
        "type" => "text",
        "text" => "Quelle est la réponse ultime",
        "answer" => "42",
        "score" => 2
    ),
    array(
        "name" => "cheval",
        "type" => "radio",
        "text" => "Quelle est la couleur du cheval blanc d'henri IV ?",
        "choices" => [
            array(
                "text" => "Bleu",
                "value" => "bleu"),
            array(
                "text" => "Vert",
                "value" => "vert"),
            array(
                "text" => "Blanc",
                "value" => "blanc")
            ],
        "answer" => "blanc",
        "score" => 2
    ),
    array(
      "name" => "capital",
      "type" => "text",
      "text" => "Quelle est la capital de la France? (mettre une majuscule)",
      "answer" => "Paris",
      "score" => 1
    ),
    array(
      "name" => "annee",
      "type" => "radio",
      "text" => "En quelle année l'iut à vue le jour?",
      "choices" => [
        array(
          "text"  => "1950",
          "value" => "1950"
        ),
        array(
          "text" => "1960",
          "value" => "1960",
        ),
        array(
          "text" => "1966",
          "value" => "1966"
        ),
        array(
          "text" => "2050",
          "value" => "2050"
        )
        ],
      "answer" => "1966",
      "score" => 2
    ),
    array(
      "name" => "php",
      "type" => "radio",
      "text" => "Quelle technologie a t-on utilisé pour faire ce site?",
      "answer" => "PHP",
      "score" => 4,
      "choices" => [
        array(
          "text" => "python",
          "value" => "python"
        ),
        array(
          "text" => "une technologie extraterrestre",
          "value" => "une technologie extraterrestre"
        ),
        array(
          "text" => "PHP",
          "value" => "PHP"
        ),
        array(
          "text" => "la telepathie",
          "value" => "la telepathie"
        ),
        ]
    ),
    array(
      "name" => "velo",
      "type" => "text",
      "text" => "Qui est l'actuelle champion du monde de cyclisme? (mettre une majuscule)",
      "answer" => "Alaphilippe",
      "score" => 20,
    ),
    array(
      "name" => "histoire",
      "type" => "radio",
      "text" => "En quelle année Hitler a été elu chancelier?",
      "choices" => [
        array(
          "text"  => "2021",
          "value" => "2021"
        ),
        array(
          "text" => "1920",
          "value" => "1920",
        ),
        array(
          "text" => "1933",
          "value" => "1933"
        ),
        array(
          "text" => "1936",
          "value" => "1936"
        )
        ],
      "answer" => "1933",
      "score" => 2
    ),
        ];

$questions_total = 0;
$question_correct = 0;
$score_max = 0;
$score = 0;

// Affichage d'une question de type text
function question_text($q){
    echo $q['text'] ."<br/><input type='text' name='$q[name]'><br/>";
}

// traitement de la réponse à une question de type text
function answer_text($q,$v){
    global $question_correct,$score_max, $score;
    $score_max += $q['score'];
    if (is_null($v)) return;
    if ($q['answer'] == $v){
        $question_correct += 1;
        $score += $q['score'];
    }
}

function question_radio($q){
    $html = $q['text'] . "<br/>";
    $i = 0;
    foreach($q['choices'] as $c){
        $i += 1;
        $html .= "<input type='radio' name='$q[name]' value='$c[value]' id='$q[name]-$i'>";
        $html .= "<label for='$q[name]-$i'>$c[text]</label>";
    }
    echo $html;
}

$question_handlers = array(
    "text" => "question_text",
    "radio" => "question_radio",
    "checkbox" => "question_checkbox"
);
$answer_handlers = array(
    "text" => "answer_text",
    "radio" => "answer_text",
    "checkbox" => "answer_checkbox"
);

if ($_SERVER['REQUEST_METHOD'] == 'GET'){
    // On présente les questions
    echo "<fieldset>
    <legend>Quiz version professeur</legend><br/><br/>";
    echo "<form method='POST' action='quiz_julesvelluet.php'><ol>";
    foreach ($questions as $q){
        echo "<li>";
        $question_handlers[$q['type']]($q);
    }
    echo "</ol><input type='submit' value='Repondre'></form><fieldset>";
}
else{
    // On répond au client et on calcule son score
    $questions_total = 0;
    $questions_total=0;
    $question_correct=0;
    $score_max=0;
    $score=0;
    foreach ($questions as $q){
        $questions_total += 1;
        $answer_handlers[$q['type']]($q, $_POST[$q['name']] ?? NULL);
    }
    echo("Bonjour il est temps de voir votre score <br/>");
    echo "Réponses correctes:" . $question_correct . "/" . $questions_total ."<br/>";
    echo "Votre score: " . $score . "/" . $score_max ."<br/>";

    $AGENT=$_SERVER['HTTP_USER_AGENT'];
    echo $AGENT;
    echo("\n<P>");
    if (stristr($AGENT,"MSIE")) {
 	    echo("Changer de Navigateur !");
     }
    elseif (preg_match("/Firefox/i",$AGENT))
    {
	     echo("Vous semblez utiliser Firefox !");
      }
 		elseif (preg_match("/chrome/i",$AGENT))
      {
	       echo("Vous semblez utiliser Chrome !");
       }
      elseif (preg_match("/Safari/",$AGENT))
 	    {
	       echo("Vous semblez utiliser Safari !");
       }
       else echo "Navigateur Inconnu !";
}
