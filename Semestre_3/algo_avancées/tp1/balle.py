#! /usr/bin/env python3

import pygame

class Balle:
    def __init__(self, position_x, position_y, vitesse_x, vitesse_y, couleur, taille):
        self.position_x = position_x
        self.position_y = position_y
        self.vitesse_x = vitesse_x
        self.vitesse_y = vitesse_y
        self.couleur = couleur
        self.taille = taille

    def avance(self, t):
        self.position_x = int(self.vitesse_x*t) % 800
        self.position_y = int(self.vitesse_y*t) % 600

    def dessine(self, s):
        pygame.draw.circle(s, self.couleur, (self.position_x,self.position_y), self.taille)

    def contient(self, position):
        if (position[0] > self.position_x-self.taille and position[0] < self.position_x + self.taille) and (position[1]>self.position_y-self.taille and position[1] < self.position_y + self.taille):
            return True
        else:
            return False
