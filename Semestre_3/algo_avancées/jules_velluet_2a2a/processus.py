#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Processus:
    def __init__(self,pid=1,ppid=0):
        self.pid = pid
        self.ppid = ppid
        self.max_pid_descendance = pid
        self.etat = "p" # p:pause, e:execution
        self.pfils = []

    def fork(self,pid):
        """
        Ajoute un processus fils à la liste pfils
        """
        self.pfils.append(Processus(pid,self.ppid))

    def est_feuille(self):
        return self.pfils == []

    def execution(self):
        self.etat = "e"

    def est_actif(self):
        return self.etat == "e"

    def pause(self):
        self.etat = "p"
