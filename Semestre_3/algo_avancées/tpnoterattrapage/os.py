#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from processus import Processus

"""
Ecrivez une fonction nb_processus_crees(p) qui renvoie le nombre de processus
qui ont été créés à partir de p ou d'un de ces descendants. On comptera p lui-
même dans le résultat.
"""
def nb_processus_crees(p):
    res = 1
    for e in p.pfils:
        res += nb_processus_crees(e)
    return res

"""
Ecrivez une fonction pid_max_descendance(p) qui va mettre à jour l'attribut
max_pid_descendance du processus p. A la fin de la fonction, on renverra la
valeur de cette attribut après mise-à-jour.
"""

def pid_max_descendance(p):
    cpt = 0
    if cpt = 0:
        max = p.pid
    p.max_pid_descendance = max
    for e in p.pfils:
        cpt += 1
        e.pid_max_descendance
    return max

"""
Ecrivez une fonction processus_actif(p) qui renvoie le processus actif de la
descendance de p, si un tel processus existe. Si aucun processus actif n'est
présent, la fonction renverra None.
"""
def processus_actif(p:Processus):
    res = None
    if p.etat = "e":
        return p
    for e in p.pfils:
        res = processus_actif(e)
    return res

"""
Ecrivez une  fonction liste_ancetres(ancetre,pid) qui renvoie la liste des
 processus qui ont été créés afin de pouvoir créer le processus ayant le pid
donné. Par exemple, si un processus p1 est le père d'un processus p2 lui meme
père d'un processus p3 de pid 3, alors l'appel
liste_ancetres(p1,3) doit renvoyer [p1,p2,p3].
"""
def liste_ancetres(ancetre,pid):
    res = []
    for e in p.pfils:
        res.append(e)
        liste_ancetres()
    return res

"""
BONUS: Ecrivez une fonction maj_pid_max_opt(ancetres) qui prend en
paramètre la liste des ancetres menant au processus venant d'être ajouté et qui
est présent à la fin de la liste.
"""

def maj_pid_max_opt(ancetres,nouveau_pid):
    pass



# ajoutez vos tests ici
init = Processus(1,0)
pro1 = Processus(2,1)
pro2 = Processus(3,1)
pro3 = Processus(4,2)
pro1.fork(pro3)
init.fork(pro1)
init.fork(pro2)
