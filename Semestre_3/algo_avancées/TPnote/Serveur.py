#!/usr/bin/env python3

class Serveur:
    def __init__(self, nom, ip, serveur):
        self.nom = nom
        self.ip = ip
        self.serveurs = serveur

    def get_serveurs(self):
        return self.serveurs

    def ajout_sous_domaine(self, serveur):
        """ ajoute le serveur responsable du sous - domaine dans la liste """
        self.serveurs.append(serveur)

    def nb_serveur(self):
        res = 0
        if self.get_serveurs() == []:
            return 1
        for i in self.get_serveurs():
            res += i.nb_serveur()
        return res

    # def question2_2(self):
    #     cpt = len(self.get_serveurs())
    #     for serveur in self.get_serveurs():
    #         h = len(serveur.question2_2())
    #         if len(serveur.get_serveurs()) > cpt:
    #             if h < cpt:
    #                 cpt = h
    #     return cpt


serveur1 = Serveur("1","1",[])
serveur2 = Serveur("2","2",[])
serveur3 = Serveur("3","3",[serveur1, serveur2])
serveurf = Serveur("4","4",[serveur3])
print(serveurf.nb_serveur())
# print(serveurf.question2_2())

#question 3: Il y a 17 serveurs standards.
