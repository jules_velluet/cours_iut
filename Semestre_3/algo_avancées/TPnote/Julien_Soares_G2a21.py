class Serveur:
    def __init__(self ,nom ,ip):
        self.nom = nom
        self.ip = ip
        self.serveurs = []

    def ajout_sous_domaine(self ,serveur ):
        """ ajoute le serveur responsable du sous -domaine dans la liste """
        self.serveurs.append(serveur)

    # Exercice 1
    # Question 1:
    # Si le serveur ne possède pas de sous domaine alors c'est un serveur standard
    # sinon c'est un serveur de nom

    # Question 2:
    # 5

    # Question 3:
    # 17

    # Question 4:
    # Si le serveur racine n'a pas de sous domaine alors la fonction retourne 1 
    # sinon  on fait le total du nombre de serveur standard de chaque sous domaine 

def nb_serveur(S):
    total = len(S.serveurs)
    for serveur in S.serveurs:
        total += nb_serveur(serveur)
    return total

def plus_petit_chemin(S):
    total = None
    if S.serveurs == []:
        return 1
    for serveur in S.serveurs:
        if total is None or plus_petit_chemin(serveur) < total:
            total = plus_petit_chemin(serveur) + 1
    if total is None:
        return 1
    else:
        return total

def serveur_standard(S):
    res = []
    if S.serveurs == []:
        return [S.nom]
    for serveur in S.serveurs:
        res.extend(serveur_standard(serveur))
    return res

def ip_serveur(S,nom):
    if S.nom == nom:
        return S.ip
    for serveur in S.serveurs:
        if ip_serveur(serveur,nom) != None:
            return ip_serveur(serveur,nom)
    return None


www1 = Serveur("www","1")
dns = Serveur("DNS","16")
univorleans = Serveur("univ-orleans","125.2.365.24")
univorleans.ajout_sous_domaine(dns)
univorleans.ajout_sous_domaine(www1)

fr = Serveur("fr","125.2.365.24")
fr.ajout_sous_domaine(univorleans)

www2 = Serveur("www","2")
qwant = Serveur("qwant","125.65.215.34")
qwant.ajout_sous_domaine(www2)
com = Serveur("www","125.2.365.24")
com.ajout_sous_domaine(qwant)

www3 = Serveur("www","6")
iana = Serveur("iana","125.2.365.24")
iana.ajout_sous_domaine(www3)
framasoft = Serveur("framasoft","125.2.365.24")
org = Serveur("org","125.2.365.24")
org.ajout_sous_domaine(framasoft)
org.ajout_sous_domaine(iana)

point = Serveur("point","125.2.365.24")
point.ajout_sous_domaine(fr)
point.ajout_sous_domaine(com)
point.ajout_sous_domaine(org)