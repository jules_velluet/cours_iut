#!/usr/bin/env python3

class Piece:
    def __init__(self, nom, cout, duree, petit):
        self.__nom = nom
        self.__cout = cout
        self.__duree = duree
        self.__petit_bou_de_chou = petit


    def nom(self):
        return self.__nom

    def cout(self):
        return self.__cout

    def duree(self):
        return self.__duree

    def petit(self):
        return self.__petit_bou_de_chou

    def add(self, petit):
        self.__petit_bou_de_chou.append(petit)

    def __repr__(self):
        repr_enfants = ",".join(("%r" % e) for e in self.enfants())
        return "%r<%s>" % (self.etiquette(), repr_enfants)

    def prix(self):
        res = self.cout()
        for child in self.petit():
            res += child.prix()
        return res

    def assemblage(self):
        res = self.duree()
        for child in self.petit():
            res += child.assemblage()
        return res


manche = Piece("manche", 50, 3, [])
caisse = Piece("caisse", 100, 5, [])
corps = Piece("corps", 50, 1, [caisse,manche])
cordes = Piece("corde", 10, 0, [])
guitare = Piece("guitare", 100, 1, [cordes,corps])
print(guitare.prix())
print(guitare.assemblage())
