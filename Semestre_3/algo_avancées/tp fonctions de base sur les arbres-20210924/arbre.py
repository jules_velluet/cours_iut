#!/usr/bin/env python3

class Arbre:
    def __init__(self, etiquette, enfants):
        self.__etiquette = etiquette
        self.__enfants = enfants

    def etiquette(self):
        return self.__etiquette

    def enfants(self):
        return self.__enfants

    @classmethod
    def Feuille(cls_arbre, etiquette): #méthode statique
        return cls_arbre(etiquette, [])

    def nb_feuille(self):
        res = 0
        if self.enfants() == []:
            return 1
        else:
            for child in self.enfants():
                if child.enfants != []:
                    res = res + child.nb_feuille()
        return res

    def nb_noeud(self):
        res = 1
        for child in self.enfants():
            res += child.nb_noeud()
        return res

    def hauteur(self):
        if self.enfants()== []:
            return 0
        else:
            maxi = 0
        for e in self.enfants():
            h = e.hauteur()
            if h > maxi:
                maxi = h
        return maxi + 1

    def max_enfants(self):
        cpt = len(self.enfants())
        for child in self.enfants():
            h = len(child.max_enfants())
            if len(child.enfants()) > cpt:
                if h > cpt:
                    cpt = h
        return cpt

    def verif(self):
        if self.etiquette() == "*" or self.etiquette() == "+":
            if len(self.enfants()) != 2:
                return False
        else:
            if len(self.enfants()) != 0:
                if int(self.etiquette()) == false:
                    return False

        for child in self.enfants():
            res = child.verif()
            if res == False:
                return "coucou blaireau"
        return True

    def question_9(self):
        res = 0
        if self.etiquette() != "*" and self.etiquette() != "+":
            return self.etiquette()
        for child in self.enfants():
            if self.etiquette() == "+":
                res += int(child.question_9())
            elif self.etiquette() == "*":
                if res == 0:
                    res = int(child.question_9())
                else:
                    res *= int(child.question_9())
        return res

    def add(self, nourisson):
        """
        ajoute un enfant à l'arbre
        """
        self.__enfants.append(nourisson)

    def __repr__(self):
        repr_enfants = ",".join(("%r" % e) for e in self.enfants())
        return "%r<%s>" % (self.etiquette(), repr_enfants)

mon_arbre3 = Arbre("7",[])
mon_arbre4 = Arbre("8",[])
mon_arbre = Arbre("1",[])
mon_arbre2 = Arbre("+",[mon_arbre4,mon_arbre3])
mon_vrai_arbre = Arbre("*",[mon_arbre, mon_arbre2])
arbrison = Arbre("4",[mon_vrai_arbre])
mon_3h = Arbre("5",[arbrison])
print(mon_3h.enfants())
print(mon_3h.nb_feuille())
print(mon_3h.nb_noeud())
print(mon_vrai_arbre.hauteur())
print(mon_arbre2.verif())
print(mon_arbre2.question_9())
