//Exo1, Loi de Bernoulli
p = 0.5;
U = rand();
if (U < p)
  disp("Pile");
else
  disp("Face");
end

// Loi uniforme
K = 10;
ceil(K*rand()), ceil(K*rand()), ceil(K*rand()) // ceil = partie entière


K = 8, F = 1, s = 0.5:K+0.5;
N = 1000, data = ceil(K * rand(1, N));
clf, histplot(s, data), plot2d3(F, ones(F)/K);

// Exo 2
clear;

function loi = loiBinomiale(n, p)
  loi = zeros(1, n+1);
  for n1 = 0:n
    U = rand();
    if (U < p)
      S = S + 1;
    end
  end
endfunction

// nb tirages
K = 1600;

//param loi loiBinomiale
N = 10;
p = 0.25;

// init graphique
clf;
xtitle( "Historigramme: loi binomiale de paramètre " + string(N) + " et " + string(p) + ". Nombre de simulations : " + string(K));

F = 0:N
ln = loiBinomiale(N, p);
plot2d3(F, lb, 5);

//subdivision
s = -0.5:N+0.5;

// K tirages
data = zeros(1,K);
for K = 1:K
  data(K) = simBinomiale(N, p);
end
histplot(s, data);
