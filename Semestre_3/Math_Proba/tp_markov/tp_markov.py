import numpy as np
import matplotlib.pyplot as plt
import random

Q = [0.9,0.1,0.2,0.95]
P = np.array([[0.1,0.9,0,0],[0.9,0,0.1,0],[0,0.8,0,0.2],[0,0,0.05,0.95]])

def P_puissance(P,n):
    matrice = P
    for i in range(n):
        P = P.dot(matrice)
    return P

print(P_puissance(P,50))

def trajectoire3(Q,n):
    X = np.zeros(n+1, int)
    X[0] = 4
    for i in range(1,n+1):
        m = random.uniform(0,1)
        if (m <= Q[X[i-1]-1]):
            if X[i-1] <= 3:
                X[i] = X[i-1]+1
            else:
                X[i] = 4
        else:
            if X[i-1] >= 2:
                X[i] = X[i-1]-1
            else:
                X[i]=1
    return X



def affichage():
    a = trajectoire3(Q,10)
    plt.subplot(2,2,1)
    plt.plot(a)
    plt.title("n = 10")
    a = trajectoire3(Q,50)
    plt.subplot(2,2,2)
    plt.plot(a)
    a = trajectoire3(Q,100)
    plt.subplot(2,2,3)
    plt.plot(a)
    plt.show()
    return "Done"

print(trajectoire3(Q,4))
print(affichage())

def temps(n):
    X = trajectoire3(Q,n)
    res = [0,0,0,0]
    for etat in X:
        if etat == 1:
            res[0]+=1
        if etat == 2:
            res[1]+=1
        if etat == 3:
            res[2]+=1
        if etat == 4:
            res[3]+=1
    return res

def trajectoire2(Q,n):
    X = np.zeros(n+1, int)
    X[0] = 5
    for i in range(1,n+1):
        m = random.uniform(0,1)
        if (m <= Q[X[i-1]-1]):
            if X[i-1] <= 3 or X[i-1] == 5:
                X[i] = X[i-1]+1
            else:
                X[i] = 4
        else:
            if X[i-1] >= 2:
                X[i] = X[i-1]-1
            else:
                X[i]=1
    return X

def affichage2(Q):
    a = trajectoire2(Q,10)
    plt.subplot(2,2,1)
    plt.plot(a)
    plt.title("n = 10")
    a = trajectoire2(Q,50)
    plt.subplot(2,2,2)
    plt.plot(a)
    a = trajectoire2(Q,100)
    plt.subplot(2,2,3)
    plt.plot(a)
    plt.show()
    return "Done"

def temps_moyen(Q):
    res = []
    for i in range(200):
        a = trajectoire2(Q,50)
        for j in range(len(a)):
            if a[j] == 4:
                res.append(i)
            break
    print(len(res))
    return res

def moyenne(Q):
    b = temps_moyen(Q)
    print(temps_moyen)
    a = 0
    for i in b:
        a += i
    return a/200
Q2= [0.9,0.1,0.2,0.95,0.25,0.9]
P2= np.array([[0.1,0.9,0,0,0,0],[0.9,0,0.1,0,0,0],[0,0.8,0,0.2,0,0],[0,0,0.05,0.95,0,0],[0,0,0,0.75,0,0.25],[0,0,0,0,0.1,0.9]])

print(trajectoire2(Q2,10))
print(moyenne(Q2))
#print(affichage2(Q2))

print(temps(1000))
