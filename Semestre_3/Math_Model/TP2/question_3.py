import math
import numpy as np
import time
import matplotlib.pyplot as plt

a = 2
b = 0.001
p = np.arange(0,5,0.01)

def f2(x):
    return a*x-b*x**2

print(f2(500))
def euler(f ,y0 ,t):
    y= [y0]
    for i in range(len(t)-1):
        y.append(round(y[-1] + (t[i+1]-t[i]) * f(y[-1])))
        #print(round(y[-1] + (t[i+1]-t[i]) * f(y[-1])))
        time.sleep(0.01)
    return y

y1 = euler(f2,500,p)
plt.plot(p,y1)
y2 = euler(f2,1000,p)
y3 = euler(f2,1000,p)
plt.plot(p,y2)
plt.plot(p,y3)
plt.xlabel('temps')
plt.ylabel('population')
plt.show()
print(f2(1))


# print(euler(f2,500,p))
# print(euler(f2,1000,p))
# print(euler(f2,2000,p))
# print(euler(f2,3000,p))
# print(euler(f2,5000,p))
