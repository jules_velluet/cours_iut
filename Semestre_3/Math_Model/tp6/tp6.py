import numpy as np
import random
import scipy.stats as e
import matplotlib.pyplot as plt
import math

a="a"

while a!="q":
    a= input()
    if a == "exo1":

        p = 2/3
        n = 50
        a = 1/100
        s = 1-(a/2)

        def bernoulli(n,p):
            return e.bernoulli.rvs(p, size = n)

        print(bernoulli(n,p))

        def moyenne_empirique(n,p):
            return  np.mean(bernoulli(n,p))

        print(moyenne_empirique(n,p))


        def intervalle(a,p,n,s):
            return [moyenne_empirique(n,p)-s*math.sqrt(p*(1-2/3)/50),moyenne_empirique(n,p)+s*math.sqrt(p*(1-2/3)/50)]



        print(intervalle(a,p,n,s))

        liste = []

        # #question 7
        for i in range(1,501):
            liste.append(bernoulli(i,p))

        moyenneEmpirique = []
        for l in liste:
            moyenneEmpirique.append(np.mean(l))

        #print(moyenneEmpirique)

        supp = []
        for i in range(1,501):
            supp.append(intervalle(a,p,i,s)[0])

        inf = []
        for i in range(1,501):
            inf.append(intervalle(a,p,i,s)[1])

        plt.plot(range(1,501),moyenneEmpirique)
        plt.plot(range(1,501),supp)
        plt.plot(range(1,501),inf)
        plt.show()
        # n=500

        # a=[]
        # moy=moyenne_empirique(n,p)
        # for i in range(0,n-1):
        #     a.append(i)
        # print(a,moy)
        # plt.plot(2,2)
        # plt.show()



    if a == "exo2":
        bd1 = [77.26993, 77.22386, 73.25844, 75.42146, 75.13879, 71.67470,76.62168, 71.17531, 72.50649, 76.99631,
     73.91825, 74.56725,71.75613, 72.09807, 75.70182, 74.65091, 73.81714, 72.33195, 72.80540, 79.07221,
     74.34702, 76.54801, 76.57001, 76.52649, 75.58962, 72.49529, 72.98099, 76.50278, 72.38329, 76.05508,
     73.93292, 74.20325, 73.42086, 74.53972, 76.75437, 75.90747,  74.53507, 76.74001, 78.31201, 74.98726,
     75.94098, 75.55644,  73.04419, 73.14683, 78.83954, 76.76256, 76.48416, 75.29515,  75.97078, 75.30371,
     75.08400, 75.44684, 72.97907, 79.80244,  76.60392, 74.49758, 77.42578, 73.74548, 78.42232, 74.21125,
     70.35702, 77.72824, 77.26446, 73.45137, 72.17925, 71.33094,  74.46197, 71.33214, 73.37106, 75.32714,
     76.71104, 73.36007,  74.75279, 75.50990, 78.43785, 73.08291, 71.79138, 71.30878,  76.11147, 74.87976,
     76.54417, 74.71832, 75.78619, 75.44844,  75.04708, 73.75407, 77.52402, 74.18845, 76.33353, 75.32928,
     78.56305, 76.42243, 74.32462, 74.98170, 74.74938, 70.81831,  78.39479, 77.12776, 73.46677, 75.76402]

        bd2 = [69.48379, 66.73448, 71.97981, 68.50351, 69.36717, 69.80974, 67.01175, 66.82914, 68.90291,
 	70.15217, 69.14766, 70.41189, 69.66996, 70.09078, 66.19419, 70.35411, 67.42040, 68.06854,
 	68.79030, 65.70430, 68.80093, 68.12028, 67.56298, 67.89080, 71.49098, 66.48216, 68.56923,
 	64.05608, 67.65166, 67.99741, 72.08465, 67.07596, 67.25564, 66.20474, 69.35961, 71.30818,
 	66.60293, 68.14855, 71.73262, 67.63141, 70.37102, 69.77901, 66.38921, 71.43378, 70.59035,
 	68.02359, 67.19201, 68.21916, 70.62813, 67.87150, 65.25159, 68.71419, 70.54381, 66.68176,
 	68.52417, 66.55561, 69.23361, 68.73500, 68.93263, 67.75535, 67.58127, 70.74289, 69.21028,
 	68.62613, 62.57362, 66.44876, 70.52581, 68.18637, 66.58336, 68.12135, 68.24884, 67.99712,
 	69.99323, 72.04176, 70.97618, 71.49225, 68.34027, 70.68869, 67.03785, 68.72156, 73.37088,
 	68.97434, 68.38938, 67.83157, 70.54254, 73.21238, 69.82431, 68.47747, 73.14757, 67.44234,
 	71.26307, 68.15731, 66.95651, 71.43661, 65.40048, 68.38350, 69.03103, 68.11536, 65.72398,
 	67.71720]

        def moyenne_empirique_exo2(bd):
            return np.mean(bd)

        print(moyenne_empirique_exo2(bd1))

        def intervalle_confiance(alpha,bd):
            z = e.norm.ppf(1-alpha/2, loc=0, scale = 1)
            return [moyenne_empirique_exo2(bd) - z*math.sqrt(4/len(bd)), moyenne_empirique_exo2(bd) + z*math.sqrt(4/len(bd))]

        print(intervalle_confiance(0.02,bd1))

        def mediane(bd):
            return np.median(bd)

        def quartil1(bd):
            bd = sorted(bd)
            bd = bd[:round((len(bd)/2))]
            return mediane(bd)

        def quartil2(bd):
            bd = sorted(bd)
            bd = bd[(round(len(bd)/2)):]
            return mediane(bd)

        print("medianne = "+str(mediane(bd1))+"\npremier quartil = "+str(quartil1(bd1))+"\ndeuxiéme quartil = "+str(quartil2(bd1)))

        def affichage_moustache(bd1,bd2):
            plt.boxplot([bd1,bd2])
            plt.show()
            return "trop cool"

        print(affichage_moustache(bd1,bd2))

        print(np.mean(bd2))
        print(intervalle_confiance(0.01,bd2))

        def TCL(m,n):
            esperance = 1
            variance = 4
            Z = list()
            for i in range(m):
                X = e.norm.rvs(loc=esperance,scale=variance,size=n)
                Xbar = np.mean(X)
                z = (Xbar-esperance)/(math.sqrt(variance/n))
                Z.append(z)
            return Z

        def affichage_TCL(m,n):
            plt.hist(TCL(m,n))
            plt.show()

        print(affichage_TCL(100,100))

        def affichage_mult(m):
            plt.subplot(2,3,1)
            plt.hist(TCL(m,10))
            plt.title("n=10")
            plt.subplot(2,3,2)
            plt.hist(TCL(m,20))
            plt.title("n=20")
            plt.subplot(2,3,3)
            plt.hist(TCL(m,30))
            plt.title("n=30")
            plt.subplot(2,3,4)
            plt.hist(TCL(m,500))
            plt.title("n=500")
            plt.subplot(2,3,5)
            plt.hist(TCL(m,1000))
            plt.title("n=1000")
            plt.subplot(2,3,6)
            plt.hist(TCL(m,9999999))
            plt.title("n=9999999")
            plt.show()

        print(affichage_mult(100))

        def echantillonZE(M,n):
            esp = 1/0.5
            variance = (esp)**2
            Z = list()
            for i in range(M):
                X = e.expon.rvs(scale=esp,size=n)
                Xbar = np.mean(X)
                elem = (Xbar-esp)/(math.sqrt(variance/n))
                Z.append(elem)
            return Z

        print(echantillonZE(100,10))

        plt.hist(echantillonZE(100,100))
        plt.show()

        plt.subplot(2,3,1)
        plt.hist(echantillonZE(100,10))
        plt.title("n=10")
        plt.subplot(2,3,2)
        plt.hist(echantillonZE(100,20))
        plt.title("n=20")
        plt.subplot(2,3,3)
        plt.hist(echantillonZE(100,30))
        plt.title("n=30")
        plt.subplot(2,3,4)
        plt.hist(echantillonZE(100,100))
        plt.title("n=100")
        plt.subplot(2,3,5)
        plt.hist(echantillonZE(100,500))
        plt.title("n=500")
        plt.subplot(2,3,6)
        plt.hist(echantillonZE(100,1000))
        plt.title("n=1000")
        plt.show()
