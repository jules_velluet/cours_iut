import matplotlib.pyplot as plt;
import numpy as np;

k=10
m=1

def g(x,v):
    return -(np.sqrt(k/m))**2*x

def Masse_Ressort_expl(g, x0, v0, t):
    X = np.zeros(len(t),float)
    V = np.zeros(len(t),float)
    X[0] = x0
    V[0] = v0
    N = len(t)
    for i in range(N-1):
        X[i+1] = X[i]+(t[i+1]-t[i])*V[i]
        V[i+1] = V[i]+(t[i+1]-t[i])*g(X[i],V[i])
    return X,V

t = np.arange(0,20,0.001)
x,v=Masse_Ressort_expl(g,1,0,t)
plt.plot(v,x)
# plt.plot(t,v)
plt.show()

def Masse_Ressort_simpl(g, x0, v0, t):
    V = np.zeros(len(t),float)
    X = np.zeros(len(t),float)
    X[0] = x0
    V[0] = v0
    N = len(t)
    for i in range(N-1):
        V[i+1] = V[i]+(t[i+1]-t[i])*g(X[i],V[i])
        X[i+1] = X[i]+(t[i+1]-t[i])*V[i+1]
    return X,V

t = np.arange(0,20,0.001)
x,v=Masse_Ressort_simpl(g,1,0,t)
plt.plot(v,x)
# plt.plot(t,v)
plt.show()

def Masse_Ressort_impl(w, x0, v0, t):
    V = np.zeros(len(t),float)
    X = np.zeros(len(t),float)
    X[0] = x0
    V[0] = v0
    N = len(t)
    for i in range(N-1):
        V[i+1]=(V[i]-(t[i+1]-t[i])*w*X[i])/((t[i+1]-t[i])**2*w+1)
        X[i+1]=X[i]+(t[i+1]-t[i])*V[i+1]
    return X,V

t = np.arange(0,20,0.001)
x,v=Masse_Ressort_impl(10,1,0,t)
# plt.plot(t,x)
# plt.plot(t,v)
plt.plot(x,v)
plt.show()
