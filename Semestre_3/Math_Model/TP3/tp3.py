import matplotlib.pyplot as plt;
import numpy as np;

w = 10/1


#question 3:
def g(x):
    w0= w
    return -w0*x

#question 4:
def Masse_Ressort_expl(g,v0,x0,t):
    v = [v0]
    x = [x0]
    for i in range(len(t)-1):
        v.append(v[-1]+(t[i+1]-t[i])*g(x[-1]))
        x.append(x[-1]+(t[i+1]-t[i])*v[-2])
    return v, x

#question 5: affichage
t = np.arange(0,20,0.001)
x,v=Masse_Ressort_expl(g,0,1,t)
plt.plot(t,x)
plt.plot(t,v)


#question 6:
def Masse_Ressort_simpl(g,x0,v0,t):
    v = [v0]
    x = [x0]
    for i in range(len(t)-1):
        v.append(v[-1]+(t[i+1]-t[i])*g(x[-1]))
        x.append(x[-1]+(t[i+1]-t[i])*v[-2])
    return x,v

t = np.arange(0,20,0.001)
x,v=Masse_Ressort_simpl(g,1,0,t)
plt.plot(t,x)
plt.plot(t,v)
plt.show()