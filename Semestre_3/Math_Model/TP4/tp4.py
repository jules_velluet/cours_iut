import numpy as np
import random
import scipy.stats as e
import matplotlib.pyplot as plt
import random

a="a"

while a!="q":
    a = input()
    if a=="exo1":
        p = 0.1
        N = 1000

        def Bernoulli(p,N):
            return e.bernoulli.rvs(p, size = N)

        print(Bernoulli(p,N))

        def somme(nbval,p,N):
            res = 0
            liste = Bernoulli(p,N)
            for i in range(nbval-1):
                res += liste[i]
            return res

        print(somme(22,p,N))

        def somme2(nbval,p,N):
            res = 0
            liste = Bernoulli(p,N)
            for i in range(nbval-1,nbval+21-1):
                res += liste[i]
            return res

        def max_somme(p,N):
            max = None
            for i in range(1,365-22):
                sum = somme2(i,p,N)
                if max == None or sum>max:
                    max = sum
            return max

        for i in range(10):
            print(max_somme(p,N))

        def moyenne_empirique(p,N):
            res = []
            for i in range(0,N-1):
                bernoulli = Bernoulli(p,i)
                res.append(np.mean(bernoulli))
            return res

        def affiche_graph(p,N):
            liste = moyenne_empirique(p,N)
            a = []
            for i in range (0,N-1):
                a.append(i)
            plt.plot(a,liste)
            plt.show()
            return None

        print(affiche_graph(p,N))


    elif a=="exo2":
        N = 20

        def chocapic(N):
            valentin = set()
            cpt_valou = 0
            while len(valentin) != N:
                valentin.add(random.randint(1,20))
                cpt_valou = cpt_valou + 1
            return cpt_valou

        print(chocapic(N))
        c = 0
        res = 0
        for i in range(1000):
            res = res + chocapic(N)
            c+= 1
        print(res/c)
        def nb_vignette(N):
            pass



        def zacharie(N):
           set_jean = set()
           set_zacharie = set()
           cpt = 0
           while (len(set_jean) != N and len(set_zacharie) != N):
               carte = random.randint(1,N)
               if (carte not in set_jean):
                   set_jean.add(carte)
               else:
                    set_zacharie.add(carte)
               cpt += 1
           return cpt

        print(zacharie(N))

    elif a =="exo3":
        mu = 1
        nu = 1
        n = 100
        t = 50

        def simul_tab(mu,n):
            t0 = e.expon.rvs(scale=1/mu, size=n)
            t = [t0[0]]
            for i in range(n-1):
                t0 = t0 + e.expon.rvs(mu)
                t.append(t[i-1]+t[0])
            return t

        print("-------question1----------------\n")
        print(simul_tab(mu,n))

        def N(t,mu,n):
            tab = simul_tab(mu,n)
            res = 0
            for elem in tab:
                if elem < t:
                    res += 1
            return res

        print("-------question2-----------\n")
        print(N(t,mu,n))

        def duree_passage(nu,n):
            return e.expon.rvs(scale=1/nu, size=n)

        print("------------------question3-------------------\n")
        print(duree_passage(nu,n))

        def recurrence(mu,nu,n):
            d = duree_passage(nu,n)
            instant = simul_tab(mu,n)
            tab = []
            tab.append(instant[0])
            for i in range(1,n):
                tab.append(max(instant[i], d[i-1] + tab[i-1]))
            return tab

        print("------------------question4-------------------\n")
        print(recurrence(mu,nu,n))

        def client_passer_guichet(mu,nu,n,t):
            tab = recurrence(mu,nu,n)
            cpt = 0
            for elem in tab:
                if elem < t:
                    cpt +=1
            return cpt

        print("------------------question5-------------------\n")
        print(client_passer_guichet(mu,nu,n,t))

        def nbAttenteFile(mu,nu,t,n):
            N = N(t,mu,n)
            S = client_passer_guichet(mu,nu,n,t)
            res = N-S
            if res < 0:
                return 0
            return res

        print(nbAttenteFile(mu,nu,t,n))

        def  nb_attente(mu,nu,n):
            t = simul_tab(mu,n)
            x = np.zeros(n, float)
            for i in range(n):
                x[i] = client_passer_guichet(mu,nu,n,t[i])
            return t, x

        t, x = nb_attente(mu, nu, n)
        print(x)
        plt.plot(t, x)
        plt.show()

        def poisson(n,a,b,l):
            res = e.poisson.rvs(l, size=n)
            res = res * a + b
            return res

        print(poisson(100,1,15,10))
