#include <stdio.h>

const int ci1 = 5;

const char* cc1 = "Affichage1\n";
const char* cc2 = "Affichage1\n";

int gi1 = 3;
int gi2;

static int sgi1;

int fct1() {
  static int si1 = 5;
  int i, j;

  i = si1 * 2;
  j = si1 - 1;

  si1 += 1;

  return i+j;
}

int fct2(int arg) {
  int i = 5;
  int j = i + fct1();

  return i+j; //+ ci1;
}


int main() {
  int a, b;

  printf("%s\n", cc1);

  a = fct1();
  b = fct2(a);

  printf("a=%d\tb=%d\n", a, b);
  printf("%s\n", cc2);

  return 0;
}
