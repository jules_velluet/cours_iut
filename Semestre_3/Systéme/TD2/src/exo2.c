#include <stdio.h>

int* fct(int a) {
  static int i = 11;

  i += a;

  return &i;
}

int main(){

  int* result;
  int value = 5;

  result = fct(value);
  result = fct(value);

  printf("result=%i\n", *result);

  return 0;
}
