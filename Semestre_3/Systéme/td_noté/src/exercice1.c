#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>


typedef struct{
  unsigned int id;
  int* tab1;
  int* tab2;
  size_t size;
  int res;
}produit_scalaire;

void * calcul(void* args){
  //args vers la structurre
  produit_scalaire * a = (produit_scalaire*) args;
  int resultat = 0;
  for (size_t i = a->id * (a->size)/2 ; i < (a->id+1)*(a->size)/2; ++i){
    resultat += a->tab1[i]*a->tab2[i];
  }
  a -> res = resultat;
  return NULL;
}


int main(){
  srand(time(NULL)); // pour avoir des random différents a chaque exécution
  size_t const size = 1000;

  //creation des tableaux
  int * tab1 = (int*)malloc(size * sizeof(int));
  int * tab2 = (int*)malloc(size * sizeof(int));

  //remplissage des tableaux
  for (size_t i = 0; i<size; ++i){
    tab1[i] = rand()%20-10;
    tab2[i] = rand()%20-10;
  }

  // creation des structurres
  produit_scalaire ps[2];
  pthread_t thread[2];
  for (size_t i = 0; i < 2; ++i){
    ps[i].id = i;
    ps[i].tab1 = tab1;
    ps[i].tab2 = tab2;
    ps[i].size = size;

    pthread_create(&thread[i],NULL, calcul,(void *)&ps[i]);
  }
  //on join les thread pour éviter que le programme se termine avant
  for (size_t i = 0; i <2; ++i){
    pthread_join(thread[i], NULL);
    printf("sum[%zu]= %i\n",i, ps[i].res);
  }
  printf("total=%i\n",ps[0].res + ps[1].res);
  return 0;
}
