#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>


typedef struct{
  unsigned int id;
  int* tab1;
  int* tab2;
  size_t size;
}struct1;


void * inverse(void* args){
  struct1 * a = (struct1*) args;
  for (size_t i = a->id * (a->size)/2 ; i < (a->id+1)*(a->size)/2; ++i){
    a->tab2[i] = a->tab1[a->size-i-1];
  }
  return NULL;
}

int main(){
  srand(time(NULL)); // pour avoir des random différents a chaque exécution
  size_t const size = 1000;

  //creation des tableaux
  int * tab1 = (int*)malloc(size * sizeof(int));
  int * tab2 = (int*)malloc(size * sizeof(int));

  //remplissage des tableaux
  for (size_t i = 0; i<size; ++i){
    tab1[i] = i;
  }

  // creation des structurres
  struct1 ps[2];
  pthread_t thread[2];
  for (size_t i = 0; i < 2; ++i){
    ps[i].id = i;
    ps[i].tab1 = tab1;
    ps[i].tab2 = tab2;
    ps[i].size = size;

    pthread_create(&thread[i],NULL, inverse,(void *)&ps[i]);
  }
  //on join les thread pour éviter que le programme se termine avant
  for (size_t i = 0; i <2; ++i){
    pthread_join(thread[i], NULL);
  }
  printf("le tableau initial\n");
  for (size_t i = 0; i<size; ++i){
    printf("%i,",tab1[i]);
  }
  printf("tableau inversé\n");
  for (size_t i = 0; i<size; ++i){
    printf("%i,",tab2[i]);
  }
  return 0;
}
