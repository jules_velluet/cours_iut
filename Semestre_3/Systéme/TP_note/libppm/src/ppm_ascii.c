#include <ppm_ascii.h>
#include <stdio.h>


ppm_t ppm_read_ascii( char const * filename )
{
  ppm_t tmp;
  return tmp;
}


void ppm_write_ascii( ppm_t const * pa, char const * filename )
{
  FILE * file = fopen(filename,"w+");
  if(file){
    fprintf(file, "%s\n", "P3");
    fprintf(file,"%i ",pa->h);
    fprintf(file,"%i\n",pa->w);
    for( size_t i = 0 ; i < pa->w * pa->h*3; i+=3 ){
      fprintf(file, "%i ", pa->data[i]);
      fprintf(file, "%i ", pa->data[i+1]);
      fprintf(file, "%i \n", pa->data[i+2]);
    }
    fclose( file );
  } else {
    puts( "ascii" );
  }

}


void ppm_display_ascii( ppm_t const * pa )
{

}
