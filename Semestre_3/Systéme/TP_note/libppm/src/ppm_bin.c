#include <ppm_bin.h>
#include <stdio.h>
#include <stdlib.h> 

ppm_t ppm_read_bin( char const * filename )
{
  FILE * file = fopen(filename,"r");
  if(file){
    ppm_t tmp;
    char magic[ 3 ] = "  \0";
    fscanf( file, "%2s", magic );
    fscanf(file,"%i", &tmp.h);
    fscanf(file,"%i", &tmp.w);
    fscanf( file, "%i", &tmp.max );

    unsigned char *pixels = (unsigned char*)malloc(tmp.w*tmp.h*3*sizeof(unsigned char));
    fread(magic,1,1,file); //read newline character before the binary section
    fread(pixels,1,tmp.w*tmp.h*3,file);
    tmp.data = pixels;
    fclose( file );
    return tmp;
  }else{
    perror( "error" );
  }
  
}


void ppm_write_bin( ppm_t const * pb, char const * filename )
{

}


void ppm_display_bin( ppm_t const * pb )
{

}
