#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void * fct()
{
  puts("Hello thread");
}

void * coucou(void * args){
    //sleep(1);
    unsigned int id = *((unsigned int *)args);
    printf("coucou %u\n", id);
}


int main()
{
  unsigned int ids[10000];
  pthread_t thread[10000];
  for (unsigned int i=0; i<10000; i++){
    ids[i] = i;
    pthread_create( &thread[i], NULL, coucou, (void *)&(ids[i]));
  }
  //sleep(3);
  puts("Hello main");
  for (unsigned int i=0; i<10000; i++){
    pthread_join(thread[i], NULL);
  }
  return 0;
}
