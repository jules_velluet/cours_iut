#include <stdio.h>
#include <stdlib.h>

int lire(){
  int a = 0;
  FILE * file = fopen("data.txt","r");
  if (file){
    int n = fscanf(file, "%i",&a);
    if (n>0){
      printf("a = %i\n", a);
    }
    fclose(file);
  }
  else {
    perror("error");
  }

  return 0;
}

int ecrire(){
  int a = 1234567;
  FILE * file2 = fopen("data2.txt","w");
  if (file2){
    int n = fprintf(file2, "%i", a);
    if (n>0){
      puts("c'est bien écrit bg");
    }
    fclose(file2);
  }
  else {
    perror("error");
  }

  return 0;
}

int tableau(){
  size_t size = 0;
  int * tab = NULL;
  FILE * file3 = fopen("data.txt","r");
  if (file3){

    int n = fscanf(file3, "%zu", &size);
    printf("%zu\n", size);
    if (n>0){
      tab = (int *)malloc(size*sizeof(int));
      for (size_t i = 0; i< size; ++i){
        fscanf(file3, "%i", &tab[i]);
        printf("%i\n", tab[i]);
      }
    }
    fclose(file3);
  }
  else {
    perror("error");
  }
  free(tab);
  return 0;
}

int ecriretab(){
  size_t size = 100;
  int * tab = (int *)malloc(size*sizeof(int));
  for (size_t i = 0; i<size; ++i){
    tab[i] = rand()%100;
  }
  FILE * file3 = fopen("tab.txt","w+");
  if (file3){
    fprintf(file3, "%zu", size);
    for (size_t i = 0; i < size; ++i){
      fprintf(file3, " %i", tab[i]);
    }
    fclose(file3);
  }
  else {
    perror("error");
  }
  free(tab);
  return 0;
}

int main(){
  int *tab = (int*)malloc(100*sizeof(int));
  lire();
  ecrire();
  tableau();
  ecriretab();
  return 0;
}
