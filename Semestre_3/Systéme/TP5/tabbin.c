#include <stdio.h>
#include <stdlib.h>

int main(){
  size_t size = 100;
  int * tab = (int*)malloc(size*sizeof(int));
  for (size_t i=0; i<size;++i){
    tab[i] = rand()%100;
  }
  FILE * file = fopen("tab.bin", "w+");
  if (file){
    fwrite(&size, sizeof(size_t),1,file);
    fwrite(tab,sizeof(int), size, file);
    fclose(file);
  }
}
