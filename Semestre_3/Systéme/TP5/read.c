#include <stdio.h>
#include <stdlib.h>

int main(){
  FILE * file = fopen("test.bin", "r");
  if (file){
    int a = 0;
    fread(&a, sizeof(int),1,file);
    printf("%i\n", a);
    fclose(file);
  }
  return 0;
}
