#include <stdio.h>
#include <stdlib.h>

typedef struct{
  int x,y;
}point2d_t;

int main() {
  printf("Taille de votre tableau pas trop gros svp\n");
  int a;
  scanf("%d", &a);
  point2d_t tab[a];
  for (int i=0; i<a; i++){
    int b;
    int c;
    printf("quelle valeur pour x en %i\n", i);
    scanf("%i", &b);
    printf("quelle valeur pour y en %i\n", i);
    scanf("%i", &c);
    point2d_t w = {b,c};
    tab[i] = w;
  }

  return 0;
}
