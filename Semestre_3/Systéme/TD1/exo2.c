#include <stdio.h>

int main() {
  char c0 = 'a';
  short s0 = -5;
  int i0 = -213;
  long long l0 = 234;

  printf("%c\n", c0);
  printf("%hhi\n", c0);

  printf("%hi %i %lli\n", s0, i0, l0);

  unsigned char c1 = 65;
  unsigned short s1 = 65000;
  unsigned int i1 = 2345;
  unsigned long long l1 = 354323;

  printf("%c\n", c1);
  printf("%hhu\n", c1);

  printf("%hu %u %llu\n", s1, i1, l1);

  float f0 = 3.5f;
  double d0 = 1234.5678;

  printf("%f %lf\n", f0, f0);
  return 0;
}
