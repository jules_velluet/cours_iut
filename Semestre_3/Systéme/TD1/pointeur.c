#include <stdio.h>

int main() {
  int a = 5;

  printf( "contenu de a = %d\n", a);
  printf("adresse de a = %p\n", &a);

  int* pa = &a;
  *pa = 4;

  printf("a = %d\n", a);
  printf("*pa = %d\n", *pa);

  return 0;
}
