#include <stdio.h>

void hello() {
  puts("Hello!");
}

int plus(int a, int b){
  return a + b;
}

int main(){
  hello();
  printf("%d\n", plus(7,8));
  return 0;
}
