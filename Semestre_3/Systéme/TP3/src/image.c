#include <IL/il.h>
#include <stdio.h>
#include <stdlib.h>
int main(){
  unsigned int image;
  // Allocation d’une image de 100x100 pixels en RGB
  unsigned char* data = (unsigned char*)malloc(100*100*3);
  // Dessin
  // for (int i=0; i<100; ++i){
  //   data[ 3*(100*i + i)] = 255;
  //   data[ 3*(100*i + i) + 1] = 255;
  //   data[ 3*(100*i + i) + 2] = 255;
  // }
  for(int j=0; j<100;++j){
        for(int i=0;i<100;++i){

                data[3*(100*i + j) + 2] = 150;
                data[3*(100*i + j) + 1] = 150;

            if((j-50)*(j-50)+(i-40)*(i-40) < 30*30){
                data[3*(100*i + j)] = (rand()%256);
                data[3*(100*i+ j) + 1] = (rand()%256);
                data[3*(100*i + j) + 2] = (rand()%256);
            }
            if((j-30)*(j-30)+(i-60)*(i-60) < 20*20){
                data[3*(100*i + j)] = (rand()%256);
                data[3*(100*i+ j) + 1] = (rand()%256);
                data[3*(100*i + j) + 2] = (rand()%256);
            }
            if((j-70)*(j-70)+(i-60)*(i-60) < 20*20){
                data[3*(100*i + j)] = (rand()%256);
                data[3*(100*i+ j) + 1] = (rand()%256);
                data[3*(100*i + j) + 2] = (rand()%256);
            }

        }
    }

  ilInit(); // Initialisation de la bibliothèque
  ilGenImages(1, &image); // Génération de l’image
  ilBindImage(image) ; // Activation de l’image
  // Définition de l’image : 100x100 pixels, 3 composantes RGB, données
  ilTexImage(100, 100, 1, 3, IL_RGB, IL_UNSIGNED_BYTE, data) ;
  ilEnable(IL_FILE_OVERWRITE); // Ecrase l’image si elle existe déjà
  ilSaveImage("out.jpg"); // Sauvegarde de l’image dans le fichier out.jpg
  ilDeleteImages(1, &image); // Libération des ressources liées à l’image
  free(data); // Libération du tableau image
  puts("salut");
  return 0;
}
