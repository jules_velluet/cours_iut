#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

typedef struct{
    unsigned int id;
    int* tab;
    size_t size;
    int sum;
}targ_t;

void* reduce(void* arg){
    targ_t * a = (targ_t*) arg;
    int sum = 0;
    for (size_t i = a->id * (a->size)/2 ; i < (a->id+1)*(a->size)/2; ++i){
        sum += a->tab[i];
    }
    a->sum = sum;
    return NULL;
}

int main(){
    srand(time(NULL));
    size_t const size = 100;
    int * tab = (int*)malloc(size * sizeof(int));

    for(size_t i = 0 ; i < size ; ++i){
        tab[i] = rand()%10;
    }

    pthread_t threads[2];
    targ_t args[2];

    for(size_t i = 0 ; i < 2 ; ++i){
        args[i].id = i;
        args[i].tab = tab;
        args[i].size = size;
        pthread_create(&threads[i], NULL, reduce, (void*)&args[i]);
    }
    for (size_t i = 0 ; i < 2 ; ++i){
        pthread_join( threads[i],NULL);
        printf("sum[%zu]= %i\n",i, args[i].sum);
    }
    printf("total=%i\n",args[0].sum + args[1].sum);
    return 0;
}
