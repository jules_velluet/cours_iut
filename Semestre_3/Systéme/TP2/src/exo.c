#include <stdio.h>

long long int factorielle(long long int nb){
  long long int res = 1;
  for (int i=1; i<=nb; i++){
    res = res * i;
  }
  return res;
}

int main(){
  printf("Entrez une valeur\n");
  long long int a;
  scanf("%d", &a);
  printf("factorielle de %lli =\n", a );
  printf("%lli\n", factorielle(a));
}
