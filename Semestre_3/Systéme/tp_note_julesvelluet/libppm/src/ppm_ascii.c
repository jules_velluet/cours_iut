#include <ppm_ascii.h>
#include <stdio.h>
#include <stdlib.h>


ppm_t ppm_read_ascii( char const * filename )
{
  FILE * file = fopen( filename, "r" );
  if (file){
    ppm_t struct_image;
    char magic[ 3 ] = "  \0";//type de l'image
    fscanf(file, "%2s", magic);
    fscanf(file,"%i", &struct_image.w);
    fscanf(file,"%i", &struct_image.h);
    fscanf(file, "%i", &struct_image.max);

    unsigned char *rgb = (unsigned char*) malloc(struct_image.w*struct_image.h*3*sizeof(unsigned char));

    for( size_t i = 0 ; i < struct_image.w *struct_image.h*3; ++i){
      fscanf(file, "%hhu", &rgb[i]);
    }
    struct_image.data = rgb;
        
    fclose( file );
    return struct_image;
  }
  else {
    perror("error");
  }
  
}


void ppm_write_ascii( ppm_t const * pa, char const * filename )
{
  
}


void ppm_display_ascii( ppm_t const * pa )
{

}
