#include <ppm_bin.h>
#include <stdio.h>


ppm_t ppm_read_bin( char const * filename )
{
  ppm_t tmp;

  return tmp;
}


void ppm_write_bin( ppm_t const * pb, char const * filename )
{
  FILE * file = fopen(filename,"w+");
  if(file){
    fprintf(file, "%s\n", "P6");
    fprintf(file,"%i ",pb->w);
    fprintf(file,"%i\n",pb->h);
    fprintf(file, "%i\n",pb->max);
    fwrite(pb->data,sizeof(unsigned char), pb->w*pb->h*3, file);
  }
  else{
    perror("error");
  }
}


void ppm_display_bin( ppm_t const * pb )
{

}
