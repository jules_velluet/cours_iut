#include <ppm.h>

#include <stdio.h>


int main()
{
  ppm_t in = ppm_read_ascii( "images/in.ppm" );

  /*for( size_t i = 0; i < in.w * in.h * 3 ; ++i )
  {
    in.data[ i ] =  in.data[ i ];
  }*/
  
  ppm_write_bin( &in, "images/out.ppm" );
  
  return 0;
}
