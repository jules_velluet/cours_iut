 //#include <ppm.h>
#include <ppm_struct.h>
#include <stdio.h>
#include <ppm_bin.h>
#include <ppm_ascii.h>

int main()
{
  ppm_t in = ppm_read_bin("images/in_binary.ppm");

  // for( size_t i = 0; i < in.w * in.h * 3 ; ++i )
  // {
  //   in.data[ i ] = 255 - in.data[ i ];
  // }  
  
  ppm_write_ascii( &in, "images/out_ascii.ppm" );
  
  return 0;
}
