import java.util.Scanner;

public class Bank {
 public static void main(String[] args)
  {
    Client dupont = new Client("Dupont");
    //Compte compte = new Compte("CCP01234");
    //dupont.ajouterCompte(compte);
    //System.out.println(compte);
    String rep="",num,type;
    double montant;
    Compte compte;
    Scanner sc = new Scanner(System.in);

    while(!rep.equals("1")){
      // AFFICHAGE DU MENU
      System.out.print("\033[H\033[2J");System.out.flush(); // pour vider la console
      System.out.println("GESTION DES COMPTES DE "+dupont.getNom());
      System.err.println("---------------------------------------");
      System.out.println("1. Quitter l'application");
      System.out.println("2. Consulter un compte");
      System.out.println("3. Débiter un compte");
      System.out.println("4. Créditer un compte");
      System.out.println("5. Créer un nouveau compte");
      System.out.println("6. Afficher l'historique d'un compte");


      // INSTRUCTION UTILISATEUR
      rep = sc.nextLine();
      switch(rep){
        case "1": 
          System.out.println("Au revoir.");
          break;
        case "2":
          System.out.println("Numéro du compte à consulter :");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            System.out.println(compte);
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;
        case "3":
          System.out.println("Numéro du compte à Débiter :");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            System.out.println("Montant à Débiter :");
            montant = Double.parseDouble(sc.nextLine());
            if(compte.debiter(montant)){
              System.out.println("Le compte a été débité.");
            }
            else{
              System.out.println("Opération impossible.");
            }
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;
        case "4":
          System.out.println("Numéro du compte à créditer :");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            System.out.println("Montant à créditer :");
            montant = Double.parseDouble(sc.nextLine());
            if(compte.crediter(montant)){
              System.out.println("Le compte a été crédité.");
            }
            else{
              System.out.println("Opération impossible.");
            }
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;
        case "5":
          System.out.println("Numéro du compte à créer :");
          num = sc.nextLine();
          System.out.println("Type de compte (C)ourant ou (E)pargne ? ");
          type = sc.nextLine();
          if(type.equals("C")) compte = new CompteCourant(num);
          else compte = new CompteEpargne(num);
          if(dupont.ajouterCompte(compte)){
            System.out.println("Le compte "+num+" a été ajouté.");
          }
          else{
            System.out.println("Opération impossible : le compte "+num+" existe déjà.");
          }
          break;
        case "6":
          System.out.println("Numéro du compte à consulter :");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            compte.afficheHistorique();
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;  
        }
      rep = sc.nextLine();
    }

    sc.close();
  }
}
