import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Operation {
    private Date date;
    private double montant;

    public Operation(double montant){
        this.date = new Date();
        this.montant = montant;
    }

    @Override
    public String toString(){
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return format.format(this.date)+"\t"+this.montant;
    }
}
