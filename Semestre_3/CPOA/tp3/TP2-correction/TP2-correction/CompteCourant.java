public class CompteCourant extends Compte{

    private double decouvertAutorise;

  public void setDecouvertAutorise (double newVar) {
    decouvertAutorise = newVar;
  }

  public double getDecouvertAutorise () {
    return decouvertAutorise;
  }

  public CompteCourant(String numero){
      super(numero);
      this.decouvertAutorise = 100;
  }

  public boolean debiter(double montant)
  {
    double newSolde = this.solde - montant;
    if(newSolde >= -this.decouvertAutorise){
      this.solde = newSolde;
      this.historique.add(0,new Operation(-montant));//on ajoute l'opération au début
      return true;
    }
    else{
      return false;
    }
  }

  @Override
  public String toString(){
    return super.toString()+"\n  Decouvert autorisé :"+this.decouvertAutorise;
  }


}
