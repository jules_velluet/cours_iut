import javax.swing.JLabel;
import javax.swing.JFrame;

public class Widget extends JFrame implements IObservateur {

  private JLabel message;
  
  public Widget(String titre){
    super(titre);
    this.setSize(300);
    this.message = new JLabel("Compte OK");
    this.add(this.message);
    this.setVisible(true);
   }
  /**
   * Set the value of message
   * @param newVar the new value of message
   */
  public void setMessage (JLabel newVar) {
    message = newVar;
  }

  /**
   * Get the value of message
   * @return the value of message
   */
  public JLabel getMessage () {
    return message;
  }

  /**
   * @return       String
   */
  public String notification(){};


}
