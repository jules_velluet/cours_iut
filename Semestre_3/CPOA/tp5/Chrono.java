import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.JComponent;
import java.util.*;

public class Chrono implements IEvenement, Runnable {

  //
  // Fields
  //
  private Automate m_controleur;
  private int x, y, diametre;
	private JComponent proprietaire;
	private Thread deroulement;
	private long tempsEcoule = 0; // exprime en millisecondes
	private long duree; // nombre de millisecondes pour un tour complet
	private long momentDebut = 0;
	private long momentSuspension;
  private boolean finir;
  private boolean suspendu=false;

  public Chrono(JComponent proprietaire, int duree, int x, int y, int diametre) {
		this.duree = duree * 1000;
		this.x = x;
		this.y = y;
		this.diametre = diametre;
		this.proprietaire = proprietaire;
    this.m_controleur = new Automate(this);
    this.m_controleur.setEtatCourant(new Etat1());
	}
  //
  // Methods
  //
  public void setControlleur(Automate newVar){
    m_controleur = newVar;
  }

  //
  // Accessor methods
  //

  //
  // Other methods
  //
  public Automate getControleur(){
    return m_controleur;
  }

  /**
   */
  public void lancerChrono()
  {
    deroulement = new Thread(this);
		deroulement.start();
  }


  /**
   */
  public void stopperChrono()
  {
    suspendu=false;
    finir = true;
    notifyAll();
  }


  /**
   */
  public void demarrer(){
    m_controleur.demarrer();
  }


  /**
   */
  public void arreter(){
    m_controleur.arreter();
  }

  public void run() {
		Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
		finir = false;
		momentDebut = System.currentTimeMillis();
		while((tempsEcoule < duree) && (!finir))
		{
			tempsEcoule = System.currentTimeMillis() - 
			momentDebut;
			proprietaire.repaint(new Rectangle(x, y, diametre, diametre));
			try {
				Thread.sleep(200);
				synchronized(this) {
					while (suspendu && !finir) wait();
				}
			}
			catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}

  public void dessine(Graphics g) {
		g.setColor(Color.YELLOW);
		g.fillArc(x, y, diametre, diametre, 90,
		(int)(360 - tempsEcoule * 360 / duree));
		g.setColor(Color.GREEN);
		g.fillArc(x, y, diametre, diametre,90,
		(int)(-tempsEcoule * 360 / duree));
	}


}
