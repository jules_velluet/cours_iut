
import java.util.*;

public class Automate implements IEvenement {

  //
  // Fields
  //

  private Chrono m_controle;
  private IEtat m_etatCourant;
  public Automate (Chrono chrono) {
    this.m_controle = chrono;
  }
  
  //
  // Methods
  //


  //
  // Accessor methods
  //
  public Chrono getControleur(){
    return this.m_controle;
  }
  /**
   * Set the value of m_etatCourant
   * @param newVar the new value of m_etatCourant
   */
  public void setEtatCourant(IEtat newVar) {
    m_etatCourant = newVar;
  }

  /**
   * Get the value of m_etatCourant
   * @return the value of m_etatCourant
   */
  private IEtat getEtatCourant() {
    return m_etatCourant;
  }

  //
  // Other methods
  //

  /**
   * @param        etat
   */
  public void changementEtat(IEtat etat)
  {
    m_etatCourant = etat;
  }


  /**
   */
  public void demarrer(){
    m_etatCourant.demarrer(this);
  }


  /**
   */
  public void arreter(){
    m_etatCourant.arreter(this);
  }
}
