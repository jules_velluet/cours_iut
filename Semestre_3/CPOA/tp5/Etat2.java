
public class Etat2 implements IEtat {
  public Etat2 () { };
  /**
   * @param        a
   */
  public void arreter(Automate a){
    a.getControleur().stopperChrono();
    a.changementEtat(new Etat1());
  }


  /**
   * @param        a
   */
  public void demarrer(Automate a){
    a.getControleur().lancerChrono();
    a.changementEtat(new Etat2());
  };


}
