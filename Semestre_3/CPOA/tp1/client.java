
import java.util.*;

public class client {

  //
  // Fields
  //

  private String nom;

  private Vector compteVector = new Vector();  public client () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of nom
   * @param newVar the new value of nom
   */
  public void setNom (String newVar) {
    nom = newVar;
  }

  /**
   * Get the value of nom
   * @return the value of nom
   */
  public String getNom () {
    return nom;
  }

  /**
   * Add a Compte object to the compteVector List
   */
  public void addCompte (compte new_object) {
    compteVector.add(new_object);
  }

  /**
   * Remove a Compte object from compteVector List
   */
  private void removeCompte (compte new_object)
  {
    compteVector.remove(new_object);
  }

  /**
   * Get the List of Compte objects held by compteVector
   * @return List of Compte objects held by compteVector
   */
  private List getCompteList () {
    return (List) compteVector;
  }


  //
  // Other methods
  //

  /**
   * @return       client
   * @param        nom
   */
  public client(String nom)
  {
    this.nom = nom;
  }


}
