import java.util.*;

public class compte {

  //
  // Fields
  //

  private float solde;
  private String numero;
  private Integer decouvertAutorise;  public compte () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of solde
   * @param newVar the new value of solde
   */
  public void setSolde (float newVar) {
    solde = newVar;
  }

  /**
   * Get the value of solde
   * @return the value of solde
   */
  public float getSolde () {
    return solde;
  }

  /**
   * Set the value of numero
   * @param newVar the new value of numero
   */
  public void setNumero (String newVar) {
    numero = newVar;
  }

  /**
   * Get the value of numero
   * @return the value of numero
   */
  public String getNumero () {
    return numero;
  }

  /**
   * Set the value of decouvertAutorise
   * @param newVar the new value of decouvertAutorise
   */
  public void setDecouvertAutorise (Integer newVar) {
    decouvertAutorise = newVar;
  }

  /**
   * Get the value of decouvertAutorise
   * @return the value of decouvertAutorise
   */
  public Integer getDecouvertAutorise () {
    return decouvertAutorise;
  }

  //
  // Other methods
  //

  /**
   * @param        montant
   */
  public void debiter_montant(float montant)
  {
    if (this.solde - montant >=this.decouvertAutorise){
    	this.solde -= montant;
    }
  }


  /**
   * @param        montant
   */
  public void crediter(float montant)
  {
    this.solde += montant;
  }


  /**
   * @return       compte
   * @param        numero
   */
  public compte(String numero)
  {
    this.numero = numero;
    this.solde = 0;
    this.decouvertAutorise = 0;
  }


  /**
   * @return       String
   */
  public String toString()
  {
    return this.numero + "argent = "+this.solde;
  }


}
