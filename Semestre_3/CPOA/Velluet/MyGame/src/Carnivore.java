

/**
 * Class carnivore
 */
public class Carnivore implements IManger {

  //
  // Fields
  //


  //
  // Constructors
  //
  public Carnivore () { };

  //
  // Methods
  //


  //
  // Accessor methods
  //

  //
  // Other methods
  //

  /**
   */
  public void seNourrir(Animaux animal){
    Lieux l = animal.getPosition();
    for (Animaux a: l.getListeAnimauxLieux()){
      if (a instanceof Lapin){
        if (animal.getEnergie() < animal.getEnergieMax()){
          System.out.println("renard mange un délicieux lapin");
          animal.setEnergie(3);
          Monde.getInstance().removeAnimal(a);
          l.removeAnimalLieux(a);
        }
      }
      return;
    }

  }


}
