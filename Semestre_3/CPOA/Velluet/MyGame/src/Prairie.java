import java.util.*;

/**
 * Class Prairie
 */
public class Prairie extends Lieux {

  //
  // Fields
  //
  private List<Carotte> listecarotte = new ArrayList<>();

  //
  // Constructors
  //
  public Prairie(int posx, int posy) {
    this.x = posx;
    this.y = posy;
    Monde.getInstance().addListeLieux(this);
  }

  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Add a ListeCarotte object to the listecarotteVector List
   */
  public void addListeCarotte () {
    listecarotte.add(new Carotte());
  }

  /**
   * Remove a ListeCarotte object from listecarotteVector List
   */
  public void removeListeCarotte()
  {
    if (listecarotte.size() != 0){
      listecarotte.remove(0);
    }

  }

  /**
   * Get the List of ListeCarotte objects held by listecarotteVector
   * @return List of ListeCarotte objects held by listecarotteVector
   */
  public List<Carotte> getListeCarotteList() {
    return listecarotte;
  }
  //
  // Other methods
  //

  /**
   * @return       int
   */
  public int getNbCarotte(){
    return listecarotte.size();
  }

  @Override
  public String toString(){
    return "Prairie avec "+getNbCarotte()+" carotte(s)";
  }
}
