import java.util.*;

/**
 * Class Animaux
 */
abstract public class Animaux {

  //
  // Fields
  //

  public String modeDeVie;
  public String regimeAlimentaire;
  public int age;
  public int energie;
  public int energieMax;
  public String sexe;


  public IManger m_manger;

  public Deplacer m_deplacer;

  public Lieux m_position;

  //
  // Constructors
  //
  public Animaux () {
    this.age = 0;
    this.energieMax = 20;
    this.energie = 20;
    Random rand = new Random();
    if (rand.nextInt(2) == 0){
      this.sexe = "malealpha";
    }
    else {
      this.sexe = "femelle";
    }
    setPosition();
  }

  public Animaux (Lieux l) {
    this.age = 0;
    this.energieMax = 4;
    this.energie = 4;
    Random rand = new Random();
    if (rand.nextInt(2) == 0){
      this.sexe = "malealpha";
    }
    else {
      this.sexe = "femelle";
    }
    this.m_position = l;
  }
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of modeDeVie
   * @param newVar the new value of modeDeVie
   */
  public void setModeDeVie (String newVar) {
    modeDeVie = newVar;
  }

  /**
   * Get the value of modeDeVie
   * @return the value of modeDeVie
   */
  public String getModeDeVie () {
    return modeDeVie;
  }

  /**
   * Set the value of regimeAlimentaire
   * @param newVar the new value of regimeAlimentaire
   */
  public void setRegimeAlimentaire (String newVar) {
    regimeAlimentaire = newVar;
  }

  /**
   * Get the value of regimeAlimentaire
   * @return the value of regimeAlimentaire
   */
  public String getRegimeAlimentaire () {
    return regimeAlimentaire;
  }

  /**
   * Set the value of age
   * @param newVar the new value of age
   */
  public void setAge () {
    age += 1;
  }

  /**
   * Get the value of age
   * @return the value of age
   */
  public int getAge () {
    return age;
  }

  /**
   * Set the value of energie
   * @param newVar the new value of energie
   */
  public void setEnergie (int nb) {
      energie += nb;
  }

  /**
   * Get the value of energie
   * @return the value of energie
   */
  public int getEnergie () {
    return energie;
  }

  /**
   * Set the value of energieMax
   * @param newVar the new value of energieMax
   */
  public void setEnergieMax (int newVar) {
    energieMax = newVar;
  }

  /**
   * Get the value of energieMax
   * @return the value of energieMax
   */
  public int getEnergieMax () {
    return energieMax;
  }

  /**
   * Set the value of sexe
   * @param newVar the new value of sexe
   */
  public void setSexe (String newVar) {
    sexe = newVar;
  }

  /**
   * Get the value of sexe
   * @return the value of sexe
   */
  public String getSexe () {
    return sexe;
  }

  /**
   * Set the value of m_manger
   * @param newVar the new value of m_manger
   */
  private void setManger (IManger newVar) {
    m_manger = newVar;
  }

  /**
   * Get the value of m_manger
   * @return the value of m_manger
   */
  private IManger getManger () {
    return m_manger;
  }

  /**
   * Set the value of m_deplacer
   * @param newVar the new value of m_deplacer
   */
  private void setDeplacer (Deplacer newVar) {
    m_deplacer = newVar;
  }

  /**
   * Get the value of m_deplacer
   * @return the value of m_deplacer
   */
  private Deplacer getDeplacer () {
    return m_deplacer;
  }

  /**
   * Set the value of m_position
   * @param newVar the new value of m_position
   */
  public void setPosition() {
    List<Lieux> l = Monde.getInstance().getListeLieuxList();
    Random r = new Random();
    int n = r.nextInt(l.size());
    Lieux lieux = l.get(n);
    m_position = lieux;
    lieux.addListeAnimauxLieux(this);
  }

  public void setPos(Lieux l){
    m_position = l;
  }

  /**
   * Get the value of m_position
   * @return the value of m_position
   */
  public Lieux getPosition () {
    return m_position;
  }

  //
  // Other methods
  //

  /**
   */
  public void seDeplacer()
  {
    m_deplacer.seDeplacer(this);
  }


  /**
   */
  public void seNourrir()
  {
    m_manger.seNourrir(this);
  }

  public void reproduction(){
    int cpt =0;
    int cptr = 0;
    List<Animaux> ani = this.m_position.getListeAnimauxLieux();
    for (Animaux a: ani){
      if (this instanceof Lapin && a instanceof Lapin){
        if (this.sexe == "malealpha" && a.sexe == "femelle"){
          cpt +=1;
          a.setEnergie(-3);
        }
      }
      else if (this instanceof Renard && a instanceof Renard){
        if (this.sexe == "malealpha" && a.sexe == "femelle"){
          cptr +=1;
        }
      }
    }
    newL(cpt);
    newR(cptr);
  }
  public void newL(int cpt){
    for (int i = 0; i< cpt; i++){
      System.out.println("Un lapereau est née");
      this.setEnergie(-3);
      new Lapin(this.m_position);
    }
  }

  public void newR(int cpt){
    for (int i = 0; i< cpt; i++){
      System.out.println("Un Renardeau est née");
      this.setEnergie(-3);
      new Renard(this.m_position);
    }
  }

}
