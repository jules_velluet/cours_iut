/**
 * Class Forêt
 */
public class Foret extends Lieux{

  //
  // Fields
  //


  //
  // Constructors
  //
  public Foret (int posx, int posy) {
    this.x = posx;
    this.y = posy;
    Monde.getInstance().addListeLieux(this);
  }

  @Override
  public String toString(){
    return "x = "+this.x + " ; y = "+this.y;
  }
  //
  // Methods
  //


  //
  // Accessor methods
  //

  //
  // Other methods
  //

}
