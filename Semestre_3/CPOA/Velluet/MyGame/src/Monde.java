import java.util.*;

/**
 * Class Monde
 */
public class Monde {

  //
  // Fields
  //

  static private Monde instance;
  private int date;

  private List<Lieux> listelieux = new ArrayList<>();
  private List<Animaux> listeAnimaux = new ArrayList<>();

  //
  // Constructors
  //
  public Monde() {
    if (instance == null) {
      instance = this;
    }
    this.date = 0;
  }

  //
  // Methods
  //
  public void addListeAnimaux(Animaux animal){
    listeAnimaux.add(animal);
  }

  public void removeAnimal(Animaux animal){
    listeAnimaux.remove(animal);
  }

  public List<Animaux> getListeAnimaux(){
    return listeAnimaux;
  }

  //
  // Accessor methods
  //

  /**
   * Set the value of instance
   * @param newVar the new value of instance
   */
  public void setInstance (Monde newVar) {
    instance = newVar;
  }

  /**
   * Get the value of instance
   * @return the value of instance
   */
  public static Monde getInstance () {
    return instance;
  }

  /**
   * Set the value of date
   * @param newVar the new value of date
   */
  public void setDate() {
    date += 1;
    List<Animaux> la = new ArrayList<>(listeAnimaux);
    for (Animaux a: la){
      a.setAge();
      a.setEnergie(-1);
      a.seDeplacer();
      if (a.getEnergie() <= 15){
        a.seNourrir();
      }
      else {
        a.reproduction();
      }
      // if (date%2 == 0){
      //   a.reproduction();
      // }
      // else {
      //   a.seNourrir();
      // }
      if (a.energie <= 0){
        Monde.getInstance().removeAnimal(a);
        a.m_position.removeAnimalLieux(a);
        System.out.println("un animal est mort");
      }
    }

    List<Lieux> li = new ArrayList<>(listelieux);
    for (Lieux l: li){
      if (l instanceof Prairie){
        Prairie p = (Prairie)l;
        p.addListeCarotte();
      }
    }
    nbLapinRenard();
  }

  public void nbLapinRenard(){
    int lapin = 0;
    int renard = 0;
    for (Animaux a: this.listeAnimaux){
      if (a instanceof Lapin){
        lapin += 1;
      }
      else if (a instanceof Renard){
        renard += 1;
      }
    }
    System.out.println("il y a "+ lapin + " lapin(s) et "+ renard + "renard(s) dans le monde");
  }

  /**
   * Get the value of date
   * @return the value of date
   */
  public int getDate () {
    return date;
  }

  /**
   * Add a ListeLieux object to the listelieuxVector List
   */
  public void addListeLieux (Lieux new_object) {
    listelieux.add(new_object);
  }

  /**
   * Remove a ListeLieux object from listelieuxVector List
   */
  public void removeListeLieux (Lieux new_object)
  {
    listelieux.remove(new_object);
  }

  /**
   * Get the List of ListeLieux objects held by listelieuxVector
   * @return List of ListeLieux objects held by listelieuxVector
   */
  public List<Lieux> getListeLieuxList () {
    return listelieux;
  }

  @Override
  public String toString(){
    return "Lieux: " + this.listelieux + "\nAnimaux: " + this.listeAnimaux;
  }

  //
  // Other methods
  //

}
