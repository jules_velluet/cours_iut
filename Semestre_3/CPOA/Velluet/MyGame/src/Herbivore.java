/**
 * Class herbivore
 */
public class Herbivore implements IManger {

  //
  // Fields
  //


  //
  // Constructors
  //
  public Herbivore () { };

  //
  // Methods
  //


  //
  // Accessor methods
  //

  //
  // Other methods
  //

  /**
   */
  public void seNourrir(Animaux animal){
    Lieux l = animal.getPosition();
    if (l instanceof Prairie){
      Prairie p = (Prairie) l;
      if (p.getNbCarotte() > 0 ){
        if (animal.getEnergie() < animal.getEnergieMax()){
          animal.setEnergie(3);
          p.removeListeCarotte();
          System.out.println("un lapin mange une carotte");
        }
      }
    }
  }
}
