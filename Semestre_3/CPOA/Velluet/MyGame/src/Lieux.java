import java.util.*;

/**
 * Class Lieux
 */
abstract public class Lieux {

  //
  // Fields
  //

  public int x;
  public int y;
  public List<Animaux> listeAnimaux = new ArrayList<>();

  //
  // Constructors
  //
  public Lieux () {};

  //
  // Methods
  //
  public void addListeAnimauxLieux(Animaux ani){
    listeAnimaux.add(ani);
  }

  public List<Animaux> getListeAnimauxLieux(){
    return listeAnimaux;
  }

  public void removeAnimalLieux(Animaux ani){
    listeAnimaux.remove(ani);
  }

  //
  // Accessor methods
  //

  /**
   * Set the value of x
   * @param newVar the new value of x
   */
  public void setX (int newVar) {
    x = newVar;
  }

  /**
   * Get the value of x
   * @return the value of x
   */
  public float getX () {
    return x;
  }

  /**
   * Set the value of y
   * @param newVar the new value of y
   */
  public void setY (int newVar) {
    y = newVar;
  }

  /**
   * Get the value of y
   * @return the value of y
   */
  public float getY () {
    return y;
  }
}
