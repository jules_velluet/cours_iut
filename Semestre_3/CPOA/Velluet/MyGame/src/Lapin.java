/**
 * Class Lapin
 */
public class Lapin extends Animaux {

  //
  // Fields
  //


  //
  // Constructors
  //
  public Lapin() {
    this.modeDeVie = "Solitaire";
    this.regimeAlimentaire = "herbivore";
    Monde.getInstance().addListeAnimaux(this);
    m_manger = new Herbivore();
    m_deplacer = new Quatripode();
  }

  public Lapin(Lieux l){
    this.modeDeVie = "Solitaire";
    this.regimeAlimentaire = "herbivore";
    Monde.getInstance().addListeAnimaux(this);
    m_manger = new Herbivore();
    m_deplacer = new Quatripode();
  }
  //
  // Methods
  //
  //
  // Accessor methods
  //
  @Override
  public String toString(){
    return "\nLAPIN: "+this.sexe+" energie: "+this.energie+" age: "+this.age;
  }
  //
  // Other methods
  //

}
