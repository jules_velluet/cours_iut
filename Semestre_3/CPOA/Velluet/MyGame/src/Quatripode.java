import java.util.*;

/**
 * Class 4Pattes
 */
public class Quatripode implements Deplacer {

  //
  // Fields
  //


  //
  // Constructors
  //
  public Quatripode() {

  }

  //
  // Methods
  //


  //
  // Accessor methods
  //

  //
  // Other methods
  //

  /**
   */
  public void seDeplacer(Animaux animal){
    Lieux l = animal.getPosition();
    Random rand = new Random();
    List<Lieux> li = Monde.getInstance().getListeLieuxList();
    int r = rand.nextInt(Monde.getInstance().getListeLieuxList().size());
    Lieux lieux = li.get(r);
    animal.setPos(lieux);
    l.removeAnimalLieux(animal);
    lieux.addListeAnimauxLieux(animal);
  }


}
