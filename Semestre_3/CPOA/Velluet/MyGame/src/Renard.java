

/**
 * Class Renard
 */
public class Renard extends Animaux {

  //
  // Fields
  //


  //
  // Constructors
  //
  public Renard () {
    this.modeDeVie = "Solitaire";
    this.regimeAlimentaire = "carnivore";
    Monde.getInstance().addListeAnimaux(this);
    m_manger = new Carnivore();
    m_deplacer = new Quatripode();
  }

  public Renard(Lieux l){
    this.modeDeVie = "Solitaire";
    this.regimeAlimentaire = "carnivore";
    Monde.getInstance().addListeAnimaux(this);
    m_manger = new Carnivore();
    m_deplacer = new Quatripode();
  }

  @Override
  public String toString(){
    return "\nRENARD: "+this.sexe+" energie: "+this.energie+" age: "+this.age;
  }
  //
  // Methods
  //


  //
  // Accessor methods
  //

  //
  // Other methods
  //

}
