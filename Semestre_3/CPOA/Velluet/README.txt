Jules Velluet 2a2a
Rendu mini-projet Rabbit&Co CPOA

Vous trouverez à la racine du projet:
  - Diagramme_classe.png: le Diagramme de classe utilisé
  - une archive .jar contenant l'excutable java (java -jar )
  - un dossier MyGame:
    -bin: pour les .class (MainClass = main.class)
    -src: le code source



Pour lancer le jeu il faut faire: java -jar croque_lapin_3000.jar
Si vous voulez le recompiler: placer dans Mygame est exécuté: javac ./src/*.java -d


Description du travail réalisé:

j'ai utilisé deux patterns: le pattern stratégie et le pattern singleton(sur le monde).
J'ai utilisé le pattern singleton sur le monde car on peut créé qu'une seule instance de celui-ci
J'ai utilisé un pattern stratégie car les animaux n'ont pas tous le même comportement (un lapin mange des carottes et un renard des lapins)

Mon implémentation permet aux animaux de se déplacer de prairie en prairie en perdant une énergie. Ils peuvent se nourrir lorsqu'il n'ont plus beaucoup d'énergie, les lapins
en mangeant des carottes et les renards en mangeant des lapins. Lorsqu'ils mangeant il gagne de l'énergie.
Mes animaux peuvent se reproduire entre un male et une femelle de même espèce, cette action donne naissance à un bébé de même espèce. Les parents perdent trois énergies une fois l'acte terminé.
Lorsqu'un animal est arrivé 0 d'énergie il est supprimer du monde.


Amélioration:
- faire en sorte que les mâles cherchent les femelles pour se reproduire
- ajouter la notion de mère et une action allaite
- notion de fuite chez les lapins


Problème:
- après un trop grand nombre de jours une population devient quasiment infinie et le code bug
