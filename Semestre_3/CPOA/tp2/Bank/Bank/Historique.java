public class Historique {

    private String date;
    private double montant;

    public Historique(String date, double val){
        this.date = date;
        this.montant = val;
    }

    @Override
    public String toString(){
        return "Opération effectuer le "+this.date+" d'un montant de "+this.montant;
    }
}
