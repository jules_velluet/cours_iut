import java.util.*;

public class Compte {

  //
  // Fields
  //

  private double solde;
  private String numero;
  private double decouvertAutorise;
  private ArrayList<Historique> michel = new ArrayList<Historique>();
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of solde
   * @param newVar the new value of solde
   */
  public void setSolde (double newVar) {
    solde = newVar;
  }

  /**
   * Get the value of solde
   * @return the value of solde
   */
  public double getSolde () {
    return solde;
  }

  /**
   * Set the value of numero
   * @param newVar the new value of numero
   */
  public void setNumero (String newVar) {
    numero = newVar;
  }

  /**
   * Get the value of numero
   * @return the value of numero
   */
  public String getNumero () {
    return numero;
  }

  /**
   * Set the value of decouvertAutorise
   * @param newVar the new value of decouvertAutorise
   */
  public void setDecouvertAutorise (double newVar) {
    decouvertAutorise = newVar;
  }

  /**
   * Get the value of decouvertAutorise
   * @return the value of decouvertAutorise
   */
  public double getDecouvertAutorise () {
    return decouvertAutorise;
  }

  //
  // Other methods
  //

  /**
   * @param        numero
   */
  public Compte(String numero)
  {
    this.numero = numero;
    this.solde = 0;
    this.decouvertAutorise = 0;
  }


  /**
   * @return       boolean
   * @param        montant
   */
  public boolean debiter(double montant)
  {
    double newSolde = this.solde - montant;
    if(newSolde >= -this.decouvertAutorise){
      this.solde = newSolde;
      this.michel.add(0,new Historique("la",montant*-1));
      return true;
    }
    else{
      return false;
    }
  }

  public ArrayList<Historique> getMichel(){
    return this.michel;
  }


  /**
   * @return       boolean
   * @param        montant montant à créditer (valeur positive)
   */
  public boolean crediter(double montant)
  {
    this.solde += montant;
    this.michel.add(0,new Historique("la",montant));
    return true;
  }

  @Override
  public String toString(){
    return "Compte n°"+this.numero+"\n  Solde : "+this.solde+"\n  Decouvert autorisé :"+this.decouvertAutorise;
  }

}
