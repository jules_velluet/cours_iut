import java.util.ArrayList;
import java.util.Scanner;

public class Bank {
 public static void main(String[] args)
  {
    Client dupont = new Client("Dupont");
    //Compte compte = new Compte("CCP01234");
    //dupont.ajouterCompte(compte);
    //System.out.println(compte);
    String rep="",num;
    double montant;
    Compte compte;
    Scanner sc = new Scanner(System.in);

    while(!rep.equals("1")){
      // AFFICHAGE DU MENU
      System.out.println("GESTION DES COMPTES DE "+dupont.getNom());
      System.err.println("---------------------------------------");
      System.out.println("1. Quitter l'application");
      System.out.println("2. Consulter un compte");
      System.out.println("3. Débiter un compte");
      System.out.println("4. Créditer un compte");
      System.out.println("5. Créer un nouveau compte");
      System.out.println("6. Consulter l'historique d'un compte");

      // INSTRUCTION UTILISATEUR
      rep = sc.nextLine();
      switch(rep){
        case "1": 
          System.out.println("Au revoir.");
          break;
        case "2":
          System.out.println("Numéro du compte à consulter :");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            System.out.println(compte);
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;
        case "3":
          System.out.println("Numéro du compte à Débiter :");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            System.out.println("Montant à Débiter :");
            montant = Double.parseDouble(sc.nextLine());
            if(compte.debiter(montant)){
              System.out.println("Le compte a été débité.");
            }
            else{
              System.out.println("Opération impossible.");
            }
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;
        case "4":
          System.out.println("Numéro du compte à créditer :");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            System.out.println("Montant à créditer :");
            montant = Double.parseDouble(sc.nextLine());
            if(compte.crediter(montant)){
              System.out.println("Le compte a été crédité.");
            }
            else{
              System.out.println("Opération impossible.");
            }
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;
        case "5":
          System.out.println("Numéro du compte à créer :");
          num = sc.nextLine();
          if (dupont.chercheCompte(num) == null){
            dupont.ajouterCompte(new Compte(num));
            System.out.println("Le compte "+num+" a été ajouté.");
            break; 
          }
          else {
            System.out.println("C'est pas possible petit chenapan");
          }
          case "6":
          System.out.println("Numéro du compte à consulter:");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            ArrayList<Historique> a = compte.getMichel();
            for (int i=0; i<a.size(); i++){
              System.out.println(a.get(i));
            }
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;
      }
    }

    sc.close();
  }
}
