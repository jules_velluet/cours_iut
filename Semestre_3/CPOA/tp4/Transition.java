
import java.util.*;

public class Transition {

  //
  // Fields
  //

  private char symbol;

  private Etat m_arrivee;  public Transition () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of symbol
   * @param newVar the new value of symbol
   */
  public void setSymbol (char newVar) {
    symbol = newVar;
  }

  /**
   * Get the value of symbol
   * @return the value of symbol
   */
  public char getSymbol () {
    return symbol;
  }

  /**
   * Set the value of m_arrivee
   * @param newVar the new value of m_arrivee
   */
  public void setArrivee (Etat newVar) {
    m_arrivee = newVar;
  }

  /**
   * Get the value of m_arrivee
   * @return the value of m_arrivee
   */
  public Etat getArrivee () {
    return m_arrivee;
  }
}
