
public class Automate {

  private Etat etatInital;  public Automate () { };
  
  //
  // Methods
  //


  /**
   * Set the value of nouvel_attribut
   * @param newVar the new value of nouvel_attribut
   */
  public void setEtatInitial (Etat newVar) {
    etatInital = newVar;
  }

  /**
   * Get the value of nouvel_attribut
   * @return the value of nouvel_attribut
   */
  public Etat getNouvel_attribut () {
    return etatInital;
  }

  //
  // Other methods
  //

  /**
   * @return       boolean
   * @param        mot
   */
  public boolean analyserMot(String mot)
  {
    etatInitial.TraiterMot(mot);
  }


}
