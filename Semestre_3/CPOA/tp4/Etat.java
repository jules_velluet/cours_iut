
import java.util.*;

public class Etat {

  //
  // Fields
  //


  private Vector transitionsVector = new Vector();  public Etat () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Add a Transitions object to the transitionsVector List
   */
  public void addTransitions (Transition new_object) {
    transitionsVector.add(new_object);
  }

  /**
   * Remove a Transitions object from transitionsVector List
   */
  public void removeTransitions (Transition new_object)
  {
    transitionsVector.remove(new_object);
  }

  /**
   * Get the List of Transitions objects held by transitionsVector
   * @return List of Transitions objects held by transitionsVector
   */
  public List getTransitionsList () {
    return (List) transitionsVector;
  }


  //
  // Other methods
  //

  /**
   * @return       boolean
   * @param        mot
   */
  public boolean TraiterMot(String mot)
  {
  }


}
