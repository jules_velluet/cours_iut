# -*- coding: utf-8 -*-
"""
                           Projet Splaby'O
        Projet Python 2020-2021 de 1ere année et AS DUT Informatique Orléans

"""
from carte import *
from participants import *
from matrice import *
import random

def Init_plateau(plateau):
    """
    Initialise le plateau avec le cartes initiales positionnées
    modifie la matrice du plateau mais ne retourne rien
    """
    cartes_initiales = [((0,0),9),((0,2),1),((0,4),1),((0,6),3),
                        ((2,0),8),((2,2),8),((2,4),1),((2,6),2),
                        ((4,0),8),((4,2),4),((4,4),2),((4,6),2),
                        ((6,0),12),((6,2),4),((6,4),4),((6,6),6)
                        ]
    for i in range(len(cartes_initiales)):
        carte = Carte(False,False,False,False)
        decoder_murs(carte,cartes_initiales[i][1])
        set_valeur(plateau,cartes_initiales[i][0][0],cartes_initiales[i][0][1],carte)

def Plateau(les_joueurs, taille=7, nb_objets=3):
    """
    créer un nouveau plateau contenant les joueurs passés en paramètres

    :param les_joueurs: la liste des joueurs participant à la partie
    :param taille: un entier qqui donne la taille du labyrinthe
    :param nb_objets: un entier qui indique combien d'objets différents existent
    :return: un couple contenant
              * une matrice de taille taillextaill représentant un plateau de labyrinthe où les cartes
                ont été placée de manière aléatoire
              * la carte amovible qui n'a pas été placée sur le plateau
    """
    plateau = Matrice(taille,taille,0)
    Init_plateau(plateau)
    liste_cartes = creer_cartes_amovibles()
    num_carte = 0
    for i in range(get_nb_lignes(plateau)):
        for j in range(get_nb_colonnes(plateau)):
            if get_valeur(plateau,i,j) == 0:
                carte = liste_cartes[num_carte]
                tourner_aleatoire(carte)
                set_valeur(plateau,i,j,carte)
                num_carte += 1
    poser_les_objets(plateau,len(les_joueurs),nb_objets)
    pos_depart = [[0,0],[6,6],[0,6],[6,0]]
    if len(les_joueurs) > 4:
        nb_joueurs = 4
    else :
        nb_joueurs = len(les_joueurs)
    for num in range(nb_joueurs):
        poser_joueur(get_valeur(plateau,pos_depart[num][0],pos_depart[num][1]),les_joueurs[num])
    return (plateau,liste_cartes[-1])
    


def creer_cartes_amovibles():
    """
    fonction utilitaire qui permet de créer les cartes amovibles du jeu 
    la fonction retourne la liste, mélangée aléatoirement, des cartes ainsi créées

    :return: la liste mélangée aléatoirement des cartes amovibles créees
    """
    liste_cartes = list()
    for _ in range(16):
        carte = Carte(True,True,False,False)
        tourner_aleatoire(carte)
        liste_cartes.append(carte)
    for _ in range(6):
         #if randint(0,4) == 0:
            #carte = Carte(False,False,False,False)
        #else:
        carte = Carte(True,False,False,False)
        tourner_aleatoire(carte)
        liste_cartes.append(carte)
    for _ in range(12):
        carte = Carte(True,False,True,False)
        tourner_aleatoire(carte)
        liste_cartes.append(carte)
    random.shuffle(liste_cartes)
    return liste_cartes

def poser_les_objets(plateau, nb_joueurs, nb_objets):
    """
    cette fonction va poser de manière aléatoire les objets sur le plateau, il y aura un objet de chaque types par joueur

    :param plateau: le plateau
    :param nb_joueurs: un entier indiquant le nombre de joueurs participant à la partie
    :param nb_objets: un entier le nombre de type d'objets différents
    :return: cette fonction ne retourne rien mais modifie le plateau
    """
    for _ in range(nb_joueurs):
        for num_objet in range(1,nb_objets+1):
            place = False
            while not place:
                alea_lig = randint(0,6)
                alea_col = randint(0,6)
                if ( ( alea_lig + alea_col ) % 7 ) != 0 and not get_objet(get_valeur(plateau,alea_lig,alea_col)): #pas d'objet aux emplacements iniciaux des joueurs et pas d'objet
                    poser_objet(get_valeur(plateau,alea_lig,alea_col),num_objet)
                    place = True

def get_coordonnees_joueur(plateau, joueur):
    """
    retourne les coordonnées sous la forme (lig,col) du joueur passé en paramètre

    :param plateau: le plateau considéré
    :param joueur: le joueur à trouver
    :return: un couple d'entiers donnant les coordonnées du joueur ou None si le joueur n'est pas sur le plateau
    """
    for i in range(get_nb_lignes(plateau)):
        for j in range(get_nb_colonnes(plateau)):
            if possede_joueur(get_valeur(plateau,i,j),joueur):
                return (i,j)
    return None
    

def passage_plateau(plateau, lig, col, direction):
    """
    indique si il y a bien un passage dans le direction passée en paramètre à partir de la position
    lig,col sur le plateau. La fonction retourne None si il n'y a pas de passage ou les coordonnées
    de la case où on arrive en prenant le passage s'il y en a un

    :param plateau: le plateau
    :param lig: un entier donnant le numéro de la ligne
    :param col: un entier donnant le numéro de la colonne
    :param direction: un caractère 'N', 'S', 'O' ou 'E' indiquant la direction où on veut aller
    :return: None s'il n'y a pas de passage possible
             (x,y) les coordonnées où on arrive en prenant le passage s'il existe (un couple d'entiers)
    """
    res = None
    if direction == "N":
        if lig-1 >= 0 : #Il y a une carte au nord
            if passage_nord(get_valeur(plateau,lig,col),get_valeur(plateau,lig-1,col)):
                res = (lig-1,col)
    elif direction == "E":
        if col+1 < get_nb_colonnes(plateau) : #Il y a une carte a l'est
            if passage_est(get_valeur(plateau,lig,col),get_valeur(plateau,lig,col+1)):
                res = (lig,col+1)
    elif direction == "S":
        if lig+1 < get_nb_lignes(plateau) : #Il y a une carte au sud
            if passage_sud(get_valeur(plateau,lig,col),get_valeur(plateau,lig+1,col)):
                res = (lig+1,col)
    elif direction == "O":
        if col-1 >= 0 : #Il y a une carte a l'ouest
            if passage_ouest(get_valeur(plateau,lig,col),get_valeur(plateau,lig,col-1)):
                res = (lig,col-1)
    return res

def marquage_direct(calque,mat,val=1,marque=1):
    """
    marque avec la valeur marque les éléments du calque tel que la valeur 
    correspondante n'est pas un mur (de valeur differente de 1) et 
    qu'un de ses voisins dans le calque à pour valeur val
    la fonction doit retourner True si au moins une case du calque a été marquée
    """
    res = False
    nb_lignes = get_nb_lignes(calque)
    nb_colonnes = get_nb_colonnes(calque)
    directions = ["N","E","S","O"]
    for i in range(nb_lignes):
        for j in range(nb_colonnes):
            if get_valeur(calque,i,j) == 0 : #case non marquée
                for direction in directions:
                    if passage_plateau(mat,i,j,direction):
                        i1 = i
                        j1 = j
                        if direction == "N":
                            i1 = i-1
                        elif direction == "E":
                            j1 = j+1
                        elif direction == "S":
                            i1 = i+1
                        elif direction == "O":
                            j1 = j-1
                        if get_valeur(calque,i1,j1) == 1:
                            set_valeur(calque,i,j,marque)
                            res = True
    return res

def accessible(plateau, lig_depart, col_depart, lig_arrivee, col_arrivee):
    """
    indique si il y a un chemin entre la case lig_depart,col_depart et la case lig_arrivee,col_arrivee du labyrinthe

    :param plateau: le plateau considéré
    :param lig_depart: la ligne de la case de départ
    :param col_depart: la colonne de la case de départ
    :param lig_arrivee: la ligne de la case d'arrivée
    :param col_arrivee: la colonne de la case d'arrivée
    :return: un boolean indiquant s'il existe un chemin entre la case de départ
              et la case d'arrivée
    """
    non_toutes_marquees = True
    calque = Matrice(get_nb_lignes(plateau),get_nb_colonnes(plateau),0)
    set_valeur(calque,lig_depart,col_depart,1)
    res = False
    while non_toutes_marquees and not res:
        non_toutes_marquees = marquage_direct(calque,plateau)
        if get_valeur(calque,lig_arrivee,col_arrivee) == 1:
            res = True
    return res
 

def peindre_direction_couleur(plateau, lig, col, direction, couleur, reserve_peinture, traverser_mur, tester=False):
    """
    Permet de peindre d'un couleur les cases accessibles à partir de lig,col dans la direction direction avec la reserve
    de peinture disponible.
    La fonction retourne la liste des joueurs touchés et le nombre de cases peintes.
    Attention si le paramètre tester est à True les cases ne sont pas réellement peintes (on teste juste combien de
    cases seraient peintes)

    :param plateau: un plateau
    :param lig: ligne de départ
    :param col: colonne de départ
    :param direction: un caractère valeur 'N','E','S' ou 'O'
    :param couleur: une couleur de peinture
    :param reserve_peinture: le nombre maximum de cases pouvant être peinte
    :param traverser_mur: booléen permettant de traverser une fois un mur (pouvoir du pistolet)
    :param tester: booléen indiquant si on souhaite vraiment peindre les cases ou juste tester combien on peut en peindre
    :return: un couple contenant la liste des joueurs touchés lors de l'action de peindre et le nombre de cases peintes
    """
    liste_joueurs_touches = list()
    if reserve_peinture > 0:
        #if get_couleur(get_valeur(plateau,lig,col)) != couleur: #######
        nb_cases_peintes = 1
        if not tester:
            set_couleur(get_valeur(plateau,lig,col),couleur)
        #else :
            #nb_cases_peintes = 0
            reserve_peinture -= 1
        liste_joueurs_touches.extend(get_liste_joueurs(get_valeur(plateau,lig,col)))
    else :
        nb_cases_peintes = 0
    passage = True
    utilisation_pouvoir_pistolet = 0
    case_actuelle = (lig,col) 
    while reserve_peinture > 0 and passage : #Tant qu'il y a de la peinture on que l'on peut passer
        case_arrivee = passage_plateau(plateau,case_actuelle[0],case_actuelle[1],direction)
        if case_arrivee is not None :
            passage = True
        elif traverser_mur and utilisation_pouvoir_pistolet == 0:
            utilisation_pouvoir_pistolet = 1
            if direction == "N":
                if case_actuelle[0]-1 >= 0 : #Il y a une carte au nord
                    case_arrivee = (case_actuelle[0]-1,case_actuelle[1])
                else :
                    passage = False
            elif direction == "E":
                if case_actuelle[1]+1 < get_nb_colonnes(plateau) : #Il y a une carte a l'est
                    case_arrivee = (case_actuelle[0],case_actuelle[1]+1)
                else :
                    passage = False
            elif direction == "S":
                if case_actuelle[0]+1 < get_nb_lignes(plateau) : #Il y a une carte au sud
                    case_arrivee = (case_actuelle[0]+1,case_actuelle[1])
                else :
                    passage = False
            elif direction == "O":
                if case_actuelle[1]-1 >= 0 : #Il y a une carte a l'ouest
                    case_arrivee = (case_actuelle[0],case_actuelle[1]-1)
                else :
                    passage = False
        else :
            passage = False
        if passage :
            liste_joueurs_touches.extend(get_liste_joueurs(get_valeur(plateau,case_arrivee[0],case_arrivee[1])))
            case_actuelle = case_arrivee
            #if get_couleur(get_valeur(plateau,case_arrivee[0],case_arrivee[1])) != couleur: #######
            nb_cases_peintes += 1
            if not tester:
                set_couleur(get_valeur(plateau,case_arrivee[0],case_actuelle[1]),couleur)
                reserve_peinture -= 1
    return (liste_joueurs_touches,nb_cases_peintes)

def nb_cartes_par_couleur(plateau):
    """
    calcule le nombre de cartes coloriées pour chaque couleur

    :param plateau: le plateau
    :return: un dictionnaire contenant pour chaque couleur présente sur le plateau le nombre de carte de cette couleur
    """
    nb_lignes = get_nb_lignes(plateau)
    nb_colonnes = get_nb_colonnes(plateau)
    dico_couleurs = dict()
    for i in range(nb_lignes):
        for j in range(nb_colonnes):                
            couleur = get_couleur(get_valeur(plateau,i,j))
            if couleur not in dico_couleurs.keys():
                dico_couleurs[couleur] = 0
            dico_couleurs[couleur] += 1
    dico_couleurs.pop("aucune")
    return dico_couleurs


def affiche_plateau(plateau):
    """
    affichage redimentaire d'un plateau

    :param plateau: le plateau
    :return: rien mais affiche le plateau
    """
    remplissage = ' ' * 30
    print(remplissage, end='')
    for i in range(1, 7, 2):
        print(" " + str(i), sep='', end='')
    print()
    for i in range(get_nb_lignes(plateau)):
        print(remplissage, end='')
        if i % 2 == 0:
            print(' ', sep='', end='')
        else:
            print(str(i), sep='', end='')
        for j in range(get_nb_colonnes(plateau)):
            print(to_char(get_valeur(plateau, i, j)), end='')
        if i % 2 == 0:
            print(' ', sep='', end='')
        else:
            print(str(i), sep='', end='')
        print()
    print(' ', sep='', end='')
    print(remplissage, end='')
    for i in range(1, 7, 2):
        print(" " + str(i), sep='', end='')
    print()

