# -*- coding: utf-8 -*-
"""
                           Projet Splaby'O
        Projet Python 2020-2021 de 1ere année et AS DUT Informatique Orléans

"""
from joueur import *
from random import randint


def Participants(noms_joueurs, liste_couleurs, humain=True):
    """
    crée une liste de participants dont les noms sont dans la liste de noms passés en paramètre.
    Les joueurs sont numéroté de 1 à N dans l'ordre de leur création
    Par défaut tous les joueurs sont des ordinateurs sauf le joueur 1 en fonction du paramètre humain
    Attention! il ne s'agit pas d'une simple liste de joueurs mais il faudra des informations permettant
    de gérer la notion de joueur courant, de tour de jeu (une fois que tous les joueurs ont joué)

    :param noms_joueurs: une liste de chaines de caractères
    :param liste_couleurs: la liste des couleurs choisis pour chaque joueur
    :param humain: un booléen indiquant si le joueur 1 est humain ou non
    :return: la structure que vous avez choisie pour représenter les participants
    """
    res = dict()
    res["liste_joueurs"] = list()
    res["joueur_courant"] = None
    res["premier_joueur"] = None
    if len(noms_joueurs) > 0:
        for i in range(len(noms_joueurs)):
            res["liste_joueurs"].append(Joueur(noms_joueurs[i],liste_couleurs[i]))
        if humain == True :
            set_type_joueur(res["liste_joueurs"][0],"H")
        res["joueur_courant"] = 0
        res["premier_joueur"] = 0
    return res

def ajouter_joueur(participants, joueur):
    """
    ajoute un nouveau joueur

    :param participants: les participants actuels
    :param joueur: le joueur à ajouter
    :return: cette fonction ne retourne rien mais modifie la liste des participants
    """
    participants["liste_joueurs"].append(joueur)
    if len(participants["liste_joueurs"]) > 4:
        participants["liste_joueurs"].pop(4)

    
def init_aleatoire_premier_joueur(participants):
    """
    tire au sort le premier joueur courant

    :param participants: les participants
    :return: la fonction ne retourne rien mais modifie la liste des participants
    """
    participants["premier_joueur"] = randint(0,len(participants)-1)
    participants["joueur_courant"] = participants["premier_joueur"]

def get_num_joueur_courant(participants):
    """
    retourne le numéro du joueur courant

    :param participants: les participants
    :return: un nombre entre 1 et 4 indiquant le numéro du joueur courant
    """

    res = participants["joueur_courant"] 
    return res

def get_num_premier_joueur(participants):
    """
    return le numéro du joueur qui a joué en premier

    :param participants: les participants
    :return: un nombre entre 1 et 4 indiquant le numéro du joueur qui a joué en premier
    """
    return participants["premier_joueur"]


def init_premier_joueur(participants, num_joueur):
    """
    initialialise le premier joueur courant au joueur qui porte le numéro num_joueur

    :param participants: les participants
    :param num_joueur: un nombre entre 1 et 4
    :return: rien cette fonction modifie la liste des participants
    """
    participants["premier_joueur"] = num_joueur

def set_joueur_courant(participants, num_joueur):
    """
    force le joueur courant au joueur qui porte le numéro num_joueur

    :param participants: les participants
    :param num_joueur: un nombre entre 1 et 4
    :return: rien cette fonction modifie la liste des participants
    """
    participants["joueur_courant"] = num_joueur

def changer_joueur_courant(participants):
    """
    passe au joueur suivant (change le joueur courant donc)

    :param participants: les participants
    :return: cette fonction modifie la liste des participants et
             retourne un booléen indiquant si on est revenu au joueur qui a commencé la partie
    """
    num_joueur_courant = get_num_joueur_courant(participants)
    num_joueur_courant += 1
    if num_joueur_courant == get_nb_joueurs(participants)+1:
        num_joueur_courant = 1
    set_joueur_courant(participants,num_joueur_courant)
    res = False
    if num_joueur_courant == get_num_premier_joueur(participants):
        res = True
    return res

def get_nb_joueurs(participants):
    """
    retourne le nombre de joueurs participant à la partie

    :param participants: les participants
    :return: le nombre de joueurs de la partie
    """
    return len(participants["liste_joueurs"])


def get_joueur_par_num(participants, num_joueur):
    """
    retourne le joueur de numéro num_joueur

    :param participants: les participants
    :param num_joueur: le numéro du joueur souhaité
    :return: le joueur qui porte le numéro indiqué
    """
    if num_joueur < get_nb_joueurs(participants)+1:
        return participants["liste_joueurs"][num_joueur-1]


def get_joueur_par_nom(participants, nom_joueur):
    """
    retourne le joueur de nom nom_joueur

    :param participants: les participants
    :param nom_joueur: le nom du joueur souhaité
    :return: le joueur qui porte le nom indiqué, None si aucun joueur ne porte ce nom
    """
    res = None
    for i in range(get_nb_joueurs(participants)):
        if participants["liste_joueurs"][i]["nom"] == nom_joueur :
            res = participants["liste_joueurs"][i]
    return res


def get_joueur_courant(participants):
    """
    retourne le joueur courant

    :param participants: les participants
    :return: cette fonction ne retourne rien mais modifie la liste des participants
    """
    return participants["liste_joueurs"][participants["joueur_courant"]-1]


def mise_a_jour_surface(participants, couverture):
    """
    permet de mettre à jour la surface couverte par chaque joueur

    :param participants: les participants
    :param couverture: un dictionnaire qui indique pour chaque couleur le nombre de cases de cette couleur
    :return: cette fonction ne retourne rien mais modifie la liste des participants
    """
    for i in range(get_nb_joueurs(participants)):
        if participants["liste_joueurs"][i]["couleur"] in couverture.keys():
            participants["liste_joueurs"][i]["surface"] = couverture[participants["liste_joueurs"][i]["couleur"]]

    
def classement_joueurs(participants):
    """
    Retourne une liste de participants triée suivant les critètres 1) la surface couverte 2) la réserve d'encre

    :param participants: les participants
    :return: une liste de joueurs triée dans l'ordre décroissant
    """
    res = list()
    nb_joueurs = get_nb_joueurs(participants)
    for i1 in range(get_nb_joueurs(participants)):
        somme = 0
        for i2 in range(get_nb_joueurs(participants)):
            if i1 != i2 :
                if comparer(participants["liste_joueurs"][i1],participants["liste_joueurs"][i2]) == 1 :
                    somme += 1
        res.insert(nb_joueurs-somme-1,participants["liste_joueurs"][i1])
    return res


