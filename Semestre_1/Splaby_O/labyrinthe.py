# -*- coding: utf-8 -*-
"""
                           Projet Splaby'O
        Projet Python 2020-2021 de 1ere année et AS DUT Informatique Orléans

"""
from plateau import *

# dictionnaire qui gère le nombre d'unités de peinture gagnées ou perdues en fonction des actions
POINTS = {"objet": 5, "couleur_joueur": 2, "couleur_neutre": -1, "couleur_adversaire": -2, "peindre_adversaire": 3,
          "ordre_errone": -1, }
# constantes indiquant les différents objets disponibles
BOMBE = 1
PISTOLET = 2
BOUCLIER = 3


def Labyrinthe(noms_joueurs, couleurs_joueurs, humain=False, nb_tours=100, duree_objet=10):
    """
    permet de créer un labyrinthe avec comme participants les joueurs dont les noms et les couleurs
    sont donnés sous forme de liste. Seul le joueur 1 peut être humain, les autre sont automatiques.
    un joueur courant est choisi aléatoirement.

 

    :param noms_joueurs: la liste des noms des joueurs participant à la partie
    :param couleurs_joueurs: la liste des couleurs choisies par chacun des joueurs
    :param humain: un caractère valant 'H' ou 'O' qui indique si le joueur 1 est humain ou ordinateur
    :param nb_tours: un entier indiquant le nombre de tours de la partie
    :param duree_objet: un entier indiquant la durée de validité d'un objet pris par un joueur
    :return: le labyrinthe crée
    """
    res = dict()
    res["les_joueurs"] = Participants(noms_joueurs,couleurs_joueurs,humain)
    res["plateau"] , res["carte_sup"] = Plateau(res["les_joueurs"]["liste_joueurs"])
    res["nb_tours"] = nb_tours
    res["rangee_interdite"] = None
    res["direction_interdite"] = None
    res["duree_objet"] = duree_objet
    return res

    

def initialiser_labyrinthe(participants, plateau, carte_supplementaire, nb_tours, rangee_interdite, direction_interdite,
                           duree_objet):
    """
    Cette fonction permet d'initialiser un labyrinthe avec des valeurs bien précises (permettant de charger une partie
    en cours)

    :param participants: les participants à la partie
    :param plateau: le plateau dans son état actuel
    :param carte_supplementaire: la carte amovible qui est à l'extérieur du plateau
    :param nb_tours: le nombre de tours restants
    :param rangee_interdite: la rangée interdite pour la carte amovible
    :param direction_interdite: la colonne interdite pour la carte amovible
    :param duree_objet: la durée de vie des objets possédés par un joueur
    :return: retourne le labyrinthe avec les caractéristiques passées en paramètre
    """
    res = dict()
    res["les_joueurs"] = participants
    res["plateau"] = plateau
    res["carte_sup"] = carte_supplementaire
    res["nb_tours"] = nb_tours
    res["rangee_interdite"] = rangee_interdite
    res["direction_interdite"] = direction_interdite
    res["duree_objet"] = duree_objet
    return res


def get_nb_tours_restants(labyrinthe):
    """
    donne le nombre de tours restants dans la partie

    :param labyrinthe: un labyrinthe
    :return: un entier donnant le nombre de tours restants
    """
    return labyrinthe["nb_tours"]


def get_plateau(labyrinthe):
    """
    retourne la matrice représentant le plateau de jeu

    :param labyrinthe: labyrinthe le labyrinthe considéré
    :return: la matrice représentant le plateau de ce labyrinthe
    """
    return labyrinthe["plateau"]


def get_coordonnees_joueur_courant(labyrinthe):
    """
    donne les coordonnées du joueur courant sur le plateau

    :param labyrinthe: le labyrinthe considéré
    :return: les coordonnées du joueur courant ou None si celui-ci n'est pas sur le plateau
    """
    return get_coordonnees_joueur(get_plateau(labyrinthe),get_joueur_courant(get_participants(labyrinthe)))


def prendre_joueur_courant(labyrinthe, lig, col):
    """
    enlève le joueur courant de la carte qui se trouve sur la case lin,col du plateau
    si le joueur ne s'y trouve pas la fonction ne fait rien

    :param labyrinthe: le labyrinthe considéré
    :param lig: la ligne où se trouve la carte
    :param col: la colonne où se trouve la carte
    :return: la fonction ne retourne rien mais modifie le labyrinthe
    """
    prendre_joueur(get_valeur(get_plateau(labyrinthe),lig,col),get_joueur_courant(get_participants(labyrinthe)))


def poser_joueur_courant(labyrinthe, lin, col):
    """
    pose le joueur courant sur la case lin,col du plateau

    :param labyrinthe: le labyrinthe considéré
    :param lig: la ligne où se trouve la carte
    :param col: la colonne où se trouve la carte
    :return: la fonction ne retourne rien mais modifie le labyrinthe
    """
    poser_joueur(get_valeur(get_plateau(labyrinthe),lig,col),get_joueur_courant(get_participants(labyrinthe)))


def get_participants(labyrinthe):
    """
    retourne la liste des participants (structure créée dans participants.py

    :param labyrinthe: le labyrinthe considéré
    :return: les joueurs sous la forme de la structure implémentée dans participants.py
    """
    return labyrinthe["les_joueurs"]


def get_carte_a_jouer(labyrinthe):
    """
    donne la carte à jouer celle qui est hors du plateau

    :param labyrinthe: le labyrinthe considéré
    :return: la carte à jouer
    """
    return labyrinthe["carte_sup"]


def get_duree_objet(labyrinthe):
    """
    permet de connaitre la durée de validité des objets quand ils sont pris par un joueur

    :param labyrinthe: le labyrinthe considéré
    :return: un entier indiquant la durée de validité des objets
    """
    return labyrinthe["duree_objet"]


def get_rangee_interdite(labyrinthe):
    """
    retourne le numéro de la rangée interdite

    :param labyrinthe: le labyrinthe considéré
    :return: un entier indiquant la rangée interdite
    """
    return labyrinthe["rangee_interdite"]


def get_direction_interdite(labyrinthe):
    """
    retourne le numéro de la direction interdite

    :param labyrinthe: le labyrinthe considéré
    :return: un entier indiquant la direction interdite
    """
    return labyrinthe["direction_interdite"]


def coup_interdit(labyrinthe, direction, rangee):
    """
    retourne True si le coup proposé correspond au coup interdit elle retourne False sinon

    :param labyrinthe: le labyrinthe considéré
    :param direction: un caractère 'N', 'E', 'S' ou 'O' indiquant la direction choisie
    :param rangee: un entier indiquant la colonne ou la ligne choisie
    :return: un booléen indiquant si le coup est interdit ou non
    """
    return ( rangee == get_rangee_interdite(labyrinthe) and direction == get_direction_interdite(labyrinthe) )


def lave_et_transfert_joueurs(carte_a_jouer, carte_inseree):
    """
    Permet de laver la carte à jouer (enlever la couleur) et de transférer la liste des joueurs
    qui se trouvent sur la carte_a_jouer vers la carte_inseree

    :param carte_a_jouer: la carte qui vient d'être expulsée du plateau
    :param carte_inseree: la carte qui vient d'être remuise sur le plateau
    :return: cette fonction ne retourne rien
    """
    set_liste_joueurs(carte_inseree,get_liste_joueurs(carte_a_jouer))
    set_couleur(carte_a_jouer,"aucune")
    set_liste_joueurs(carte_a_jouer,list())

#########################################
# A eclaircir
#########################################
def jouer_carte(labyrinthe, direction, rangee):
    """
    fonction qui joue la carte amovible dans la direction et sur la rangée passées
    en paramètres. Cette fonction
       - met à jour le plateau du labyrinthe
       - met à jour la carte à jouer
       - met à jour la nouvelle direction interdite

    :param labyrinthe: le labyrinthe
    :param direction: un caractère qui indique la direction choisie ('N','S','E','O')
    :param rangee:  le numéro de la ligne ou de la colonne choisie
    :return: None si tout s'est bien passé ou une chaine de caractères indiquants le problème survenu.
    Voici les messages possibles
        * Carte insérée!
        * Rangée invalide
        * Direction invalide
        * Coup interdit
    """
    message = None
    if coup_interdit(labyrinthe,direction,rangee):
        message = "Coup interdit"
        if rangee not in [1,3,5]:
            message = "Rangée invalide"
        elif direction not in {"N","E","S","O"}:
            message = "Direction invalide"
    else :
        if direction == "N":
            i_a_jouer = 6
            j_a_jouer = rangee
            i_inseree = 0
            j_inseree = rangee
        elif direction == "E":
            i_a_jouer = rangee
            j_a_jouer = 0
            i_inseree = rangee
            j_inseree = 6
        elif direction == "S":
            i_a_jouer = 0
            j_a_jouer = rangee
            i_inseree = 6
            j_inseree = rangee
        elif direction == "O":
            i_a_jouer = rangee
            j_a_jouer = 6
            i_inseree = rangee
            j_inseree = 0
        carte_a_jouer = get_valeur(get_plateau(labyrinthe),i_a_jouer,j_a_jouer)
        carte_inseree = get_carte_a_jouer(labyrinthe)
        lave_et_transfert_joueurs(carte_a_jouer,carte_inseree)
        if direction == "N":
            decalage_colonne_en_bas(get_plateau(labyrinthe),rangee,carte_inseree)
        elif direction == "E":
            decalage_ligne_a_gauche(get_plateau(labyrinthe),rangee,carte_inseree)
        elif direction == "S":
            decalage_colonne_en_haut(get_plateau(labyrinthe),rangee,carte_inseree)
        elif direction == "O":
            decalage_ligne_a_droite(get_plateau(labyrinthe),rangee,carte_inseree)
        message = "Carte inserée!"
        labyrinthe["rangee_interdite"] = rangee
        labyrinthe["direction_interdite"] = direction
        labyrinthe["carte_sup"] = carte_a_jouer
    return message

def tourner_carte(labyrinthe, sens='H'):
    """
    Tourne la carte à jouer dans le sens indiqué en paramètre (H horaire A antihoraire)

    :param labyrinthe:  le labyrinthe considéré
    :param sens: un caractère indiquant le sens dans lequel tourner la carte ('A' ou 'H')
    :return: un chaine de caractères indiquant ce qui s'est passé
        * Carte tournée!
        * Ordre pour tourner la carte inconnu
    """
    message = "Carte tournée!"
    if sens == "H":
        tourner_horaire(get_carte_a_jouer(labyrinthe))
    elif sens == "A":
        tourner_antihoraire(get_carte_a_jouer(labyrinthe))
    else :
        message = "Ordre pour tourner la carte inconnu"
    return message

def peindre(labyrinthe, direction):
    """
    Permet de peindre de la couleur du joueur courant toutes les cases atteignables dans la direction choisie.
    Si direction vaut 'X' la fonction ne fait rien (le joueur courant ne veut pas peindre)
    Cette fonction va donc
        * peindre toutes les cartes atteignables à partir de la position du joueur courant dans la direction choisie
        * enlever au joueur courant le nombre d'unités de peinture correspond au nombre de cases peintes
        * enlever aux joueurs ne possedant pas de bouclier et touchés par le jet de peinture le nombre d'unités
          prévu par POINTS["peindre_adversaire"]
        * ajouter au joueur courant le nombre d'unités de peinture enlevés aux adversaires
    De plus si le joueur courant possède un pistolet son jet de peinture doit traverser un mur et si il possède une
    bombe le jet de peinture doit aller dans toutes les directions en commençant par direction et en allant dans le
    sens des aiguilles d'une montre

    :param labyrinthe: le labyrinthe considéré
    :param direction: un caractère qui indique la direction choisie ('X', 'N','S','E','O')
    :return: un chaine de caractères indiquant ce qui s'est passé
        * Le joueur ne veut pas peindre
        * Le joueur a peint dans la direction ...
        * Direction inconnue
    """
    if direction == "X":
        message = "Le joueur ne veut pas peindre"
    elif direction not in {"N","E","S","O"}:
        message = "Direction inconnue"
    else :
        objet = get_objet_joueur(get_joueur_courant(get_participants(labyrinthe)))
        traverser_mur = False
        if objet == PISTOLET:
            traverser_mur = True
        joueurs_touches , nb_cases_peintes = peindre_direction_couleur(get_plateau(labyrinthe),
                                                                        get_coordonnees_joueur_courant(labyrinthe)[0],
                                                                        get_coordonnees_joueur_courant(labyrinthe)[1],
                                                                        direction,
                                                                        get_couleur_joueur(get_joueur_courant(get_participants(labyrinthe))),
                                                                        get_reserve_peinture(get_joueur_courant(get_participants(labyrinthe))),
                                                                        traverser_mur
        )
        ajouter_peinture(get_joueur_courant(get_participants(labyrinthe)),-nb_cases_peintes)
        if get_joueur_courant(get_participants(labyrinthe)) in joueurs_touches:
            joueurs_touches.remove(get_joueur_courant(get_participants(labyrinthe)))
        for joueur in joueurs_touches:
            if get_objet_joueur(joueur) != BOUCLIER:
                peinture_perdue = get_reserve_peinture(joueur)
                ajouter_peinture(joueur,-POINTS["peindre_adversaire"])
                peinture_perdue -= get_reserve_peinture(joueur)
                ajouter_peinture(get_joueur_courant(get_participants(labyrinthe)),peinture_perdue)
        if get_objet_joueur(get_joueur_courant(get_participants(labyrinthe))) == BOMBE:
            ajouter_objet(get_joueur_courant(get_participants(labyrinthe)),0)
            liste_dir = ["N","E","S","O","N","E","S"]
            i_debut = liste_dir.index(direction)
            for dir_ in liste_dir[i_debut+1:i_debut+4]:
                peindre(labyrinthe,dir_)
            ajouter_objet(get_joueur_courant(get_participants(labyrinthe)),BOMBE)
        dir_to_char = {"N":"Nord","E":"Est","S":"Sud","O":"Ouest"}
        message = "Le joueur a peint dans la direction " + dir_to_char[direction]
    return message

def deplacer(labyrinthe, direction):
    """
    Déplace le joueur courant dans la direction souhaitée si c'est possible. Cette fonction va notamment:
    * verifier que le déplacement est possible
    * déplacer le joueur
    * donner au joueur courant l'objet qu'il y a sur la carte d'arrivée (si elle possède un objet) en augmentant
      la réserve de peinture du jouieur de POINTS["objet"]
    * mettre à jour la réserve de peinture du joueur en fonction de la couleur de la case d'arrivée
      (POINTS["couleur_joueur"], POINTS["couleur_neutre"] ou POINTS["couleur_adversaire"]
    Si le joueur donne un ordre erroné ou un déplacement impossible il perd POINTS["ordre_errone"] unité de peinture

    :param labyrinthe: le labyrinthe considéré
    :param direction: un caractère qui indique la direction choisie ('N','S','E','O')
    :return: un chaine de caractères indiquant ce qui s'est passé
        * La direction est inconnue
        * Le déplacement est impossible
        * Le joueur s'est déplacé vers ...
        * Le joueur s'est déplacé vers ... et un trouvé un objet
    """
    if direction not in {"N","E","S","O"}:
        message = "Direction inconnue"
        ajouter_peinture(get_joueur_courant(get_participants(labyrinthe)),POINTS["ordre_errone"])
    else :
        coords = get_coordonnees_joueur(get_plateau(labyrinthe),get_joueur_courant(get_participants(labyrinthe)))
        if coords is not None :
            i , j = coords
            if passage_plateau(get_plateau(labyrinthe),i,j,direction):
                prendre_joueur(get_valeur(get_plateau(labyrinthe),i,j),get_joueur_courant(get_participants(labyrinthe)))
                i_arrivee = i
                j_arrivee = j
                if direction == "N":
                    i_arrivee -= 1
                elif direction == "E":
                    j_arrivee += 1
                elif direction == "S":
                    i_arrivee += 1
                elif direction == "O":
                    j_arrivee -= 1
                poser_joueur(get_valeur(get_plateau(labyrinthe),i_arrivee,j_arrivee),get_joueur_courant(get_participants(labyrinthe)))
                dir_to_char = {"N":"le Nord","E":"l'est","S":"le Sud","O":"l'Ouest"}
                message = "Le joueur s'est déplacer vers " + dir_to_char[direction]
                if get_objet(get_valeur(get_plateau(labyrinthe),i_arrivee,j_arrivee)) != 0:
                    ajouter_objet(get_joueur_courant(get_participants(labyrinthe)),get_objet(get_valeur(get_plateau(labyrinthe),i_arrivee,j_arrivee)))
                    prendre_objet(get_valeur(get_plateau(labyrinthe),i_arrivee,j_arrivee))
                    message += " et a trouvé un objet"
                    ajouter_peinture(get_joueur_courant(get_participants(labyrinthe)),POINTS["objet"])
                couleur_arrivee = get_couleur(get_valeur(get_plateau(labyrinthe),i_arrivee,j_arrivee))
                if couleur_arrivee == get_couleur_joueur(get_joueur_courant(get_participants(labyrinthe))):
                    ajouter_peinture(get_joueur_courant(get_participants(labyrinthe)),POINTS["couleur_joueur"])
                elif couleur_arrivee == "aucune":
                    ajouter_peinture(get_joueur_courant(get_participants(labyrinthe)),POINTS["couleur_neutre"])
                else:
                    ajouter_peinture(get_joueur_courant(get_participants(labyrinthe)),POINTS["couleur_adversaire"])
            else :
                message = "Le déplacement est impossible"
                ajouter_peinture(get_joueur_courant(get_participants(labyrinthe)),POINTS["ordre_errone"])
        else :
            message = "Le déplacement est impossible"
            ajouter_peinture(get_joueur_courant(get_participants(labyrinthe)),POINTS["ordre_errone"])
    return message


def interpreter_ordre(labyrinthe, ordre):
    """
    Cette fonction executes les ordres fournie sous la forme d'une chaine de caractères commençant par l'ordre de
    peinture puis l'ordre de déplacement ou l'ordre de modification du labyrinthe. Tous les ordres commencent par un P
    suivi d'une des lettre X N O S ou E indiquant la direction où peindre (X indiquant que le joueur ne souhaite pas
    peindre) . La seconde partie de l'ordre est
        * soit un D (pour déplacement) suivi d'une des lettres N O S ou E
        * soit un T (pour tourner) suivi d'une suite de H ou de A suivi une des lettres N O S ou E suivi d'un chiffre
    Par exemple:
        * "PODE" indique que le joueur peint vers l'ouest et se déplace vers l'est
        * "PXTAAN3" indique que le joueur ne souhaite pas peindre et qu'il tourne la carte amovible deux fois dans le
            sens antihoraire puis insère la carte amovible au nord dans la colonne 3

    :param labyrinthe: le labyrinthe considéré
    :param ordre: un chaine de caractère comme indiquée ci-dessus
    :return: une chaine de caractères indiquant ce qui s'est passé
    """
    message = peindre(labyrinthe,ordre[1])
    if ordre[2] == "D":
        message += "\n" + deplacer(labyrinthe,ordre[3])
    else :
        i = 3
        while ordre[i] in {"A","H"}:
            message += "\n" + tourner_carte(labyrinthe,ordre[i])
            i += 1
        message += "\n" + jouer_carte(labyrinthe,ordre[i],int(ordre[i+1]))
    print(message)
    return message

    
def finir_tour(labyrinthe):
    """
     met a jour les différentes informations sur les joueurs
     * le temps restant de l'objet du joueur courant
     * la surface couverte par chaque joueur
     change le joueur courant (et met à jour le compteur de tour si nécessaire
    vérifie si la partie est terminée, si ce n'est pas le cas passe au joueur suivant

    :param labyrinthe: labyrinthe considéré
    :return: Cette fonction retourne rien mais elle modifie le labyrinthe
    """
    mise_a_jour_temps(get_joueur_courant(get_participants(labyrinthe)))
    mise_a_jour_surface(get_participants(labyrinthe),nb_cartes_par_couleur(get_plateau(labyrinthe)))
    changer_joueur_courant(get_participants(labyrinthe))
    if get_joueur_courant(get_participants(labyrinthe)) == get_joueur_par_num(get_participants(labyrinthe),get_num_premier_joueur(get_participants(labyrinthe))):
        if get_nb_tours_restants(labyrinthe) > 0:
            labyrinthe["nb_tours"] -= 1

###################################################
### Fonctions utilitaires qui permettent de transmettre l'état du labyrinthe à une intelligence artificielle
###################################################

def joueur_2_dico(joueur):
    return {
        "nom": get_nom_joueur(joueur), "objet": get_objet_joueur(joueur), "couleur": get_couleur_joueur(joueur),
        "reserve_peinture": get_reserve_peinture(joueur), "surface": get_surface(joueur),
        "type_joueur": get_type_joueur(joueur), "temps_restant": get_temps_restant(joueur)
    }


def participants_2_dico(participants):
    nb_participants = get_nb_joueurs(participants)
    liste_joueurs = [joueur_2_dico(get_joueur_par_num(participants, i)) for i in range(1, nb_participants + 1)]
    return {"liste_joueurs": liste_joueurs, "joueur_courant": get_num_joueur_courant(participants),
            "premier_joueur": get_num_premier_joueur(participants)
            }


def carte_2_dico(carte):
    return {"murs": coder_murs(carte), "pions": [get_nom_joueur(joueur) for joueur in get_liste_joueurs(carte)],
            "objet": get_objet(carte), "couleur": get_couleur(carte)}


def matrice_2_dico(matrice):
    nb_lig = get_nb_lignes(matrice)
    nb_col = get_nb_colonnes(matrice)
    res = {"nb_lignes": nb_lig, "nb_colonnes": nb_col,
           "les_valeurs": [carte_2_dico(get_valeur(matrice, i, j)) for i in range(nb_lig) for j in range(nb_col)]}
    return res


def labyrinthe_2_dico(labyrinthe):
    return {
        "les_joueurs": participants_2_dico(get_participants(labyrinthe)),
        "plateau": matrice_2_dico(get_plateau(labyrinthe)),
        "carte": carte_2_dico(get_carte_a_jouer(labyrinthe)),
        "nb_tours": get_nb_tours_restants(labyrinthe),
        "rangee_interdite": get_rangee_interdite(labyrinthe),
        "direction_interdite": get_direction_interdite(labyrinthe),
        "duree_objet": get_duree_objet(labyrinthe)
    }


def labyrinthe_from_dico(dico_lab):
    """
    oui
    """
    participants = Participants([], [])
    for dico_joueur in dico_lab["les_joueurs"]["liste_joueurs"]:
        joueur = Joueur(dico_joueur["nom"], dico_joueur["couleur"], dico_joueur["reserve_peinture"],
                        dico_joueur["surface"], dico_joueur["type_joueur"],dico_joueur["objet"],
                        dico_joueur["temps_restant"])
        ajouter_joueur(participants, joueur)
    set_joueur_courant(participants, dico_lab["les_joueurs"]["joueur_courant"])
    init_premier_joueur(participants, dico_lab["les_joueurs"]["premier_joueur"])
    nb_lig = dico_lab["plateau"]["nb_lignes"]
    nb_col = dico_lab["plateau"]["nb_colonnes"]
    plateau = Matrice(nb_lig, nb_col)
    for i in range(nb_lig * nb_col):
        carte_dico = dico_lab["plateau"]["les_valeurs"][i]
        carte = Carte(True, True, True, True)
        decoder_murs(carte, carte_dico["murs"])
        poser_objet(carte, carte_dico["objet"])
        set_couleur(carte, carte_dico["couleur"])
        pions = [get_joueur_par_nom(participants, nom) for nom in carte_dico["pions"]]
        set_liste_joueurs(carte, pions)
        set_valeur(plateau,i//nb_col,i%nb_col,carte)
    carte = Carte(True, True, True, True)
    decoder_murs(carte, dico_lab["carte"]["murs"])
    poser_objet(carte, dico_lab["carte"]["objet"])
    return initialiser_labyrinthe(participants, plateau, carte, dico_lab["nb_tours"], dico_lab["rangee_interdite"],
                                  dico_lab["direction_interdite"], dico_lab["duree_objet"])


