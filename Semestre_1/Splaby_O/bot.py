from labyrinthe import *

def calculer_action(labyrinthe_dico):
    """
    :param labyrinthe_dico: un dictionnaire qui permet de reconstruire le jeu suivant votre représentation
           grâce à la fonction labyrinthe_from_dico
    :return: un ordre sous la forme d'une chaîne de caractères (voir docstring de interpreter_ordre)
    """
    laby = labyrinthe_from_dico(labyrinthe_dico)  # récupération du labyrinthe
    moi = get_joueur_courant(get_participants(laby))  # le joueur qui joue cette IA
    res = ""
    lignes_autorisees = [-1,1,3,5,7]
    colonnes_autorisees = [-1,1,3,5,7]
    ################################################################################
    # Etat des lieux
    ################################################################################
    #PLATEAU
    plateau = get_plateau(laby)
    #MA_POS_X
    ma_pos_x = get_coordonnees_joueur(plateau,moi)[0]
    #MA_POS_Y
    ma_pos_y = get_coordonnees_joueur(plateau,moi)[1]
    #NB_CASES_PAR_JOUEUR
    nb_cases_par_joueur = nb_cartes_par_couleur(plateau)
    #MA_COULEUR
    ma_couleur = get_couleur_joueur(moi)
    #MA_RESERVE
    ma_reserve = get_reserve_peinture(moi)
    #POSSEDE PISTOLET
    possede_pistolet = False
    if get_objet_joueur(moi) == PISTOLET:
        possede_pistolet = True
    #POSSEDE BOUCLIER
    possede_bouclier = False
    if get_objet_joueur(moi) == BOUCLIER:
        possede_bouclier = True
    #POSSEDE BOMBE
    possede_bombe = False
    if get_objet_joueur(moi) == BOMBE:
        possede_bombe = True
    #RANGEE INTERDITE
    rangee_interdite = get_rangee_interdite(laby)
    #DIRECTION INTERDITE
    direction_interdite = get_direction_interdite(laby)
    #Ligne/Colonne interdite
    if direction_interdite in {"N","S"}:
        colonnes_autorisees.remove(rangee_interdite)
    elif direction_interdite in {"E","O"}:
        lignes_autorisees.remove(rangee_interdite)
    ########################################
    # Position objets
    ########################################
    positions_objets = list()
    for i in range(get_nb_lignes(plateau)):
        for j in range(get_nb_colonnes(plateau)):
            if get_objet(get_valeur(plateau,i,j)) != 0:
                positions_objets.append((i,j))
    ########################################
    # Possibilités peinture
    ########################################
    etat_peinture = dict()
    for direction in ["N","E","S","O"]:
        joueurs_touches , nb_cases_peintes = peindre_direction_couleur(plateau,
                                                            ma_pos_x,
                                                            ma_pos_y,
                                                            direction,
                                                            ma_couleur,
                                                            ma_reserve,
                                                            possede_pistolet,
                                                            tester=True
                                                            )
        peinture_restante = ma_reserve - nb_cases_peintes
        if peinture_restante > 0: #Quantité de peinture à ne pas franchir à définir
            etat_peinture[direction] = {"joueurs_touches":joueurs_touches , "nb_joueurs_touches":len(joueurs_touches) , "nb_cases_peintes":nb_cases_peintes , "peinture_restante":peinture_restante}
    ########################################
    # Test passage
    ########################################
    etat_passage = dict()
    for direction in ["N","E","S","O"]:
        etat_passage[direction] = passage_plateau(plateau,ma_pos_x,ma_pos_y,direction)
    ########################################
    # Direction par couleur
    ########################################
    direction_par_couleur = {"ma_couleur":list(),"aucune":list(),"adversaire":list()}
    for direction in etat_passage:
        if etat_passage[direction] is not None :
            couleur_case = get_couleur(get_valeur(plateau,etat_passage[direction][0],etat_passage[direction][1]))
            if couleur_case == ma_couleur:
                direction_par_couleur["ma_couleur"].append(direction)
            elif couleur_case == "aucune":
                direction_par_couleur["aucune"].append(direction)
            else :
                direction_par_couleur["adversaire"].append(direction)
    ########################################
    # Objets accessibles
    ########################################
    positions_objets_accessibles = list()
    for i , j in positions_objets:
        if accessible(plateau , ma_pos_x , ma_pos_y , i , j):
            positions_objets_accessibles.append((i,j))
    ########################################
    # Situation adversaire
    ########################################
    # Potentielle erreure (changement de joueur bloqué)
    joueurs_a_attaquer = list()
    for i in range(1,get_nb_joueurs(get_participants(laby))+1):
        if i != get_num_joueur_courant(get_participants(laby)):
            joueur = get_joueur_par_num(get_participants(laby),i)
            joueur_pos_x = get_coordonnees_joueur(plateau,joueur)[0]
            joueur_pos_y = get_coordonnees_joueur(plateau,joueur)[1]
            etat_passage_joueur = dict()
            for direction in ["N","S","E","O"]:
                etat_passage_joueur[direction] = passage_plateau(plateau,joueur_pos_x,joueur_pos_y,direction)
            compteur = 0
            directions_accessibles_joueur = list()
            for direction in etat_passage_joueur.keys():
                if etat_passage_joueur[direction] is None:
                    compteur += 1
                else :
                    directions_accessibles_joueur.append(direction)
            #Si le joueur ne peut pas bouger, on ne bougera pas les rangees adjacentes
            commande = {"joueur":joueur,"rangee":None,"direction":None,"mur":None}
            if compteur == 4:
                if joueur_pos_x - 1 in lignes_autorisees:
                    lignes_autorisees.remove(joueur_pos_x - 1)
                if joueur_pos_x in lignes_autorisees:
                    lignes_autorisees.remove(joueur_pos_x)
                if joueur_pos_x + 1 in lignes_autorisees:
                    lignes_autorisees.remove(joueur_pos_x + 1)
                if joueur_pos_y + 1 in colonnes_autorisees:
                    colonnes_autorisees.remove(joueur_pos_y + 1)
                if joueur_pos_y in colonnes_autorisees:
                    colonnes_autorisees.remove(joueur_pos_y)
                if joueur_pos_y - 1 in colonnes_autorisees:
                    colonnes_autorisees.remove(joueur_pos_y - 1)
            #Si le joueur n'a qu'un coté par où partir, on bougera cette rangee si elle existe
            elif compteur == 3:
                rangee_a_bouger_joueur = None
                direction_a_bouger_joueur = None
                direction_mur = None
                if directions_accessibles_joueur[0] == "N":
                    if joueur_pos_x - 1 in lignes_autorisees:
                        rangee_a_bouger_joueur = joueur_pos_x - 1
                        if joueur_pos_y + 1  ==  7:
                            direction_a_bouger_joueur = "E"
                            direction_mur = "S"
                        elif joueur_pos_y - 1  ==  -1:
                            direction_a_bouger_joueur = "O"
                            direction_mur = "S"
                        elif mur_sud(get_valeur(plateau,rangee_a_bouger_joueur,joueur_pos_y + 1)):
                            direction_a_bouger_joueur = "E"
                        elif mur_sud(get_valeur(plateau,rangee_a_bouger_joueur,joueur_pos_y - 1)):
                            direction_a_bouger_joueur = "O"
                elif directions_accessibles_joueur[0] == "E":
                    if joueur_pos_y + 1 in colonnes_autorisees:
                        rangee_a_bouger_joueur = joueur_pos_y + 1
                        if joueur_pos_x + 1  ==  7:
                            direction_a_bouger_joueur = "N"
                            direction_mur = "O"
                        elif joueur_pos_x - 1  ==  -1:
                            direction_a_bouger_joueur = "S"
                            direction_mur = "O"
                        elif mur_ouest(get_valeur(plateau,joueur_pos_x + 1,rangee_a_bouger_joueur)):
                            direction_a_bouger_joueur = "N"
                        elif mur_ouest(get_valeur(plateau,joueur_pos_x - 1,rangee_a_bouger_joueur)):
                            direction_a_bouger_joueur = "S"
                elif directions_accessibles_joueur[0] == "S":
                    if joueur_pos_x + 1 in lignes_autorisees:
                        rangee_a_bouger_joueur = joueur_pos_x + 1
                        if joueur_pos_y + 1  ==  7:
                            direction_a_bouger_joueur = "E"
                            direction_mur = "N"
                        elif joueur_pos_y - 1  ==  -1:
                            direction_a_bouger_joueur = "O"
                            direction_mur = "N"
                        elif mur_nord(get_valeur(plateau,rangee_a_bouger_joueur,joueur_pos_y + 1)):
                            direction_a_bouger_joueur = "E"
                        elif mur_nord(get_valeur(plateau,rangee_a_bouger_joueur,joueur_pos_y - 1)):
                            direction_a_bouger_joueur = "O"
                elif directions_accessibles_joueur[0] == "O":
                    if joueur_pos_y - 1 in colonnes_autorisees:
                        rangee_a_bouger_joueur = joueur_pos_y - 1
                        if joueur_pos_x + 1  ==  7:
                            direction_a_bouger_joueur = "N"
                            direction_mur = "E"
                        elif joueur_pos_x - 1  ==  -1:
                            direction_a_bouger_joueur = "S"
                            direction_mur = "E"
                        elif mur_est(get_valeur(plateau,joueur_pos_x + 1,rangee_a_bouger_joueur)):
                            direction_a_bouger_joueur = "N"
                        elif mur_est(get_valeur(plateau,joueur_pos_x - 1,rangee_a_bouger_joueur)):
                            direction_a_bouger_joueur = "S"
                commande = {"joueur":joueur,"rangee":rangee_a_bouger_joueur,"direction":direction_a_bouger_joueur,"mur":direction_mur}
            if commande["rangee"] is not None and commande["direction"] is not None:
                joueurs_a_attaquer.append(commande)
    def get_score(commande):
        return commande["joueur"]["surface"]
    a_attaquer = None
    if len(joueurs_a_attaquer) > 0:
        a_attaquer = sorted(joueurs_a_attaquer, key=get_score, reverse=True)[0]
    ################################################################################
    # Choix peindre
    ################################################################################
    def le_plus_de_joueurs_touches(etat_peinture):
        """
        param: etat_peinture : un dictionnaire représentant les différentes directions éligible pour peindre
        :return: la liste des meilleures directions ou None s'il n'y en a pas
        """
        res = None
        if len(etat_peinture) != 0:
            dict_freq_joueurs_touches = dict()
            for direction in etat_peinture.keys():
                nb_joueurs_touches = etat_peinture[direction]["nb_joueurs_touches"]
                if nb_joueurs_touches not in dict_freq_joueurs_touches.keys():
                    dict_freq_joueurs_touches[nb_joueurs_touches] = list()
                dict_freq_joueurs_touches[nb_joueurs_touches].append(direction)
            res = dict_freq_joueurs_touches[sorted(dict_freq_joueurs_touches, reverse=True)[0]]
        return res
    def actualiser_dictionnaire(etat_peinture,liste_directions):
        """
        param: etat_peinture : un dictionnaire représentant les différentes directions éligible pour peindre
        param: liste_directions : la liste des directions à garder
        :return: un nouveau dictionnaire d'état de la peinture
        """
        res = dict()
        for direction in liste_directions:
            res[direction] = etat_peinture[direction]
        return res
    def le_plus_de_cases_peintes(etat_peinture):
        """
        param: etat_peinture : un dictionnaire représentant les différentes directions éligible pour peindre
        :return: la liste des meilleures directions ou None s'il n'y en a pas
        """
        res = None
        if len(etat_peinture) != 0:
            dict_freq_cases_peintes = dict()
            for direction in etat_peinture.keys():
                nb_cases_peintes = etat_peinture[direction]["nb_cases_peintes"]
                if nb_cases_peintes not in dict_freq_cases_peintes.keys():
                    dict_freq_cases_peintes[nb_cases_peintes] = list()
                dict_freq_cases_peintes[nb_cases_peintes].append(direction)
            res = dict_freq_cases_peintes[sorted(dict_freq_cases_peintes, reverse=True)[0]]
        return res
    ########################################
    # Décisions
    ########################################
    ##################
    # P(NESOX)
    ##################
    res += "P"
    # Gestion de la bombe
    if possede_bombe:
        total_peinture_a_depenser = 0
        for direction in etat_peinture.keys():
            total_peinture_a_depenser += etat_peinture[direction]["nb_cases_peintes"]
        if ma_reserve - total_peinture_a_depenser > 0: #Quantité de peinture à ne pas franchir à définir
            res += "N" #Amélioration possible
        else :
            res += "X"
    elif len(etat_peinture) == 0:
        res += "X"
    else :
        liste_directions = le_plus_de_joueurs_touches(etat_peinture)
        if liste_directions is None :
            res += "X"
        else :
            etat_peinture = actualiser_dictionnaire(etat_peinture,liste_directions)
            liste_directions = le_plus_de_cases_peintes(etat_peinture)
            if liste_directions is None :
                res += "X"
            else :
                res += liste_directions[0]
    ################################################################################
    # Choix Deplacer / Tourner Carte
    ################################################################################
    if a_attaquer is not None:
        res += "T"
        carte = get_carte_a_jouer(laby)
        if a_attaquer["mur"] == "N":
            if mur_nord(carte):
                res += "AH"
            elif mur_ouest(carte):
                res += "H"
            else :
                res += "HH"
        elif a_attaquer["mur"] == "E":
            if mur_est(carte):
                res += "AH"
            elif mur_nord(carte):
                res += "H"
            else :
                res += "HH"
        elif a_attaquer["mur"] == "S":
            if mur_sud(carte):
                res += "AH"
            elif mur_est(carte):
                res += "H"
            else :
                res += "HH"
        elif a_attaquer["mur"] == "O":
            if mur_ouest(carte):
                res += "AH"
            elif mur_sud(carte):
                res += "H"
            else :
                res += "HH"
        res += a_attaquer["direction"]
        res += str(a_attaquer["rangee"])
    elif len(direction_par_couleur["ma_couleur"]) > 0:
        res += "D"
        res += direction_par_couleur["ma_couleur"][0]
    elif len(direction_par_couleur["aucune"]) > 0:
        res += "D"
        res += direction_par_couleur["aucune"][0]
    elif len(direction_par_couleur["adversaire"]) > 0:
        res += "D"
        res += direction_par_couleur["adversaire"][0]
    else :
        res += "DN"
    #################################################################
    return res


