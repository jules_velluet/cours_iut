# =====================================================================
# M1103 - Structures de données - Feuille 2
# =====================================================================
print("Exercice : Perrette et son troupeau ==========================")
# =====================================================================


def total_animaux(troupeau):
    """
    Résultat : renvoie le nombre total (int) d'animaux dans le troupeau
    """
    res=0
    for nb_animaux in troupeau.values():
        res=res+nb_animaux
    return res

troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
troupeau_vide = dict()
assert total_animaux(troupeau_de_perrette) == 63
assert total_animaux(troupeau_de_jean) == 32
assert total_animaux(troupeau_vide) == 0

print("Bravo ! Tu peux passer à la question 4.3 :)")


def tous_les_animaux(troupeau):
    """
    Résultat : renvoie l'ensemble des animaux (str) du troupeau
    """
    res=set()
    for animal in troupeau.keys():
        res.add(animal)
    return res

troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
troupeau_vide = dict()
assert tous_les_animaux(troupeau_de_perrette) == {'veau', 'vache', 'poule'}
assert tous_les_animaux(troupeau_de_jean) == {'veau', 'vache', 'cochon'}
assert tous_les_animaux(troupeau_vide) == set()

print("Super :)  ! Tu peux passer à la question 4.4")


def specialisé(troupeau):
    """
    Résultat : vérifie si le troupeau contient 30 individus ou plus
    d'un même type d'animal 
    
    """
    res=False
    for nb in troupeau.values():
        if nb>=30:
            res=True
    return res

troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
troupeau_vide = dict()
assert specialisé(troupeau_de_perrette)
assert not specialisé(troupeau_de_jean)
assert not specialisé(troupeau_vide)

print("Félicitations ! Tu peux passer à la question 4.5")



def le_plus_representé(troupeau):
    """
    Résultat : renvoie le nom de l'animal qui a le plus d'individus 
    dans le troupeau (renvoie None si le troupeau est vide)
    
    """
    nb_grand=None
    if troupeau==dict():
        res=None
    else:
        for (animal,nb) in troupeau.items():
            if nb_grand==None:
                nb_grand=nb
                res=animal
            elif nb>nb_grand:
                nb_grand=nb
                res=animal
    return res

troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
troupeau_vide = dict()
assert le_plus_representé(troupeau_de_perrette) == "poule"
assert le_plus_representé(troupeau_de_jean) == "cochon"
assert le_plus_representé(troupeau_vide) is None

print("Tu es très fort ! Tu peux passer à la question 4.6")

def quantite_suffisante(troupeau):
    """
    Résultat : vérifie si le troupeau contient au moins 5 individus
    de chaque type d'animal
    
    """
    for nb in troupeau.values():
        if nb<=5:
            return False
    return True

troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
troupeau_vide = dict()
assert quantite_suffisante(troupeau_de_perrette)
assert not quantite_suffisante(troupeau_de_jean)
assert quantite_suffisante(troupeau_vide)

print("Youpi ! Tu as terminé l'exercice 4")



# =====================================================================
print("Exercice : Qu'est ce qu'on mange ce soir ? ===================")
# =====================================================================

print("Si tu veux passer directement à l'exercice 6, tu dois mettre en commentaire tous les tests de cet exercice")


lundi = {'Carottes rapées': (15, 1), 'Nouilles au beurre':(20, 2), 'Mousse au chocolat':(30, 7)}
mardi = {'Soupe': (40, 3), 'Steak':(20, 4), 'Fromage':(1, 0)  }
mercredi = {'Pizza': (60, 7), 'Omelette':(15, 3), 'Pomme':(1, 0)}
jeudi = dict() # jour de diète


def recette_la_plus_facile(menu):
    """
    Résultat : un str, la recette la plus facil a faire
    
    """
    difficulte_min = None
    recette_facile = None
    for (recette, (_ , difficulte))  in menu.items():
        if difficulte_min is None or difficulte_min > difficulte:
            difficulte_min = difficulte
            recette_facile = recette
    return recette_facile


assert recette_la_plus_facile(lundi)=="Carottes rapées"
assert recette_la_plus_facile(mardi)=="Fromage"
assert recette_la_plus_facile(jeudi)==None



def temps_total_de_preparation(menu):
    """
    Résultat : renvoie le temps total de préparation des recettes du menu
    """
    res=0
    for (temps,_) in menu.values():
        res=res+temps
    return res

assert temps_total_de_preparation(lundi) == 65
assert temps_total_de_preparation(mardi) == 61
assert temps_total_de_preparation(jeudi) == 0


def menu_de_chef(menu):
    """
    Résultat : vérifie si le menu contient au moins une recette
    difficile (difficulté > 5)
    
    """
    for (_,difficulté) in menu.values():
        if difficulté>5:
            return True
    return False

assert menu_de_chef(lundi)
assert not menu_de_chef(mardi)
assert not menu_de_chef(jeudi)


def recette_la_plus_longue(menu):
    """
    Résultat : le nom de la recette qui demande le plus de temps
    de préparation et cuisson
    
    """
    temps_plus_grand=None
    nom_recette=None
    for (nom, (temps, _))  in menu.items():
        if temps_plus_grand==None:
            temps_plus_grand=temps
            nom_recette=nom
        elif temps>temps_plus_grand:
            temps_plus_grand=temps
            nom_recette=nom
    return nom_recette

assert recette_la_plus_longue(lundi) == 'Mousse au chocolat'
assert recette_la_plus_longue(mardi) == 'Soupe'
assert recette_la_plus_longue(jeudi) is None


# =====================================================================
print("Exercice : Fan club ==========================================")
# =====================================================================

mon_sondage ={'Florent':'Trust','Celine':'SuperBus',
    'Julien':'ACDC','Denys':'ACDC','Caroline':'Trust','Gérard':'ACDC'}


# fonction  nombre_de_fans_de
def nombre_de_fans_de(dictionaire,groupe_musique):
    """
    parametre:  un dictionnaire et un str représantant un groupe de musique
    résultat: un int
    """
    res=0
    for groupe in dictionaire.values():
        if groupe==groupe_musique:
            res=res+1
    return res

assert nombre_de_fans_de(mon_sondage,"Trust")==2, "pb"
assert nombre_de_fans_de(mon_sondage,"ACDC")==3, "pb"

# fonction  fans_de
def fans_de(dictionaire,groupe_musique):
    res=set()
    for (nom,groupe) in dictionaire.items():
        if groupe==groupe_musique:
            res.add(nom)
    return res

assert fans_de(mon_sondage,"SuperBus")=={"Celine"}
assert  fans_de(mon_sondage,"Trust")=={"Florent","Caroline"}

# fonction  tous_les_groupes
def tous_les_groupes(dictionaire):
    res=set()
    for groupe in dictionaire.values():
        if groupe not in res:
            res.add(groupe)
    return res

assert tous_les_groupes(mon_sondage)=={'ACDC', 'Trust', 'SuperBus'}
assert tous_les_groupes({"h":"k","g":"y"})=={"y","k"}

# fonction  top_groupe
def top_groupe(dictionaire):
    fans_avant=0
    for groupe in dictionaire.values():
        e=nombre_de_fans_de(dictionaire,groupe)
        if e>fans_avant:
            res=groupe
            fans_avant=e
    return res
assert top_groupe(mon_sondage)=="ACDC"
assert top_groupe({"u":"moi","y":"e","o":"moi"})=="moi"



print("Avez-vous pensé à écrire au moins un test par fonction ?")


# =====================================================================
print("Exercice : Des super héros ===================================")
# =====================================================================


# exemples de dictionnaires bidons pour faire des tests :
exemple1 = { 
    'Spiderman' : (5, 5, 'araignée à quatre pattes'),
    'Hulk':(7, 4,"Grand homme vert"),
    'Agent 13':(2, 3,'agent 13'),
    'M Becker':(2, 9, 'Expert en graphes'),
}
exemple2 = {'a':(1, 1, 'a'), 'b':(3, 9, 'b'), 'c':(7, 2, 'c')}
exemple3 = {'a':(1, 1, 'a'), 'b':(3, 9, 'b'), 'd':(4, 4, 'd')}

# N OUBLIEZ PAS D ECRIRE AU MOINS UN TEST PAR FONCTION

# fonction intelligence_moyenne
def intelligence_moyenne(dictionaire):
    cpt=0
    intelligence=0
    for caracteristique in dictionaire.values():
        intelligence=intelligence+caracteristique[1]
        cpt=cpt+1
    return intelligence/cpt

assert intelligence_moyenne(exemple1)==5.25
assert intelligence_moyenne(exemple2)==4.0
assert intelligence_moyenne(exemple3)==14/3

# fonction kikelplusfort
def kikelplusfort(dictionaire):
    le_plus_fort=None
    for (nom,caracteristique) in dictionaire.items():
        if le_plus_fort==None or caracteristique[0]>le_plus_fort:
            le_plus_fort=caracteristique[0]
            res=nom
    return res

assert kikelplusfort(exemple1)=="Hulk", "pb"
assert kikelplusfort(exemple2)=="c", "pb"
assert kikelplusfort(exemple3)=="d", "pb"

# fonction combien_de_cretins
def combien_de_cretins(dictionaire):
    moyenne=intelligence_moyenne(dictionaire)
    res=0
    for caracteristique in dictionaire.values():
        if caracteristique[1]<moyenne:
            res=res+1
    return res

assert combien_de_cretins(exemple1)==3, "pb"
assert combien_de_cretins(exemple2)==2, "pb"
assert combien_de_cretins(exemple3)==2, "pb"

print("Avez-vous pensé à écrire au moins un test par fonction ?")


# =====================================================================
print("Exercice : la maison qui rend fou ============================")
# =====================================================================


mqrf1 = {
    "Abribus":"Astus",
    "Jeancloddus":"Abribus",
    "Plexus":"Gugus",
    "Astus":None,
    "Gugus":"Plexus",
    "Saudepus":None
    }

mqrf2 = {
    "Abribus":"Astus",
    "Jeancloddus":None,
    "Plexus":"Saudepus",
    "Astus":"Gugus",
    "Gugus":"Plexus",
    "Saudepus":None,
    }

mqrf3 = {
    "Abribus":"Astus",
    "Jeancloddus":None,
    "Plexus":"Jeancloddus",
    "Astus":"Gugus",
    "Gugus":"Plexus",
    "Saudepus":"Bielorus",
    }


def quel_guichet(mqrf, guichet_de_depart):
    """
    paramètres :
      - mqrf est une maison qui rend fou qui vérifie la condition de César
      - guichet est le nom d'un guichet de la mqrf
    résultat : le nom du guichet qui finit par donner le formulaire
    """
    pass

assert quel_guichet(mqrf1, "Abribus") == "Astus"
assert quel_guichet(mqrf2, "Abribus") == "Saudepus"



def quel_guichet(mqrf, guichet_de_depart):
    """
    paramètres :
      - mqrf est une maison qui rend fou qui vérifie la condition de César
      - guichet est le nom d'un guichet de la mqrf
    résultat : le nom du guichet qui finit par donner le formulaire
    """
    pass

assert quel_guichet(mqrf1, "Abribus") == ("Astus", 2)
assert quel_guichet(mqrf2, "Abribus") == ("Saudepus", 5)

