# =====================================================================
# Structures de données - Feuille d'exercices n°8
# =====================================================================

# =====================================================================
# Min, max et sorted
# =====================================================================


centre = {'Loiret' : {'Orleans', 'Trainou', 'Chécy', 'Ardon', 'Amilly'},
    'Cher' : {'Vierzon', 'Quincy', 'Bourges'},
    'Indre' : {'Chateauroux', 'Issoudin', 'Déols', 'Le Blanc'}}

bretagne = {'Morbihan': {'Lorient', 'Vannes', 'Ploemeur'},
    'Finistère' : {'Cast', 'Brest', 'Quimper', 'Concarneau', 'Landerneau'},
    'Cotes Armor': {'Lannion'}}

def tri_selon_nombre_de_villes(region):
    """
    region est un dictionnaire dont les clés sont des régions/départements et
    la valeur associée un ensemble de villes
    renvoie la liste des clés rangées dans l'ordre croissant de leur
    nombre de villes
    """
    def nb_ville(departement):
        return len(region[departement])
    return sorted(region, key=nb_ville)

assert tri_selon_nombre_de_villes(centre) == ['Cher', 'Indre', 'Loiret']
assert tri_selon_nombre_de_villes(bretagne) == ['Cotes Armor', 'Morbihan', 
    'Finistère']


# =====================================================================
# Bibliothèque
# =====================================================================

exemple = { 
    'Aurélien' : ('Aragon', 'Roman'),
    'Brocéliande' : ('Aragon', 'Poésie'),
    "L'Avare" : ('Molière', 'Théâtre'),
    '1984' : ('Orwell', 'Roman'),
    'La Ferme des animaux' : ('Orwell', 'Roman'),
    'Elsa' : ('Aragon', 'Poésie')}

# Q4.1

def cherche_auteurs(bibliotheque, genre_recherché):
    res=set()
    for genre in bibliotheque.values():
        if genre[1]==genre_recherché:
            res.add(genre[0])
    return res

assert cherche_auteurs(exemple, 'Encyclopédie') == set()
assert cherche_auteurs(exemple, 'Roman') == {'Aragon', 'Orwell'}

# Q4.2

def auteurs(bibliotheque):
    res=dict()
    for auteur in bibliotheque.values():
        if auteur[0] not in res.keys():
            res[auteur[0]]=1
        else:
            res[auteur[0]]+=1
    return res

assert auteurs(exemple) == {'Aragon': 3, 'Molière': 1, 'Orwell': 2}

# Q4.3
def ouvrages_par_genre(bibliotheque):
    res=dict()
    for (titre,genre) in bibliotheque.items():
        if genre[1] not in res.keys():
            res[genre[1]]={titre}
        else:
            res[genre[1]].add(titre)
    return res

assert ouvrages_par_genre(exemple) == {
    'Roman': {'1984', 'Aurélien', 'La Ferme des animaux'},
    'Poésie': {'Brocéliande', 'Elsa'},
    'Théâtre': {"L'Avare"}}
    
# Q4.4

def le_moins_représenté(bibliotheque):
    nb_ouvrage=auteurs(bibliotheque)
    res=None
    aux=None
    for (auteur,nb) in nb_ouvrage.items():
        if aux is None or nb<aux:
            res=auteur
            aux=nb
    return res
    
assert le_moins_représenté(exemple) == 'Molière'