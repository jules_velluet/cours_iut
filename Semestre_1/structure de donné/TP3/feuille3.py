# =====================================================================
# Structures de données - Feuille d'exercices n°3
# =====================================================================

# =====================================================================
# Fan Club
# =====================================================================

mon_sondage ={'Florent':'Trust', 'Celine':'SuperBus', 'Julien':'ACDC',
              'Denys':'ACDC', 'Caroline':'Trust', 'Gérard':'ACDC'}

def dico_freq(dictionnaire):
    """renvoie le dico de fréquence des différents groupes"""
    res=dict()
    for groupe in dictionnaire.values():
        if groupe in res.keys():
            res[groupe]+=1
        else:
            res[groupe]=1
    return res

assert dico_freq(mon_sondage) == {'Trust': 2, 'ACDC': 3, 'SuperBus': 1}

def nombre_de_fans_de(sondage, groupe): # Complexité = O(n)
    """renvoie le nombre de fans du groupe"""
    nb = 0
    for gpe in sondage.values():
        if gpe == groupe:
            nb+=1
    return nb
assert nombre_de_fans_de(mon_sondage, 'Trust') == 2

def top_groupe(sondage): # Complexité = O(n²)
    """renvoie le nom du groupe qui a le plus de fans"""
    groupe_au_top = None
    nb_fan_au_top = 0
    for groupe in sondage.values(): # boucle
        if nombre_de_fans_de(sondage, groupe) >= nb_fan_au_top:
            nb_fan_au_top = nombre_de_fans_de(sondage, groupe)
            groupe_au_top = groupe
    return groupe_au_top
    
assert top_groupe(mon_sondage) == 'ACDC'

def top_groupe_ameliorrer(sondage):
    groupe_au_top = None
    nb_fan_au_top = 0
    for groupe in sondage.values():
        nb_fan=nombre_de_fans_de(sondage, groupe)
        if nb_fan>=nb_fan_au_top:
            nb_fan_au_top=nb_fan
            groupe_au_top=groupe
    return groupe_au_top

assert top_groupe(mon_sondage) == 'ACDC'

# ==========================
# Petites bêtes
# ==========================

# Modélisation v1

mon_pokedex = [('Bulbizarre', 'Plante'),  ('Aeromite', 'Poison'),  ('Abo', 'Poison')]

def frequences_types(liste_pokemon): #O(n)
    """renvoi les fréquence des types des pokemon
    parametre: une liste
    résultat: un dict
    """
    res=dict()
    for (_,type_pokemon) in liste_pokemon:
        if type_pokemon in res.keys():
            res[type_pokemon]+=1
        else:
            res[type_pokemon]=1
    return res

assert frequences_types(mon_pokedex) == {'Plante': 1, 'Poison': 2}

def dico_par_types(liste_pokemon): #O(n)
    """renvoie en clé les type de pokemon et en valeur leur nom
    parametre: une liste
    resultat: un dict
    """
    res=dict()
    for (nom,type_pokemon) in liste_pokemon:
        if type_pokemon in res.keys():
            res[type_pokemon].add(nom)
        else:
            res[type_pokemon]={nom}#pb est la gros
    return res

assert dico_par_types(mon_pokedex) == {'Plante': {'Bulbizarre'}, 'Poison': {'Aeromite', 'Abo'}}

def type_le_plus_représenté(liste_pokemon): #O(n)
    """renvoi le type le plus représenter
    parametre: une liste
    resultat: un str
    """
    nb_max=None
    frequence_pokemon=frequences_types(liste_pokemon)
    for (nom,nb) in frequence_pokemon.items():
        if nb_max is None or nb>nb_max:
            res=nom
            nb_max=nb
    return res 

assert type_le_plus_représenté(mon_pokedex) == 'Poison'

# Modélisation v2

mon_pokedex = [('Bulbizarre', {'Plante', 'Poison'}),
  ('Aeromite', {'Poison', 'Insecte'}), ('Abo', {'Poison'})]

def frequences_types_v2(liste_pokemon):
    """renvoie le dictionnaire de fréquence des types de pokemon
    parametre: une liste
    resultat: un dict
    """
    res=dict()
    for (_,type_pokemon) in liste_pokemon:
        for type_p  in type_pokemon:
            if type_p in res.keys():
                res[type_p]+=1
            else:
                res[type_p]=1
    return res

assert frequences_types_v2(mon_pokedex) == {'Plante': 1, 'Poison': 3, 'Insecte':1}

def dico_par_types_v2(liste_pokemon):
    res=dict()
    for (nom,type_pokemon) in liste_pokemon:
        for type_p in type_pokemon:
            if type_p in res.keys():
                res[type_p].add(nom)
            else:
                res[type_p]={nom}
    return res

assert dico_par_types_v2(mon_pokedex) == { 'Plante': {'Bulbizarre',},'Poison': {'Bulbizarre', 'Aeromite', 'Abo'}, 'Insecte': {'Aeromite'}}

def type_le_plus_représenté_v2(liste_pokemon):
    nb_max=None
    frequence_pokemon=frequences_types_v2(liste_pokemon)
    for (nom,nb) in frequence_pokemon.items():
        if nb_max is None or nb>nb_max:
            res=nom
            nb_max=nb
    return res 

assert type_le_plus_représenté_v2(mon_pokedex) == 'Poison'


# ==========================
# Perrette et son troupeau
# ==========================

troupeau_de_perrette={'veau':14, 'vache':7, 'poule':42}

def ajoute_animaux(troupeau, animal, nombre):
    """
    Cette fonction ajoute des animaux dans le troupeau.
    Par exemple 'ajoute_animaux(troupeau, "Zébu", 4)' ajoute
    4 Zébus au troupeau passé en paramètre
    Résultat : None
    """
    res=troupeau
    if animal in res.keys():
        res[animal]+=nombre
    else:
        res[animal]=nombre
    return res

ajoute_animaux(troupeau_de_perrette, 'vache', 23)
assert troupeau_de_perrette == {'veau':14, 'vache':30, 'poule':42}
ajoute_animaux(troupeau_de_perrette, 'girafe', 2)
assert troupeau_de_perrette == {'veau':14, 'vache':30, 'poule':42, 'girafe':2}


def reunit_troupeaux(troupeau1, troupeau2):#O(n)
    """
    Cette fonction réunit deux troupeaux d'animaux (modélisés par 
    un dictionnaire { nom_animal : nombre }
    Résultat : un nouveau dictionnaire qui est la réunion des deux troupeaux
    """
    res=troupeau1
    for (animaux,nb) in troupeau2.items():
        if animaux in troupeau1.keys():
            res[animaux]+=nb
        else:
            res[animaux]=nb
    return res

perrette = {'veau':14, 'vache':7, 'poule':42}
jean = {'vache':12, 'cochon':17, 'veau':3}
assert reunit_troupeaux(perrette, jean) == {'veau':17, 'vache':19, 'poule':42, 'cochon':17}

# ==========================
# Occupations
# ==========================


lundi_lea = [('Sommeil', 7.5), ('Repas', 0.25), ('Douche', 0.1),
    ('Transport', 0.75), ('Travail', 4), ('Repas', 1),
    ('Sommeil',0.5), ('Travail',4.5)]

vendredi_moi=[("Sommeil",8), ("Repas", 0.10), ("Douche", 0.10), ("Travail", 4), ("Repas", 0.50), ("Détente", 1), ("Travail", 1.50), ("Tour_de_France_2020", 1), ("Travail", 3), ("Repas", 1), ("Détente",2)]

def temps_travail(journee):
    res=0
    for (activité, temps) in journee:
        if activité=="Travail":
            res=res+temps
    return res

assert temps_travail(lundi_lea)==8.5
assert temps_travail(vendredi_moi)==8.5

def activité_longue(journee):
    fréquence_activité=dict()
    termps_activité=None
    for (activité,temps) in journee:
        if activité in fréquence_activité.keys():
            fréquence_activité[activité]+=temps
        else:
            fréquence_activité[activité]=temps
    for (activité,temps) in fréquence_activité.items():
        if termps_activité==None or temps>termps_activité:
            termps_activité=temps
            res=activité
    return res

assert activité_longue(vendredi_moi)=="Travail", "pb"
assert activité_longue(lundi_lea)=="Travail", "pb"

# ==========================
# Qui a mon livre
# ==========================


dico_prets ={'M Becker':'Alice', 'Alice':'Boris', 
    'Charlotte':'Denis', 'Boris':'Charlotte'}

def qui_a_prete(dico_prets, emprunteur):
    """
    Paramètres: 
       - dico_prets est un dictionnaire dont les clés sont des personnes
         qui ont eu le livre, et la valeur associée est le nom de la
         personne à qui chacune a confié le livre.
      - emprunteur, le nom d'une personne
    Résultat : le nom de la personne qui a prêté le livre à 'emprunteur '
    Note: on suppose qu'emprunteur a effectivement reçu le livre et que
    chaque personne n'a eu le livre qu'une seule fois
    """
    for (donneur,receveur) in dico_prets.items():
        if receveur==emprunteur:
            res=donneur
    return res

assert qui_a_prete({'M Becker':'Alex', 'Alex':'Boris'}, 'Alex') == 'M Becker'

def qui_a_maintenant(dico_prets):
    """
    Paramètre: dico_prets est un dictionnaire dont les clés sont des
    personnes qui ont eu le livre, et la valeur associée est le nom de la
    personne à qui chacune a confié le livre.
    Résultat : le nom de la personne qui a maintenant le livre
    Note: on suppose que chaque personne n'a eu le livre qu'une seule fois
    """
    for receveur in dico_prets.values():
        res=receveur
    return res

assert qui_a_maintenant(dico_prets)=="Charlotte"

print('As-tu pensé à écrire un test pour la fonction qui_a_maintenant ?')
print("Oui, Madame")

def intermediaires(dico_prets, depart, arrivee):
    """
    Paramètres: 
       - dico_prets est un dictionnaire dont les clés sont des personnes
         qui ont eu le livre, et la valeur associée est le nom de la
         personne à qui chacune a confié le livre.
      - 'depart' et 'arrivee' sont deux personnes
    Résultat : la liste des personnes qui ont eu le livre entre 
    depart (compris) et arrivee (non compris), dans l'ordre où elles se
    sont passées le livre.
    Note: on suppose que chaque personne n'a eu le livre qu'une seule fois,
    et que 'depart' et 'arrivee' l'ont eu, et que 'depart' l'a eu avant 'arrivee'
    """
    res=list()
    donneur_avant=None
    for (donneur,receveur) in dico_prets.items():
        if donneur_avant==None:
            res.append(donneur)
            donneur_avant=receveur
        elif donneur_avant==donneur:
            res.append(donneur)
            donneur_avant=receveur
    return res

d = {'M Becker':'Alice', 'Alice': 'Boris', 'Charlotte':'Denis', 'Boris':'Charlotte'}
assert intermediaires(d,'M Becker','Charlotte') == ['M Becker', 'Alice', 'Boris']
