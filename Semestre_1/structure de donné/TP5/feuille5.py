# =====================================================================
# Structures de données - Feuille d'exercices n°5
# =====================================================================
def mystere (liste1, liste2):
    """ paramètres: liste1 , liste2 deux listes triées dans l'ordre croissant
    résultat : ??? """
    res = []
    (i1, i2) = (0, 0)
    while i1 < len(liste1) and i2 < len(liste2):
        (e1, e2) = (liste1[i1] , liste2[i2])
        if e1 <= e2:
            res.append(e1)
            i1+= 1
        else:
            res.append(e2)
            i2+= 1
    res = res + liste1[i1:] +liste2[i2:]
    return res
assert mystere([1, 1, 2, 5, 6, 7, 8, 9], [3, 9, 10, 10, 12]) == [1, 1, 2, 3,5,6,7,8,9,9,10,10,12]

def intersection(liste1,liste2):
    """donne l'intersection des deux listes
    paramètre: deux liste trié
    resultat: une liste
    """
    res=[]
    i=0
    j=0
    while i<len(liste1) and j<len(liste2):
        if liste1[i]<liste2[j]:
            i=i+1
        elif liste1[i]>liste2[j]:
            j=j+1
        else:
            res.append(liste1[i])
            i=i+1
            j=j+1
    return res    

assert intersection([1, 1, 2, 3], [4, 5, 6]) == []
assert intersection([1, 1, 2, 3, 6, 7], [3, 4, 5, 6, 8]) == [3,6]
assert intersection([1, 1, 2, 3], [2,3]) == [2, 3]
assert intersection([1, 1, 2, 3], [1, 1, 1, 1]) == [1, 1]
assert intersection([1, 1, 2, 2, 3], [1, 1, 1, 2]) == [1, 1, 2]
assert intersection([1, 1, 2, 3], [1,2,3]) == [1, 2, 3]



#avant boucle: haut=7, bas=0, liste[milieu]=None
#pendant la  boucle: haut=7, bas=4, liste[milieu]=9, cible<liste[milieu]=False
#                    haut=4, bas=4, liste[milieu]=15, cible<liste[milieu]=True
#sorti boucle:       haut=4, bas=5, liste[milieu]=12, cible<liste[milieu]=False

def recherche(liste, cible):#O(n)
    """
    param:
    - liste est une liste de nombres triés dans l'ordre croissant
    - cible est un nombre
    résultat : True si la cible est dans la liste False sinon
    """
    bas = 0
    haut = len(liste) -1
    while bas <= haut:
        milieu = (haut + bas) // 2
        if cible < liste[milieu]:
            haut = milieu -1
        else:
            bas = milieu + 1
    return (liste[haut] == cible)
exemple = [3, 6, 9, 11, 12, 15, 18, 21]
assert recherche(exemple, 13) == False
assert recherche(exemple, 14) == False
assert recherche(exemple, 15) == True

#recherche([0, 3, 6, 9, 12, 15, 18, 21],13), résultat == False, haut == 4, bas == 5
#recherche([0, 3, 6, 9, 12, 15, 18, 21],14), résultat == False, haut == 4, bas == 5
#recherche([0, 3, 6, 9, 12, 15, 18, 21],15), résultat == True, haut == 5, bas == 6
#recherche([0, 3, 6, 9, 12, 15, 18, 21],25), résultat == False, haut == 7, bas == 8

#on est sur de sortir de la boucle car bas va forcément être supérieur à haut
# 
# # =====================================================================
# Association des Dresseurs de Pokemons
# =====================================================================

novembre_2020 = {
    'Sasha': 2005,
    'Florent': 1800,
    'Jasmine': 1400,
    'Zoé' : 1100,
    'Théo':700,
    'Morgan': 1700,
    'Maxime':1650,
    'Olive':1500,
    'Will': 1610
    }


decembre_2020 = {
    'Sasha': 1880,
    'Florent': 1810,
    'Jasmine': 1510,
    'Zoé' : 980,
    'Théo':850,
    'Morgan': 1650,
    'Maxime':1400,
    'Olive':1670,
    'Will': 1460
    }


def duellistes(dresseurs): #O(n²)
    """
    param: dresseurs est un dictionnaires :
    - clé: le nom du dressur (str)
    - valeur : son classement Elo (int)
    résultat: renvoie le nom des deux dresseurs qui ont les classements
    Elo les plus proches
    """
    res=None
    a_avant=None
    for (dresseurs1,classement) in dresseurs.items():
        for (dresseurs2,classement2) in dresseurs.items():
            if dresseurs1!=dresseurs2:
                if classement>classement2:
                    a=classement-classement2
                else:
                    a=classement2-classement
                if a_avant is None or a_avant>a:
                    if classement>classement2:
                        res=(dresseurs2,dresseurs1)
                    else:
                        res=(dresseurs1,dresseurs2)
                    a_avant=a
    return res

assert duellistes(novembre_2020) == ("Will","Maxime")
assert duellistes(decembre_2020) == ('Morgan', 'Olive')

def dictionnaire_en_liste(dico):
    classement_dresseur=[]
    for (dresseur,classement) in dico.items():
        classement_dresseur.append((classement,dresseur))
    return classement_dresseur

def duellistes_v2(dresseur): #O(n logn)
    classement_dresseur=dictionnaire_en_liste(dresseur)
    classement_dresseur_trie=sorted(classement_dresseur)
    res=None
    plus_petit_ecart=None
    for i in range(len(classement_dresseur_trie)-1):
        a=abs(classement_dresseur_trie[i][0]-classement_dresseur_trie[i+1][0])
        if plus_petit_ecart==None or a<plus_petit_ecart:
            res=(classement_dresseur_trie[i][1],classement_dresseur_trie[i+1][1])
            plus_petit_ecart=a
    return res

assert duellistes_v2(novembre_2020) == ("Will","Maxime")
assert duellistes_v2(decembre_2020) == ('Morgan', 'Olive')
# =====================================================================
# Quelques fonctions sur des listes (triées ou non)
# =====================================================================

def rend_unique(liste): # Complexité = O(n)
    """
    parametre: une liste d'éléments comparables
    résultat: une liste contenant les éléments de 'liste', une fois chacune
    de leur première occurence .
    """
    deja_vus = set ()
    res = []
    for elem in liste:
        if elem not in deja_vus :
            res.append(elem)
            deja_vus.add(elem)
    return res

assert rend_unique([1, 6, 5, 1, 4, 7, 1, 2, 5, 6, 1, 7]) == [1, 6, 5, 4, 7, 2]


def rend_unique_liste_triee(liste): # Complexité = O(n)
    """
    parametre: une liste dont les éléments sont triés dans l'ordre croissant
    résultat: une liste contenant les éléments de 'liste', une fois chacune
    de leur première occurence .
    """
    res=[]
    for elem in liste:
        if elem not in res:
            res.append(elem)
    return res

assert rend_unique([1, 1, 2, 5, 5, 5, 5, 7, 8, 9, 9]) == [1,2, 5, 7, 8, 9]



# =====================================================================
# Un peu d'algorithmique
# =====================================================================


def le_plus_proche(liste, cible):
    """
    param:
      - liste est une liste de nombres triés dans l'ordre croissant
      - cible est un nombre
    résultat : l'élément de la liste qui est le plus proche de cible
    """
    a=None
    for nb in liste:
        b=abs(nb-cible)
        if a==None or b<a:
            res=nb
            a=b
    return res

exemple = [0, 3, 6, 9, 12, 15, 18, 21]
assert le_plus_proche(exemple, 13) == 12
assert le_plus_proche(exemple, 14) == 15
assert le_plus_proche(exemple, 15) == 15
assert le_plus_proche(exemple, 1) == 0
assert le_plus_proche(exemple, 3) == 3
assert le_plus_proche(exemple, 21) == 21
assert le_plus_proche(exemple, 25) == 21


# =====================================================================
# Location de surf
# =====================================================================

planches_exemple = [152, 161, 161, 170, 175, 185, 190, 200]

personnes_exemple_lundi = [("Alex",182), ("Bachir",172), ("Chen",171), ("Dalila",173), ("Estéban",179), ("Fanta",165), ("Gérard",195), ("Henrietta",156)]  # A COMPLETER
personnes_exemple_mardi = None # A COMPLETER


def attribution(planches, personnes):
    """
    parametres:
      - planches est la liste des tailles des planches disponibles,
        triées dans l'ordre croissant
      - personnes est une sdd qui modéliste les personnes qui veulent
        louer des planches
      On suppose que len(personnes) <= len(planches)
    resultat : un dictionnaire où chaque clé est le nom d'une personne
    et la valeur associée est la taille de la planche qui lui est
    attribuée.
    On cherche une attribution qui minimise la somme des différences
    entre la taille de chaque personne et la taille de la planche
    qui lui a été attribuée.
    """    
    res=dict()
    for (pers,taille) in personnes:
        b=None
        for planche in planches:
            a=abs(taille-planche)
            if b is None or a<b:
                res[pers]=planche
                b=a
        planches.remove(res[pers])
    return res

#(attribution(planches_exemple, personnes_exemple_lundi))#== {'Alex': 190, 'Bachir': 170, 'Chen': 161, 'Dalila': 175,'Estéban': 185, 'Fanta': 161, 'Gérard': 200, 'Henrietta' : 152}