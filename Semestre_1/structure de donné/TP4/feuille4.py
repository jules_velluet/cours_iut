# =====================================================================
# Structures de données - Feuille d'exercices n°4
# =====================================================================
import copy

# =====================================================================
# Choix de modélisation et complexité
# =====================================================================

# version 1
pokedex_anakin_v1 = {
    ('Carmache', 'Dragon'), ('Carmache', 'Sol'),
    ('Colimucus', 'Dragon'), ('Palkia', 'Dragon'),
    ('Palkia', 'Eau')}

# version 2
pokedex_anakin_v2 = {
    'Carmache': {'Dragon','Sol'},
    'Colimucus': {'Dragon'},
    'Palkia': {'Dragon', 'Eau'}}

# version 3
pokedex_anakin_v3 = {
    'Dragon': {'Carmache','Colimucus', 'Palkia'},
    'Sol': {'Carmache'},
    'Eau': {'Palkia'}}

# Pokedex de romain
pokedex_romain_v1 = {("Maraiste", "Eau"), ("Maraiste", "Sol"), 
    ("Racaillou", "Sol"), ("Racaillou", "Roche")} # A compéter
pokedex_romain_v2 = {"Maraiste": {"Eau","Sol"} ,
    "Racaillou": {"Sol", "Roche"}} # A compéter
pokedex_romain_v3 = {"Sol": {"Maraiste", "Racaillou"},
    "Eau": {"Maraiste"},
    "Roche": {"Racaillou"}} # A compéter

# =====================================
# CHOIX DE lA VERSION  : COMPLETER LE 'print' ET
# DECOMMENTER LES DEUX LIGNES DE LA VERSION CHOISIE
print('Avec la version ??? =============')
# pokedex_anakin = pokedex_anakin_v1
# pokedex_romain = pokedex_romain_v1
# pokedex_anakin = pokedex_anakin_v2
# pokedex_romain = pokedex_romain_v2
# pokedex_anakin = pokedex_anakin_v3
# pokedex_romain = pokedex_romain_v3
# =====================================


# IMPLEMENTATION DES FONCTIONS

def appartient(pokemon, pokedex): # Complexité : o(n)
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for (pokemon_present,_) in pokedex:
        if pokemon_present==pokemon:
            return True
    return False

assert not appartient("Racaillou", pokedex_anakin_v1)
assert appartient("Racaillou", pokedex_romain_v1)==True


def toutes_les_attaques(pokemon, pokedex): # Complexité : o(n)
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    res=set()
    for (poke,poke_type) in pokedex:
        if poke==pokemon:
            res.add(poke_type)
    return res

assert toutes_les_attaques("Palkia", pokedex_anakin_v1) == {'Eau', 'Dragon'}
assert toutes_les_attaques("Colimucus", pokedex_anakin_v1) == {'Dragon'}


def nombre_de(attaque, pokedex): # Complexité : o(n)
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    res=0
    for (poke,poke_type) in pokedex:
        if poke_type==attaque:
            res=res+1
    return res

assert nombre_de("Dragon", pokedex_anakin_v1) == 3
assert nombre_de("Dragon", pokedex_romain_v1) == 0

def ajoute(pokemon, attaque, pokedex): # Complexité : o(1)
    """
    Ajoute au pokedex le pokemon (str) qui a pour un seul type 'attaque' (str)
    dans le pokedex
    resultat : rien
    """
    pokedex.add((pokemon,attaque))

copie = copy.deepcopy(pokedex_anakin_v1)
ajoute("Sancoki", "Eau", copie)
assert copie =={
    ('Carmache', 'Dragon'), ('Carmache', 'Sol'),
    ('Colimucus', 'Dragon'), ('Palkia', 'Dragon'),
    ('Palkia', 'Eau'), ("Sancoki","Eau")} # A COMPLETER


# =====================================
# CHOIX DE lA VERSION  : COMPLETER LE 'print' ET
# DECOMMENTER LES DEUX LIGNES DE LA VERSION CHOISIE
print('Avec la version ??? =============')
# pokedex_anakin = pokedex_anakin_v1
# pokedex_romain = pokedex_romain_v1
pokedex_anakin = pokedex_anakin_v2
pokedex_romain = pokedex_romain_v2
# pokedex_anakin = pokedex_anakin_v3
# pokedex_romain = pokedex_romain_v3
# =====================================


# IMPLEMENTATION DES FONCTIONS

def appartient(pokemon, pokedex): # Complexité : o(1)
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    if pokemon in pokedex.keys():
        return True
    return False

assert not appartient("Racaillou", pokedex_anakin)
assert appartient("Racaillou", pokedex_romain)==True


def toutes_les_attaques(pokemon, pokedex): # Complexité : o(n)
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    for (poke,attaque) in pokedex.items():
        if poke==pokemon:
            res=attaque
    return res

assert toutes_les_attaques("Palkia", pokedex_anakin) == {'Eau', 'Dragon'}
assert toutes_les_attaques("Colimucus", pokedex_anakin) == {'Dragon'}


def nombre_de(attaque, pokedex): # Complexité : o(n)
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    res=0
    for type_poke in pokedex.values():
        if attaque in type_poke:
            res=res+1
    return res

assert nombre_de("Dragon", pokedex_anakin) == 3
assert nombre_de("Dragon", pokedex_romain) == 0

def ajoute(pokemon, attaque, pokedex): # Complexité : o(1)
    """
    Ajoute au pokedex le pokemon (str) qui a pour un seul type 'attaque' (str)
    dans le pokedex
    resultat : rien
    """
    pokedex[pokemon]={attaque}

copie = copy.deepcopy(pokedex_anakin)
ajoute("Sancoki", "Eau", copie)
print(copie)
assert copie =={
    'Carmache': {'Dragon','Sol'},
    'Colimucus': {'Dragon'},
    'Palkia': {'Dragon', 'Eau'}, "Sancoki": {"Eau"}} # A COMPLETER


# =====================================
# TRANSFORMER UNE VERSION EN UNE AUTRE
# =====================================

# Version 1 ==> Version 2

def v1_to_v2(pokedex_v1):
    """
    param: prend en paramètre un pokedex version 1
    renvoie le même pokedex mais en version 2
    """
    pokedex_v2 = dict()
    for (pokemon, attaque) in pokedex_v1:
        if pokemon in pokedex_v2.keys():
            pokedex_v2[pokemon].add(attaque)
        else:
            pokedex_v2[pokemon]={attaque}
    return pokedex_v2

assert v1_to_v2(pokedex_anakin_v1) == pokedex_anakin_v2
assert v1_to_v2(pokedex_romain_v1) == pokedex_romain_v2


# Version 1 ==> Version 2

def v2_to_v3(pokedex_v2):
    """
    param: prend en paramètre un pokedex version2
    renvoie le même pokedex mais en version3
    """
    pokedex_v3 = dict()
    for (pokemon, attaques) in pokedex_v2.items():
        for attaque in attaques:
            if attaque in pokedex_v3.keys():
                pokedex_v3[attaque].add(pokemon)
            else:
                pokedex_v3[attaque]={pokemon}
    return pokedex_v3

assert v2_to_v3(pokedex_anakin_v2) == pokedex_anakin_v3
assert v2_to_v3(pokedex_romain_v2) == pokedex_romain_v3



# =====================================
# CHOIX DE lA VERSION  : COMPLETER LE 'print' ET
# DECOMMENTER LES DEUX LIGNES DE LA VERSION CHOISIE
print('Avec la version ??? =============')
# pokedex_anakin = pokedex_anakin_v1
# pokedex_romain = pokedex_romain_v1
# pokedex_anakin = pokedex_anakin_v2
# pokedex_romain = pokedex_romain_v2
pokedex_anakin = pokedex_anakin_v3
pokedex_romain = pokedex_romain_v3
# =====================================


# IMPLEMENTATION DES FONCTIONS

def appartient(pokemon, pokedex): # Complexité : o(n)
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for attaque in pokedex.values():
        if pokemon in attaque:
            return True
    return False

assert not appartient("Racaillou", pokedex_anakin)
assert appartient("Racaillou", pokedex_romain)==True


def toutes_les_attaques(pokemon, pokedex): # Complexité : o(n)
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    res=set()
    for (type_poke,poke) in pokedex.items():
        if pokemon in poke:
            res.add(type_poke)
    return res

assert toutes_les_attaques("Palkia", pokedex_anakin) == {'Eau', 'Dragon'}
assert toutes_les_attaques("Colimucus", pokedex_anakin) == {'Dragon'}


def nombre_de(attaque, pokedex): # Complexité : o(1)
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    res=0
    if attaque in pokedex.keys():
        res=pokedex[attaque]
        res=len(res)
    return res

assert nombre_de("Dragon", pokedex_anakin) == 3
assert nombre_de("Dragon", pokedex_romain) == 0

def ajoute(pokemon, attaque, pokedex): # Complexité : o(1)
    """
    Ajoute au pokedex le pokemon (str) qui a pour un seul type 'attaque' (str)
    dans le pokedex
    resultat : rien
    """
    pokedex[attaque].add(pokemon)

copie = copy.deepcopy(pokedex_anakin)
ajoute("Sancoki", "Eau", copie)
assert copie =={
    'Dragon': {'Carmache','Colimucus', 'Palkia'},
    'Sol': {'Carmache'},
    'Eau': {'Palkia', "Sancoki"}} # A COMPLETER

def v1_to_v3(pokedex_v1):
    return v2_to_v3(v1_to_v2(pokedex_v1))

assert v1_to_v3(pokedex_anakin_v1)==pokedex_anakin_v3
assert v1_to_v3(pokedex_romain_v1)==pokedex_romain_v3

def v3_to_v1(pokedex_v3):
    res=set()
    for (type_poke,poke) in pokedex_v3.items():
        for pokemon in poke:
            res.add((pokemon,type_poke))
    return res

print(v3_to_v1(pokedex_anakin_v3))

def afficheBilan(sdd):
    total=0
    for achat in sdd.values():
        for depense in achat:
            total=total+depense
    depense_par_pers=total/len(sdd.keys())
    per_achat=0
    for (personne,achat) in sdd.items():
        for depense in achat:
            per_achat=per_achat+depense
        if per_achat>depense_par_pers:
            recevoir=per_achat-depense_par_pers
            print(personne+" doit recevoir "+str(recevoir)+" euros.")
        elif per_achat<depense_par_pers:
            payer=depense_par_pers-per_achat
            print(personne+" doit payer "+str(payer)+" euros.")
        else:
            print("tu n'as rien à faire")
    return

print(afficheBilan({"pierre": [15,12,8,3], "Marie": [20,34], "Anna": [52], "Béatrice": [8], "Sasha": []}))