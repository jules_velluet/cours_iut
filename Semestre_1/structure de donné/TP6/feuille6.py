# =====================================================================
# Structures de données - Feuille d'exercices n°6
# =====================================================================

# =====================================================================
# Question de "cours"
# =====================================================================

def mystere(liste):
    """
    parametre : une liste de str
    resultat: le mot le plus long un str
    """
    return max(liste, key = len)

assert mystere(['You', 'know', 'nothing', 'John', 'Snow']) == 'nothing'


def somme(liste):
    """paramétre un tuple
    resultat: une int
    somme
    """
    return liste[0]+liste[1]

def tri_selon_la_somme(liste):
    """
    parametre : une liste de couples de nombres
    resultat: la liste triée dans l'ordre croissant de la somme des deux 
    composants des couples
    """
    return sorted(liste, key = somme)

assert tri_selon_la_somme([(5, 7), (12, 3), (9, 1), (5, 6)]) == [(9, 1), (5, 6),(5, 7), (12, 3)]
 

def deuxieme_composante(couple):
   return couple[1]


def tri_selon_la_deuxieme_composante(liste):
    """
    parametre : une liste de couples de nombres
    resultat: la liste triée dans l'ordre croissant selon la deuxième composante
    des couples
    """
    return sorted(liste, key=deuxieme_composante)

assert tri_selon_la_deuxieme_composante([(5, 7), (12, 3), (9, 1), (5, 6)]) == [(9, 1) , (12, 3), (5, 6), (5, 7)]



def mystere(intervalle1, intervalle2):
    """
    paramètres : deux intervalles [debut, fin[ représentés par des tuples
    résultat : 
    """
    (debut1, fin1) = intervalle1
    (debut2, fin2) = intervalle2
    return debut1 < fin2 and debut2 < fin1

assert mystere((1, 4), (6, 8)) == False
assert mystere((7, 9), (2, 4)) == False
assert mystere((1, 11), (6, 8)) == True
assert mystere((8, 14), (5, 10)) ==  True
# =====================================================================
# Planning pour un festival
# =====================================================================


 
# Exemples de modélisation de spectacles :
s1 = {'nom': 'JL Aubert', 'debut': 8, 'fin': 10}
s2 = {'nom': '2Be3', 'debut': 11, 'fin': 15}
s3 = {'nom': 'Tyko Moon', 'debut': 5, 'fin': 11}


# Exemple de modélisation de programme :
nikopol = [
    {'nom': 'JL Aubert', 'debut': 8, 'fin': 10}, 
    {'nom': 'JL Aubert', 'debut': 13, 'fin': 17}, 
    {'nom': 'JL Aubert', 'debut': 21, 'fin': 24}, 
    {'nom': 'C Goya', 'debut': 6, 'fin': 9}, 
    {'nom': 'C Goya', 'debut': 10, 'fin': 14}, 
    {'nom': 'C Goya', 'debut': 17, 'fin': 18}, 
    {'nom': '2Be3', 'debut': 11, 'fin': 15}, 
    {'nom': '2Be3', 'debut': 18, 'fin': 21}, 
    {'nom': 'Warhole', 'debut': 8, 'fin': 13}, 
    {'nom': 'Warhole', 'debut': 20, 'fin': 22}, 
    {'nom': 'Tyko Moon', 'debut': 5, 'fin': 11}, 
    {'nom': 'Tyko Moon', 'debut': 13, 'fin': 16}, 
    {'nom': 'Horus', 'debut': 7, 'fin': 18}, 
    {'nom': 'KKDZO', 'debut': 10, 'fin': 12}
    ]


autre_festival = [
    {'nom': 'A', 'debut': 10, 'fin': 14}, 
    {'nom': 'B', 'debut': 12, 'fin': 15}, 
    {'nom': 'C', 'debut': 11, 'fin': 16}, 
    {'nom': 'D', 'debut': 15, 'fin': 17}, 
    {'nom': 'E', 'debut': 13, 'fin': 21}, 
    {'nom': 'F', 'debut': 14, 'fin': 20}, 
    {'nom': 'G', 'debut': 16, 'fin': 16.5}, 
    {'nom': 'H', 'debut': 17, 'fin': 24},
    ]

#5.2

def compatibles(spectacle1, spectacle2):
    """
    spectacle1 et spectacle2 sont des dictionnaires
        {'nom': ..., 'debut':..., 'fin':...}
    Cette fonction détermine si les deux spectacles sont compatibles
    """
    a=(spectacle1["debut"],spectacle1["fin"])
    b=(spectacle2["debut"],spectacle2["fin"])
    return a[1]<=b[0] or a[0]>=b[1]

assert compatibles(s1, s2)==True
assert compatibles(s2, s1)==True
assert not compatibles(s1, s3)
assert not compatibles(s3, s1)


def tous_compatibles(selection, spectacle):
    """ 
    paramètres :
        - selection est une liste de spectacles (une liste de dictionnaires)
        - spectacle est un dictionnaire
            {'nom': ..., 'debut':..., 'fin':...}
    Cette fonction détermine si spectacle est compatible avec 
    tous les spectacles de la sélection """
    res=True
    for spec in selection:
        a=compatibles(spec,spectacle)
        if a==False:
            res= False
    return res

assert not tous_compatibles(nikopol, {'nom': '', 'debut': 17, 'fin': 19})
assert tous_compatibles(nikopol, {'nom': '', 'debut': 4, 'fin': 5})==True

# =====================================================================
#5.4 - Algo glouton (heure debut)
# =====================================================================
def heure_debut(spectacle):
    return spectacle["debut"]

def tri_selon_debut(programme):
    """ trie les spectacles du programme selon leur heure de début """
    return sorted(programme, key=heure_debut)

assert tri_selon_debut(autre_festival) == [{'nom': 'A', 'debut': 10, 'fin': 14},
    {'nom': 'C', 'debut': 11, 'fin': 16}, {'nom': 'B', 'debut': 12, 'fin': 15},
    {'nom': 'E', 'debut': 13, 'fin': 21}, {'nom': 'F', 'debut': 14, 'fin': 20},
    {'nom': 'D', 'debut': 15, 'fin': 17}, {'nom': 'G', 'debut': 16, 'fin': 16.5},
    {'nom': 'H', 'debut': 17, 'fin': 24}]

def prochain_spectacle(programme, heure = 5):
    """ 
    'programme' est un programme dont les spectacles sont triés par heure de début croissante
    Cette fonction renvoie le premier spectacle qui commence après l'heure indiquée. 
    """
    for spec in programme:
        if spec["debut"]>=heure:
            return spec
    return None

exemple_trié = tri_selon_debut(autre_festival)
assert prochain_spectacle(exemple_trié, 5) == {'nom': 'A', 'debut': 10, 'fin': 14}
assert prochain_spectacle(exemple_trié, 12.4) == {'nom': 'E', 'debut': 13, 'fin': 21}
assert prochain_spectacle(exemple_trié, 18) is None


def selection1(programme):
    """
    Renvoie la sélection de spectacles proposée par l'algorithme glouton
    qui consiste à choisir, à chaque étape, le spectable compatible
    avec la sélection dont l'heure de début est la plus petite
    """
    res=[]
    a=True
    heure=0
    programme_trie=tri_selon_debut(programme)
    while a==True:
        spectacle=prochain_spectacle(programme_trie,heure)
        if spectacle!=None:
            res.append(spectacle)
            heure=spectacle["fin"]
        else:
            a=False
    return res

proposition1 = selection1(nikopol)
print("Proposition 1 : ", proposition1)

# =====================================================================
#5.5 - Algo glouton (durée)
# =====================================================================

s1 = {'nom': 'JL Aubert', 'debut': 8, 'fin': 10}
s2 = {'nom': '2Be3', 'debut': 11, 'fin': 15}
s3 = {'nom': 'Tyko Moon', 'debut': 5, 'fin': 11}

def plus_petit_dure(spectacle):
    return spectacle["fin"]-spectacle["debut"]

def tri_selon_duree(programme):
    """ trie les spectacles du programme selon leur heure de début """
    return sorted(programme, key=plus_petit_dure)

assert tri_selon_duree(autre_festival) == [{'nom': 'G', 'debut': 16, 'fin': 16.5},
    {'nom': 'D', 'debut': 15, 'fin': 17}, {'nom': 'B', 'debut': 12, 'fin': 15},
    {'nom': 'A', 'debut': 10, 'fin': 14}, {'nom': 'C', 'debut': 11, 'fin': 16},
    {'nom': 'F', 'debut': 14, 'fin': 20}, {'nom': 'H', 'debut': 17, 'fin': 24},
    {'nom': 'E', 'debut': 13, 'fin': 21}]


def prochain_spectacle(programme, selection):
    """ 
    'programme' est un programme dont les spectacles sont triés (selon un certain critère)
    Cette fonction renvoie le premier spectacle compatible avec tous les autres spactacles de la sélection
    """
    for spectacle in programme:
        if tous_compatibles(selection, spectacle):
            return spectacle


autre_festival = [
    {'nom': 'A', 'debut': 10, 'fin': 14}, 
    {'nom': 'B', 'debut': 12, 'fin': 15}, 
    {'nom': 'C', 'debut': 11, 'fin': 16}, 
    {'nom': 'D', 'debut': 15, 'fin': 17}, 
    {'nom': 'E', 'debut': 13, 'fin': 21}, 
    {'nom': 'F', 'debut': 14, 'fin': 20}, 
    {'nom': 'G', 'debut': 16, 'fin': 16.5}, 
    {'nom': 'H', 'debut': 17, 'fin': 24},
    ]
   
exemple_trié = tri_selon_duree(autre_festival)

ma_selection = []
assert prochain_spectacle(exemple_trié, ma_selection) == {'fin': 16.5, 'debut': 16, 'nom': 'G'}
ma_selection = [{'nom': 'G', 'debut': 16, 'fin': 16.5}]
assert prochain_spectacle(exemple_trié, ma_selection) == {'nom': 'B', 'fin': 15, 'debut': 12}
ma_selection = [{'nom': 'G', 'debut': 16, 'fin': 16.5}, {'debut': 16, 'fin': 16.5, 'nom': 'G'}]
assert prochain_spectacle(exemple_trié, ma_selection) == {'debut': 12, 'nom': 'B', 'fin': 15}


def selection2(programme,heure_arrivee):
    """
    Renvoie la sélection de spectacles proposée par l'algorithme glouton
    qui consiste à choisir, à chaque étape, le spectable compatible
    avec la sélection qui a la durée la plus courte
    """
    res=[]
    a=True
    heure=heure_arrivee
    programme_trie=tri_selon_duree(programme)
    while a==True:
        spectacle=prochain_spectacle(programme_trie,res)
        if spectacle!=None:
            res.append(spectacle)
            heure=spectacle["fin"]
        else:
            a=False
    return res    

proposition2 = selection2(nikopol,0)
print("Proposition 2 : ", proposition2)


# =====================================================================
#5.6 et 4.6 - Algo glouton (heure de fin)
# =====================================================================
def heure_de_fin(spectacle):
    return spectacle["fin"]

def tri_selon_fin(programme):
    """ trie les spectacles du programme selon leur heure de début """
    return sorted(programme, key=heure_de_fin)

# un test
assert tri_selon_fin(autre_festival) == [{'nom': 'A', 'debut': 10, 'fin': 14},
    {'nom': 'B', 'debut': 12, 'fin': 15}, {'nom': 'C', 'debut': 11, 'fin': 16},
    {'nom': 'G', 'debut': 16, 'fin': 16.5}, {'nom': 'D', 'debut': 15, 'fin': 17},
    {'nom': 'F', 'debut': 14, 'fin': 20}, {'nom': 'E', 'debut': 13, 'fin': 21},
    {'nom': 'H', 'debut': 17, 'fin': 24}]


def selection3(programme):
    """
    Renvoie la sélection de spectacles proposée par l'algorithme glouton
    qui consiste à choisir, à chaque étape, le spectable compatible
    avec la sélection qui dont l'heure de fin est la plus petite
    """    
    pass
    
# proposition3 = selection3(nikopol)
# print("Proposition 3 : ", proposition3)

# =====================================================================
# Algo glouton (forme plus générale)
# =====================================================================

def selection(programme, critere_de_tri):
    """
    Renvoie la sélection de spectacles proposée par l'algorithme glouton
    qui consiste à choisir, à chaque étape, le spectable compatible
    avec la sélection qui est le plus petit au sens du critere_de_tri 
    passé en paramètre
    
    """
    pass
    

# print("Proposition 1 : ", selection(nikopol, tri_selon_debut))
# print("Proposition 2 : ", selection(nikopol, tri_selon_duree))
# print("Proposition 3 : ", selection(nikopol, tri_selon_fin))


# =====================================================================
# pere Noel
# =====================================================================

train = {"nom": "train", "taille": 18}
nours = {"nom": "peluche", "taille": 48}
velo = {"nom": "velo", "taille": 25}
stylo = {"nom": "stylo", "taille": 1}
console = {"nom": "console", "taille": 5}

cadeaux_2020 = [train, nours,  velo, stylo, console]
def addition_taille(cadeaux, taille):
    if taille+cadeaux["taille"]<=50:
        return True
    else:
        return False

def range_betement(liste_des_cadeaux):
    """
    renvoie le traineau contenant tous les cadeaux de la liste rangés dans des malles
    la contenance des malles étant de 50 max
    """
    res=[]
    malle=[]
    taille=0
    for cadeaux in liste_des_cadeaux:
        if addition_taille(cadeaux,taille)==True:
            malle.append(cadeaux)
            taille=taille+cadeaux["taille"]
        else:
            res.append(malle)
            malle=[]
            taille=0
            malle.append(cadeaux)
            taille=taille+cadeaux["taille"]
    res.append(malle)
    return res

assert range_betement(cadeaux_2020) == [[train], [nours], [velo, stylo, console]]

def plus_grand(cadeaux):
    return cadeaux["taille"]

def trie_liste(liste_des_cadeaux):
    a=sorted(liste_des_cadeaux, key=plus_grand)
    a.reverse()
    return a

def malle_compatible(taille,cadeaux):
    if taille+cadeaux["taille"]<=50:
        return True
    else:
        return False

def malle(liste_des_cadeaux, taille):
    for cadeaux in liste_des_cadeaux:
        if malle_compatible(taille,cadeaux):
            return cadeaux
    return None
def range_traineau(liste_des_cadeaux):
    """
    renvoie le traineau contenant tous les cadeaux de la liste rangés dans des malles
    la contenance des malles étant de 50 max
    """
    res=[]
    w=[]
    taille=0
    a=True
    liste_des_cadeaux_triee=trie_liste(liste_des_cadeaux)
    while liste_des_cadeaux_triee!=[]:
        cadeaux=malle(liste_des_cadeaux_triee,taille)
        if cadeaux!=None:
            w.append(cadeaux)
            taille=taille+cadeaux["taille"]
            liste_des_cadeaux_triee.remove(cadeaux)
        else:
            res.append(w)
            w=[]
            taille=0
            w.append(cadeaux)
    res.append(w)
    return res