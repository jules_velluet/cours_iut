# =====================================================================
# Structures de données - Feuille d'exercices n°7
# =====================================================================


# =====================================================================
# Tri fusion
# =====================================================================

def divise_en_singletons(liste):
    """
    param : une liste non vide de nombres
    renvoie une liste de listes à un élément, contenant chaque élément de liste
    """
    res=[]
    for nb in liste:
        b=[]
        b.append(nb)
        res.append(b)
    return res

assert divise_en_singletons([6, 4, 2, 7, 1]) == [[6], [4], [2], [7], [1]]

def fusionne(liste1, liste2):
    """
    param: deux listes triées dans l'ordre croissant
    renvoie la liste triée composée de tous les éléments de liste1 et liste2
    """
    res = []
    i1 = 0
    i2 = 0
    while i1 < len(liste1) and i2 < len(liste2):
        elem1 = liste1[i1]
        elem2 = liste2[i2]
        if elem1 < elem2:
            res.append(elem1)
            i1+=1
        else:
            res.append(elem2)
            i2+=1
    res.extend(liste1[i1:])
    res.extend(liste2[i2:])
    return res

assert fusionne([5, 8, 9, 10], [1, 2, 3, 5, 17, 18]) == [1, 2, 3, 5, 5, 8, 9, 10, 17, 18]

def fusionne_sous_listes(liste_de_listes):
    """
    param: une liste de listes de listes. Les listes internes sont triées dans
    l'ordre croissant.
    résultat : une nouvelle liste de listes, obtenues en fusionnant 
    liste_de_listes[0] avec     liste_de_listes[1] pour la première, 
    liste_de_listes[2] avec liste_de_listes[3] pour la deuxième, etc.
    Si len(liste_de_listes) est impair, alors la dernière sous-liste est copiée
    dans le résultat en dernière position.
    """
    res=[]
    i=0
    j=1
    while i<len(liste_de_listes) and j<len(liste_de_listes):
            a=fusionne(liste_de_listes[i],liste_de_listes[j])
            res.append(a)
            i=i+2
            j=j+2
    if i==len(liste_de_listes)-1:
        res.append(liste_de_listes[i])
    return res

assert fusionne_sous_listes([[4, 6], [2], [5, 7], [1, 3], [0, 10, 16],[2,4]]) == [[2, 4, 6], [1, 3, 5, 7], [0,2,4, 10, 16]]


def tri_fusion(liste):
    if liste == []:
        return []
    sous_listes = divise_en_singletons(liste)
    while len(sous_listes) > 1:
        # ICI
        sous_listes = fusionne_sous_listes(sous_listes)
    return sous_listes[0]
    
assert tri_fusion([4, 5, 1, 3, 1]) == [1, 1, 3, 4, 5]
assert tri_fusion([4, 7, 1, 3, 9, 2, 3, 3]) == [1, 2, 3, 3, 3, 4, 7, 9]


# =====================================================================
# Pour réviser
# =====================================================================

exemple = {'neon ': 'Gaz', 'fer ': 'Solide', 'helium': 'Gaz'}

def frequences(dico_elements):
    res=dict()
    for element in dico_elements.values():
        if element in res.keys():
            res[element]+=1
        else:
            res[element]=1
    return res

assert frequences(exemple) == {'Gaz': 2, 'Solide': 1}


def etat_min(dico_elements): # version 1
    aux=None
    a=frequences(dico_elements)
    for (element,occcurence) in a.items():
        if aux is None or occcurence<aux:
            res=element
            aux=occcurence
    return res

assert etat_min(exemple) == 'Solide'


def etat_min(dico_elements): # version 2
    freq = frequences(dico_elements)
    def get_val(key):
        return freq[key]
    return min(freq, key = get_val)

assert etat_min(exemple) == 'Solide'

# =====================================================================
# S'entrainer pour l'évaluation
# =====================================================================


def inverse(dictionnaire):
    """
    param : un dictionnaire dont les valeurs sont des ensembles
    revoie un dictionnaire dont les clés et les valeurs sont inversées
    """
    res=dict()
    for (clés,valeur) in dictionnaire.items():
        for val in valeur:
            if val not in res.keys():
                res[val]={clés}
            else:
                res[val].add(clés)
    return res

dico1 = {1:{'a', 'b', 'd'}, 2:{'b', 'z'}, 3:{'b', 'd'}}
dico2 = {'b': {1, 2, 3}, 'a': {1}, 'd': {1, 3}, 'z': {2}}
inverse(dico1) == dico2 
inverse(dico2) == dico1 

dico3 = {'a':{'a', 'b', 'd'}, 'b':{'b', 'z'}, 'c':{'b', 'd'}}
dico4 = {'b': {'a', 'b', 'c'}, 'a': {'a'}, 'd': {'a', 'c'}, 'z': {'b'}}
inverse(dico3) == dico4
inverse(dico4) == dico3

# =====================================================================
# Ecosystème
# =====================================================================

ecosysteme_1 = { 'Loup': 'Mouton', 'Mouton':'Herbe', 'Dragon':'Lion', 'Lion':'Lapin', 'Herbe':None, 'Lapin':'Carotte', 'Requin':'Surfer'}
ecosysteme_2 = { 'Renard':'Poule', 'Poule':'Ver de terre', 'Ver de terre':'Renard', 'Ours':'Renard' }
eco = {1:2, 2:3, 3:4, 4:5, 5:17, 6:4, 7:6, 8:9, 9:10, 10:11, 11:8}


def extinction_immédiate(ecosysteme):
    """
    Renvoie l'ensemble des espèces qui ne peuvent pas survivre
    dans cet écosystème, c-a-d les espèces dont l'alimentation
    principale n'est ni None, ni présente dans l'écosystème
    """
    res=set()
    for (animaux,manger) in ecosysteme.items():
        if manger not in ecosysteme.keys() and manger!=None:
            res.add(animaux)
    return res

assert extinction_immédiate(ecosysteme_1)=={'Lapin', 'Requin'}
assert extinction_immédiate(ecosysteme_2) == set()

def especes_en_voie_disparition(ecosysteme):
    """
    Renvoie l'ensemble des espèces qui sont en voie de disparition
    """
    res=extinction_immédiate(ecosysteme)
    i=0
    while i!=1000:
        for (animaux,manger) in ecosysteme.items():
            if manger in res:
                res.add(animaux)
        i=i+1
    return res
    
assert especes_en_voie_disparition(ecosysteme_1) == {'Lapin', 'Requin', 'Lion', 'Dragon'}
assert especes_en_voie_disparition(ecosysteme_2) == set()
assert especes_en_voie_disparition(eco) == {1, 2, 3, 4, 5, 6, 7}


# =====================================================================
# Naturalistes en herbe
# =====================================================================

print("N'oubliez pas que vous avez toujours le droit d'écrire des fonctions auxilliaires ;) !")

regime_alimentaire_exemple = {
    'Requin':{'Nageur', 'Sac Plastique', 'Poisson'},
    'Nageur':{'Poisson', 'Noisette', 'Pizza'},
    'Lion':{'Gazelle'},
    'Ecureuil':{'Noisette'},
    'Prof':{'Poisson', 'Pizza', 'Noisette'}
}
nourriture_disponible_exemple = {
    'Ocean':{'Poisson', 'Sac Plastique', 'Nageur'},
    'Savane':{'Gazelle'},
    'Jardin avec piscine':{'Nageur', 'Noisette', 'Pizza'},
}

def peutSurvivre(regimeAlimentaire,nourritureDisponible):
    """
    Paramètre : un dictionnaire regimeAlimentaire={animal:liste des aliments}
    et un dictionnaire  nourritureDisponible={lieu:liste des aliments}
    Renvoie un dictionnaire {lieu:ensemble des animaux qui peuvent survivre dans ce lieu}
    """
    res=dict()
    for (milieu,bouffe) in nourritureDisponible.items():
        res[milieu]=set()
        for (animaux,regime) in regimeAlimentaire.items():
            for manger in regime:
                if manger in bouffe:
                    res[milieu].add(animaux)
    return res

assert peutSurvivre(regime_alimentaire_exemple,nourriture_disponible_exemple)== {'Ocean': {'Nageur', 'Prof', 'Requin'}, 'Savane': {'Lion'}, 'Jardin avec piscine': {'Nageur', 'Prof', 'Ecureuil', 'Requin'}}
