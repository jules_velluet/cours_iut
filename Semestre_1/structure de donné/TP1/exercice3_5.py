def note_pouri(listeNotes):
    note_avant=listeNotes[0]
    for i in range(len(listeNotes)):
        if listeNotes[i]<note_avant:
            res=i
            note_avant=listeNotes[i]
    return res            


def moyennePonderee(listeNotes, listeCoeff):
    """
    paramètres: listeNotes et listeCoeff sont deux listes de nombres
    résultat: la moyenne pondérée des notes (float)
    """
    note_enleve=note_pouri(listeNotes)
    listeNotes.pop(note_enleve)
    listeCoeff.pop(note_enleve)
    sommePonderee = 0
    sommeCoeff = 0
    for i in range(len(listeNotes)):
        sommePonderee+= listeNotes[i] * listeCoeff[i]
        sommeCoeff+= listeCoeff[i]
    return sommePonderee/sommeCoeff

print(moyennePonderee([14,11,10,12],[2,2,2,20]))