# =====================================================================
# Structures de données - Feuille d'exercices n°1
# =====================================================================
# Petit problème : mot de passe
# =====================================================================
# Codé par Anakin, jeune padawan de l'informatique

def longueur_mdp(mot_de_passe):
    """
    parametre: un mdp de type str
    resultat: true si longueur superieur a 8 false sinon
    """
    if len(mot_de_passe)<8:
        longueur_mdp=False
    else:
        longueur_mdp=True
    return longueur_mdp

assert longueur_mdp("youpi")==False, "pb"
assert longueur_mdp("sddc'estcool")==True, "pb"

def chiffre_ok(mot_de_passe):
    '''
    :param mot_de_passe: str
    :return:s'il existe bien un chiffre dans le mot de passe
    '''
    for lettre in mot_de_passe:
        if lettre.isdigit():
            return True
    return False

assert chiffre_ok("azerty")==False, "pb"
assert chiffre_ok("azerty123456789")==True, "pb"

def sans_espace(mot_de_passe):
    """
    parametre: un mdp, un str
    r�sultat: True si pas d'espace et False sinon
    """
    sans_espace=True
    for lettre in mot_de_passe:
        if lettre==" ":
            sans_espace=False
    return sans_espace

assert sans_espace("azerty")==True, "pb"
assert sans_espace("azer ty")==False, "pb"

def majuscule(mot_de_passe):
    """
    parametre: un mdp, un str
    resultat: True si mdp contient au moins une majuscule et faux sinon
    """
    i=0
    majuscule_ok=False
    while i<len(mot_de_passe) and majuscule_ok==False:
        if mot_de_passe[i].lower() != mot_de_passe[i]:
            majuscule_ok=True
        i=i+1
    return majuscule_ok

assert majuscule("azerty")==False, "pb"
assert majuscule("Azerty")==True, "pb"

def majuscule_consecutif(mot_de_passe):
    """
    parametre: un mdp, un str
    r�sultat: True si deux majuscule cons�cutif et false sinon
    """
    i=0
    majuscule_consecutif=False
    lettre_avant=mot_de_passe[i]
    while i<len(mot_de_passe) and majuscule_consecutif==False:
        if mot_de_passe[i].lower() != mot_de_passe[i] and lettre_avant.lower()!=lettre_avant and i!=0:
            majuscule_consecutif=True
        lettre_avant=mot_de_passe[i]
        i=i+1
    return majuscule_consecutif

assert majuscule_consecutif("Azerty")==False, "pb" 
assert majuscule_consecutif("AZrtyu")==True, "pb"

def ponctuation(mot_de_passe):
    """
    parametre:  un mdp, un str
    resultat: True si mdp contient ponctuation False sinon
    """
    i=0
    ponctuation_ok=False
    ponctuation=[".",",",";",":","?","!","...","(",")","[","]"]
    while i<len(mot_de_passe) and ponctuation_ok==False:
        if mot_de_passe[i] in ponctuation:
            ponctuation_ok=True
        i=i+1
    return ponctuation_ok

assert ponctuation("azerty")==False, "pb"
assert ponctuation("azert.y")==True, "pb"

def affichage(longueur_mdp, chiffre_ok, sans_espace,majuscule,majuscule_consecutif,ponctuation):
    """paramètre: longueur_ok, chiffre_ok, sans_espace des booléens
       résultat: Si le mot de passe est correct retourne True sinon False"""
    mot_de_passe_correct = False
    if not longueur_mdp:
        print("Votre mot de passe doit comporter au moins 8 caractères")
    elif not chiffre_ok:
        print("Votre mot de passe doit comporter au moins un chiffre")
    elif not sans_espace:
        print("Votre mot de passe ne doit pas comporter d'espace")
    elif not majuscule:
        print("Votre mot de passe doit comporter au moins une majuscule")
    elif majuscule_consecutif==True:
        print("Votre mot de passe ne doit pas comporter deux majuscules cons�cutif")
    elif ponctuation==True:
        print("Votre mot de passe ne doit pas comporter de ponctuation")
    else:
        mot_de_passe_correct = True
    return mot_de_passe_correct

def dialogue_mot_de_passe():
    """
    parametre: ce que l'utilisateur entre
    r�sultat: votre mot de passe est correct
    """
    mot_de_passe_bon=False
    while not mot_de_passe_bon:
        mot_de_passe = input("Entrez votre mot de passe : ")
        bon=affichage(longueur_mdp(mot_de_passe),chiffre_ok(mot_de_passe),sans_espace(mot_de_passe),majuscule(mot_de_passe),majuscule_consecutif(mot_de_passe),ponctuation(mot_de_passe))
        if bon==True:
            mot_de_passe_bon=True
    print("Votre mot de passe est correct")
    return mot_de_passe_bon
print(dialogue_mot_de_passe())

# =====================================================================
# Reconnaître les briques de base pour écrire des algorithmes
# =====================================================================

def les_legers(pokedex):
    """
    cette fonction retourne le nom des pokemon dont leur poid est inf�rieur � 10
    param�tre: une liste de tuple
    r�sultat: une liste
    fonction min/max
    """
    legers = list()
    for (pokemon, poids) in pokedex:
        if poids < 10:
            legers.append(pokemon)
    return legers

assert les_legers([("Mew", 4), ("M�lo", 3), ("Draco", 16), ("Xatu", 15)])==["Mew","M�lo"], "pb"
assert les_legers([("a",0),("b",45)])==["a"], "pb"

def poids_total(pokedex):
    """
    cette fonction retourn le poid du dernier pokemon
    param�tre: une liste de tuple
    resultat: un int
    fonction de cumuls
    """
    total = 0
    for ( _ , poids) in pokedex:
        total+= poids
    return poids

assert poids_total([("Mew", 4), ("M�lo", 3), ("Draco", 16), ("Xatu", 15)])==15, "pb"
assert poids_total([("a",0),("b",45)])==45, "pb"

def contient_pika(pokedex):
    """
    cette fonction dit si Pikachu est dans notre pokedex
    param�tre: une liste de tuple
    resultat: un boolen
    fonction de v�rification
    """
    for (pokemon, _ ) in pokedex:
        if pokemon == "Pikachu":
            return True
    return False

assert contient_pika([("Mew", 4), ("M�lo", 3), ("Draco", 16), ("Xatu", 15)])==False, "pb"
assert contient_pika([("a",0),("Pikachu",45)])==True, "pb"

def le_plus_lourd(pokedex):
    """
    cette dit quel pokemon est le plus lourd
    param�tre: une liste de tuple
    r�sultat: un str
    fonction de verification
    """
    (nom_lourd, poids_lourd)= pokedex[0]
    for (pokemon, poids) in pokedex:
        if poids > poids_lourd:
            poids_lourd = poids
            nom_lourd = pokemon
    return nom_lourd

assert le_plus_lourd([("Mew", 4), ("M�lo", 3), ("Draco", 16), ("Xatu", 15)])=="Draco", "pb"
assert le_plus_lourd([("a",0),("b",45)])=="b", "pb"

def bien_ranger(pokedex):
    """
    cette fonction dit si les pokemon sont classe du plus leger ou plus lourd
    param�tre: une liste de tuple
    resultat: un boolen
    fonction de v�rification
    """
    for i in range(1,len(pokedex)):
        ( _ , poids1) = pokedex[i-1]
        ( _ , poids2) = pokedex[i]
        if poids1 > poids2:
             return False
    return True

assert bien_ranger([("Mew", 4), ("M�lo", 3), ("Draco", 16), ("Xatu", 15)])==False, "pb"
assert bien_ranger([("a",0),("b",45)])==True, "pb"

#exercice 5: question1
#a=["Mew","Melo"] b=15 c=False d="Draco" e=False

# =====================================================================
# Savoir choisir la bonne structure de données
# =====================================================================

# MODELISATION 1
courses_morticia = ["bave de crapeau", "oeufs de dragon", 
                    "sang de lézard", "ketchup", "sel"]
facture_morticia = [24, 157, 17, 2, 1]
courses_gomez = ["chaussures de tango", "couteau", 
                 "explosif", "sumac veneneux"]
facture_gomez = [247, 15, 167, 27]

# =====================================================================
# MODELISATION 2

courses_morticia = ["bave de crapeau", 24, "oeufs de dragon", 157,
                    "sang de lézard", 17, "ketchup", 2, "sel", 1]
courses_gomez = ["chaussures de tango", 247, "couteau", 15, 
                 "explosif", 167, "sumac veneneux", 27]
# =====================================================================
# MODELISATION 3
courses_morticia = [("bave de crapeau", 24), ("oeufs de dragon", 157),
                    ("sang de lézard", 17), ("ketchup", 2), ("sel", 1)]
courses_gomez = [("chaussures de tango", 247), ("couteau", 15), 
                 ("explosif", 167), ("sumac veneneux", 27)]

def montant_total(courses):
    """
    parametre: une liste de tuple(str,int)
    r�sutat: un int
    """
    prix_des_courses=0
    for (_,prix) in courses:
        prix_des_courses=prix_des_courses+prix
    return prix_des_courses

assert montant_total(courses_morticia)==201, "pb"
assert montant_total(courses_gomez)==456, "pb"

def article_moins_cher(courses):
    article_moins_cher=courses[0][1]
    for (article,prix) in courses:
        if prix<article_moins_cher:
            res=article
            article_moins_cher=prix
    return res

assert article_moins_cher(courses_morticia)=="sel", "pb"
assert article_moins_cher(courses_gomez)=="couteau", "pb"

def v_2_total(article, prix):
    prix_des_courses=0
    for i in range(len(article)):
        prix_des_courses=prix_des_courses+prix[i]
    return prix_des_courses

assert v_2_total(["bave de crapeau", "oeufs de dragon","sang de lézard", "ketchup", "sel"],[24, 157, 17, 2, 1])==201, "pb"
assert v_2_total(["chaussures de tango", "couteau", "explosif", "sumac veneneux"],[247, 15, 167, 27])==456, "pb"

def v_2_moins_cher(article, prix):
    article_moins_cher=prix[0]
    for i in range(len(article)):
        if prix[i]<article_moins_cher:
            res=article[i]
            article_moins_cher=prix[i]
    return res

assert v_2_moins_cher(["bave de crapeau", "oeufs de dragon","sang de lézard", "ketchup", "sel"],[24, 157, 17, 2, 1])=="sel", "pb"
assert v_2_moins_cher(["chaussures de tango", "couteau", "explosif", "sumac veneneux"],[247, 15, 167, 27])=="couteau", "pb"