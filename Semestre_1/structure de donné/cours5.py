# =====================================================================
# Exercice 1 : échauffement
# =====================================================================

# pokemon = (nom, attaque, defense, poids)
crustabri = ("Crustabri", 5, 8, 132.5)
nosferapti  = ("Nosferapti", 2, 2, 7.5)
papilusion = ("Papilusion", 4, 2, 32.0)
paras=("Paras", 4, 3, 5.4)
doduo = ("Doduo", 4, 2, 39.2)

def get_attaque(pokemon):
    """ 
    parametre : pokemon = (nom, attaque, defense, poids)
    renvoie la valeur d'attaque du pokemon 
    """
    return pokemon[1]

crustabri = ("Crustabri", 5, 8, 132.5)
assert get_attaque(crustabri) == 5

def longueur_du_nom(pokemon):
    """ 
    parametre : pokemon = (nom, attaque, defense, poids)
    renvoie la longueur du nom du pokemon
    """
    return len(pokemon[0])

nosferapti  = ("Nosferapti", 2, 2, 7.5)
assert longueur_du_nom(nosferapti) == 10

def force(pokemon):
    """ 
    parametre : pokemon = (nom, attaque, defense, poids)
    renvoie la force du pokemon
    force = attaque + defense 
    """
    res=pokemon[1]+pokemon[2]
    return res

papilusion = ("Papilusion", 4, 2, 32.0)
assert force(papilusion) == 4 + 2

print(" Super ! Maintenant que tu es échauffé.e, on va pouvoir passer aux choses sérieuses")


# =====================================================================
# Exercice 2 : Avoir un bon cardio
# =====================================================================


def meilleure_attaque(liste_de_pokemons):
  """
  param: une liste non vide de pokemons (nom, att, def, poids)
  renvoie le pokemon qui a la meilleure attaque
  """
  res=None
  for (nom,att,defense,poids) in liste_de_pokemons:
    if res is None or att>att_meil:
      res=(nom,att,defense,poids)
      att_meil=att
  return res

exemple = [nosferapti, papilusion, crustabri]
assert meilleure_attaque(exemple) == ("Crustabri", 5, 8, 132.5)


print(" Génial ! Tu vas pouvoir faire l'exercice 3 maintenant :)")



# =====================================================================
# Exercice 3 : Double dose
# =====================================================================

def nom_le_plus_court(liste_de_pokemons):
  """
  param: une liste non vide de pokemons (nom, att, def, poids)
  renvoie le pokemon qui a le nom le plus court
  """
  res=None
  for pokemon in liste_de_pokemons:
    a=longueur_du_nom(pokemon)
    if res is None or a<a_avant:
      res=pokemon
      a_avant=a
  return res

exemple = [nosferapti, paras, crustabri]
nom_le_plus_court(exemple) == ("Paras", 4, 3, 5.4)

def le_plus_faible(liste_de_pokemons):
  """
  param: une liste non vide de pokemons (nom, att, def, poids)
  renvoie le pokemon qui a la force la plus petite
  """
  le_min=liste_de_pokemons[0]
  for poke in liste_de_pokemons:
    if force(poke)<force(le_min):
      le_min=poke
  return le_min

exemple = [nosferapti, paras, crustabri]
assert le_plus_faible(exemple) == ("Nosferapti", 2, 2, 7.5)

print(" Parfait ! Et maintenant, on passe aux choses sérieuses : les neurones vont chauffer !")


# =====================================================================
# Exercice 4 : Refactorisation du code
# =====================================================================


def nom_le_plus_court(liste_de_pokemons):
  """
  param: une liste non vide de pokemons (nom, att, def, poids)
  renvoie le pokemon qui a le nom le plus court
  """
  return min(liste_de_pokemons, key= longueur_du_nom)

exemple = [nosferapti, paras, crustabri]
assert nom_le_plus_court(exemple) == ("Paras", 4, 3, 5.4)

def le_plus_faible(liste_de_pokemons):
  """
  param: une liste non vide de pokemons (nom, att, def, poids)
  renvoie le pokemon qui a la force la plus petite
  """
  return min(liste_de_pokemons, key=force)
  
exemple = [nosferapti, paras, crustabri]
assert le_plus_faible(exemple) == ("Nosferapti", 2, 2, 7.5)

print(" Merveilleux ! Il reste un exercice surprise ;) ")



# =====================================================================
# Exercice 5 : Trier une liste
# =====================================================================

# pokemon = (nom, attaque, defense, poids)

cru = ("Crustabri", 5, 8, 132.5)
nos  = ("Nosferapti", 2, 2, 7.5)
pap = ("Papilusion", 4, 2, 32.0)
par=("Paras", 4, 3, 5.4)
dod = ("Doduo", 4, 2, 39.2)
qwi = ("Qwilfish", 5, 3, 3.9)
tou = ("Toudoudou", 2, 1, 1.0)
pij = ("Pijako", 3, 2, 1.9)
nin = ("Ninjask", 5, 2, 12.0)
smo = ("Smogo", 3, 4, 1.0)



exemple = [cru, nos, pap, par, dod, qwi, tou, pij, nin, smo]

# trier la liste des pokemons selon l'ordre croissant de la longueur de leur nom

liste_triéee_1 = None # A compléter

assert liste_triéee_1 == [('Paras', 4, 3, 5.4), ('Doduo', 4, 2, 39.2), ('Smogo', 3, 4, 1.0), ('Pijako', 3, 2, 1.9), ('Ninjask', 5, 2, 12.0), ('Qwilfish', 5, 3, 3.9), ('Crustabri', 5, 8, 132.5), ('Toudoudou', 2, 1, 1.0), ('Nosferapti', 2, 2, 7.5), ('Papilusion', 4, 2, 32.0)]



# trier la liste des pokemons selon l'ordre décroissant de leur force

liste_triéee_2 = None # A compléter

assert liste_triéee_2 == [('Crustabri', 5, 8, 132.5), ('Qwilfish', 5, 3, 3.9), ('Paras', 4, 3, 5.4), ('Ninjask', 5, 2, 12.0), ('Smogo', 3, 4, 1.0), ('Papilusion', 4, 2, 32.0), ('Doduo', 4, 2, 39.2), ('Pijako', 3, 2, 1.9), ('Nosferapti', 2, 2, 7.5), ('Toudoudou', 2, 1, 1.0)]


print(" Amazing ! Tu as le droit à la question mystère : Quel excellent film est dissimulé dans cette vidéo ?")

