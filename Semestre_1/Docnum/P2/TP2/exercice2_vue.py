from jinja2 import Environment, FileSystemLoader # ne pas modifier
from autoroute_modele import *

# ne modifiez pas cette fonction !
def creer_html(fichier_template, fichier_sortie,**infos):
    """
    Cette fonction génère automatiquement un fichier à partir d'un
    template et d'informations
    paramètres :
        fichier_template : un fichier template (template HTML par exemple)
        fichier_sortie : le nom du fichier généré
        **infos : un nombre indéfini de paramètres qu'il suffit de nommer
    return : -
    """
    env=Environment(loader=FileSystemLoader('.'),trim_blocks=True)
    template=env.get_template(fichier_template)
    html=template.render(infos)
    f = open(fichier_sortie, 'w')
    f.write(html)
    f.close()

creer_html("exercice2_template.html",
          "exercice2.html",
          iParis=0,
          sorties=sorties,
          y=len(sorties),
          prixParis=sorti(0,sorties,prix),
          icheteaudun=10,
          prixchateaudun=sorti(10,sorties,prix),
          )