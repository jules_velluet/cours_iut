#!/usr/bin/python3
# ===========
# L'autoroute A10
# ===========
# La liste des sorties de l'autoroute A10 dans le sens Paris -> Vivonne
sorties=["Paris", "Massy-Palaiseau", "Orsay","Briis-sous-Forges","Dourdan","Allainville",
"Étampes","Allaines-Mervilliers","Janville","Chartres","Châteaudun","Artenay","Montargis",
"Sens","Pithiviers","Auxerre","Orléans-Nord", "Meung-sur-Loire""Mer","Blois","Château-Renault",
"Amboise","Tours","Sainte-Radegonde","Chambray-lès-Tours","Sorigny", "Montbazon",
"Sainte-Maure-de-Touraine","Châtellerault-Nord", "La Roche-Posay","Châtellerault-Sud",
"Naintré","Futuroscope","Chasseneuil-du-Poitou","Limoges","Poitiers-Nord","Angoulême",
"Lusignan","Vivonne"]

# La liste des prix à payer pour chaque portion de l'autoroute A10
prix=[0.5,1.8,1.1,0.9,1.4,1.2,2.3,0.8,0.1,0.5,1.9,0.2,2.9,0.5,1.1,2.0,0.7,1.4,0.7,0.7,1.2,0.6,
2.6,1.2,0.1,1.6,1.0,1.3,2.6,2.7,0.7,2.1,1.3,2.6,0.6,2.4,1.8,0]


def sorti(depart,sorti,prix):
  a = 0
  ans = [('Paris',0)]
  for i in range(0,len(sorties)):
    if (depart == sorties[0]):
      a = i
      break
 
  for i in range(a,len(sorties)-1):
    ans.append((sorties[i+1],ans[i][1]+prix[i]))
 
  return ans

print(sorti(0,sorties,prix))


def dico(sorties,prix):
  res=dict()
  for i in range(len(sorties)):
    res[sorties[i]]=sorti(i,sorties,prix)
    for j in range(i):
      res[sorties[i]].insert(0,'')
  return res