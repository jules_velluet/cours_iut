#!/usr/bin/python3
# ===========
from planetes_donnees_modele import liste_des_planetes

"""
liste_des_planetes est une liste de planètes
Chaque planète est modélisé par un tuple (nom_planete, localisation, espece)
  - nom_planete : est le nom de la planète (str)
  - localisation : est un str qui précise la zone galactique dans laquelle est située la planète.
  - espece est un str qui précise l'espèce intelligente dominante qui vit sur la planète (None s'il n'y en a pas)
  
Par exemple le tuple ('Glee Anselm', 'Bordure médiane', 'Anselmi'} modélise la planète nommé 'Glee Anselm'.
Cette planète se situe en 'Bordure médiane'
L'espèce intelligente dominante sont les `Anselmi`
"""

# liste_des_planetes.append(('Alpha du Centaure', 'Mondes du Noyau', 'Humains'),)

# ================================
# Outils de traitement des données
# ================================

""" Une liste bidon pour faire les tests"""
liste_bidon=[
    ('Aba', 'Mondes du Noyau', 'Humain'), 
    ('Bob', 'Mondes du Noyau', 'Wookie'),      
    ('Cib', 'Bordure', 'Humain'),
    ('Dub', 'Mondes du Noyau', None),
    ] 

# Niveau facile

def nb_planetes(liste_planetes, localisation_etudiée):
    """
    paramètres :
        - liste_planetes est une liste de tuples (nom,  localisation,  espèce)
        - localisation_etudiée est une chaine de caractères
    renvoie le nombre de planètes qui sont rescencées dans la localisation passée en paramètre
    """
    res=0
    for (_,localisation,_) in liste_planetes:
        if localisation==localisation_etudiée:
            res=res+1
    return res

assert nb_planetes(liste_bidon, 'Mondes du Noyau') == 3
assert nb_planetes(liste_bidon,"Bordure")==1
assert nb_planetes(liste_bidon,"Terre")==0


# Niveau intermédiaire

def les_especes(liste_planetes, localisation_etudiée):
    """
    paramètres :
        - liste_planetes est une liste de tuples (nom,  localisation,  espèce)
        - localisation_etudiée est une chaine de caractères
    renvoie l'ensemble des espèces intelligentes que l'on peut trouver dans la localisation_etudiée
    """
    res=set()
    for (_,localisation,espece) in liste_planetes:
        if localisation==localisation_etudiée:
            res.add(espece)
    return res

assert les_especes(liste_bidon, 'Mondes du Noyau') == {'Humain', 'Wookie', None}
assert les_especes(liste_bidon,"Bordure")=={"Humain"}
assert les_especes(liste_bidon,"Terre")==set()


# Niveau plus avancé

def planetes_par_espece(liste_planetes, localisation_etudiée):
    """
    paramètres :
        - liste_planetes est une liste de tuples (nom,  localisation,  espèce)
    renvoie un dictionnaire dont :
        - clé : le nom des espèces
        - valeur : les ensembles des planètes 
    """
    res=dict()
    for (nom,localisation,espece) in liste_planetes:
        if localisation==localisation_etudiée:
            if espece in res.keys():
                res[espece].add(nom)
            else:
                res[espece]={nom}
    return res


assert planetes_par_espece(liste_bidon, 'Mondes du Noyau') == {'Wookie': {'Bob'}, 'Humain': {'Aba'}, None: {'Dub'}}
assert planetes_par_espece(liste_bidon, "Bordure")=={"Humain": {"Cib"}}

# Pour la suite (Exercice 2)

def toutes_les_localisations(liste_planetes):
    """
    paramètres :
        - liste_planetes est une liste de tuples (nom,  localisation,  espèces intelligentes)
    renvoie l'ensemble des localisations de la liste de planètes 
    """
    for (_,localisation,_) in liste_planetes:
        if localisation not in res:
            res.add(localisation)
    return res

# assert toutes_les_localisations(liste_bidon) == {'Bordure', 'Mondes du Noyau'}

