#!/usr/bin/python3
# =====================
# NOM : 
# ====================
from jinja2 import Environment, FileSystemLoader

from planetes_donnees_modele import liste_des_planetes
from planetes_traitement_modele import *

# ne modifiez pas cette fonction
def creer_html(fichier_template, fichier_sortie,**infos):
    """
    Cette fonction génère automatiquement un fichier à partir d'un
    template et d'informations
    paramètres :
        fichier_template : un fichier template (template HTML par exemple)
        fichier_sortie : le nom du fichier généré
        **infos : un nombre indéfini de paramètres qu'il suffit de nommer
    return : -
    """
    env = Environment(loader=FileSystemLoader('.'),trim_blocks=True)
    template = env.get_template(fichier_template)
    html=template.render(infos)
    f = open(fichier_sortie, 'w', encoding='utf-8')
    f.write(html)
    f.close()

# ==================================================

creer_html("./planetes_template.html", "./planetes1.html",
        nb=nb_planetes(liste_des_planetes,"Mondes du Noyau"),
        espece=les_especes(liste_des_planetes,"Mondes du Noyau"),
        tableau=planetes_par_espece(liste_des_planetes,"Mondes du Noyau")
        )

creer_html("./planetes_template.html", "./planetes2.html",
        nb=nb_planetes(liste_des_planetes,"Bordure médiane"),
        espece=les_especes(liste_des_planetes,"Bordure médiane"),
        tableau=planetes_par_espece(liste_des_planetes,"Bordure médiane")
        )

