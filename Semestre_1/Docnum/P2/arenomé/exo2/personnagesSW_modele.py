#!/usr/bin/python3
# ===========
# Les données
# ===========

la_menace_fantome=["Anakin Skywalker", "Obi-Wan Kenobi", "Qui-Gon Jinn", "Padmé Amidala", 
    "Shmi Skywalker",     "Sio Bibble", "Jar Jar Binks", "Yoda", "R2-D2", "C-3PO", 
    "Boss Nass", "Capitaine Panaka",     "Sénateur Palpatine", "Chancelier Valorum", "Dark Maul"
    ]
l_attaque_des_clones=["Anakin Skywalker", "Obi-Wan Kenobi", "Padmé Amidala", "Mace Windu",
    "Comte Dooku", "Zam Wesell", "Poggle le Bref", "Boba Fett", "Jango Fett", "Taun We", 
    "Chancelier suprême Palpatine", "Dexter Jettster", "Capitaine Typho" 
    ]
la_revanche_des_sith=["Anakin Skywalker", "Dark Vador", "Padmé Amidala", "Palpatine", 
    "Obi-Wan Kenobi", "Mace Windu", "Comte Dooku", "C-3PO", "R2-D2", "Yoda", "Général Grievous", 
    "Chewbacca", "Tarfful", "Bail Organa"
    ]
un_nouvel_espoir=["Luke Skywalker", "Han Solo", "Princesse Leia Organa", "Le Grand Moff Tarkin",
    "Obi-Wan Kenobi", "C-3PO", "R2-D2", "Chewbacca", "Dark Vador", "Owen Lars", "Beru Lars"
    ]
l_empire_contre_attaque = ["Luke Skywalker", "Han Solo", "Princesse Leia Organa",
    "Lando Calrissian", "C-3PO", "R2-D2", "Dark Vador", "Chewbacca", "Obi-Wan Kenobi",
    "Boba Fett"
    ]
le_retour_du_jedi=["Luke Skywalker",  "han Solo", "Princesse Leia Organa", "Lando Calrissian",
    "C-3PO", "R2-D2","Chewbacca",  "Dark Vador", "Empereur Palpatine", "Yoda", "Obi-Wan Kenobi"
    ]
    
# ================================
# Outils de traitement des données
# ================================

def personage_present(I,II):
    perso_present_I_et_II=list()
    for perso in I:
        if perso in II:
            perso_present_I_et_II.append(perso)
    return perso_present_I_et_II
print(personage_present(la_menace_fantome,l_attaque_des_clones))

def personage_present_1_film(I,II):
    personage_present=list()
    for perso in I:
        if perso not in II:
            personage_present.append(perso)
    for perso in II:
        if perso not in I:
            personage_present.append(perso)
    return personage_present

print(personage_present_1_film(la_menace_fantome,l_attaque_des_clones))