from jinja2 import Environment, FileSystemLoader # ne pas modifier
from personnagesSW_modele import *

# ne modifiez pas cette fonction !
def creer_html(fichier_template, fichier_sortie,**infos):
    """
    Cette fonction génère automatiquement un fichier à partir d'un
    template et d'informations
    paramètres :
        fichier_template : un fichier template (template HTML par exemple)
        fichier_sortie : le nom du fichier généré
        **infos : un nombre indéfini de paramètres qu'il suffit de nommer
    return : -
    """
    env=Environment(loader=FileSystemLoader('.'),trim_blocks=True)
    template=env.get_template(fichier_template)
    html=template.render(infos)
    f = open(fichier_sortie, 'w')
    f.write(html)
    f.close()

creer_html("personnagesSW_template.html", "personnagesSW_episodes_I_et_II.html",
            titre="Personnages principaux des films Star Wars (épisodes I et II)",
            sous_titre="Personnages principaux de l'episode I et II",
            personnages_film_1=la_menace_fantome,
            num1="I",
            num2="II",
            film1="La menace fantome",
            film2="l'attaque des clone",
            liste_persos_film_1=la_menace_fantome,
            liste_persos_film_2=l_attaque_des_clones,
            perso_en_commun=personage_present(la_menace_fantome,l_attaque_des_clones),
            perso_1_fois=personage_present_1_film(la_menace_fantome,l_attaque_des_clones)
            )

creer_html("personnagesSW_template.html", "personnagesSW_episode_IV_et_V.html",
            titre="Personnages principaux des films Star Wars (épisodes IV et V)",
            sous_titre="Personnages principaux de l'episode IV et V",
            personnages_film_1=un_nouvel_espoir,
            num1="IV",
            num2="V",
            film1="Un nouvel espoir",
            film2="l'empir contre attaque",
            liste_persos_film_1=un_nouvel_espoir,
            liste_persos_film_2=l_empire_contre_attaque,
            perso_en_commun=personage_present(un_nouvel_espoir,l_empire_contre_attaque),
            perso_1_fois=personage_present_1_film(un_nouvel_espoir,l_empire_contre_attaque)
            )