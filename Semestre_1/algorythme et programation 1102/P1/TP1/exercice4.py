def fonction4(a,b,c,d):
    """foction4 calcul le nombre le plus petit des quatre
    parametre: a,b,c,d des nombres différents
    résultat: le plus petit des quatres un nombre"""

    if a<b :
        res=a
    else:
        res=b
    if c<res:
        res=c
    if d<res :
        res=d
    print(res)
    return res

assert fonction4(1,2,3,4)==1, "pb fonction4(1,2,3,4)"
assert fonction4(45,78,12,36)==12, "pb fonction4(45,78,12,36)"
print(fonction4(12,45,2,125))
