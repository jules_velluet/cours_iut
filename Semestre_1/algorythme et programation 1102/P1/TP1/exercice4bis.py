def fonction4bis(m):
    """foction4bis dit sin le mot a plus de voyelles que de consonnes
    paramètre: m un mot
    resultat: vrai ou faux: plus de voyelles
    négatif: plus de consonnes"""

    res=0
    for lettre in m:
        if lettre in "aeiouy":
            res=res+1
        else:
            res=res-1
    return res>0

assert fonction4bis('michel')==False, "pb fonction4bis(a)"
assert fonction4bis('moi')==True, "pb fonction4bis(a)"