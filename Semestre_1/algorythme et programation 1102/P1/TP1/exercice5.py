import math

def fonction5(a,b,c):
    
    """fonction 5 trouve résultat équation second degre
    paramètre: a,b,c
    résultat: si delta supérieur à zéro deux solution
    si delta égale à zéro une solution
    si delta inférieur à zéro pas de solution"""
    
    delta=b**2-4*a*c
    
    x1=0
    x2=0

    if delta>0 :
        #deux solution
        x1=(-b-math.sqrt(delta))/2*a
        x2=(-b+math.sqrt(delta))/2*a
        
    
    if delta==0 :
        #une solution
        x1=-b/(2*a)
        x2=None
    
    if delta<0:
        #pas de solution
        x1=None
        x2=None
    
    return  (x1, x2)

assert fonction5(3,-2,10)==(None, None), "pb fonction 5"
print(fonction5(5555555555555555555555555,44444444444444444444,55555555555))

