def nb_ocurrence_même_nb(liste):
    """cacul la longueur de la plus grande suite de nb consécutif
    paramétre: une liste de nombre
    résultat: un nombre entier"""
    nombre_avant=None
    res=0
    cpt=0
    for x in liste:
        if nombre_avant==None:
            nombre_avant=x

        if x==nombre_avant:
            cpt=cpt+1
        else:
            cpt=1
            nombre_avant=x

        if cpt>res:
            res=cpt            

    return res

print(nb_ocurrence_même_nb([1,1,1,3,2,2,2,2,1]))