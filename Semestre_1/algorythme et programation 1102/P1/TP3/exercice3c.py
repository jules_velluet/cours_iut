def somme_n_premierentier_paire(n):
    """cacul la somme des n premier entier paire
    parametre: n:un nombre entier
    résultat: un nombre pair"""
    
    res=0
    for x in range (n+1):
        if x%2==0:
            res=res+x
            
    return res

print(somme_n_premierentier_paire(4))
assert somme_n_premierentier_paire(4)==6, "pb"
assert somme_n_premierentier_paire(6)==12,"pb"