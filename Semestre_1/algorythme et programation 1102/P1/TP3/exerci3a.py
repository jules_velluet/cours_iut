def somme_paire(liste):
    """cette fonction calcul le somme des entiers pair d'une liste
    parametre: la liste de nombres entré
    résultat: un entier pair"""
    somme=0
    for nombre in liste:
        
        if nombre%2==0:
            somme=somme+nombre
            
    return somme

assert somme_paire([12,13,6,5,7])==18, "probleme"
assert somme_paire([45,77,69,63])==0, "probleme"
assert somme_paire([12,4,2,3,5])==18, "probleme"