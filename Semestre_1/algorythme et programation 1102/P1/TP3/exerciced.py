def nb_consécutif_lettre(mot):
    """dit si un mot contient deux fois le même caractére successivement
    parametre: le mot
    résultat: true ou false"""
    
    lettreavant=None
    res=0
    for lettre in mot:
        if lettre==lettreavant:
            res=True
        
        lettreavant=lettre
        
    if res!=True:
        res=False
    
    return res

print(nb_consécutif_lettre("greeet"))
assert nb_consécutif_lettre("gree")==True, "pb"
assert nb_consécutif_lettre("greet")==True, "pb"
assert nb_consécutif_lettre("gre")==False, "pb"