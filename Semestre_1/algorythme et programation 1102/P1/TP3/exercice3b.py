def somme_n_premier_entier(n):
    """cacul la somme des n premier entier
    paramétre: un nombre entier
    résultat: un nombre entier
    """
    res=0
    
    for x in range(n+1):
        res=res+x
        
    return res


assert somme_n_premier_entier(4) ==10, "pb"
assert somme_n_premier_entier(6) ==21, "pb"