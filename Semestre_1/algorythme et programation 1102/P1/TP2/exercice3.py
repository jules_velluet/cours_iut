def radar_contravention(v,l):
    """cette fonction donne les sanctions encourues en fonction del’excès de vitesse que l’on a commis 
    parametre: vitesse du conducteur et limitation de la route
    résultat: montant amande, nombre de point perdu et suspension ou non du permis"""

    if v<l:
        sanctions='bien joué vous avez ralentit au bon moment'
    
    if v>l and v<20+l:
        if l>50:
            sanctions='amende: 135 euros, retrait d un point sur le permis, aucune suspension'

        else:
            sanctions='amende: 68 euros, retrait d un point sur le permis, aucune suspension'

    if v>=20+l and v<30+l:
        sanctions='amende: 135 euros, retrait de deux points sur le permis, aucue suspension'

    if v>=30+l and v<40+l:
        sanctions='amende: 135 euros, retrait de trois points sur le permis, trois ans de suspension'

    if v>=40+l and v<50+l:
        sanctions='amende: 135 euros, retrait de quatre points sur le permis, trois ans de suspension'
    
    if v>=50+l:
        sanctions='amende: 1500 euros, retrait de six points sur le permis, trois ans de suspension'
    
    return sanctions

print(radar_contravention(100,50))