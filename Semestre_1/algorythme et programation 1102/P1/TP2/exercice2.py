def heure_de_la_journee(t):
   
    """Cette foction vous dit si vous êtes le jour ou la nuit et si vous vous trouvez le matin, l'aprés midi, le soir ou la nuit.
    parametre: t: l'heure qu'il fait
    résultat: une chaine de caractére"""
    
    if t<0 or t>24:
        heure="impossible"
        return heure
    if 7<=t and t<19:
       
        if t<12:
            heure='on est le matin et il fait jour'
       
        elif t<18:
            heure='on est l aprés midi et il fait jour'
        
        else:
            heure='on est le soir et il fait jour'
    
    else:
        
        if 6<=t and 
            heure='on est le matin et il fait nuit'
      
        if 18<=t and t<21:
            heure='on est le soir et il fait nuit'
      
        if 21<=t or t<6:
            heure='on est la nuit et il fait nuit'
   
    return heure

print(heure_de_la_journee(24))