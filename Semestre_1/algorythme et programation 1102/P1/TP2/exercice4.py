def qualif_jo_100m(s,v,champion,sexe):
    
    if sexe=="homme":
        
        if champion=="oui":
            qualifier="oui"
        
        else:
            
            if s<12 and v>=3:
                qualifier="oui"
            
            else:
                qualifier="non peut être l'année prochaine"
        
    else:
        
        if champion=='oui':
            qualifier="oui"
        
        else:
            
            if s<15 and v>=3:
                qualifier='oui'
            
            else:
                qualifier="non peut être l'année prochaine"
            
    return qualifier

print(qualif_jo_100m(25,3,"oui","klk"))

assert qualif_jo_100m(56,0,"oui","homme")=="oui", "probléme"

assert qualif_jo_100m(56,0,"oui","femme")=="oui", "probléme"

assert qualif_jo_100m(56,3,"non","homme")=="non peut être l'année prochaine", "probléme"

assert qualif_jo_100m(10,3,"non","homm")=="oui", "probléme"