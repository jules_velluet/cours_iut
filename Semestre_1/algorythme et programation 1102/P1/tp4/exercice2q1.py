def initialise(N):
    res=[]
    for i in range(0,N+1):
        if i<=1:
            res.append(False)
        else:
            res.append(True)
            
    return res

print(initialise(5))
assert initialise(5)==[False, False, True, True, True, True], "pb"
assert initialise(2)==[False, False, True], "pb"

def tri(x,res):
    
    for z in range(len(res)):
        if z!=x and z%x==0:
            res[z]=False
    return res
print(tri(2,initialise(5)))

def crible(N):
    res=[]
    liste=initialise(N)
    for w in range (2,N):
        liste=tri(w,liste)
        if liste[w]:
            res.append(w)
    return res
    
print(crible(99999))    