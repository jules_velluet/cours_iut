def retourne_liste_mot(phrase, mot_chercher):


    """donne la positioon d'un mot dans une chaine de caractére
    paramétre: le mot chercher et la phrase
    résultat: une liste"""
    res=[]
    mot=""
    for x in range(len(phrase)):
        if phrase[x].isalpha()==True or phrase[x]=="-":
            mot=mot+phrase[x]
        
        else:
            mot=""

        if mot == mot_chercher:
            res.append(x-(len(mot_chercher))+1)
        
    return res

assert retourne_liste_mot("le lundi, c’est le premier jour de la semaine","le")==[0,16], "pb"
assert retourne_liste_mot("Python est de plus en plus sympa avec moi","plus")==[14,22], "pb"


print(retourne_liste_mot("le lundi, c’est le premier jour de la semaine","le"))        
        
    