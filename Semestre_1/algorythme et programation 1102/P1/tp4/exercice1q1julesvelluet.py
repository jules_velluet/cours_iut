def retourne_liste_mot(phrase):
    """cette fonction change une chaine de caractére en une liste de mot
    paramétre: une chaine de caractére
    résultat: une liste de chaine de caractére"""
    
    res=[]
    mot=""
    for x in phrase:
        if x.isalpha()==True or x=="-":
            mot=mot+x
        
        else:
                           
            res.append(mot)
            mot=""

        
    res.append(mot)            
    return res

assert retourne_liste_mot("le lundi, c’est le premier jour de la semaine")==['le', 'lundi', '', 'c', 'est', 'le', 'premier', 'jour', 'de', 'la', 'semaine'], "pb"
assert retourne_liste_mot("youpi algo-prog")==['youpi', 'algo-prog'], "pb"