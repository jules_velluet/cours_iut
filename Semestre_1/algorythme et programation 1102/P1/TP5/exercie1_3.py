def trie(liste1,liste2):
    """tri dans l'ordre croissant deux listes
    paramétre: deux listes trié dans l'ordre croissant
    résultat: une liste"""
    i=0
    w=0
    res=[]
    while i<len(liste1) and w<len(liste2):
        if liste1[i]<liste2[w]:
            res.append(liste1[i])
            i=i+1
        else:
            res.append(liste2[w])
            w=w+1
    while i<len(liste1):
        res.append(liste1[i])
        i=i+1
    while w<len(liste2):
        res.append(liste2[w])
        w=w+1
    return res
print(trie([1,2,4,6,7,8,9],[2,3,5,7]))
assert trie([1,6,7,88,99],[2,5,6,77])==[1, 2, 5, 6, 6, 7, 77, 88, 99], "pb"
assert trie([1,45,78,89],[4,6,9,14,26,35])==[1, 4, 6, 9, 14, 26, 35, 45, 78, 89], "pb"