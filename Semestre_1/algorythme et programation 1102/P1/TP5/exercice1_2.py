def chaine(chaine1,chaine2):
    """compare deux chaines de caractére et les ranges dans l'ordre alphabétique
    paramétre: 2 chaine de caractére
    résultat: un nombre entier 0 -1 1"""
    
    i=0
    res=None
    while res!=1 and res!=-1 and i<len(chaine1) and i<len(chaine2):
        if chaine1[i]<chaine2[i]:
            res=1
        elif chaine1[i]==chaine2[i]:
            res=0
        else:
            res=-1
        i=i+1
    return res
print(chaine("aaaa","aaaz"))
