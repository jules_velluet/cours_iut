def plus_grand_cent(liste):
    """créé une liste constitué des qutre premier chiffre supérieur à cent a partir d'une autre liste
    paramétre: une liste de nombre entier
    résultat: une liste de 4 nombres supérieurs à cent """
    i=0
    res=[]
    while len(res)<4 and i<len(liste):
        if liste[i]>=100:
            res.append(liste[i])
        i=i+1
    return res
assert plus_grand_cent([1,2,100,2,100,145,147])==[100, 100, 145, 147], "pb"
assert plus_grand_cent([14,26,47,158,1000258963,1475989,14785,1456987])==[158, 1000258963, 1475989, 14785], "pb"
