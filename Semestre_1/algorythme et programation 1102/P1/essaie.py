COULEURS = ["PI", "CO", "CA", "TR"]
HAUTEURS = ["_1", "_2", "_3", "_4", "_5", "_6", "_7", "_8", "_9", "10", "VA", "DA", "RO"]
VALEURS = [4, 0, 0, 0, 0, 0, 0, 0, 0, 5, 1, 2, 3]

def couleur(carte):
   if carte[0:1] in COULEURS[0]:
       res="PI"
   elif carte[0:1] in COULEURS[1]:
        res="CO"
   elif carte[0:1] in COULEURS[2]:
        res="CA"
   elif carte[0:1] in COULEURS[3]:
        res="TR"
   return res

print(couleur("CA_01"))