def strmois(numMois,nomsMois):
    res=nomsMois[numMois]
    return res
assert strmois(2,['Janvier','Fevrier','Mars','avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'])=="Mars", "pb"

def ligneMois(nomsMois):
    mois=""
    res=""
    for i in nomsMois:
        mois=i
        mois_3=mois[0:3]
        res=res+mois_3+" "
    return res

def releveDuMois(nomMois,releve,mois):
    """donne le releve pour un mois precis
    parametre: les mois, les releve, et le mois choisi
    resultat: un int"""
    res=None
    i=0
    while res==None and i<len(nomMois):
        if nomMois[i]==mois:
            res=releve[i]
        i=i+1
    return res
assert releveDuMois(['Janvier','Fevrier','Mars','avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],[0,1,2,3,4,5,6,7,8,9,10,11], "Decembre")==11, "pb"

def cumulPrecipitations(releve):
    res=0
    for i in releve:
        res=res+i
    return res
assert cumulPrecipitations([52, 44, 46, 49, 64, 45, 60, 50, 50, 64, 58, 58])==640, "pb"

def DepuisJanvier(releve):
    res=[]
    releve_avant=0
    for i in releve:
        releve_avant=releve_avant+i
        res.append(releve_avant)
    return res
assert DepuisJanvier([52,44, 46, 49, 64, 45, 60, 50, 50, 64, 58, 58])==[52, 96,142, 191, 255, 300, 360, 410, 460, 524, 582, 640], "pb"

def differenciel(releve):
    res=[]
    for i in range(len(releve)):
        if i==0:
            calcul=releve[(len(releve)-1)]-releve[i]
            res.append(calcul)
        else:
            res.append(abs(releve[i-1]-releve[i]))
    return res
assert differenciel([52,44,46,49,64,45,60,50,50,64,58,58])==[6, 8, 2, 3, 15, 19, 15, 10, 0, 14, 6, 0], "pb"

def moisLePlusSec(releve,moi):
    for i in range(len(releve)):
        if i==0:
            nb_grand=releve[i]
            res_1=i
        elif nb_grand>releve[i]:
            nb_grand=releve[i]
            res_1=i
    res=moi[res_1]
    return res
assert moisLePlusSec([52, 44, 46, 49, 64, 45, 60, 50, 50, 64, 58, 58],['Janvier','Fevrier','Mars','avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'])=="Fevrier", "pb"

def moisLePlusHumide(releve,moi):
    for i in range(len(releve)):
        if i==0:
            nb_grand=releve[i]
            res_1=i
        elif nb_grand<releve[i]:
            nb_grand=releve[i]
            res_1=i
    res=moi[res_1]
    return res
assert moisLePlusHumide([52, 44, 46, 49, 64, 45, 60, 50, 50, 64, 58, 58],['Janvier','Fevrier','Mars','avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'])=="Mai", "pb"

