#!/usr/bin/python3
import unittest
import sys
from unittest.mock import patch

import cartes

class Test_tp_cartes(unittest.TestCase):
    def message(self,fun,liste_param):
        res="Pb avec l'appel "+fun.__name__+"("
        pref=" "
        for param in liste_param:
            res+=pref+str(param)
            pref=", "
        res+=" )"
        return res
    def enlever_caracteres(self,chaine,cars=' '):
        res=''
        for c in chaine:
            if c not in cars:
                res+=c
        return res
    
    def setUp(self):
        self.COULEURS = ["PI", "CO", "CA", "TR"]
        self.HAUTEURS = ["_1", "_2", "_3", "_4", "_5", "_6", "_7", "_8", "_9", "10", "VA", "DA", "RO"]
        self.VALEURS = [4, 0, 0, 0, 0, 0, 0, 0, 0, 5, 1, 2, 3]
        self.main1 = ["PI_1", "PIVA", "CORO", "CO_3", "CA_3", "CADA", "TR_3", "TR_4", "TR_5", "TRDA"]
        self.main2 = ["CA_1", "CA_3", "TRRO", "TR_4", "TR_5", "CADA", "TR_3", "CAVA", "TRDA"]
        self.main3 = ["PI_1", "PIVA", "PIRO", "CO_3", "CARO", "CADA", "TR_3", "TR_4", "TR_5"]
        self.main_triee = ["PIRO", "PIVA", "PI_1", "CO_3", "CADA", "CA_3", "TRDA", "TR_5", "TR_4", "TR_3"]
        self.main_incorrecte1 = ["CO_3", "CARO", "CADA", "bonjour", "TR_4", "TR_5"]
        self.main_incorrecte2 = ["CO_3", "CARO", "CADA", "TR_4", "TR_5", "ROCA"]
        
        self.toutes_les_cartes = []
        for coul in self.COULEURS:
            for i in range(len(self.HAUTEURS) - 1, -1, -1):
                carte = coul + self.HAUTEURS[i]
                self.toutes_les_cartes.append(carte)


    def test_get_indice(self):
        self.assertEqual(cartes.get_indice(self.COULEURS, "CO"), 1,
                         self.message(cartes.get_indice, [self.COULEURS, "CO"]))
        self.assertEqual(cartes.get_indice(self.HAUTEURS, "RO"), 12,
                         self.message(cartes.get_indice, [self.HAUTEURS, "RO"]))
        self.assertEqual(cartes.get_indice(self.HAUTEURS, "_4"), 3,
                         self.message(cartes.get_indice, [self.HAUTEURS, "_4"]))
        self.assertEqual(cartes.get_indice(self.HAUTEURS, "VE"), None,
                         self.message(cartes.get_indice, [self.HAUTEURS, "VE"]))
        self.assertEqual(cartes.get_indice([], "RO"), None,
                         self.message(cartes.get_indice, [[], "RO"]))

    def test_get_couleur(self):
        les_cartes = ["CO10", "PIRO", "CA_3", "TR_1", "TRVA"]
        les_couleurs = ["CO", "PI", "CA", "TR", "TR"]
        for i in range(len(les_cartes)):
            self.assertEqual(cartes.get_couleur(les_cartes[i]), les_couleurs[i],
                             self.message(cartes.get_couleur, [les_cartes[i]]))

    def test_get_hauteur(self):
        les_cartes = ["CO10", "PIRO", "CA_3", "TR_1", "TRVA"]
        les_hauteurs = ["10", "RO", "_3", "_1", "VA"]
        for i in range(len(les_cartes)):
            self.assertEqual(cartes.get_hauteur(les_cartes[i]), les_hauteurs[i],
                             self.message(cartes.get_hauteur, [les_cartes[i]]))


    def test_get_valeur(self):
        HAUTEURS = self.HAUTEURS
        VALEURS = self.VALEURS
        les_cartes = ["CO10", "PIRO", "CA_3", "TR_1", "TRVA"]
        les_valeurs = [5, 3, 0, 4, 1]
        for i in range(len(les_cartes)):
            self.assertEqual(cartes.get_valeur(les_cartes[i]), les_valeurs[i],
                             self.message(cartes.get_valeur, [les_cartes[i]]))

    def test_est_carte(self):
        COULEURS = self.COULEURS
        HAUTEURS = self.HAUTEURS
        les_cartes = ["CO10", "PIRO", "", "CA_3", "Bonjour", "TR_1", "VATR", "TRDA"]
        les_reponses = [True, True, False, True, False, True, False, True]
        for i in range(len(les_cartes)):
            self.assertEqual(cartes.est_carte(les_cartes[i]), les_reponses[i],
                             self.message(cartes.est_carte, [les_cartes[i]]))
        for coul in COULEURS:
            for haut in HAUTEURS:
                carte = coul + haut
                self.assertEqual(cartes.est_carte(carte), True, self.message(cartes.est_carte, [carte]))

    def test_est_jeu(self):
        COULEURS = self.COULEURS
        HAUTEURS = self.HAUTEURS
        self.assertTrue(cartes.est_jeu(self.main1), self.message(cartes.est_jeu, [self.main1]))
        self.assertTrue(cartes.est_jeu(self.main2), self.message(cartes.est_jeu, [self.main2]))
        self.assertTrue(cartes.est_jeu(self.main3), self.message(cartes.est_jeu, [self.main3]))
        self.assertTrue(cartes.est_jeu(self.main_triee), self.message(cartes.est_jeu, [self.main_triee]))
        self.assertTrue(cartes.est_jeu(self.toutes_les_cartes), self.message(cartes.est_jeu, [self.toutes_les_cartes]))
        self.assertFalse(cartes.est_jeu(self.main_incorrecte1), self.message(cartes.est_jeu, [self.main_incorrecte1]))
        self.assertFalse(cartes.est_jeu(self.main_incorrecte2), self.message(cartes.est_jeu, [self.main_incorrecte2]))
        self.assertTrue(cartes.est_jeu([]), self.message(cartes.est_jeu, [[]]))

    def test_est_superieure(self):
        COULEURS = self.COULEURS
        HAUTEURS = self.HAUTEURS
        les_cartes = ["CO10", "PIRO", "CA_3", "TR_1", "PI_2", "PI_7", "TRVA", "TR_8"]
        les_resultats = [False, True, True, False, False, True, True]
        for i in range(len(les_cartes) - 1):
            self.assertEqual(cartes.est_superieure(les_cartes[i], les_cartes[i + 1]), les_resultats[i],
                             self.message(cartes.est_superieure, [les_cartes[i], les_cartes[i + 1]]))


    def test_max_carte(self):
        COULEURS = self.COULEURS
        HAUTEURS = self.HAUTEURS
        self.assertEqual(cartes.max_carte(self.main1), "PIVA", self.message(cartes.max_carte, [self.main1]))
        self.assertEqual(cartes.max_carte(self.main2), "CADA", self.message(cartes.max_carte, [self.main2]))
        self.assertEqual(cartes.max_carte(self.main3), "PIRO", self.message(cartes.max_carte, [self.main3]))
        self.assertEqual(cartes.max_carte(self.main_triee), "PIRO", self.message(cartes.max_carte, [self.main_triee]))
        self.assertEqual(cartes.max_carte([]), None, self.message(cartes.max_carte, [[]]))

    def test_est_triee(self):
        COULEURS = self.COULEURS
        HAUTEURS = self.HAUTEURS
        self.assertEqual(cartes.est_triee(self.main1), False, self.message(cartes.est_triee, [self.main1]))
        self.assertEqual(cartes.est_triee(self.main2), False, self.message(cartes.est_triee, [self.main2]))
        self.assertEqual(cartes.est_triee(self.main3), False, self.message(cartes.est_triee, [self.main3]))
        self.assertEqual(cartes.est_triee(self.main_triee), True, self.message(cartes.est_triee, [self.main_triee]))
        self.assertEqual(cartes.est_triee(self.toutes_les_cartes), True, self.message(cartes.est_triee, [self.toutes_les_cartes]))

    def test_calcul_points(self):
        COULEURS = self.COULEURS
        HAUTEURS = self.HAUTEURS
        VALEURS = self.VALEURS
        self.assertEqual(cartes.calcul_points(self.main1), 12, self.message(cartes.calcul_points, [self.main1]))
        self.assertEqual(cartes.calcul_points(self.main2), 12, self.message(cartes.calcul_points, [self.main2]))
        self.assertEqual(cartes.calcul_points(self.main3), 13, self.message(cartes.calcul_points, [self.main3]))
        self.assertEqual(cartes.calcul_points(self.main_triee), 12, self.message(cartes.calcul_points, [self.main_triee]))
        self.assertEqual(cartes.calcul_points(self.toutes_les_cartes), 60, self.message(cartes.calcul_points, [self.toutes_les_cartes]))

    def test_extraire_couleur(self):
        self.assertEqual(cartes.extraire_couleur(self.main1, "PI"), ["PI_1", "PIVA"],
                         self.message(cartes.extraire_couleur, [self.main1, "PI"]))
        self.assertEqual(cartes.extraire_couleur(self.main1, "CO"), ["CORO", "CO_3"],
                         self.message(cartes.extraire_couleur, [self.main1, "CO"]))
        self.assertEqual(cartes.extraire_couleur(self.main1, "CA"), ["CA_3", "CADA"],
                         self.message(cartes.extraire_couleur, [self.main1, "CA"]))
        self.assertEqual(cartes.extraire_couleur(self.main1, "TR"), ["TR_3", "TR_4", "TR_5", "TRDA"],
                         self.message(cartes.extraire_couleur, [self.main1, "TR"]))
        self.assertEqual(cartes.extraire_couleur(self.main2, "PI"), [],
                         self.message(cartes.extraire_couleur, [self.main2, "PI"]))
        self.assertEqual(cartes.extraire_couleur(self.main2, "CO"), [],
                         self.message(cartes.extraire_couleur, [self.main2, "CO"]))
        self.assertEqual(cartes.extraire_couleur(self.main2, "CA"), ["CA_1", "CA_3", "CADA", "CAVA"],
                         self.message(cartes.extraire_couleur, [self.main2, "CA"]))
        self.assertEqual(cartes.extraire_couleur(self.main2, "TR"), ["TRRO",  "TR_4", "TR_5", "TR_3","TRDA"],
                         self.message(cartes.extraire_couleur, [self.main2, "TR"]))

    def test_contient_carre(self):
        self.assertEqual(cartes.contient_carre(self.main1), False, self.message(cartes.contient_carre, [self.main1]))
        self.assertEqual(cartes.contient_carre(self.main2), False, self.message(cartes.contient_carre, [self.main2]))
        self.assertEqual(cartes.contient_carre(self.toutes_les_cartes), True,
                         self.message(cartes.contient_carre, [self.toutes_les_cartes]))
        self.assertEqual(cartes.contient_carre(["COVA", "PIVA"]), False,
                         self.message(cartes.contient_carre, [["COVA", "PIVA"]]))


if __name__ == '__main__':
    unittest.main()


