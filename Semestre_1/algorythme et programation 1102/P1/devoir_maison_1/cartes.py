COULEURS = ["PI", "CO", "CA", "TR"]
HAUTEURS = ["_1", "_2", "_3", "_4", "_5", "_6", "_7", "_8", "_9", "10", "VA", "DA", "RO"]
VALEURS = [4, 0, 0, 0, 0, 0, 0, 0, 0, 5, 1, 2, 3]

def get_indice(liste_chaines, chaine):
    """
    retourne l'indice de la première occurrence de la chaine dans liste_chaines
    si chaine n'est pas dans liste_chaines, la fonction retourne None
    paramètres liste_chaines une liste de chaine de caractères
               chaine une chaine de caractères
    resultat un entier (défini ci-dessus)
    """
    indice=None #invariant
    for occu in range(len(liste_chaines)):
        if liste_chaines[occu]==chaine:
            indice=occu
    return indice
    
def get_couleur(carte):
    """
    retourne la couleur d'une carte
    paramètre: carte un str de longueur 5 représentant la carte
    résultat: un str de deux caractères représentant la couleur de la carte
    """
    if carte[0:2] in COULEURS[0]:
        res="PI"
    elif carte[0:2] in COULEURS[1]:
         res="CO"
    elif carte[0:2] in COULEURS[2]:
         res="CA"
    elif carte[0:2] in COULEURS[3]:
         res="TR"
    return res
            
def get_hauteur(carte):
    """
    retourne la hauteur d'une carte
    paramètre: carte un str de longueur 4 représentant la carte
    résultat: un str de deux caractères représentant la hauteur de la carte
    """
    res=""
    for i in HAUTEURS:
        if carte[2:4] in i:
            res=i
    return res

def get_valeur(carte):
    """
    retourne la valeur d'une carte
    paramètre: carte un str de longueur 4 représentant la carte
    résultat: un int indiquant la valeur de la carte
    """
    valeur=VALEURS[get_indice(HAUTEURS,get_hauteur(carte))]
    return valeur
    
def est_carte(carte):
    """
    indique si la chaine de caractères passées en parmètre représente bien une carte
    paramètre: carte un str que l'on doit vérifier
    résultat: True si carte représente bien une carte, False sinon
    """
    carte_ou_pas=False
    if len(carte)==4:
        if carte[0:2] in COULEURS:
            if carte[2:4] in HAUTEURS:
                carte_ou_pas=True
    return carte_ou_pas

def est_jeu(liste_cartes):
    """
    indique si une liste de cartes est bien constituée uniquement de cartes
    paramètre: liste_cartes une liste de chaines de caractères
    resultat True si liste_cartes est effectivement une liste de carte, False sinon
    """
    i=0
    x=True #invariant
    while x==True and i<len(liste_cartes):
        x=est_carte(liste_cartes[i])
        i=i+1
    return x

def est_superieure(carte1, carte2):
    """
    indique si la carte1 est supérieure ou égale à la carte2
    paramètres: carte1 et carte2 deux str représentant des cartes
    résultat: True si carte1 est supérieure ou égale à carte2
    ATTENTION! on considère que carte1 et carte2 sont bien des cartes
    Pas la peine de vérifier cela dans la fonction
    """
    couleur_carte1=get_couleur(carte1)
    couleur_carte2=get_couleur(carte2)
    hauteur_carte1=get_hauteur(carte1)
    hauteur_carte2=get_hauteur(carte2)
    if couleur_carte1==couleur_carte2 and hauteur_carte1==hauteur_carte2:
        res=True
    else:
        if couleur_carte1==couleur_carte2:
            indice_hauteur_carte1=get_indice(HAUTEURS,hauteur_carte1)
            indice_hauteur_carte2=get_indice(HAUTEURS,hauteur_carte2)
            if indice_hauteur_carte1>indice_hauteur_carte2:
                res=True
            else:
                res=False
        else:
            indice_couleur_carte1=get_indice(COULEURS,couleur_carte1)
            indice_couleur_carte2=get_indice(COULEURS,couleur_carte2)
            if indice_couleur_carte1<indice_couleur_carte2:
                res=True
            else:
                res=False
    return res
    
def max_carte(liste_cartes):
    """
    retourne la meilleure carte de la liste, None si cette liste est vide
    paramètre liste_cartes une liste de str représentant des cartes
    resultat : un str représentant la meilleure carte de la liste
    ATTENTION! On considère que toutes les chaines de liste_cartes représentent bien une carte
               Pas besoin de vérifier cela dans la fonction
    """
    if liste_cartes==[]:
        meilleur_carte=None
    else:
        for liste in range(len(liste_cartes)):
            if liste==0:
                meilleur_carte=liste_cartes[liste]
            else:
                compare_carte=est_superieure(liste_cartes[liste],meilleur_carte)
                if compare_carte==True:
                    meilleur_carte=liste_cartes[liste]
    return meilleur_carte
    
def est_triee(liste_cartes):
    """
    verifie qu'une liste de cartes est bien triee de la meilleure à la moins bonne
    paramètre liste_cartes une liste de str représentant des cartes
    resultat True si la liste est bien triée False sinon
    ATTENTION! On considère que toutes les chaines de liste_cartes représentent bien une carte
               Pas besoin de vérifier cela dans la fonction
    """
    i=1
    triee=True #invariant
    while triee==True and i<len(liste_cartes):
        triee=est_superieure(liste_cartes[i-1],liste_cartes[i])
        i=i+1
    return triee
        
    
def calcul_points(liste_cartes):
    """
    calcule la valeur d'une liste de carte
    paramètre liste_cartes une liste de str représentant des cartes
    resultat un entier donnant la valeur de la liste
    ATTENTION! On considère que toutes les chaines de liste_cartes représentent bien une carte
               Pas besoin de vérifier cela dans la fonction
    """
    valeur_liste=0 #invariant
    for carte in liste_cartes:
        hauteur_carte=get_hauteur(carte)
        indice_hauteur=get_indice(HAUTEURS,hauteur_carte)
        valeur_carte=VALEURS[indice_hauteur]
        valeur_liste=valeur_liste+valeur_carte
    return valeur_liste
    
def extraire_couleur(liste_cartes,couleur):
    """
    retourne toutes les cartes de liste_cartes qui sont de la couleur couleur
    paramètres: liste_cartes une liste de str représentant des cartes
                couleur un str représentant une couleur ("PI", "CO", "CA" ou "TR")
    resultat une liste de str contenant toutes les cartes de liste_cartes de la 
             couleur passée en paramètre 
    """
    meme_couleur=[] #invariant
    for carte in liste_cartes:
        couleur_carte=get_couleur(carte)
        if couleur_carte==couleur:
            meme_couleur.append(carte)
    return meme_couleur
    
def contient_carre(liste_cartes):
    """
    indique si une liste de cartes contient un carré c'est-à-dire les quatre 
    cartes de la même hauteur
    Paramètre : liste_cartes une liste de cartes
    Résultat: True si la liste de cartes contient 4 cartes de la même hauteur et False sinon
    """
    i=0
    hauteur_carte=[]
    carre=False #invariant
    for carte in liste_cartes:
        hauteur_carte.append(get_hauteur(carte))
    while carre==False and i<len(HAUTEURS):
        nb_occurence=hauteur_carte.count(HAUTEURS[i])
        if nb_occurence==4:
            carre=True
        i=i+1          
    return carre