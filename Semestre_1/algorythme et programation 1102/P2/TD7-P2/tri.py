################################################
## Exercice de tri                           ##
################################################

def tribulle(liste):
    """
    tri une liste en suivant l'algorithme du tri à bulle
    parametre:
    resultat: Attention cette fonction ne retourne aucun résultat mais modifie la liste
    N'oubliez le print(liste) à la fin de chaque itération de la grande boucle
    """
    for i in range(len(liste)-1):
        for j in range(len(liste)-1-i):
            if liste[j]>liste[j+1]:
                liste_2=liste[j]
                liste[j]=liste[j+1]
                liste[j+1]=liste_2
        print(liste)
    return liste


# A décommenter lorsque vous avez fini votre implémentation      
l=[15,2,78,5,34,1]
print(tribulle(l))
tribulle(l)
assert l==[1,2,5,15,34,78],"Pb Appel tribulle("+str(l)+")"
# vos tests