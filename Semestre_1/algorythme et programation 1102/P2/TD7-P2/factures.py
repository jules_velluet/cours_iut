################################################
## Exercice sur les facture                   ##
################################################

def factures(liste_commandes):
    """parametre: une liste
    resultat: une liste"""
    res=[]
    for commandes in liste_commandes:
        la_commande=[]
        prix=0        
        for i in range(len(commandes)):
            if i ==2:
                for achat in commandes[i]:
                    prix=prix+achat[1]*achat[2]
                la_commande.append(prix)
            else:
                la_commande.append(commandes[i])
        la_commande=tuple(la_commande)
        res.append(la_commande)
    return res

assert factures([(123 , " Dupont " ,[(" Verre " ,6 ,2.4) ,(" Assiette " ,6 ,1.5) ]) ,(125 ," Durand ",[(" vase " ,1 ,10.0) ])])==[(123, ' Dupont ', 23.4), (125, ' Durand ', 10.0)], "pb"  
assert factures([(1,"Michel",[("bière",150,2)]),(2,"Patrick",[("vin",40,20)])])==[(1,"Michel",300),(2,"Patrick",800)], "pb"
# vos tests

def affiche_factures(liste_commandes):
    '''
    paramètre: une liste
    resultat: un str
    '''
    res=""
    commandes_en_cours = 0
    for (num, nom, commandes) in liste_commandes:
        res += "--------------------\nnuméro: " + str(num).ljust(5) + "Nom: " + nom + "\n"
        res += "produit" + "qte".rjust(20) + "prix".rjust(10) + "total\n".rjust(15) 
        for ( article, nombre, prix) in commandes:
            res += article + str(nombre).rjust(27 - len(article)) + " * " + format(prix,'5.2f').rjust(7) + " = " + format(prix*nombre,'5.2f').rjust(11) + "\n"
        res += "--------\n".rjust(52) + "total" + format(factures(liste_commandes)[commandes_en_cours][2],'5.2f').rjust(46) + "\n"
        commandes_en_cours += 1
    return res
print(affiche_factures([(123,"Dupont",[("Verre",6,2.4),("Assiette",6,1.5)]),(125,"Durand",[("vase",1,10.0)])]))

# affiche_factures([(123,"Dupont",[("Verre",6,2.4),("Assiette",6,1.5)]),(125,"Durand",[("vase",1,10.0)])])
