#! /usr/bin/python3

def Personne(nom,age,moyen):
    """
    creer une personne qui doit être représentée par un dictionnaire
    paramètres: nom:
                age:
                moyen:
    resultat:
    """
    pers=dict()
    pers["nom"]= nom
    pers["age"]=age
    pers["moyen"]=moyen
    return pers

def get_nom(pers):
    """
    retourne le nom de la personne
    """
    return pers["nom"]

def get_age(pers):
    """
    retourne l'age de la personne
    """
    return pers["age"]

def get_moyen_transport(pers):
    """
    retourne le moyen de transport utilisé par la personne
    """
    return pers["moyen"]

def set_nom(pers,nom):
    """
    change le nom de la persone
    """
    pers["nom"]=nom

def set_age(pers,age):
    """
    change l'age de la personne
    """
    pers["age"]=age

def set_moyen_transport(pers,moyen):
    """ 
    change le moyen de transport de la personne
    """
    pers["moyen"]=moyen

def affiche_personne(pers):
    """
    affiche une personne
    """
    print('-'*10)
    print('Nom:',get_nom(pers))
    print('Age:',get_age(pers))
    print('Moyen de transport:', get_moyen_transport(pers))
    print('-'*10)


def lire_fichier_personnes(nom_fic):
    """
    lit une liste de personnes contenue dans un fichier
    """
    fic=open(nom_fic)
    liste_pers=[]
    for ligne in fic:
        [nom,age,moyen_trans]=ligne[:len(ligne)-1].split(',')
        liste_pers.append(Personne(nom,int(age),moyen_trans))
    fic.close()
    return liste_pers

def affiche_liste_personnes(liste_pers):
    """
    affiche une liste de personnes
    """
    for i in range(len(liste_pers)):
        res.append(liste_pers[i])
    return res

def age_moyen_utilisateur_transport(liste_pers,nom_moyen_transport):
    """
    retourne l'age moyen des personnes qui utilise comme moyen de transport
    celui passé en paramètres. Si aucune personne n'utilise ce moyen de transport
    la fonction doit retourner -1
    """
    res=-1
    somme=0
    cpt=0
    for pers in liste_pers:
        if pers["moyen"]==nom_moyen_transport:
            somme=somme+pers["age"]
            cpt=cpt+1
            res=0
    if res!=-1:
        res=somme/cpt
    return res
    
def liste_moyens_transport(listePers):
    """
    retourne sous la forme d'une liste de chaines de caractères la liste des 
    moyens de transport utilisés par les personne de listePers
    """
    res=[]
    for pers in listePers:
        if pers["moyen"] not in res:
            res.append(pers["moyen"])
    return res

### programme principal
if __name__=='__main__':
    p1=Personne("Pierre",18,"velo")
    print(p1)
    print(get_nom(p1))
    lire_fichier_personnes('personnes.txt')
    #ajoutez vos appels aux fonctions sur les personnes