def Matrice(nb_lignes,nb_colonnes,valeur_par_defaut=0):
    '''
    Crée une matrice de nb_lignes lignes et nb_colonnes colonnes
    contenant toute la valeur valeur_par_defaut
    paramètres: un  int, un int, un int
    résultat:un dict
    '''
    res=dict()
    w=[]
    a=[]
    res["nb_lignes"]=nb_lignes
    res["nb_colonnes"]=nb_colonnes
    for i in range(nb_lignes):
        w=[]
        for j in range(nb_colonnes):
            w.append(valeur_par_defaut)
        a.append(w)
    res["valeur_par_defaut"]=a
    return res

assert Matrice(3,3,0)=={'nb_lignes': 3, 'nb_colonnes': 3, 'valeur_par_defaut': [[0, 0, 0], [0, 0, 0], [0, 0, 0]]}

def get_nb_lignes(matrice):
    '''
    Permet de connaitre le nombre de lignes d'une matrice
    paramètre: un dict
    resultat: un int
    '''
    return matrice["nb_lignes"]

assert get_nb_lignes({'nb_lignes': 3, 'nb_colonnes': 3, 'valeur_par_defaut': [[0, 0, 0], [0, 0, 0], [0, 0, 0]]})==3

def get_nb_colonnes(matrice):
    '''
    Permet de connaitre le nombre de colonnes d'une matrice
    paramètre: un dict
    resultat: un int 
    '''
    return matrice["nb_colonnes"]

assert get_nb_colonnes({'nb_lignes': 3, 'nb_colonnes': 3, 'valeur_par_defaut': [[0, 0, 0], [0, 0, 0], [0, 0, 0]]})==3

def get_val(matrice,lig,col):
    '''
    retourne la valeur qui se trouve à la ligne lig colonne col de la matrice
    paramètres: matrice: un dict lig: un int col: un int
    resultat: un int 
    '''
    res=matrice["valeur_par_defaut"]
    return res[lig][col]

assert get_val({'nb_lignes': 3, 'nb_colonnes': 3, 'valeur_par_defaut': [[0, 0, 0], [0, 0, 0], [0, 0, 0]]},1,1)==0

def set_val(matrice,lig,col,val):
    '''
    place la valeur val à la ligne lig colonne col de la matrice
    paramètres: matrice: un tuple lig: un int col: un int val: int ou float
    resultat: cette fonction ne retourne rien mais modifie la matrice
    '''
    valeur=matrice["valeur_par_defaut"]
    valeur[lig][col]=val
    return matrice

assert set_val({'nb_lignes': 3, 'nb_colonnes': 3, 'valeur_par_defaut': [[0, 0, 0], [0, 0, 0], [0, 0, 0]]},1,1,1)=={'nb_lignes': 3, 'nb_colonnes': 3, 'valeur_par_defaut': [[0, 0, 0], [0, 1, 0], [0, 0, 0]]}