from matriceAPI2 import*
from matrice import*
def get_ligne(matrice,lig):
    """
    retourne la ligne choisi de la matrice
    parametre: matrice: une liste lig: un int
    resultat: une liste
    """
    res=[]
    Col=get_nb_colonnes(matrice)
    for i in range(Col):
        res.append(get_val(matrice,lig,i))
    return res

assert get_ligne([[10,11,12,13],[14,15,16,17],[18,19,20,21]],0)==[10,11,12,13]

def get_colonne(matrice,col):
    """
    retourne la colonne choisi de la matrice
    parametre: matrice: une liste col: un int
    resultat: une liste
    """
    res=[]
    Lig=get_nb_lignes(matrice)
    for i in range(Lig):
        res.append(get_val(matrice,i,col))
    return res

assert get_colonne([[10,11,12,13],[14,15,16,17],[18,19,20,21]],0)==[10,14,18]

def get_diagonale_principale(matrice):
    """retourne la diagonal d'une matrice carré
    parametre: une liste
    resultat: une liste
    """
    res=[]
    if is_carre(matrice)==True:
        nb_ligne=get_nb_lignes(matrice)
        for i in range(nb_ligne):
            res.append(get_val(matrice,i,i))
    else:
        res=None
    return res

assert get_diagonale_principale([[2,4,5],[7,2,6],[7,8,2]])==[2,2,2]

def get_diagonale_secondaire(matrice):
    """retourne la diagonal secondaire d'une matrice carré
    parametre: une liste
    resultat: une liste
    """
    res=[]
    if is_carre(matrice)==True:
        nb_ligne=get_nb_lignes(matrice)
        for i in range(nb_ligne):
            res.append(get_val(matrice,-i-1,i))
    res.reverse()
    return res

assert get_diagonale_secondaire([[2,4,5],[7,2,6],[7,8,2]])==[5,2,7]

def transpose(matrice):
    """
    retourne la transposer de la matrice
    paramétre: une liste
    resultat: une liste
    """
    nb_ligne=get_nb_lignes(matrice)
    nb_colonne=get_nb_colonnes(matrice)
    res=[]
    for i in range(nb_ligne):
        w=[]
        for ligne in matrice:
            w.append(ligne[i])
        res.append(w)
    return res

assert transpose([[2,4,5],[7,2,6],[7,8,2]])==[[2,7,7],[4,2,8],[5,6,2]]

def is_triangulaire_inf(matrice):
    """
    dit si une matrice est triangulaire inférieur
    paramétre: une liste
    resultat: un booleen
    """
    nb_ligne=get_nb_lignes(matrice)
    nb_colonne=get_nb_colonnes(matrice)
    w=[]
    a=[]
    res=False
    for i in range(nb_ligne):
        for j in range(nb_colonne):
            if j>i:
                w.append(matrice[i][j])
    for b in range(len(w)):
        a.append(0)
    if a==w:
        res=True
    return res

assert is_triangulaire_inf([[4,0,0],[2,4,0],[5,6,7]])==True
assert is_triangulaire_inf([[2,4,5],[7,2,6],[7,8,2]])==False

def is_triangulaire_sup(matrice):
    """
    dit si une matrice est triangulaire supérieur
    paramétre: une liste
    resultat: un boolenen
    """
    nb_ligne=get_nb_lignes(matrice)
    nb_colonne=get_nb_colonnes(matrice)
    w=[]
    a=[]
    res=False
    for i in range(nb_ligne):
        for j in range(nb_colonne):
            if j<i:
                w.append(matrice[i][j])
    for b in range(len(w)):
        a.append(0)
    if a==w:
        res=True
    return res

assert is_triangulaire_sup([[1,2,3],[0,4,5],[0,5,6]])==False
assert is_triangulaire_sup([[1,2,3],[0,4,5],[0,0,6]])==True