#!/usr/bin/python3
import unittest


from matrice import *

class Test_matrices(unittest.TestCase):
    def setUp(self):
        self.m1=(3,4,[0,1,2,3,
                      4,5,6,7,
                      8,9,10,11])
        self.m2=(2,5,['a','b','c','d','e',
                      'f','g','h','i','j'])
        self.m3=(4,4,[0]*16)
        self.m4=(3,3,[1,2,3,
                      2,4,5,
                      3,5,6])
        self.m5=(3,3,[6,5,4,
                       5,3,2,
                       4,2,1])

        self.m6=(3,3,[7,7,7,
                       7,7,7,
                       7,7,7])
        self.m7=(3,4,[1]*12)
        self.m8=(3,4,[1,2,3,4,
                       5,6,7,8,
                       9,10,11,12])


    def test_Matrice(self):
        self.assertEqual(Matrice(3,4),(3,4,[0]*12),"Pb avec l'appel Matrice(3,4)")
        self.assertEqual(Matrice(5,2,'a'),(5,2,['a']*10),"Pb avec l'appel Matrice(5,2,'a')")

    def test_get_nb_lignes(self):
        self.assertEqual(get_nb_lignes(self.m1),3,"Pb avec l'appel get_nb_lignes("+str(self.m1)+")")
        self.assertEqual(get_nb_lignes(self.m2),2,"Pb avec l'appel get_nb_lignes("+str(self.m2)+")")

    def test_get_nb_colonnes(self):
        self.assertEqual(get_nb_colonnes(self.m1),4,"Pb avec l'appel get_nb_colonnes("+str(self.m1)+")")
        self.assertEqual(get_nb_colonnes(self.m2),5,"Pb avec l'appel get_nb_colonnes("+str(self.m2)+")")

    def test_get_val(self):
        self.assertEqual(get_val(self.m1,1,3),7,"Pb avec l'appel get_val("+str(self.m1)+"1,3)")
        self.assertEqual(get_val(self.m2,1,3),'i',"Pb avec l'appel get_val("+str(self.m2)+"1,3)")

    def test_set_val(self):
        set_val(self.m1,2,0,100)
        self.assertEqual(self.m1,(3,4,[0,1,2,3,4,5,6,7,100,9,10,11]),"Pb avec l'appel set_val("+str(self.m1)+"2,0,100)")
        set_val(self.m2,0,4,'z')
        self.assertEqual(self.m2,(2,5,['a','b','c','d','z','f','g','h','i','j']),"Pb avec l'appel set_val("+str(self.m2)+"0,4,'z')")

    def test_is_nulle(self):
        self.assertFalse(is_nulle(self.m1),"Pb avec l'appel is_nulle("+str(self.m1)+")")
        self.assertFalse(is_nulle(self.m2),"Pb avec l'appel is_nulle("+str(self.m2)+")")
        self.assertTrue(is_nulle(self.m3),"Pb avec l'appel is_nulle("+str(self.m3)+")")

    def test_is_carre(self):
        self.assertFalse(is_carre(self.m1),"Pb avec l'appel is_carre("+str(self.m1)+")")
        self.assertFalse(is_carre(self.m2),"Pb avec l'appel is_carre("+str(self.m2)+")")
        self.assertTrue(is_carre(self.m3),"Pb avec l'appel is_carre("+str(self.m3)+")")
        self.assertTrue(is_carre(self.m4),"Pb avec l'appel is_carre("+str(self.m4)+")")

    def test_moyenne(self):
        self.assertEqual(moyenne(self.m1),66/12,"Pb avec l'appel moyenne("+str(self.m1)+")")
        self.assertEqual(moyenne(self.m3),0.,"Pb avec l'appel moyenne("+str(self.m3)+")")
        self.assertEqual(moyenne(self.m4),31/9,"Pb avec l'appel moyenne("+str(self.m4)+")")
        
    def test_addition_mat(self):
        self.assertEqual(addition_mat(self.m1,self.m7),self.m8,"Problème avec l'addition")
        self.assertEqual(addition_mat(self.m4,self.m5),self.m6,"Problème avec l'addition")
        

if __name__ == '__main__':
    unittest.main()
