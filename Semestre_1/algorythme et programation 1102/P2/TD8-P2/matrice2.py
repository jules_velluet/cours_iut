'''
   -----------------------------------------
   Une implémentation des matrices 2D en python
   -----------------------------------------
'''

from matriceAPI2 import *

# === POUR L'EXERCICE 2
# === DU DEBUT JUSQU'ICI  A SAUVEGARDER DANS LE FICHIER matriceAP1.py
# === ET REMPLACER PAR from matriceAP1 import *

# Affichage d'une matrice
def affiche_ligne_separatrice(matrice,taille_cellule=4):
    ''' 
    fonction annexe pour afficher les lignes séparatrices
    paramètres: matrice la matrice à afficher
                taille_cellule la taille en nb de caractères d'une cellule
    résultat: cette fonction ne retourne rien mais fait un affichage
    '''
    print()
    for i in range(get_nb_colonnes(matrice)+1):
        print('-'*taille_cellule+'+',end='')
    print()

def affiche_matrice(matrice,taille_cellule=4):
    '''
    affiche le contenue d'une matrice présenté sous le format d'une grille
    paramètres: matrice la matrice à afficher
                taille_cellule la taille en nb de caractères d'une cellule
    résultat: cette fonction ne retourne rien mais fait un affichage
    '''

    nb_colonnes=get_nb_colonnes(matrice)
    nb_lignes=get_nb_lignes(matrice)
    print(' '*taille_cellule+'|',end='')
    for i in range(nb_colonnes):
        print(str(i).center(taille_cellule)+'|',end='')
    affiche_ligne_separatrice(matrice,taille_cellule)
    for i in range(nb_lignes):
        print(str(i).rjust(taille_cellule)+'|',end='')
        for j in range(nb_colonnes):
            print(str(get_val(matrice,i,j)).rjust(taille_cellule)+'|',end='')
        affiche_ligne_separatrice(matrice,taille_cellule)
    print()

#-----------------------------------------
# AJOUTER ICI LE CODE DES FONCTIONS DEMANDEES DANS L'EXERCICE 1
#-----------------------------------------
def is_nulle(matrice): 
    """
    dit si une matrice contient que des valeur nulle
    parametre: un tuple
    resultat: un booleen
    """
    lig=get_nb_lignes(matrice)
    col=get_nb_lignes(matrice)
    Nul=Matrice(lig,col,0)
    if Nul==matrice:
        res=True
    else:
        res=False
    return res

def is_carre(matrice):
    """
    indique si la matrice est carré ou non
    parametre: un tuple
    resultat: un booleen
    """
    lig=get_nb_lignes(matrice)
    col=get_nb_colonnes(matrice)
    if lig==col:
        res=True
    else:
        res=False
    return res

def moyenne(matrice):
    """
    calcul la moyenne des valeurs de la matrice
    parametre: un tuple
    resultat: un float
    """
    somme=0
    cpt=0
    lig=get_nb_lignes(matrice)
    col=get_nb_colonnes(matrice)
    for i in range(lig):
        for j in range(col):
            somme=somme+get_val(matrice,i,j)
            cpt=cpt+1
    return somme/cpt

def addition_mat(mat1,mat2):
    """
    fait l'addition de deux matrice
    parametre: mat1: un tuple mat2: un tuple
    resultat: None si matrice pas au même dimension un tuple sinon
    """
    if get_nb_lignes(mat1) == get_nb_lignes(mat2) and get_nb_colonnes(mat1) == get_nb_colonnes(mat2):
        res=Matrice(get_nb_lignes(mat1),get_nb_colonnes(mat1),0)
        for i in range(get_nb_lignes(mat1)):
            for j in range(get_nb_colonnes(mat1)):
                set_val(res,i,j,get_val(mat1,i,j)+get_val(mat2,i,j))
        return res
    else:
        return None