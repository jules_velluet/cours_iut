'''
   -----------------------------------------
   Une deuxième implémentation des matrices 2D en python
   -----------------------------------------
'''

def Matrice(nbLignes,nbColonnes,valeur_par_defaut=0):
    '''
    Crée une matrice de nbLignes lignes et nbColonnes colonnes
    contenant toute la valeur valeur_par_defaut
    paramètres:
    résultat: un tuple
    '''
    res=[]
    for i in range(nbLignes):
        w=[]
        for j in range(nbColonnes):
            w.append(valeur_par_defaut)
        res.append(w)
    return res

def get_nb_lignes(matrice):
    '''
    Permet de connaitre le nombre de lignes d'une matrice
    paramètre: un tuple
    resultat: un int
    '''
    return len(matrice)

def get_nb_colonnes(matrice):
    '''
    Permet de connaitre le nombre de colonnes d'une matrice
    paramètre: un tuple
    resultat: un int 
    '''
    return len(matrice[0])

def get_val(matrice,lig,col):
    '''
    retourne la valeur qui se trouve à la ligne lig colonne col de la matrice
    paramètres: matrice: un tuple lig: un int col: un int
    resultat: un int 
    '''
    return matrice[lig][col]

def set_val(matrice,lig,col,val):
    '''
    place la valeur val à la ligne lig colonne col de la matrice
    paramètres: matrice: un tuple lig: un int col: un int val: int ou float
    resultat: cette fonction ne retourne rien mais modifie la matrice
    '''
    matrice[lig][col]=val
    return matrice