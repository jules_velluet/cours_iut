'''
   -----------------------------------------
   Une implémentation des matrices 2D en python
   -----------------------------------------
'''

def Matrice(nb_lignes,nb_colonnes,valeur_par_defaut=0):
    '''
    Crée une matrice de nb_lignes lignes et nb_colonnes colonnes
    contenant toute la valeur valeur_par_defaut
    paramètres: un  int, un int, un int
    résultat:un tuple
    '''
    res=[]
    res.append(nb_lignes)
    res.append(nb_colonnes)
    res.append(nb_lignes*nb_colonnes*[valeur_par_defaut])
    return tuple(res)


def get_nb_lignes(matrice):
    '''
    Permet de connaitre le nombre de lignes d'une matrice
    paramètre: un tuple
    resultat: un int
    '''
    return matrice[0]

def get_nb_colonnes(matrice):
    '''
    Permet de connaitre le nombre de colonnes d'une matrice
    paramètre: un tuple
    resultat:un int    
    '''
    return matrice[1]

def get_val(matrice,lig,col):
    '''
    retourne la valeur qui se trouve à la ligne lig colonne col de la matrice
    paramètres: un tuple, un int, un int
    resultat:        
    '''
    i=matrice[1]*lig+col
    return matrice[2][i]

def set_val(matrice,lig,col,val):
    '''
    place la valeur val à la ligne lig colonne col de la matrice
    paramètres: un tuple, un int, un int, un int ou float
    resultat: cette fonction ne retourne rien mais modifie la matrice
    '''
    i=matrice[1]*lig+col
    matrice[2][i]=val
    return matrice