#!/usr/bin/python3
import unittest
import os
import time

def changer_import(mod,ancien,nouveau,nouveau_mod):
    try:
        fic=open(mod+".py")
        cont=fic.read()
        fic.close()
        cont=cont.replace(ancien,nouveau)
        fic=open(nouveau_mod+".py","w")
        fic.write(cont)
        fic.flush()
        fic.close()
    except:
        pass

changer_import("matrice","matriceAPI2","matriceAPI1","matrice1")
time.sleep(0.2) #temporisation sinon erreurs aleatoires
changer_import("matrice1","matriceAPI1","matriceAPI2","matrice2")
time.sleep(0.2) #temporisation sinon erreurs aleatoires

import matrice1
import matrice2

class TestMatrices(unittest.TestCase):
    def setUp(self):
        self.m11=(3,4,[0,1,2,3,
                      4,5,6,7,
                      8,9,10,11])
        self.m12=(2,5,['a','b','c','d','e',
                      'f','g','h','i','j'])
        self.m13=(4,4,[0]*16)
        self.m14=(3,3,[1,2,3,
                      2,4,5,
                      3,5,6])
        self.m15=(3,3,[6,5,4,
                       5,3,2,
                       4,2,1])

        self.m16=(3,3,[7,7,7,
                       7,7,7,
                       7,7,7])
        self.m17=(3,4,[1]*12)
        self.m18=(3,4,[1,2,3,4,
                       5,6,7,8,
                       9,10,11,12])
        self.m21=[[0,1,2,3],
                 [4,5,6,7],
                 [8,9,10,11]]
        self.m22=[['a','b','c','d','e'],
                 ['f','g','h','i','j']]
        self.m23=[[0,0,0,0],
                 [0,0,0,0],
                 [0,0,0,0],
                 [0,0,0,0]]
        self.m24=[[1,2,3],
                 [2,4,5],
                 [3,5,6]]
        self.m25=[[6,5,4],
                 [5,3,2],
                 [4,2,1]]
        self.m26=[[7,7,7],
                  [7,7,7],
                  [7,7,7]]
        self.m27=[[1,1,1,1],
                  [1,1,1,1],
                  [1,1,1,1]]
        self.m28=[[1,2,3,4],
                  [5,6,7,8],
                  [9,10,11,12]]
                  
                  

    def test_Matrice1(self):
        self.assertEqual(matrice1.Matrice(3,4),(3,4,[0]*12),"Pb avec l'appel Matrice(3,4)")
        self.assertEqual(matrice1.Matrice(5,2,'a'),(5,2,['a']*10),"Pb avec l'appel Matrice(5,2,'a')")

    def test_get_nb_lignes1(self):
        self.assertEqual(matrice1.get_nb_lignes(self.m11),3,"Pb avec l'appel get_nb_lignes("+str(self.m11)+")")
        self.assertEqual(matrice1.get_nb_lignes(self.m12),2,"Pb avec l'appel get_nb_lignes("+str(self.m12)+")")

    def test_get_nb_colonnes1(self):
        self.assertEqual(matrice1.get_nb_colonnes(self.m11),4,"Pb avec l'appel get_nb_colonnes("+str(self.m11)+")")
        self.assertEqual(matrice1.get_nb_colonnes(self.m12),5,"Pb avec l'appel get_nb_colonnes("+str(self.m12)+")")

    def test_get_val1(self):
        self.assertEqual(matrice1.get_val(self.m11,1,3),7,"Pb avec l'appel get_val("+str(self.m11)+"1,3)")
        self.assertEqual(matrice1.get_val(self.m12,1,3),'i',"Pb avec l'appel get_val("+str(self.m12)+"1,3)")

    def test_set_val1(self):
        matrice1.set_val(self.m11,2,0,100)
        self.assertEqual(self.m11,(3,4,[0,1,2,3,4,5,6,7,100,9,10,11]),"Pb avec l'appel set_val("+str(self.m11)+"2,0,100)")
        matrice1.set_val(self.m12,0,4,'z')
        self.assertEqual(self.m12,(2,5,['a','b','c','d','z','f','g','h','i','j']),"Pb avec l'appel set_val("+str(self.m12)+"0,4,'z')")

    def test_is_nulle1(self):
        self.assertFalse(matrice1.is_nulle(self.m11),"Pb avec l'appel is_nulle("+str(self.m11)+")")
        self.assertFalse(matrice1.is_nulle(self.m12),"Pb avec l'appel is_nulle("+str(self.m12)+")")
        self.assertTrue(matrice1.is_nulle(self.m13),"Pb avec l'appel is_nulle("+str(self.m13)+")")

    def test_is_carre1(self):
        self.assertFalse(matrice1.is_carre(self.m11),"Pb avec l'appel is_carre("+str(self.m11)+")")
        self.assertFalse(matrice1.is_carre(self.m12),"Pb avec l'appel is_carre("+str(self.m12)+")")
        self.assertTrue(matrice1.is_carre(self.m13),"Pb avec l'appel is_carre("+str(self.m13)+")")
        self.assertTrue(matrice1.is_carre(self.m14),"Pb avec l'appel is_carre("+str(self.m14)+")")

    def test_moyenne1(self):
        self.assertEqual(matrice1.moyenne(self.m11),66/12,"Pb avec l'appel moyenne("+str(self.m11)+")")
        self.assertEqual(matrice1.moyenne(self.m13),0.,"Pb avec l'appel moyenne("+str(self.m13)+")")
        self.assertEqual(matrice1.moyenne(self.m14),31/9,"Pb avec l'appel moyenne("+str(self.m14)+")")

    def test_addition_mat1(self):
        self.assertEqual(matrice1.addition_mat(self.m11,self.m17),self.m18,"Problème avec l'addition")
        self.assertEqual(matrice1.addition_mat(self.m14,self.m15),self.m16,"Problème avec l'addition")


        
    def test_Matrice2(self):
        self.assertEqual(matrice2.Matrice(3,4),[[0,0,0,0],[0,0,0,0],[0,0,0,0]],"Pb avec l'appel Matrice(3,4)")
        self.assertEqual(matrice2.Matrice(5,2,'a'),[['a','a'],['a','a'],['a','a'],['a','a'],['a','a']],"Pb avec l'appel Matrice(5,2,'a')")
        
    def test_get_nb_lignes2(self):
        self.assertEqual(matrice2.get_nb_lignes(self.m21),3,"Pb avec l'appel get_nb_lignes("+str(self.m21)+")")
        self.assertEqual(matrice2.get_nb_lignes(self.m22),2,"Pb avec l'appel get_nb_lignes("+str(self.m22)+")")
    
    def test_get_nb_colonnes2(self):
        self.assertEqual(matrice2.get_nb_colonnes(self.m21),4,"Pb avec l'appel get_nb_colonnes("+str(self.m21)+")")
        self.assertEqual(matrice2.get_nb_colonnes(self.m22),5,"Pb avec l'appel get_nb_colonnes("+str(self.m22)+")")
    
    def test_get_val2(self):
        self.assertEqual(matrice2.get_val(self.m21,1,3),7,"Pb avec l'appel get_val("+str(self.m21)+"1,3)")
        self.assertEqual(matrice2.get_val(self.m22,1,3),'i',"Pb avec l'appel get_val("+str(self.m22)+"1,3)")

    def test_set_val2(self):
        matrice2.set_val(self.m21,2,0,100)
        self.assertEqual(self.m21,[[0,1,2,3],[4,5,6,7],[100,9,10,11]],"Pb avec l'appel set_val("+str(self.m21)+"2,0,100)")
        matrice2.set_val(self.m22,0,4,'z')
        self.assertEqual(self.m22,[['a','b','c','d','z'],['f','g','h','i','j']],"Pb avec l'appel set_val("+str(self.m22)+"0,4,'z')")
        
    def test_is_nulle2(self):
        self.assertFalse(matrice2.is_nulle(self.m21),"Pb avec l'appel is_nulle("+str(self.m21)+")")
        self.assertFalse(matrice2.is_nulle(self.m22),"Pb avec l'appel is_nulle("+str(self.m22)+")")
        self.assertTrue(matrice2.is_nulle(self.m23),"Pb avec l'appel is_nulle("+str(self.m23)+")")
        
    def test_is_carre2(self):
        self.assertFalse(matrice2.is_carre(self.m21),"Pb avec l'appel is_carre("+str(self.m21)+")")
        self.assertFalse(matrice2.is_carre(self.m22),"Pb avec l'appel is_carre("+str(self.m22)+")")
        self.assertTrue(matrice2.is_carre(self.m23),"Pb avec l'appel is_carre("+str(self.m23)+")")
        self.assertTrue(matrice2.is_carre(self.m24),"Pb avec l'appel is_carre("+str(self.m24)+")")
        
    def test_moyenne2(self):
        self.assertEqual(matrice2.moyenne(self.m21),66/12,"Pb avec l'appel moyenne("+str(self.m21)+")")
        self.assertEqual(matrice2.moyenne(self.m23),0.,"Pb avec l'appel moyenne("+str(self.m23)+")")
        self.assertEqual(matrice2.moyenne(self.m24),31/9,"Pb avec l'appel moyenne("+str(self.m24)+")")

    def test_addition_mat2(self):
        self.assertEqual(matrice2.addition_mat(self.m21,self.m27),self.m28,"Problème avec l'addition")
        self.assertEqual(matrice2.addition_mat(self.m24,self.m25),self.m26,"Problème avec l'addition")

                  
if __name__ == '__main__':
    unittest.main()
