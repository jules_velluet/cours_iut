#-----------------------------------------
# Une implémentation en orienté objet des matrices 2D en python
#-----------------------------------------
class Matrice(object):

    def __init__(self,nb_lignes,nb_colonnes,valeur_par_defaut=0):
        """
        constructeur
        """
        self.nb_lignes=nb_lignes
        self.nb_colonnes=nb_colonnes
        self.liste_des_valeurs=[valeur_par_defaut]*(nb_lignes*nb_colonnes)

    def get_nb_lignes(self):
        return self.nb_lignes

    def get_nb_colonnes(self):
        return self.nb_colonnes

    def get_val(self,lig,col):
        return self.liste_des_valeurs[lig*self.nb_colonnes+col]

    def set_val(self,lig,col,val):
        self.liste_des_valeurs[lig*self.nb_colonnes+col]=val

    def affiche_ligne_separatrice(self,taille_cellule=4):
        print()
        for i in range(self.get_nb_colonnes()+1):
            print('-'*taille_cellule+'+',end='')
        print()
   
    def affiche(self,taille_cellule=4):
        nb_colonnes=self.get_nb_colonnes()
        nb_lignes=self.get_nb_lignes()
        print(' '*taille_cellule+'|',end='')
        for i in range(nb_colonnes):
            print(str(i).center(taille_cellule)+'|',end='')
        self.affiche_ligne_separatrice(taille_cellule)
        for i in range(nb_lignes):
            print(str(i).rjust(taille_cellule)+'|',end='')
            for j in range(nb_colonnes):
                print(str(self.get_val(i,j)).rjust(taille_cellule)+'|',end='')
            self.affiche_ligne_separatrice(taille_cellule)
        print()


    def est_symetrique(self):
        ok=True
        i=0
        while i <self.get_nb_lignes() and ok:
            j=i+1
            while j<self.get_nb_colonnes() and ok:
                ok=self.get_val(i,j)==self.get_val(j,i)
                j+=1
            i+=1
        return ok


    def addition(self,m2):
        if self.get_nb_colonnes()!=m2.get_nb_colonnes() or self.get_nb_lignes()!=m2.get_nb_lignes():
            return None
        m3=Matrice(self.get_nb_lignes(), self.get_nb_colonnes(),0)
        for i in range(self.get_nb_lignes()):
            for j in range(self.get_nb_colonnes()):
                m3.set_val(i,j,self.get_val(i,j)+m2.get_val(i,j))
        return m3

            

#### programme de test
if __name__=='__main__':
    m=Matrice(5,4,0)
    k=0
    for i in range(m.get_nb_lignes()):
        for j in range(m.get_nb_colonnes()):
            m.set_val(i,j,k)
            k+=1
    m.affiche()
    m2=Matrice(5,4,2)
    m2.affiche()
    m3=m.addition(m2)
    m3.affiche()