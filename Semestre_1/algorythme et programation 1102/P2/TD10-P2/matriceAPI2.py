# implémentation des matrices sous la forme d'une liste de listes
def Matrice(nb_lig,nb_col, valeur_par_defaut = 0):
   matrice = []
   for i in range(nb_lig) :
      li = []
      for j in range(nb_col) :
         li.append(valeur_par_defaut)
      matrice.append(li)
   return matrice
         
def get_nb_lignes(mat):
   return len(mat)

def get_nb_colonnes(mat):
   return len(mat[0])

def get_val(mat,lig,col):
   return mat[lig][col]

def set_val(mat,lig,col, val):
   mat[lig][col] = val
