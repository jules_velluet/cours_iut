from matriceAPI2 import *

#-----------------------------------------
# entrées sorties sur les matrices
#-----------------------------------------
def sauve_matrice(matrice,nom_fic):
    """
    """
    fic=open(nom_fic,'w')
    ligne=str(get_nb_lignes(matrice))+','+str(get_nb_colonnes(matrice))+'\n'
    fic.write(ligne)
    for i in range(get_nb_lignes(matrice)):
        ligne=''
        for j in range(get_nb_colonnes(matrice)-1):
            val=get_val(matrice,i,j)
            if val==None:
                ligne+=','
            else:
                ligne+=str(val)+','
                val=get_val(matrice,i,j+1)
            if val==None:
                ligne+='\n'
            else:
                ligne+=str(val)+'\n'
    fic.write(ligne)
    fic.close()

def charge_matrice(nom_fic,type_val='int'):
    fic=open(nom_fic,'r')
    ligne_lin_col=fic.readline()
    liste_lin_col=ligne_lin_col.split(',')
    matrice=Matrice(int(liste_lin_col[0]),int(liste_lin_col[1]))
    i=0  
    for ligne in fic:
        liste_val=ligne.split(",")
        j=0
        for elem in liste_val:
            if elem=="" or elem=="\n":
                set_val(matrice,i,j,None)
            elif type_val=='int':
                set_val(matrice,i,j,int(elem))
            elif type_val=='float':
                set_val(matrice,i,j,float(elem))
            elif type_val=='bool':
                set_val(matrice,i,j,bool(elem))
            else:
                set_val(matrice,i,j,elem)
            j+=1
        i+=1
    fic.close()
    return matrice

def affiche_ligne_separatrice(matrice,taille_cellule=4):
    """
    Affichage d'une matrice
    fonction annexe pour afficher les lignes séparatrices
    """
    print()
    for i in range(get_nb_colonnes(matrice)+1):
        print('-'*taille_cellule+'+',end='')
    print()
   
def affiche_matrice(matrice,taille_cellule=4):
    """
    """
    nb_colonnes=get_nb_colonnes(matrice)
    nb_lignes=get_nb_lignes(matrice)
    print(' '*taille_cellule+'|',end='')
    for i in range(nb_colonnes):
        print(str(i).center(taille_cellule)+'|',end='')
    affiche_ligne_separatrice(matrice,taille_cellule)
    for i in range(nb_lignes):
        print(str(i).rjust(taille_cellule)+'|',end='')
        for j in range(nb_colonnes):
            print(str(get_val(matrice,i,j)).rjust(taille_cellule)+'|',end='')
        affiche_ligne_separatrice(matrice,taille_cellule)
    print()
   
#--------------------------------
# fonctions sur les labyrinthes
#--------------------------------

def marquage_direct(calque,mat,val,marque):
    """
    marque avec la valeur marque les éléments du calque tel que la valeur 
    correspondante n'est pas un mur (de valeur differente de 1) et 
    qu'un de ses voisins dans le calque à pour valeur val
    la fonction doit retourner True si au moins une case du calque a été marquée
    """
    res=False
    for j in range(get_nb_lignes(mat)):
        for i in range(get_nb_colonnes(mat)):
            if get_val(calque,j,i) == 0:
                if get_val(mat,j,i) != 1:
                    if j+1<get_nb_lignes(mat) and get_val(calque,j+1,i)==val:
                        set_val(calque,j,i,marque)
                        res=True 
                    elif i+1<get_nb_colonnes(mat) and get_val(calque,j,i+1)==val :
                        set_val(calque,j,i,marque)
                        res=True 
                    elif i-1>=0 and get_val(calque,j,i-1)==val:
                        set_val(calque,j,i,marque)
                        res=True 
                    elif j-1>=0 and get_val(calque,j-1,i)==val:
                        set_val(calque,j,i,marque)
                        res=True   
    return res

def est_accessible(mat,pos1,pos2):
    """
    verifie qu'il existe un chemin entre pos1 et pos2 dans la matrice mat
    """
    res=False
    suite=True
    calque=Matrice(get_nb_lignes(mat),get_nb_colonnes(mat),0)
    set_val(calque,pos1[0],pos1[1],3)
    while suite==True:
        suite=marquage_direct(calque,mat,3,3)
    if get_val(calque,pos2[0]-1,pos2[1]-1)==3:
        res=True
    affiche_matrice(calque)
    return res

def labyrinthe_valide(mat):
    """
    verifie qu'il existe un chemin entre la case en haut à gauche et la case
    en bas à droite de la matrice
    """
    res=est_accessible(mat,(0,0),(8,8))
    return res


def est_accessible2(mat,pos1,pos2):
    """
    vérifie l'accessibilité entre deux positions mais en calculant ne nombre de cases
    depuis le point de départ
    la fonction retourne le calque ces les deux cases sont accessibles et None sinon
    """
    parcour=1
    res=None
    suite=True
    calque=Matrice(get_nb_lignes(mat),get_nb_colonnes(mat),0)
    if get_val(mat,pos1[0],pos1[1])!=1:
        set_val(calque,pos1[0],pos1[1],1)
    while get_val(calque,pos2[0],pos2[1])==0 and suite==True:
        suite=marquage_direct(calque,mat,parcour,parcour+1)
        parcour=parcour+1
    if get_val(calque,pos2[0],pos2[1])!=0:
        res=True
    if res==True:
        res=calque
    return res

def chemin_decroissant(calque,pos1,pos2):
    """
    recherche un chemin décroissant à partir de pos1 vers pos2
    le chemin est une liste de positions
    la fonction suppose que le calque contient effectivement les valeurs permettant
    de retrouver ce chemin
    """
    res = [pos2]
    i, j =pos2
    while (i,j) != pos1 : 
        valeur_courante = get_val(calque, i, j)
        if i>0 and get_val(calque, i - 1, j) == valeur_courante - 1 : 
            i-=1
        if i < get_nb_lignes(calque) and get_val(calque, i + 1, j) == valeur_courante - 1 :
            i+=1
        if j > 0 and get_val(calque, i, j-1) == valeur_courante - 1 :
            j -= 1
        if j < get_nb_colonnes(calque) and get_val(calque, i, j+1) == valeur_courante - 1 :
            j+=1
        res.append((i,j))
    return res 

def plus_court_chemin(matrice,pos1,pos2):
    """
    recherche le plus court chemin entre pos1 et pos2. 
    s'il n'y pas de chemin entre ces deux positions la fonction retourne None
    sinon elle retourne le chemin
    """
    calque = est_accessible2(matrice, pos2, pos1)
    if calque is None : 
        return None 
    return chemin_decroissant(calque, pos2, pos1)