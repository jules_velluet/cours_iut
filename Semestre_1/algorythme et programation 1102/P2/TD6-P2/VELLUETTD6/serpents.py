####################################################
### IMPLEMENTEZ CI-DESSOUS VOS FONCTIONS         ###
### ATTENTION AUX NOMS DES FONCTIONS UTILISEES   ###
### DANS LA PARTIE PROGRAMME PRINCIPAL           ###
####################################################

def serpent_to_str(serpent):
    '''
    met en forme les informations d'un serpent sous la forme d'un str
    paramètre:un serpent
    resultat:string
    '''
    return "--------------------\nNom: "+ serpent[0] +"\nTaille: "+ str(serpent[1]) +"\nDanger: "+ str(serpent[2]) +"\n--------------------\n"


assert serpent_to_str(("Python3",0.3,0))=="--------------------\nNom: Python3\nTaille: 0.3\nDanger: 0\n--------------------\n", 'Pb serpent_to_str(("Python3",0.3,0))'
# ici rajoutez vos tests 


def liste_serpents_to_str(liste_serpents):
    '''
    met en forme un liste de serpent sous la forme d'un str
    paramètre:une liste de tuple
    resultat:string
    '''
    res=""
    for i in range(len(liste_serpents)):
        res=res+serpent_to_str(liste_serpents[i])
    return res

# A decommenter une fois la fonction implémentée
assert liste_serpents_to_str([("Python3",0.3,0),("Boa",3.5,4)])=="--------------------\nNom: Python3\nTaille: 0.3\nDanger: 0\n--------------------\n--------------------\nNom: Boa\nTaille: 3.5\nDanger: 4\n--------------------\n", 'Pb liste_serpents_to_str([("Python3",0.3,0),("Boa",3.5,4)])'
# ici rajoutez vos tests 

def saisir_un_serpent():
    '''
    permet de demander à l'utilisateur les informations concernant un serpent
    paramètre:utilisateur
    resultat: un tuple
    '''
    nom= input("entrer le nom du serpent")
    taille=float(input("entrer le nom du serpent"))
    danger=int(input("entrer le nom du serpent"))
    res=(nom,taille,danger)
    return res
# Le résultat de cette fonction dépend de ce qu'a saisi l'utilisateur 

def ajouter_serpents(liste_serpents):
    '''
    permet de demander à l'utilisateur des informations sur des nouveaux serpents
    et d'ajouter ces serpents au fur et à mesure ) la liste de serpents
    paramètre:une liste de tuple
    resultat:une liste
    '''
    continuer="oui"
    while continuer=="oui":
        liste_serpents.append(saisir_un_serpent())
        question=input("voulez-vous rentrer un nouveau serpent: oui/non")
        if question=="non":
            continuer="non"
    return liste_serpents
        
# Le résultat de cette fonction dépend de ce qu'a saisi l'utilisateur 


def rechercher_dangereux(liste_serpents):
    '''
    recherche la liste des serpent dangereux dans une liste de serpents
    paramètre:une liste de tuple
    resultat:une liste
    '''
    serpent_dangereux=list()
    for serpent in liste_serpents:
        if serpent[2]==5:
            serpent_dangereux.append(serpent[0])
    return serpent_dangereux
            

assert rechercher_dangereux([("Python3",0.3,0),("q",1,5)])==["q"], "pb"
    
def moyenne_taille_dangerosite(liste_serpents,dangerosite):
    '''
    calcule la taille moyenne des serpents pour une certaine dangerosité
    paramètre:une liste de tuple et un int compris entre 0 et 5
    resultat:un float
    '''
    res=0
    cpt=0
    for serpent in liste_serpents:
        if serpent[2]==dangerosite:
            res=res+serpent[1]
            cpt=cpt+1
    if cpt!=0:
        moyenne=res/cpt
    return moyenne
#ici rajoutez vos tests
assert moyenne_taille_dangerosite([("Python3",0.3,0),("q",4,5),("z",2,5)],5)==3.0, "pb"
    
def nb_serpents_par_danger(liste_serpents):
    '''
    donne le nombre de serpents pour chaque niveau de dangerosité
    paramètre:une liste de tuple
    resultat:une liste de int
    '''
    res=list()
    for niveau_danger in range(6):
        nombre_de_fois=0
        for serpent in liste_serpents:
            if serpent[2]==niveau_danger:
                nombre_de_fois=nombre_de_fois+1
        res.append(nombre_de_fois)
    return res
assert nb_serpents_par_danger([("Python3",0.3,0),("q",4,5),("z",2,5)])==[1,0,0,0,0,2], "pb"
        

#ici rajoutez vos tests


def sauver_serpents(nom_fichier,liste_serpents):
    '''
    sauvegarde une liste de serpents dans un fichier
    paramètres: nom_fic une chaine de caractère donnant le nom du fichier
                liste_serpents une liste de serpents
    resultat: aucun, mais va créer le fichier avec les informations concernant les serpents dedans
    '''
    fic=open(nom_fichier,'w')
    cpt=0
    for serpent in liste_serpents:
        ligne=serpent[0]+','+str(serpent[1])+','+str(serpent[2])+'\n'
        fic.write(ligne)
        cpt+=1
    fic.close()
    return cpt   

def charger_serpents(nom_fichier):
    '''
    charge une liste de serpents contenue dans un fichier en mémoire
    paramètre: nom_fichier 
    resultat: la liste des serpents contenus dans le fichier
    '''
    fic=open(nom_fichier,'r')
    res=[]
    for ligne in fic:
        (nom,taille,danger)=ligne.split(',')
        res.append((nom,float(taille),int(danger)))
    fic.close()
    return res

####################################################
### PROGRAMME PRINCIPAL                          ###
####################################################
if __name__=='__main__':
    rep=input('Voulez-vous charger une liste de serpents (O/N)? ')
    if rep=='O' or rep=='o':
        nom_fichier=input('Donnez le nom du fichier ')
        liste_serpents=charger_serpents(nom_fichier)
    else:
        liste_serpents=[]
    fini=False
    while not fini:
        print('-'*20)
        print('1. Afficher la liste des serpents')
        print('2. Ajouter de nouveaux serpents')
        print('3. Sauvegarder votre liste de serpents')
        print('4. Rechercher les serpents les plus dangereux')
        print('5. Calculer la taille moyenne des serpents pour un niveau de danger')
        print('6. Afficher le nombre de serpents par niveau de danger')
        print('7. Quitter')
        rep=input('Tapez votre choix ')
        if rep=='1':
            print(liste_serpents_to_str(liste_serpents))
        elif rep=='2':
            ajouter_serpents(liste_serpents)
        elif rep=='3':
            nom_fic=input("Donnez le nom du fichier SVP ")
            nbSerpents=sauver_serpents(nom_fic,liste_serpents)
            print(nbSerpents,"sauvegardés")
        elif rep=='4':
            lesDangereux=rechercher_dangereux(liste_serpents)
            print('Voici leurs noms:',lesDangereux)
        elif rep=='5':
            danger=input("Entrez le niveau de dangerosité recherchée ")
            if danger in ["0","1","2","3","4","5"]:
                taille=moyenne_taille_dangerosite(liste_serpents,int(danger))
                print('Voici le résultat',taille)
            else:
                print("Dangerosité inconnue!!!!")
        elif rep=='6':
            nbParNiv=nb_serpents_par_danger(liste_serpents)
            for i in range(len(nbParNiv)):
                print('niveau',i,'nb serpents',nbParNiv[i])
        elif rep=='7':
            fini=True
        else:
            print('Réponse incorrecte!!!')
