import java.sql.*;
import java.util.List;

public class JoueurBD {
    private ConnexionMySQL laConnexion;

    public JoueurBD(ConnexionMySQL laConnexion){
        this.laConnexion=laConnexion;
    }

    public int maxNumJoueur() throws SQLException{
      Statement st;
      st = laConnexion.createStatement();
      ResultSet rs = st.executeQuery("Select ifnull(max(numJoueur),0) lemax FROM JOUEUR");
      rs.next();
      int res = rs.getInt("lemax");
      rs.close();
      return res;
    }

    public Joueur rechercherJoueurParNum(int num)throws SQLException{
      Statement st = this.laConnexion.createStatement();
      ResultSet rs = st.executeQuery("select * from JOUEUR where numJoueur="+num);

      if (rs.next()){
        Joueur j = new Joueur(rs.getInt("numJoueur"), rs.getString(2), rs.getString("motdepasse"), rs.getInt("niveau"), rs.getString("sexe").charAt(0), rs.getString("abonne").charAt(0)=='O', rs.getBytes("avatar"));
        rs.close();
        return j;
      }
      else {
        throw new SQLException("Joueur non trouvé");
      }
    }

    public int insererJoueur( Joueur j) throws  SQLException{
      PreparedStatement pst = this.laConnexion.prepareStatement("insert into JOUEUR values(?,?,?,?,?,?,?)");
     int nouvNum = maxNumJoueur()+1;
     pst.setInt(1,nouvNum);
     pst.setString(2,j.getPseudo());
     pst.setString(3, j.getMotdepasse());
     pst.setString(4, "" + j.getSexe());
     String abo;
     if(j.isAbonne()) abo = "O"; else abo = "N";
     pst.setString(5, abo);
     pst.setInt(6, j.getNiveau());
     Blob b= laConnexion.createBlob();
     b.setBytes(1, j.getAvatar());
     pst.setBlob(7, b);
     pst.executeUpdate();
     return nouvNum;
    }

    public void effacerJoueur(int num) throws SQLException {
      PreparedStatement patrick = this.laConnexion.prepareStatement("delete from JOUEUR where numJoueur="+num);
      int michel = patrick.executeUpdate();
      if (michel == 0){
        throw new SQLException("PATRICK ET MICHEL ONT RENCONTRER UN PROBLÉME");
      }
    }

    public void majJoueur(Joueur j)throws SQLException{
      effacerJoueur(j.getIdentifiant());
      insererJoueur(j);
    }

    public List<Joueur> listeDesJoueurs() throws SQLException{
        return null;
    }

    public String rapportMessage() throws SQLException{
        return "A faire";
    }
}
