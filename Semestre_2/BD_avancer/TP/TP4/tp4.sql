--question 1
create or replace view Produit10 as
select *
from PRODUIT
where puProd > 10;

select * from Produit10;

--question 2
create or replace view DixMars2015 as
  select refprod, nomprod, puprod
  from PRODUIT natural join FACTURE natural join DETAIL
  where dateFac = STR_TO_DATE('10-03-2015','%d-%m-%Y');

--question 3
create or replace view NbClientsParVille as
  select adresseCli, count(numCli)
  from CLIENT
  group by adresseCli;
