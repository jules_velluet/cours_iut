-- TP 1
-- Nom: Velluet  , Prenom: Jules

-- +------------------+--
-- * Question 1 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Donner la liste des panels dont fait partie Caroline BOURIER.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------------+
-- | nomPan          |
-- +-----------------+
-- | France global 1 |
-- +-----------------+
-- = Reponse question 1.
select nompan
from PANEL natural join CONSTITUER natural join SONDE
where nomsond='BOURIER' and prenomsond='Caroline';

select nompan
from PANEL
where idpan in (select idpan
from CONSTITUER
where numsond in (select numsond
from SONDE
where nomsond='BOURIER' and prenomsond='Caroline'));

-- +------------------+--
-- * Question 2 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les panels dont un des sondés est de la tranche d'âge 70 à 120 ans?

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------------+
-- | nomPan          |
-- +-----------------+
-- | France global 1 |
-- | France global 2 |
-- +-----------------+
-- = Reponse question 2.
select DISTINCT nompan
from PANEL natural join CONSTITUER natural join SONDE natural join CARACTERISTIQUE natural join TRANCHE
where idtr='6';
select nompan
from PANEL
where idpan in (select idpan
from CONSTITUER
where numsond in (select numsond
from SONDE
where idc in (select idc
from CARACTERISTIQUE
where idtr in (select idtr
from TRANCHE
where valdebut=70 and valfin=120))));

-- +------------------+--
-- * Question 3 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les sondés de la tranche d'age 70-120 ans qui sont ouvriers?

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------+------------+
-- | nomSond   | prenomSond |
-- +-----------+------------+
-- | ERYS      | Imane      |
-- | BERRGAIES | Claire     |
-- | JABAT     | Rose       |
-- | WALLOCHE  | Marion     |
-- | LENUJA    | Pauline    |
-- | etc...
-- = Reponse question 3.
select distinct nomsond, prenomsond
from SONDE natural join CARACTERISTIQUE natural join TRANCHE natural join CATEGORIE
where ValDebut=70 and ValFin=120 and intituleCat='Ouvriers';

select nomsond,prenomsond
from SONDE
where idc in (select idc
from CARACTERISTIQUE
where idtr in (select idtr
from TRANCHE
where ValDebut=70 and ValFin=120 and idcat in (select idcat
from CATEGORIE
where intituleCat='Ouvriers')));

-- +------------------+--
-- * Question 4 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les ouvriers qui portent le prénom Olivier?

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------+------------+
-- | nomSond   | prenomSond |
-- +-----------+------------+
-- | THALOUERD | Olivier    |
-- | POTRININ  | Olivier    |
-- +-----------+------------+
-- = Reponse question 4.
select nomsond, prenomsond
from SONDE natural join CARACTERISTIQUE natural join CATEGORIE
where intitulecat='Ouvriers' and prenomsond='Olivier';

select nomsond, prenomsond
from SONDE
where prenomsond='Olivier' and idc in (select idc
from CARACTERISTIQUE
where idcat in (select idcat
from CATEGORIE
where intitulecat='Ouvriers'));
-- +------------------+--
-- * Question 5 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les tranches d'âge qui comportent une ou plusieurs femmes nées un 25 avril?

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------+--------+
-- | valDebut | valFin |
-- +----------+--------+
-- | 40       | 49     |
-- +----------+--------+
-- = Reponse question 5.
select distinct valdebut,valfin
from TRANCHE natural join CARACTERISTIQUE natural join SONDE
where sexe='F' and MONTH(dateNaissond)='4' and DAY(datenaissond)='25';

select valdebut, valfin
from TRANCHE
where idtr in (select idtr
from CARACTERISTIQUE
where sexe='F' and idc in (select idc
from SONDE
where MONTH(dateNaissond)='4' and DAY(datenaissond)='25'));
-- +------------------+--
-- * Question 6 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les sondés prénommés Jean qui appartiennent à au moins 2 panels différents?

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------------+----------+
-- | prenomSond | nomSond  |
-- +------------+----------+
-- | Jean       | DILY     |
-- | Jean       | JATECHU  |
-- | Jean       | PIETIENE |
-- | Jean       | FAL      |
-- | Jean       | BOYEGHE  |
-- +------------+----------+
-- = Reponse question 6.
select prenomSond, nomsond
from SONDE
where prenomsond= "Jean" and numsond in (
    select c1.numsond
    from CONSTITUER c1, CONSTITUER c2
    where c1.numsond = c2.numsond and c1.idpan != c2.idpan);
