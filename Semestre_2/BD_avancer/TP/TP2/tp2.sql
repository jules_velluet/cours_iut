-- TP 2
-- Nom: Velluet  , Prenom: Jules

-- +------------------+--
-- * Question 1 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les panels dont ne fait pas partie Louane DJARA?

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------------+
-- | nomPan          |
-- +-----------------+
-- | France global 1 |
-- | Moins de 50 ans |
-- +-----------------+
-- = Reponse question 1.
select nompan
from PANEL
where idpan not in ( select idpan
from CONSTITUER
where numsond in (select numsond
from SONDE
where prenomsond='Louane' and nomsond='DJARA'));


-- +------------------+--
-- * Question 2 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les prénoms de sondé commençant par un A qui n'apparaissent pas dans la tranche d'age 20-29 ans? Classez ces noms par ordre alphabétique.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------------+
-- | prenomSond |
-- +------------+
-- | Alice      |
-- | Allan      |
-- | Amaury     |
-- | Ambre      |
-- | Anaïs      |
-- | etc...
-- = Reponse question 2.

select distinct prenomsond
from SONDE
where prenomsond LIKE 'A%' and prenomsond not in (select prenomsond
from SONDE
where idc in (select idc
from CARACTERISTIQUE
where idtr  in (select idtr
from TRANCHE
where idtr=1)))
order by prenomsond;

-- +------------------+--
-- * Question 3 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--   Quels sont les panels dont tous les sondés ont moins de 60 ans? Rappel: CURDATE() donne la date du jour et DATEDIFF(d1,d2) donne le nombre de jours entre d1 et d2.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------------+
-- | nomPan          |
-- +-----------------+
-- | Moins de 50 ans |
-- +-----------------+
-- = Reponse question 3.

select nompan
from PANEL
where idpan not in (select idpan
from CONSTITUER
where numsond in (select numsond
from SONDE
where DATEDIFF(CURDATE(),datenaissond)>60*365));

-- +------------------+--
-- * Question 4 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quelles sont les catégories qui comportent des personnes nées en 1975? On rappelle que YEAR(d) donne l'année de la date d sous la forme d'un entier.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-------------------------------------------------+
-- | intituleCat                                     |
-- +-------------------------------------------------+
-- | Cadres, professions intellectuelles supérieures |
-- | Professions intermédiaires                      |
-- | Employés                                        |
-- | Ouvriers                                        |
-- | Inactifs ayant déjà travaillé                   |
-- +-------------------------------------------------+
-- = Reponse question 4.

select intituleCat
from CATEGORIE
where idcat in (select idcat
from CARACTERISTIQUE
where idc in (select idc
from SONDE
where YEAR(datenaissond)=1975));

-- +------------------+--
-- * Question 5 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--   Quels sont les sondés nés en 1997 qui appartiennent aux panels France global 1 et France global 2?

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------------+------------+
-- | nomSond    | prenomSond |
-- +------------+------------+
-- | TRISAULOLU | Elise      |
-- | MAMIAT     | Mathieu    |
-- | NEUSIL     | Theo       |
-- +------------+------------+
-- = Reponse question 5.

select nomsond, prenomsond
from SONDE natural join CONSTITUER natural join PANEL
where nompan='France global 1' and YEAR(datenaissond)=1997 and numsond in (select numsond
from SONDE natural join CONSTITUER natural join PANEL
where nompan='France global 2' and YEAR(datenaissond)=1997);

-- +------------------+--
-- * Question 6 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les sondés nés en 1975 qui ont la même date de naissance?

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +---------+------------+----------+------------+
-- | nomSond | prenomSond | nomSond  | prenomSond |
-- +---------+------------+----------+------------+
-- | DASA    | Maxime     | PEKARDAC | Bilal      |
-- +---------+------------+----------+------------+
-- = Reponse question 6.

select s1.nomsond, s1.prenomsond, s2.prenomsond, s2.nomsond
from SONDE s1, SONDE s2
where s1.datenaissond=s2.datenaissond and YEAR(s1.datenaissond)=1975 and s2.numsond<s1.numsond;
