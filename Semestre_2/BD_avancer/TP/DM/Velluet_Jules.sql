-- Devoir 211
-- Nom: Velluet , Prenom: Jules

-- Devoir maison 1
-- On rappelle que la fonction YEAR(date) retourne l'ann�e d'une date
--
-- Veillez � bien r�pondre aux emplacements indiqu�s.
-- Seule la premi�re requ�te est prise en compte.

-- +-----------------------+--
-- * Question 211509 : 2pts --
-- +-----------------------+--
-- Ecrire une requ�te qui renvoie les informations suivantes:
--  Quels sont les sond�s n�s en 1943 sont des Ouvriers?

-- Voici le d�but de ce que vous devez obtenir.
-- ATTENTION � l'ordre des colonnes et leur nom!
-- +---------+---------+------------+
-- | numSond | nomSond | prenomSond |
-- +---------+---------+------------+
-- | 387     | MARIEI  | Willy      |
-- | 985     | MAUHORI | Thibault   |
-- +---------+---------+------------+
-- = Reponse question 211509.

select numsond, nomsond, prenomsond
from SONDE natural join CARACTERISTIQUE natural join CATEGORIE
where YEAR(datenaissond)=1943 and intitulecat='Ouvriers';

-- +-----------------------+--
-- * Question 211532 : 2pts --
-- +-----------------------+--
-- Ecrire une requ�te qui renvoie les informations suivantes:
--  Quels sont les Panels qui ne comportent aucun sond� pr�nomm� Titouan?

-- Voici le d�but de ce que vous devez obtenir.
-- ATTENTION � l'ordre des colonnes et leur nom!
-- +-------+-----------------+
-- | idPan | nomPan          |
-- +-------+-----------------+
-- | 2     | Moins de 50 ans |
-- | 3     | France global 2 |
-- +-------+-----------------+
-- = Reponse question 211532.

select idpan, nompan
from PANEL
where idpan not in (select idpan
from CONSTITUER natural join SONDE
where prenomsond = 'Titouan');

-- +-----------------------+--
-- * Question 211587 : 2pts --
-- +-----------------------+--
-- Ecrire une requ�te qui renvoie les informations suivantes:
--  Quel est le nombre d'hommes par cat�gorie?

-- Voici le d�but de ce que vous devez obtenir.
-- ATTENTION � l'ordre des colonnes et leur nom!
-- +-------+-------------------------------------------------+----------+
-- | idCat | intituleCat                                     | nbHommes |
-- +-------+-------------------------------------------------+----------+
-- | 1     | Agriculteurs exploitants                        | 8        |
-- | 2     | Artisans, commer�ants, chefs d'entreprise       | 33       |
-- | 3     | Cadres, professions intellectuelles sup�rieures | 62       |
-- | 4     | Professions interm�diaires                      | 94       |
-- | 5     | Employ�s                                        | 104      |
-- | 6     | Ouvriers                                        | 87       |
-- | 7     | Inactifs ayant d�j� travaill�                   | 214      |
-- | 8     | Autres sans activit� professionnelle            | 72       |
-- +-------+-------------------------------------------------+----------+
-- = Reponse question 211587.

select idcat, intitulecat, count(*) nbHommes
from CATEGORIE natural join CARACTERISTIQUE natural join SONDE
where sexe='H'
group by idcat;

-- +-----------------------+--
-- * Question 211633 : 2pts --
-- +-----------------------+--
-- Ecrire une requ�te qui renvoie les informations suivantes:
--  Quels sont les panels qui comportent plus de 15 personnes n�es en 1994?

-- Voici le d�but de ce que vous devez obtenir.
-- ATTENTION � l'ordre des colonnes et leur nom!
-- +-------+-----------------+--------+
-- | idPan | nomPan          | nb1994 |
-- +-------+-----------------+--------+
-- | 2     | Moins de 50 ans | 23     |
-- +-------+-----------------+--------+
-- = Reponse question 211633.

select idpan, nompan, count(*) nb1994
from PANEL natural join CONSTITUER natural join SONDE
where YEAR(datenaissond)=1994
group by idpan having count(*)>15;

-- +-----------------------+--
-- * Question 211688 : 2pts --
-- +-----------------------+--
-- Ecrire une requ�te qui renvoie les informations suivantes:
--  Quelles sont les cat�gories dont tous les sond�s pr�nomm�s Louane sont n�s en avril? (on consid�re que les cat�gories qui n'ont aucun sond� se pr�nomme Louane font partie de la r�ponse) On rappelle que MONTH(date) donne le num�ro du mois d'une date.

-- Voici le d�but de ce que vous devez obtenir.
-- ATTENTION � l'ordre des colonnes et leur nom!
-- +-------+-------------------------------------------------+
-- | idcat | intitulecat                                     |
-- +-------+-------------------------------------------------+
-- | 1     | Agriculteurs exploitants                        |
-- | 3     | Cadres, professions intellectuelles sup�rieures |
-- +-------+-------------------------------------------------+
-- = Reponse question 211688.

select distinct idcat, intitulecat
from CATEGORIE natural join CARACTERISTIQUE natural join SONDE
where prenomsond = 'Louane' and MONTH(datenaissond)=4 or idcat not in (select idcat
from CATEGORIE natural join CARACTERISTIQUE natural join SONDE
where prenomsond='Louane');
