--exercice 2
create procedure EditerMembres()
begin
    declare fini int default false;
    declare CsigleCl, CnomCl, Cnom, Cprenom varchar(50);
    declare sigleavnt default "";
    declare cpt default 0;
    declare res varchar(300)
    declare listeMembres cursor for
        select sigleCl, nomCl, nom, prenom
        from club natural join coureur
        order by sigleCl, nom;
    declare continue handler for notfound set fini = true;
    open listeMembres;
    while not fini do
        fetch listeMembres into CsigleCl, CnomCl, Cnom, Cprenom;
        if not fini then
          if nomClavant != CsigleCl THEN
            if sigleavnt != "" then
              imprimer(cptn, " membres");
            end if;
            set cpt = 0;
            imprimer("Club ", CsigleCl, " -- ", CnomCl, "/n");

          end if;
          set cpt = cpt + 1;
          imprimer(Cnom, " ", Cprenom);
          set sigleavnt = CsigleCl;
    end while;
  imprimer(cpt, " membres");
  close listeMembres;
end;
