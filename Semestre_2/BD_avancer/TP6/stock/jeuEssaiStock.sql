insert into CATEGORIE(idCat,nomCat) values
       (1,'Alimentaire'),
       (2,'Bricolage'),
       (3,'Vaisselle'),
       (4,'Boisson');

insert into RAYON(idRayon,nomRayon) values
       (1,'Fournitures scolaires'),
       (2,'Bazar'),
       (3,'Soda et jus de fruits'),
       (4,'Produits frais'),
       (5,'Poissonnerie');

insert into PRODUIT(refProd,intituleProd,prixUnitaireProd,idCat) values
       (1,'Cabillaud',12.2,1),
       (2,'Crabe',14.4,1),
       (3,'Crayon de papier',0.45,2),
       (4,'Marteau',4.8,2),
       (5,'Tournevis',2.5,2),
       (6,'Verre',1.1,3),
       (7,'Assiette',0.45,3),
       (8,'Couteau',1.25,3),
       (9,'Fourchette',1.0,3),
       (10,'Jus de pommes',1.2,4),
       (11,'Limonade',0.8,4),
       (12,'Cola',0.9,4),
       (13,'Perceuse',52,2);

insert into CONTENIR(refProd,idRayon,Qte) values
       (1,5,43),
       (2,5,12),
       (3,2,102),
       (3,1,214),
       (4,2,11),
       (5,2,8),
       (6,2,18),
       (7,2,24),
       (8,2,25),
       (10,3,41),
       (11,3,22),
       (12,3,14),
       (12,5,11),
       (12,1,10),
       (12,2,14);
       
       
