CREATE DATABASE IF NOT EXISTS Stock DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE Stock;

CREATE TABLE CATEGORIE (
  idCat decimal(5,0),
  nomCat varchar(30),
  PRIMARY KEY (idCat)
);

CREATE TABLE PRODUIT (
  refProd decimal(8,0),
  intituleProd varchar(30),
  prixUnitaireProd decimal(8,2),
  idCat decimal(5,0),
  PRIMARY KEY (refProd)
);

CREATE TABLE CONTENIR (
  refProd decimal(8,0),
  idRayon decimal(3,0),
  Qte decimal(5,0),
  PRIMARY KEY (refProd, idRayon)
);

CREATE TABLE RAYON (
  idRayon decimal(3,0),
  nomRayon varchar(30),
  PRIMARY KEY (idRayon)
);

ALTER TABLE PRODUIT ADD FOREIGN KEY (idCat) REFERENCES CATEGORIE (idCat);
ALTER TABLE CONTENIR ADD FOREIGN KEY (idRayon) REFERENCES RAYON (idRayon);
ALTER TABLE CONTENIR ADD FOREIGN KEY (refProd) REFERENCES PRODUIT (refProd);
