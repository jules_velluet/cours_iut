delimiter |
drop function if exists bonjour |
create or replace function bonjour(nom varchar(30)) returns varchar(30)
  begin
    declare res varchar(60);
    set res = concat('bonjour ',nom);
    return res;
    end |
delimiter;

delimiter |
drop function if exists premier |
create or replace function premier(liste varchar(255)) returns varchar(30)
  begin
    declare res varchar(60);
    declare i int;
    set i = locate(',',liste);
    if i = 0 THEN
      set res = liste;
    else
      set res = substring(liste,1,i-1);
    end if;
    return res;
    end |
  delimiter ;

delimiter |
drop function if exists reste |
create or replace function reste(liste varchar(255)) returns varchar(30)
  begin
    declare res varchar(60);
    declare i int;
    set i = locate(',',liste);
    if i = 0 THEN
      set res = '';
    else
      set res = substring(liste,i+1);
    end if;
    return res;
    end |
  delimiter ;

delimiter |
drop procedure if exists testerliste |
create or replace procedure testerliste(liste varchar(250))
  begin
  declare prem varchar(50);
  while liste!='' do
    set prem = premier(liste);
    set liste = reste(liste);
    if liste != '' then
      insert into TEST_TP6 values(prem);
    end if;
  end while;
end |
delimiter ;

delimiter |
drop function if exists nbFormules |
create or replace function nbFormules(nomCompose varchar(20)) returns int
  begin
    declare res int;
    select count(*) into res
    From CONSTITUER natural join COMPOSE
    where nomComp=nomCompose;
    return res;
  end |
delimiter ;

delimiter |
drop procedure if exists creerFormule |
create or replace procedure creerFormule(idF varchar(5), nomF varchar(15), listeComposes varchar(255))
  begin
    declare prem varchar(50);
    set prem = premier (listeComposes);
    insert into FORMULE values (idF,nomF);
    insert into CONSTITUER values (idF,prem,1.0);
  end |
delimiter ;
