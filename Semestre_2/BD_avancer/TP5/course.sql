DROP TABLE EFFECTUER;
DROP TABLE COUREUR;
DROP TABLE COURSE;
DROP TABLE EVENEMENT;
DROP TABLE CLUB;
DROP TABLE CATEGORIE;
DROP TABLE LIEU;

CREATE TABLE COURSE (
  idCourse int,
  heure decimal,
  idEv int,
  idCat int,
  PRIMARY KEY (idCourse,idEv)
);

CREATE TABLE CATEGORIE (
  idCat int,
  nomCat varchar(50),
  PRIMARY KEY (idCat)
);

CREATE TABLE COUREUR (
  idCo int,
  nomCo varchar(50),
  prenomCo varchar(50),
  idCat int,
  idCl int,
  PRIMARY KEY (idCo)
);

CREATE TABLE CLUB (
  idCl int,
  sigleCl varchar(50),
  nomCl varchar(50),
  PRIMARY KEY (idCl)
);

CREATE TABLE EFFECTUER (
  idCo int,
  idEv int,
  idCourse int,
  temps decimal,
  PRIMARY KEY (idCo,idEv,idCourse)
);

CREATE TABLE EVENEMENT (
  idEv int,
  nomEv varchar(50),
  dateEv date,
  idL int,
  PRIMARY KEY (idEv)
);

CREATE TABLE LIEU(
  idL int,
  nomL varchar(50),
  PRIMARY KEY (idL)
);

ALTER TABLE EVENEMENT ADD FOREIGN KEY (idL) REFERENCES LIEU (idL);
ALTER TABLE COURSE ADD FOREIGN KEY (idEv) REFERENCES EVENEMENT (idEv);
ALTER TABLE COURSE ADD FOREIGN KEY (idCat) REFERENCES CATEGORIE (idCat);
ALTER TABLE EFFECTUER ADD FOREIGN KEY (idCo) REFERENCES COUREUR (idCo);
ALTER TABLE EFFECTUER ADD FOREIGN KEY (idEv) REFERENCES EVENEMENT (idEv);
ALTER TABLE EFFECTUER ADD FOREIGN KEY (idEv, idCourse) REFERENCES COURSE (idEv,idCourse);
ALTER TABLE COUREUR ADD FOREIGN KEY (idCat) REFERENCES CATEGORIE (idCat);
ALTER TABLE COUREUR ADD FOREIGN KEY (idCl) REFERENCES CLUB (idCl);
