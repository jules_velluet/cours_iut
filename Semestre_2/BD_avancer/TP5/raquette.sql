select *
from EVENEMENT
where idL in (select idL
from LIEU
where nomL='Orleans')
ORDER BY dateEv;

select idCo, nomCo, prenomCo
from COUREUR natural join CLUB natural join CATEGORIE natural join EFFECTUER natural join COURSE natural join EVENEMENT
where sigleCl='USO' and nomCat='Senior' and nomEv='les Foulées d’Orléans'
ORDER BY temps;

select idCourse
from COURSE natural join EFFECTUER natural join COUREUR natural join CLUB
where sigleCl='USO';

select idCo, nomCo, prenomCo, idCat, idCl
from COUREUR natural join EFFECTUER natural join COURSE natural join CATEGORIE
where nomCat='Junior';

select idCo, nomCo, prenomCo, idCat, idCl
from COUREUR natural join EFFECTUER natural join COURSE natural join EVENEMENT
where nomEv='les Foulées d’Orléans' and idEV in (select idEv
  from EVENEMENT
  where nomEv='Corrida d’Olivet');
