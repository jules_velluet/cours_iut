insert into LIEU(idL, nomL) values
  (1, "Orleans"),
  (2, "Olivet"),
  (3, "Tours");

insert into CATEGORIE(idCat, nomCat) values
  (1, 'Senior'),
  (2, "Junior");

insert into CLUB(idCl, sigleCl, nomCl) values
  (1, 'USO', 'Union Sportive Orleans'),
  (2, "ASM", "Association Sportive de Montargis");

insert into COUREUR(idCo, nomCo, prenomCo, idCat, idCl) values
  (1,'Dupont', 'Jean', 1, 1),
  (2,'Duval', 'Marie', 2, 2);

insert into EVENEMENT(idEv, nomEv, dateEv, idL) values
  (1,"les Foulées d’Orléans", STR_TO_DATE('12/12/2015', '%d/%m/%Y'), 1),
  (2, "Corrida d’Olivet", STR_TO_DATE('25/12/2015', '%d/%m/%Y'), 2),
  (3, "Cross’O d’Orléans", STR_TO_DATE('10/01/2016', '%d/%m/%Y'), 1),
  (4, "Vite-Tours", STR_TO_DATE('18/03/2016', '%d/%m/%Y'),3);

insert into COURSE(idCourse,heure, idEv, idCat) values
  (1,9.00,1,1),
  (2, 11.00, 2, 2),
  (3, 10.00, 3, 2),
  (4, 9.00, 4, 1),
  (5, 10.30, 4, 2);

insert into EFFECTUER(idCo, idEv, idCourse, temps) values
  (1, 1, 1, 12.32),
  (2, 2, 2, 11.53),
  (2, 3, 3, 8.59);
