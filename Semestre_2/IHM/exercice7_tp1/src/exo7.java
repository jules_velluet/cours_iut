import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import javax.swing.*;

import static javafx.scene.text.TextAlignment.CENTER;
import static javax.swing.SwingConstants.*;

public class exo7 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private Scene creerBorderPane() {
        BorderPane border = new BorderPane();

        //CONFIGURATION

        //TOP
        Label top = new Label("Fiche Joueur");
        top.setFont(new Font("Arial",24));
        border.setTop(top);
        BorderPane.setAlignment(top,Pos.TOP_CENTER);

        //Left
        VBox p = new VBox();
        Button b1 = new Button("Début");
        Button b2 = new Button("Prec");
        Button b3 = new Button("Valider");
        Button b4 = new Button("Nouveau");
        Button b5 = new Button("Suiv");
        Button b6  = new Button("Dern");
        b1.setPrefWidth(100);
        b2.setPrefWidth(100);
        b3.setPrefWidth(100);
        b4.setPrefWidth(100);
        b5.setPrefWidth(100);
        b6.setPrefWidth(100);
        p.getChildren().add(b1);
        p.getChildren().add(b2);
        p.getChildren().add(b3);
        p.getChildren().add(b4);
        p.getChildren().add(b5);
        p.getChildren().add(b6);
        p.setBackground(new Background(new BackgroundFill(Color.BEIGE, CornerRadii.EMPTY, Insets.EMPTY)));
        p.setPadding(new Insets(100,5,10,5));
        p.setSpacing(5);
        border.setLeft(p);

        //Center
        GridPane c =new GridPane();
        Label lpseudo = new Label("Pseudo: ");
        TextField tpseudo = new TextField();
        Label lmdp = new Label("Mot de Passe");
        PasswordField mdp = new PasswordField();
        Label niv = new Label("Niveau");
        ChoiceBox cniv = new ChoiceBox();
        RadioButton homme = new RadioButton("Homme");
        RadioButton femme = new RadioButton("Femme");
        ImageView img = new ImageView("/pinguin.png");
        img.setFitWidth (100);
        img.setPreserveRatio (true);
        c.add(img,4,1);
        c.add(lpseudo,1,2);
        c.add(tpseudo,2,2);
        c.add(lmdp,1,3);
        c.add(mdp,2,3);
        c.add(niv,1,4);
        c.add(cniv,2,4);
        c.add(homme,2,5);
        c.add(femme,2,6);
        c.setBackground(new Background(new BackgroundFill(Color.LIGHTSEAGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        c.setPadding(new Insets(10,10,10,20));
        c.setVgap(10);
        border.setCenter(c);

        //Right
//        ImageView img = new ImageView("/pinguin.png");
//        img.setFitWidth (100);
//        img.setPreserveRatio (true);
//        border.setRight(img);

        //Bottom
        BorderPane bu = new BorderPane();
        Label fichier = new Label("Nb de fichiers: 102");
        Label fiche = new Label("Numero de la fiche: 10");
        Label modifier = new Label("Fiche modifiée");
        bu.setLeft(fichier);
        bu.setCenter(fiche);
        bu.setRight(modifier);
        bu.setBackground(new Background(new BackgroundFill(Color.AQUAMARINE, CornerRadii.EMPTY, Insets.EMPTY)));
        bu.setPadding(new Insets(0,5,0,5));
        border.setBottom(bu);
        return new Scene(border, 500, 400);

    }
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle(" Ma première application JavaFX ");
        primaryStage.setScene(creerBorderPane());
        primaryStage.show();
    }
}
