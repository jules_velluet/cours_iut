public class Score{

    private String personne;
    private int score;

    public Score(String personne, int score){
        this.personne = personne;
        this.score = score;
    }

    public int getScore() {
        return this.score;
    }

    public String getPersonne(){
        return this.personne;
    }
}
