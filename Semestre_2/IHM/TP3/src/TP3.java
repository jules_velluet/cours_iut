import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class TP3 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private TextField nom;
    private TextField point;
    private TitledPane tableau;

    @Override
    public void start(Stage primaryStage) {
        ListeScore model = new ListeScore(5);
        primaryStage.setTitle("Meilleur Score");
        GridPane p = new GridPane();
        this.nom = new TextField();
        Label noml = new Label("Nom du joueur: ");
        Label pointl = new Label("Points obtenus: ");
        this.point = new TextField();
        Button valider = new Button("Valider");
        controleur abo = new controleur(this,model);
        valider.setOnAction(abo);
        this.tableau = new TitledPane();
        this.tableau.setText("tableau des scores");
        p.add(noml,1,1);
        p.add(this.nom,2,1);
        p.add(pointl,3,1);
        p.add(this.point,4,1);
        p.add(valider,3,2);
        p.add(this.tableau,3,5);
        Scene sc = new Scene(p ,600 ,200);
        primaryStage.setScene(sc);
        primaryStage.show();
    }

    public TextField getNomJoueur(){
        return this.nom;
    }

    public TextField getNbPoints(){
        return this.point;
    }

    public TitledPane getTableau(){return this.tableau;}
}
