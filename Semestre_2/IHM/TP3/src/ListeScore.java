import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;

public class ListeScore{

    private List<Score> listeScore;
    private int tailleMaxi;

    public ListeScore(int tailleMaxi){
        this.listeScore = new ArrayList<>();
        this.tailleMaxi = tailleMaxi;
    }

    public int getNbPlaces(){
        return this.tailleMaxi - this.listeScore.size();
    }

    public int getNbScores(){
        return this.listeScore.size();
    }

    public String getScore(int place){
        Score a = this.listeScore.get(place);
        return "" + a.getPersonne() + " " + a.getScore();
    }

    public void ajouterScore(String nom, int nbPoints){
        Score b = new Score(nom, nbPoints);
        this.listeScore.add(b);
        Comparator<Score> comp = new ComparateurScore();
        Collections.sort(this.listeScore, comp);

        if (this.listeScore.size() > tailleMaxi){
            this.listeScore.remove(this.tailleMaxi);
        }
    }
}
