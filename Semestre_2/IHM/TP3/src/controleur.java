import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;


public class controleur implements EventHandler<ActionEvent>{

    private TP3 form;
    private ListeScore liste;

    controleur(TP3 form, ListeScore liste){
        this.form = form;
        this.liste = liste;
    }

    public void handle(ActionEvent actionEvent){
        Button valider = (Button) actionEvent.getSource();
        Button valider1 = (Button) actionEvent.getTarget();
        Float youpi;

        try {
            this.liste.ajouterScore(this.form.getNomJoueur().getText(),Integer.valueOf(this.form.getNbPoints().getText()));
            Integer.parseInt(form.getNbPoints().getText());
            VBox box = new VBox();
            for (int i=0; i<this.liste.getNbScores(); i++){
                box.getChildren().add(new Label(this.liste.getScore(i)));
            }

            this.form.getTableau().setContent(box);
        }
        catch (NumberFormatException e){
            Alert al = new Alert(Alert.AlertType.ERROR);
            al.setTitle("Attention!!!");
            al.setHeaderText("format non pris en charge");
            al.setContentText("vous avez rentré un caractére qui n'est pas un int");
            al.showAndWait();
            this.form.getNbPoints().requestFocus();
        }


    }
}
