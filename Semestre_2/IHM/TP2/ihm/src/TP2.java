import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class TP2 extends Application {

    public static void main(String[] args) {
        launch(args);
    }
    private TextField tf;
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Evenement");
        FlowPane pane = new FlowPane();
        Button bouton = new Button("Bouton 1");
        ActionButton abo = new ActionButton(this);
        pane.getChildren().add(bouton);
        bouton.setOnAction(abo);
        Button b = new Button("Bouton 2");
        b.setOnAction(abo);
        this.tf=new TextField();
//        tf = new TextField();
        pane.getChildren().add(tf);
        pane.getChildren().add(b);
        Scene sc = new Scene(pane ,500 ,100);
        primaryStage.setScene(sc);
        primaryStage.show();
    }
    public TextField  getTf(){ return this.tf;}
}
