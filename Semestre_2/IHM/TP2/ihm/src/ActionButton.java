import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

    public class ActionButton implements EventHandler<ActionEvent> {

        private TP2 form;

        ActionButton(TP2 form){
            this.form = form;
        }
        @Override
        public void handle(ActionEvent actionEvent) {
            Button b= (Button) actionEvent.getSource();
            if(b.getText().endsWith("Bouton 1")) {
                System.out.println("Le champs texte contient " +
                        this.form.getTf().getText());
            }
            else {
                this.form.getTf ().setText("");
                System.out.println("J’efface  le  champ  texte");
            }
            //            Button b = (Button)actionEvent.getTarget();
//            System.out.println("Clic sur " +b.getText());
//            System.out.println("Le text contient " + this.form.tf.getText());
        }
    }
