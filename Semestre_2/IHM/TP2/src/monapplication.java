import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class monapplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private Scene Formulaire() {
        FlowPane g = new FlowPane();
        TextField t = new TextField();
        Button b = new Button("Bouton 1");
        ActionBouton abo = new ActionBouton();
        t.setOnAction(abo);
        b.setOnAction(abo);
        g.getChildren().add(t);
        g.getChildren().add(b);
        return new Scene(g, 700, 400);
    }
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("essai progamation évenement");
        primaryStage.setScene(Formulaire());
        primaryStage.show();
    }
}

    class ActionBouton implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent e) {
            System.out.println("Clic!");
        }
    }
