import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;



public class Calculatrice extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private TextField champ1;
    private TextField champ2;
    public Label resultat;

    @Override
    public void start(Stage primaryStage) {
        FlowPane pane = new FlowPane();
        this.champ1 = new TextField();
        this.champ2 = new TextField();
        this.resultat = new Label("résulat: ");
        RadioButton plus = new RadioButton("addition");
        RadioButton moins = new RadioButton("soustraction");
        RadioButton fois = new RadioButton("multiplication");
        RadioButton diviser = new RadioButton("division");
        plus.setSelected(true);
        ToggleGroup toggle = new ToggleGroup();
        plus.setToggleGroup(toggle);
        moins.setToggleGroup(toggle);
        fois.setToggleGroup(toggle);
        diviser.setToggleGroup(toggle);
        VBox vb = new VBox();
        vb.getChildren().addAll(plus,moins,fois,diviser);
        Button calculer = new Button("+");
        calculer.setPrefSize(100,100);
        Action abo = new Action(this);
        calculer.setOnAction(abo);
        toggle.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
                if (toggle.getSelectedToggle() != null) {
                    RadioButton button = (RadioButton) new_toggle.getToggleGroup().getSelectedToggle();
                    if (button.getText().equals("addition")) {
                        calculer.setText("+");
                    } else if (button.getText().equals("soustraction")) {
                        calculer.setText("-");
                    } else if (button.getText().equals("multiplication")) {
                        calculer.setText("*");
                    } else {
                        calculer.setText("/");
                    }
                }
            }
        });
        pane.getChildren().add(champ1);
        pane.getChildren().add(champ2);
        pane.getChildren().add(vb);
        pane.getChildren().add(calculer);
        pane.getChildren().add(resultat);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(15,15,15,15));
        Scene sc = new Scene(pane ,500 ,200);
        primaryStage.setScene(sc);
        primaryStage.show();
    }
    public TextField  getTf1(){ return this.champ1;}
    public TextField  getTf2(){ return this.champ2;}

    public double division(double d1, double d2) throws DivisionParZero{
        if (d2 == 0){
            throw new DivisionParZero();
        }
        else {
            return d1/d2;
        }
    }
}
