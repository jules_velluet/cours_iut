import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;

public class Action implements EventHandler<ActionEvent>{

    private Calculatrice form;

    Action(Calculatrice form){
        this.form = form;
    }

    public void handle(ActionEvent actionEvent){

        Button calculer = (Button) actionEvent.getSource();
        Button calculer1 = (Button) actionEvent.getTarget();
        Float youpi;


        if (calculer1.getText().endsWith("+")) {
            try {
                youpi = Float.parseFloat(form.getTf1().getText());
                try{
                    youpi = Float.parseFloat(form.getTf2().getText());
                    this.form.resultat.setText(""+(Float.parseFloat(this.form.getTf1().getText()) + Float.parseFloat(this.form.getTf2().getText())));
                }
                catch (NumberFormatException e){
                    Alert al = new Alert(Alert.AlertType.ERROR);
                    al.setTitle("Attention!!!");
                    al.setHeaderText("format non pris en charge");
                    al.setContentText("vous avez rentré un caractére qui n'est pas un chiffre");
                    al.showAndWait();
                    this.form.getTf2().requestFocus();
                }
            }
            catch (NumberFormatException e){
                Alert al = new Alert(Alert.AlertType.ERROR);
                al.setTitle("Attention!!!");
                al.setHeaderText("format non pris en charge");
                al.setContentText("vous avez rentré un caractére qui n'est pas un chiffre");
                al.showAndWait();
                this.form.getTf1().requestFocus();
            }
        }
        else if (calculer1.getText().endsWith("-")) {
            try {
                youpi = Float.parseFloat(form.getTf1().getText());
                try{
                    youpi = Float.parseFloat(form.getTf2().getText());
                    this.form.resultat.setText(""+(Float.parseFloat(this.form.getTf1().getText()) - Float.parseFloat(this.form.getTf2().getText())));
                }
                catch (NumberFormatException e){
                    Alert al = new Alert(Alert.AlertType.ERROR);
                    al.setTitle("Attention!!!");
                    al.setHeaderText("format non pris en charge");
                    al.setContentText("vous avez rentré un caractére qui n'est pas un chiffre");
                    al.showAndWait();
                    this.form.getTf2().requestFocus();
                }
            }
            catch (NumberFormatException e) {
                Alert al = new Alert(Alert.AlertType.ERROR);
                al.setTitle("Attention!!!");
                al.setHeaderText("format non pris en charge");
                al.setContentText("vous avez rentré un caractére qui n'est pas un chiffre");
                al.showAndWait();
                this.form.getTf1().requestFocus();
            }
        }
        else if (calculer1.getText().endsWith("*")){
            try {
                youpi = Float.parseFloat(form.getTf1().getText());
                try{
                    youpi = Float.parseFloat(form.getTf2().getText());
                    this.form.resultat.setText(""+(Float.parseFloat(this.form.getTf1().getText()) * Float.parseFloat(this.form.getTf2().getText())));
                }
                catch (NumberFormatException e){
                    Alert al = new Alert(Alert.AlertType.ERROR);
                    al.setTitle("Attention!!!");
                    al.setHeaderText("format non pris en charge");
                    al.setContentText("vous avez rentré un caractére qui n'est pas un chiffre");
                    al.showAndWait();
                    this.form.getTf2().requestFocus();
                }
            }
            catch (NumberFormatException e) {
                Alert al = new Alert(Alert.AlertType.ERROR);
                al.setTitle("Attention!!!");
                al.setHeaderText("format non pris en charge");
                al.setContentText("vous avez rentré un caractére qui n'est pas un chiffre");
                al.showAndWait();
                this.form.getTf1().requestFocus();
            }
        }
        else if (calculer1.getText().endsWith("/")) {
            try {
                youpi = Float.parseFloat(form.getTf1().getText());
                try{
                    youpi = Float.parseFloat(form.getTf2().getText());
                    double nb1 = Float.parseFloat(this.form.getTf1().getText());
                    double nb2 = Float.parseFloat(this.form.getTf2().getText());

                    this.form.resultat.setText(""+this.form.division(nb1,nb2));
                }
                catch (NumberFormatException e){
                    Alert al = new Alert(Alert.AlertType.ERROR);
                    al.setTitle("Attention!!!");
                    al.setHeaderText("format non pris en charge");
                    al.setContentText("vous avez rentré un caractére qui n'est pas un chiffre");
                    al.showAndWait();
                    this.form.getTf2().requestFocus();
                }
                catch (DivisionParZero oh){
                    Alert al = new Alert(Alert.AlertType.ERROR);
                    al.setTitle("Attention!!!");
                    al.setHeaderText("division par zéro");
                    al.setContentText("On ne peut pas diviser par zéro !!!");
                    al.showAndWait();
                    this.form.getTf2().requestFocus();
                }
            }
            catch (NumberFormatException e) {
                Alert al = new Alert(Alert.AlertType.ERROR);
                al.setTitle("Attention!!!");
                al.setHeaderText("format non pris en charge");
                al.setContentText("vous avez rentré un caractére qui n'est pas un chiffre");
                al.showAndWait();
                this.form.getTf1().requestFocus();
            }
        }
    }
}