import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;

public class romain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private Scene creerFormulaire() {
        javafx.scene.control.Label lNom = new javafx.scene.control.Label("Nom: ");
        Label lPrenom = new Label("Prenom: ");
        javafx.scene.control.TextField tNom = new TextField();
        TextField tPrenom = new TextField();
        FlowPane panel = new FlowPane();
        Label mdp = new Label("Choisissez un mot de passe: ");
        //Label bouton = new Label("Valider");
        Button bbouton = new Button("Valider");
        PasswordField pmdp = new PasswordField();
        panel.setHgap(12);
        panel.setVgap(12);
        panel.getChildren().add(lNom) ;
        panel.getChildren().add(tNom);
        panel.getChildren().add(lPrenom);
        panel.getChildren().add(tPrenom);
        panel.getChildren().add(mdp);
        panel.getChildren().add(pmdp);
        panel.getChildren().add(bbouton);
        return new Scene(panel, 500, 100);
    }

    private Scene creerFormulaireGrid(){
        GridPane p = new GridPane();
        javafx.scene.control.Label lNom = new javafx.scene.control.Label("Nom: ");
        Label lPrenom = new Label("Prenom: ");
        javafx.scene.control.TextField tNom = new TextField();
        TextField tPrenom = new TextField();
        FlowPane panel = new FlowPane();
        Label mdp = new Label("Choisissez un mot de passe: ");
        Button bbouton = new Button("Valider");
        PasswordField pmdp = new PasswordField();
        p.add(lNom, 1, 1);
        p.add(tNom, 2, 1);
        p.add(lPrenom,1,2);
        p.add(tPrenom,2,2);
        p.add(mdp,1,3);
        p.add(pmdp, 2, 3);
        p.add(bbouton,2,4);
        return new Scene(p, 500, 100);
    }
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle(" Ma première application JavaFX ");
        primaryStage.show();
        primaryStage.setScene(creerFormulaireGrid());
    }
}
