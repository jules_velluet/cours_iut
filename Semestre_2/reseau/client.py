import socket

ip_serveur = "127.0.0.1"
port_serveur=5555

#creation de la socket
ma_socket = socket.socket(type= socket.SOCK_DGRAM)

#envoie de donnee binaire
ma_socket.sendto(b"Bonjour serveur",
                 (ip_serveur,port_serveur))

#lecture de la reponse du serveur
BUFSIZE=1024
data= ma_socket.recv(BUFSIZE)
print(data.decode())
