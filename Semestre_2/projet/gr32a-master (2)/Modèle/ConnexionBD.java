import java.sql.*;
import java.util.List;

public class ConnexionBD{
  private String email;
  private String Mdp;
  private String pseudo;
  private UtilisateurBD userBd;

  public ConnexionBD(String email, String Mdp, String pseudo){
    this.email = email;
    this.Mdp = Mdp;
    this.pseudo = pseudo;
    this.userBd = new UtilisateurBD(VueAccueil.getConnexionMySQL());
  }

  public Utilisateur connecter() throws SQLException,NotRegisteredException{

    Statement st = VueAccueil.connexionMySQL.createStatement();
    ResultSet rs = st.executeQuery("Select * from UTILISATEUR natural join ROLE where emailut = '"+this.email+"' and mdput = '"+this.Mdp+"' and pseudout = '"+this.pseudo+"'");
    boolean res = rs.next();

    if (res) {
      Utilisateur user = null;
      if (rs.getString("nomRole").equals("Joueur")) {
        Blob b = rs.getBlob("avatarut");
        byte[] avatar = b.getBytes(1, (int) b.length());
        user = new Joueur(rs.getString("pseudout"), rs.getString("emailut"), rs.getString("mdput"), true, rs.getInt("idut"), avatar);

      } else {
        Blob b = rs.getBlob("avatarut");
        byte[] avatar = b.getBytes(1, (int) b.length());
        user = new Administrateur(rs.getString("pseudout"), rs.getString("emailut"), rs.getString("mdput"), true, rs.getInt("idut"), avatar);
      }
      userBd.updateUser(user);
      return user;
    } else{
      throw new NotRegisteredException();
    }
  }
}