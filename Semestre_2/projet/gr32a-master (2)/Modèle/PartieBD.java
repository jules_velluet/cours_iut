import java.sql.*;
import java.util.*;

public class PartieBD{
    private ConnexionMySQL laConnexion;

    public PartieBD(ConnexionMySQL laConnexion){
        this.laConnexion = laConnexion;

    }
    public List<Partie> listeParties() throws SQLException {
        List<Partie> liste = new ArrayList<>();
        Statement st = laConnexion.createStatement();
        ResultSet rs = st.executeQuery("Select * from PARTIE");
        Partie partie = null;
        while (rs.next()){
            boolean gagne = false;
            if (rs.getString("gagne") == "O"){gagne = true;}
            partie = new Partie(rs.getInt("idpa"),rs.getDate("datedebutpa"),rs.getTime("tpsresolution"),
            gagne,rs.getInt("idut"),rs.getInt("idsc"));
            liste.add(partie);
        }
        return liste;
    }
    public Partie UpdatePartie(Partie NewPartie) throws SQLException {
        PreparedStatement pst = this.laConnexion.prepareStatement("update PARTIE set datedebutpa = ?, tpsresolution = ?, gagne = ?,idut = ?, idsc = ? where idpa = ?");
        pst.setDate(1, NewPartie.getDateDebut());
        pst.setTime(2, NewPartie.getTempsResolution());
        String gagne = "N";
        if (NewPartie.getVictoire()){gagne = "O";}
        pst.setString(3, gagne);
        pst.setInt(4, NewPartie.getIdUtilisateur());
        pst.setInt(5, NewPartie.getIdScenario());
        pst.setInt(6, NewPartie.getIdPartie());
        pst.executeUpdate();
        return NewPartie;
    }
}