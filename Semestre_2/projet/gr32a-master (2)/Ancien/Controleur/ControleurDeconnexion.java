import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurDeconnexion implements EventHandler<ActionEvent> {
    private VueAccueil vueAccueil;
    private VueAccueilAdmin vueAccueilAdmin;
    private VueConnexion vueConnexion;

    public ControleurDeconnexion(VueAccueil vueAccueil) {
        this.vueAccueil = vueAccueil;
    }

    @Override
    public void handle(ActionEvent actionEvent){
        vueAccueil.SceneInit();
    }
}
