import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class VueAccueilAdmin extends Application {

    private Stage st;
    private VueConnexion vueConnexion;

    public static void main (String [] args){
        launch(args);
    }

    public VueConnexion getVueConnexion() {
        return vueConnexion;
    }

    public void setVueConnexion(VueConnexion vueConnexion) {
        this.vueConnexion = vueConnexion;
    }

    public void setScenePageAccueilAdmin(Stage stage, VueConnexion vueConnexion){
        this.st = stage;
        this.vueConnexion = vueConnexion;

        // Initialisation du bouton déconnexion
        Button bDeco = new Button("Déconnexion");
        bDeco.setTranslateX(180);
        bDeco.setTranslateY(5);
        bDeco.setPrefSize(110,20);
        bDeco.setAlignment(Pos.CENTER);
        ControleurDeconnexion controleurDeconnexion = new ControleurDeconnexion(this.vueConnexion.getVueAccueil());
        bDeco.setOnAction(controleurDeconnexion);


        // Initialisation textes
        Label lChoix = new Label("Que souhaitez-vous faire ?");
        lChoix.setFont(Font.font("Arial", FontWeight.BOLD,22));

        // Initialisation vBox et Scene
        VBox milieu = new ControleurAccueilAdmin(this);

        VBox vbScene = new VBox();
        vbScene.getChildren().addAll(bDeco, lChoix, milieu);
        vbScene.setSpacing(60);
        vbScene.setAlignment(Pos.TOP_CENTER);
        Scene sceneAccueilAdmin = new Scene(vbScene);
        this.st.setScene(sceneAccueilAdmin);

    }

    public void setScenePageAccueilAdminTest(Stage stage){
        this.st = stage;

        // Initialisation du bouton retour
        Button bDeco = new Button("Déconnexion");
        bDeco.setTranslateX(180);
        bDeco.setTranslateY(5);
        bDeco.setPrefSize(110,20);
        bDeco.setAlignment(Pos.CENTER);


        // Initialisation textes
        Label lChoix = new Label("Que souhaitez-vous faire ?");
        lChoix.setFont(Font.font("Arial", FontWeight.BOLD,22));

        // Initialisation des boutons
        Button bGérerJoueur = new Button("Gérer les joueurs");
        bGérerJoueur.setStyle("-fx-background-color: #999999ff; -fx-text-fill: white; -fx-background-radius: 20px");
        bGérerJoueur.setFont(Font.font("Arial",FontWeight.BOLD,20));
        bGérerJoueur.setPrefSize(350,50);
        Button bGérerParties = new Button("Gérer les parties");
        bGérerParties.setStyle("-fx-background-color: #999999ff; -fx-text-fill: white;-fx-background-radius: 20px");
        bGérerParties.setFont(Font.font("Arial",FontWeight.BOLD,20));
        bGérerParties.setPrefSize(350,50);

        // Initialisation vBox et Scene
        VBox milieu = new VBox();
        milieu.getChildren().addAll(bGérerJoueur, bGérerParties);
        milieu.setSpacing(20);
        milieu.setAlignment(Pos.TOP_CENTER);

        VBox vbScene = new VBox();
        vbScene.getChildren().addAll(bDeco, lChoix, milieu);
        vbScene.setSpacing(60);
        vbScene.setAlignment(Pos.TOP_CENTER);
        Scene sceneAccueilAdmin = new Scene(vbScene);
        this.st.setScene(sceneAccueilAdmin);

    }

    public Stage getSt() {return this.st;}

    public void start(Stage stage){
        this.st = stage;
        stage.setTitle("L'Échappée Belle - Administrateur");
        stage.setWidth(500);
        stage.setHeight(500);
        this.setScenePageAccueilAdminTest(this.st);
        stage.show();
    }

}
