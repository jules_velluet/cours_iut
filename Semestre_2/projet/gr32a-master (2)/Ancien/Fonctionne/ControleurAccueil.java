import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ControleurAccueil extends VBox{

    public VueAccueil appli;

    /**
     * Controleur du bouton "Administrateur" qui permet de selectionner ce role
     */
    class ControleurAdmin implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent actionEvent){
            appli.setTypeCompte("Administrateur");
            appli.getVueConnexion().setSt(appli.getStage());
            appli.getVueConnexion().setTypeCompte(appli.getTypeCompte());
            appli.getStage().setTitle("L'Échappée Belle - Administrateur");
            appli.getVueConnexion().setScenePageConnexion();
        }
    }


    /**
     * Controleur du bouton "Concepteur" qui permet de selectionner ce role
     */
    class ControleurConcepteur implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent actionEvent){
            Alert al = new Alert(Alert.AlertType.ERROR);
            al.setTitle("Erreur");
            al.setHeaderText("Le concepteur de cette conception n'a pas conceptualisé la page concepteur.");
            al.setContentText("Veuillez réessayer une des deux autres options.");
            al.showAndWait();

        }
    }


    /**
     * Controleur du bouton "Joueur" qui permet de selectionner ce role
     */
    class ControleurJoueur implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent actionEvent){
            appli.setTypeCompte("Joueur");
            appli.getVueConnexion().setSt(appli.getStage());
            appli.getVueConnexion().setTypeCompte(appli.getTypeCompte());
            appli.getStage().setTitle("L'Échappée Belle - Joueur");
            appli.getVueConnexion().setScenePageConnexion();

        }
    }


    public ControleurAccueil(VueAccueil appli){
        this.appli = appli;
        // Initialisation du bouton 1 (Administrateur)
        Button boutonAdmin = new Button("Administrateur");
        boutonAdmin.setStyle("-fx-background-color: #76a5afff; -fx-background-radius: 10px");
        boutonAdmin.setPrefWidth(200);
        boutonAdmin.setPrefHeight(50);
        boutonAdmin.setFont(Font.font("Arial", FontWeight.BOLD,20));
        boutonAdmin.setTextFill(new Color(1,1,1,1));
        boutonAdmin.setOnAction(new ControleurAdmin());

        // Initialisation du bouton 2 (Concepteur)
        Button boutonConcepteur = new Button("Concepteur");
        boutonConcepteur.setStyle("-fx-background-color: #76a5afff; -fx-background-radius: 10px");
        boutonConcepteur.setPrefWidth(200);
        boutonConcepteur.setPrefHeight(50);
        boutonConcepteur.setFont(Font.font("Arial",FontWeight.BOLD,20));
        boutonConcepteur.setTextFill(new Color(1,1,1,1));
        boutonConcepteur.setOnAction(new ControleurConcepteur());


        // Initialisation du bouton 3 (Joueur)
        Button boutonJoueur = new Button("Joueur");
        boutonJoueur.setStyle("-fx-background-color: #76a5afff; -fx-background-radius: 10px");
        boutonJoueur.setPrefWidth(200);
        boutonJoueur.setPrefHeight(50);
        boutonJoueur.setFont(Font.font("Arial",FontWeight.BOLD,20));
        boutonJoueur.setTextFill(new Color(1,1,1,1));
        boutonJoueur.setOnAction(new ControleurJoueur());

        this.getChildren().addAll(boutonAdmin,boutonConcepteur,boutonJoueur);
        this.setAlignment(Pos.CENTER);
        this.setSpacing(10);
    }

}
