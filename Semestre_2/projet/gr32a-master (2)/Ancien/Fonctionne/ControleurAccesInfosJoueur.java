import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurAccesInfosJoueur implements EventHandler<ActionEvent>{

    private VueScenarios vueScenarios;
    private VueInfosJoueur vueInfosJoueur;

    public ControleurAccesInfosJoueur(VueScenarios vueScenarios){
        this.vueScenarios = vueScenarios;
        this.vueInfosJoueur = this.vueScenarios.getVue();
    }

    @Override
    public void handle(ActionEvent actionEvent){
        this.vueScenarios.getVue().SceneInit(this.vueScenarios.getSt());
    }
}
