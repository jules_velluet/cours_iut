import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import java.util.List;
import java.util.ArrayList;

public class Tuile extends VBox{

  private boolean joueurPresent;
  private Enigme enigme;
  private double largeur, hauteur;
  private double ratio;
  private ImageView vueTuile;
  private int lig,col;
  private Image img;

  /**
  *
  * @param numTuile numéro de la tuile
  * @param joueurPresent true si un joueur est sur la tuile
  * @param enigme enigme présente sur la tuile, null si la tuile ne possède pas d'énigme
  * @param img image de la tuile
  */
  public Tuile(boolean joueurPresent, double largeur,double hauteur, double ratio, Enigme enigme, int lig, int col, Image img){
    super();
    this.largeur=largeur;
    this.hauteur=hauteur;
    this.ratio=ratio;
    this.joueurPresent = joueurPresent;
    this.enigme = enigme;
    this.lig = lig;
    this.col = col;
    this.img = img;
    this.vueTuile = new ImageView(img);
    Rectangle2D viewport = new Rectangle2D(col * largeur, lig * hauteur, largeur, hauteur);
    this.vueTuile.setViewport(viewport);
    this.vueTuile.setFitWidth(largeur*ratio);
    this.vueTuile.setFitHeight(hauteur*ratio);
    this.vueTuile.setSmooth(true);
    this.getChildren().add(vueTuile);
    this.setAlignment(Pos.CENTER);
  }

  public Tuile clone(){
    return new Tuile(this.joueurPresent,this.largeur,this.hauteur,this.ratio,this.enigme,this.lig,this.col,this.img);
  }

  /**
  *
  * @return true si un joueur est sur la tuile
  */
  public boolean getJoueurPresent(){
    return this.joueurPresent;
  }

  /**
  *
  * @return l'énigme présente sur la tuile ou null si la tuile de possède pas d'énigme
  */
  public Enigme getEnigme(){
    return this.enigme;
  }

  public static List<List<Integer>> stringToList(String chaine){
    List<List<Integer>> res = new ArrayList<>();
    List<Integer> liste = new ArrayList<>();
    String nb = "";
    for(int i = 0; i<chaine.length(); i++){
      if("0123456789".contains(chaine.substring(i,i+1))){
        nb += chaine.substring(i,i+1);
      }else if(chaine.charAt(i) == ','){
        if(!nb.equals("")){
          liste.add(Integer.parseInt(nb));
        }
        nb = "";
      }else if(chaine.charAt(i) == ']'){
        if(!nb.equals("")){
          liste.add(Integer.parseInt(nb));
        }
        nb = "";
        if(!liste.isEmpty()){
          res.add(liste);
        }
        liste = new ArrayList<>();
      }
    }
    return res;
  }

}
