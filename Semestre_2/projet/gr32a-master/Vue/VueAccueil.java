import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.scene.Scene;
import java.sql.SQLException;

public class VueAccueil extends Application{
    private Stage st;
    private ControleurAccueil controlAccueil;
    private VueConnexion vueConnexion;
    private String typeCompte;
    public static ConnexionMySQL connexionMySQL;

    public static void main(String[] args) {
        launch(args);
    }

    public VueConnexion getVueConnexion(){
        return this.vueConnexion;
    }

    public String getTypeCompte() {
        return typeCompte;
    }

    public Stage getStage(){
        return this.st;
    }

    public static ConnexionMySQL getConnexionMySQL() {
        return connexionMySQL;
    }

    public void setTypeCompte(String typeCompte){
        this.typeCompte = typeCompte;
    }

    public static ConnexionMySQL initConnexionMySQL(){
        try {
            connexionMySQL = new ConnexionMySQL();
            connexionMySQL.connecter("servinfo-mariadb","DBsanna", "sanna","sanna");
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return connexionMySQL;
    }

    public void SceneInit(){

        // Initialisation texte
        Label lBonjour = new Label("Bonjour");
        lBonjour.setFont(Font.font("Arial", FontWeight.BOLD,50));
        Label lRole = new Label("Quel est votre rôle ?");
        lRole.setFont(new Font("Arial",25));
        // Initialisation vBox et Scene
        BorderPane borderPane = new BorderPane();
        VBox vBoxPrincipale = new VBox();
        vBoxPrincipale.getChildren().addAll(lRole,this.controlAccueil);
        vBoxPrincipale.setAlignment(Pos.CENTER);
        vBoxPrincipale.setSpacing(10);
        borderPane.setTop(lBonjour);
        borderPane.setCenter(vBoxPrincipale);
        borderPane.setAlignment(lBonjour,Pos.TOP_CENTER);
        borderPane.setPadding(new Insets(60));
        Scene scenePrincipale = new Scene(borderPane);
        this.st.setScene(scenePrincipale);
    }

    public void init() {
        this.controlAccueil = new ControleurAccueil(this);
        this.vueConnexion = new VueConnexion(this);
    }

    public void start(Stage stage) {
        initConnexionMySQL();
        this.st = stage;
        this.st.setTitle("L'Échappée Belle");
        this.st.setWidth(600);
        this.st.setHeight(600);
        SceneInit();
        this.st.show();
    }
}
