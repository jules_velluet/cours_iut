import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.text.FontWeight;
import javafx.scene.paint.Color;
import java.sql.*;

public class VueJeu extends VBox {
    private Modele mod; // modèle du jeu de taquin
    private double largeurTuile; // largeur en nombre de pixels d'une tuile
    private double hauteurTuile; // hauteur en nombre de pixels d'une tuile
    private Pion pion;
    private Scenario scenario;
    private CompteARebours compteARebours;
    private EnigmeBD enigmeBD;
    private CarteBD carteBD;
    private ScenarioBD scenarioBD;
    private int idScenario;
    private Jeu jeu;

    public VueJeu(Jeu jeu, Modele mod,int idScenario) {
        super();
        this.jeu = jeu;
        this.idScenario = idScenario;

        List<Carte> listeCarte = new ArrayList<>();

        try{
          ConnexionMySQL c = new ConnexionMySQL();
          c.connecter("servinfo-mariadb","DBjsoares","jsoares","jsoares");

          this.scenarioBD = new ScenarioBD(c);
          this.scenario = new Scenario(this.idScenario,this.scenarioBD.nomScenario(this.idScenario),this.scenarioBD.resumeSc(this.idScenario),this.scenarioBD.tpsMaxSc(1),listeCarte);

          this.carteBD = new CarteBD(c);
          this.enigmeBD = new EnigmeBD(c);

          for(int idCarte: this.scenarioBD.listeIdCarte(idScenario)){
            List<Enigme> listeEnigme = new ArrayList<>();
            Image img = this.carteBD.imgTileset(idCarte);
            this.mod = new Modele((int) img.getHeight()/32,(int) img.getWidth()/32);
            Carte carte = new Carte(idCarte,this.carteBD.nomca(idCarte),this.carteBD.planca(idCarte),this.vecteurTuile(img),listeEnigme);

            for(Integer idEn: this.enigmeBD.listeIdEnigme(idCarte)){
              Enigme enigme = new Enigme(idEn,this.enigmeBD.nomEnigme(idEn),this.enigmeBD.ennoncerEnigme(idEn),this.enigmeBD.reponseEn(idEn),this.enigmeBD.imgEn(idEn),this.enigmeBD.aideEnigme(idEn),false);
              carte.add(enigme);
            }
            listeCarte.add(carte);
          }
        }catch(ClassNotFoundException e){
          System.out.println("pb");
        }catch(SQLException e){
          System.out.println("pb");
        }

        this.compteARebours = new CompteARebours(this.scenario.getTempsMax(),this, this.mod,"Temps :");
        this.compteARebours.start();
        this.scenario.getListe().get(this.scenario.getIndex()).getCarte().add(compteARebours,9,0);
        this.pion = new Pion(new ImageView( new Image("file:img/3.png")));
        this.scenario.getListe().get(this.scenario.getIndex()).ajouteJoueur(this.pion,5,9);
        this.getChildren().addAll(this.scenario.getListe().get(this.scenario.getIndex()).getCarte());

    }

    public Vector<Tuile> vecteurTuile(Image img){
      double hauteurImage = img.getHeight();
      double largeurImage = img.getWidth();
      double largeurTuile = largeurImage / this.mod.getLargeur();
      double hauteurTuile = hauteurImage / this.mod.getHauteur();
      double ratio=0;
      if(hauteurImage>largeurImage){
          ratio = img.getWidth()*2/hauteurImage;
      } else {
          ratio = img.getWidth()*2/largeurImage;
      }
      Vector<Tuile> lesTuiles = new Vector<>();
      // creer des tuiles...
      for (int ligne = 0; ligne < this.mod.getHauteur(); ligne++){
          for(int colonne = 0; colonne < this.mod.getLargeur(); colonne++){
              Tuile tuile = new Tuile(false, largeurTuile, largeurTuile, ratio, null, ligne, colonne, img);
              lesTuiles.add(tuile);
          }
      }
      return lesTuiles;
    }

    public void update(String d){

      this.pion.deplacer(d);
      try{
        this.scenario.getListe().get(this.scenario.getIndex()).getCarte().add(this.pion.getView(),this.pion.getX(),this.pion.getY());
      }catch(Exception e){

      }
      try{
        Carte carte = this.scenario.getListe().get(this.scenario.getIndex());
        int idCa = carte.getId();
        for(Enigme enigme: carte.getListeEnigme()){
          try{
            int idEn = enigme.getId();
            if(this.pion.getX() == this.enigmeBD.lig(idCa,idEn) && this.pion.getY() == this.enigmeBD.col(idCa,idEn)){
              if(!enigme.getReussi())
                new VueEnigme(this.mod,enigme);
            }
          }catch(SQLException e){
            System.out.println("pb");
          }
        }

        if(carte.finit()){
            this.scenario.index();
            this.scenario.getListe().get(this.scenario.getIndex()).getCarte().add(compteARebours,9,0);
            this.scenario.getListe().get(this.scenario.getIndex()).ajouteJoueur(this.pion,5,9);
            this.pion.setY(9);
            this.pion.setX(5);
            this.getChildren().setAll(this.scenario.getListe().get(this.scenario.getIndex()).getCarte());

            jeu.setSceneComplet(this);
        }
      }catch(Exception e){
        new VueVictoire();
      }




    }

    public Modele getModele(){
      return this.mod;
    }
}
