import java.sql.*;
import javafx.scene.image.Image;
import java.io.ByteArrayInputStream;

public class CarteBD{

  private ConnexionMySQL laConnexion;

  public CarteBD(ConnexionMySQL laConexion){
    this.laConnexion = laConexion;
  }

  public String nomca(int idca) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select nomca from CARTE where idCa="+idca);
    rs.next();
    String res = rs.getString("nomca");
    rs.close();
    return res;
  }

  public Image imgTileset(int idca) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select imagets from CARTE natural join TILESET where idca="+idca);
    rs.next();
    Image res = new Image(new ByteArrayInputStream(rs.getBytes("imagets")));
    rs.close();
    return res;
  }

  public String planca(int idca) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select planca from CARTE where idCa="+idca);
    rs.next();
    String res = rs.getString("planca");
    rs.close();
    return res;
  }

  public boolean brouillonCa(int idca) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select brouillonCa from CARTE where idCa="+idca);
    rs.next();
    String michel = rs.getString("brouillonCa");
    rs.close();
    if (michel == "O"){
      return true;
    }
    else if (michel == "N"){
      return false;
    }
    else{
      throw new SQLException("valeur possible O ou N");
    }
  }

  public int idts(int idca) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select idts from CARTE where idCa="+idca);
    rs.next();
    int res = rs.getInt("idts");
    rs.close();
    return res;
  }
}
