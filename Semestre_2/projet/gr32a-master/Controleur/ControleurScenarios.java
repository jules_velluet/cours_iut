import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurScenarios implements EventHandler<ActionEvent> {
    private VueScenarios vueScenario;

    public ControleurScenarios(VueScenarios vueScenario){
        this.vueScenario = vueScenario;
    }

    public void handle(ActionEvent actionEvent){
        this.vueScenario.getVue().setVueScenarios(this.vueScenario);
        this.vueScenario.getVue().SceneInit(this.vueScenario.getSt());
    }

}
