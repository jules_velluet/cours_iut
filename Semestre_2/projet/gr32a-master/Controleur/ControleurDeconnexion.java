import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.sql.SQLException;

public class ControleurDeconnexion implements EventHandler<ActionEvent> {
    private VueAccueil vueAccueil;
    private VueConnexion vueConnexion;
    private Utilisateur userCo;
    private UtilisateurBD userBd;

    public ControleurDeconnexion(VueAccueil vueAccueil) {
        this.vueAccueil = vueAccueil;
        this.vueConnexion = this.vueAccueil.getVueConnexion();
        this.userCo = this.vueConnexion.getUserConnecte();
        this.userBd = new UtilisateurBD(vueAccueil.connexionMySQL);
    }

    @Override
    public void handle(ActionEvent actionEvent){
        userCo.setConnexion(false);
        try {
            userBd.updateUser(userCo);
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        vueAccueil.SceneInit();
    }
}
