public class inscription{

    private Utilisateur user;
    private ConnexionMySQL laConnexion;
    private UtilisateurBD userBD;

    public inscription(Utilisateur user, ConnexionMySQL laConnexion){
        this.user = user;
        this.laConnexion = laConnexion;
        this.userBD = new UtilisateurBD(laConnexion);
    }

    public void inscrire() throws AlreadyRegisteredException{
        boolean res = false;
        try {res = this.userBD.UserIsIn(this.user);} catch (Exception e){System.out.println(e);}
        if (res){
            try {
                this.userBD.insertUser(this.user);
            } catch (Exception e){
                System.out.println(e);
            }
        } else {
            throw new AlreadyRegisteredException();
        }
    }
}