public class Joueur implements Utilisateur {
    // Attributs
    private String pseudo;
    private String mail;
    private String motDePasse;
    private boolean connecte;

    private int partiesJouees;
    private int partiesGagnees;
    private Carte carteActuelle;
    private int idut;
    private byte[] avatar;
    private String RoleU;
    
    // Constructeur

    /**
     *
     * @param pseudo sous forme de String
     * @param mail sous forme de String
     * @param motDePasse sous forme de String
     * @param connecte sous forme de boolean
     */
    public Joueur(String pseudo, String mail, String motDePasse, boolean connecte, int idut, byte[] avatar) {
        this.pseudo = pseudo;
        this.avatar = avatar;
        this.mail = mail;
        this.idut = idut;
        this.motDePasse = motDePasse;
        this.connecte = connecte;
        this.partiesJouees = 0;
        this.partiesGagnees = 0;
        this.carteActuelle = null;
        this.RoleU = "Joueur";
    }

    // Getters

    /**
     *
     * @return le pseudo de l'utilisateur
     */
    @Override
    public String getPseudo() {return this.pseudo;}

    @Override
    public int getId(){return this.idut;}

    /**
     *
     * @return l'adresse email de l'utilisateur
     */
    @Override
    public String getMail() {return this.mail;}

    /**
     *
     * @return l'avatar de l'utilisateur
     */
    @Override
    public byte[] getAvatar() {return this.avatar;}

    /**
     *
     * @return le mot de passe de l'utilisateur
     */
    @Override
    public String getMotDePasse() {return this.motDePasse;}

    /**
     *
     * @return un boolean informant si l'utilisateur est connecté ou non
     */
    @Override
    public boolean estConnecte() {return this.connecte;}

    /**
     *
     * @return un boolean informant si l'utilisateur est connecté ou non
     */
    @Override
    public String getRoleU() {return this.RoleU;}

    /**
     *
     * @return le nombre de parties jouées par l'utilisateur
     */
    public int getNbPartiesJouees() {return this.partiesJouees;}

    /**
     *
     * @return le nombre de parties gagnées par l'utilisateur
     */
    public int getNbPartiesGagnees() {return this.partiesGagnees;}

    /**
     *
     * @return le nombre de parties gagnées par l'utilisateur
     */
    public int getPourcentage() {return this.partiesGagnees/this.partiesJouees;}

    // Methods
    /**
     *
     * incrémente de 1 la variable partiesJouees
     */
    public void lancerPartie() {this.partiesJouees++;}

    /**
     *
     * incrémente de 1 la variable partiesGagnees
     */
    public void gagnerPartie() {this.partiesGagnees++;}

    /**
     *
     * @param carte (nouvelle carte sur lequel le joueur va se déplacer)
     */
    public void deplacer(Carte carte) {this.carteActuelle = carte;}

    @Override
    public void setConnexion(boolean connecte){
        this.connecte = connecte;
    }


}
