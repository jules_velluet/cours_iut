import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

public class ScenarioBD{

  private ConnexionMySQL laConnexion;

  public ScenarioBD(ConnexionMySQL laConexion){
    this.laConnexion = laConexion;
  }

  /**
    *
    * @param idSc(int, id du Scénario)
    * @return le nom du Scénario
  */
  public String nomScenario(int idSc) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select titreSc from SCENARIO where idSc="+idSc);
    rs.next();
    String res = rs.getString("titreSc");
    rs.close();
    return res;
  }

  /**
    *
    * @param idSc(int, id du Scénario)
    * @return le résumé du Scénario passer en paramétre
  */
  public String resumeSc(int idSc) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select resumeSc from SCENARIO where idSc="+idSc);
    rs.next();
    String res = rs.getString("resumeSc");
    rs.close();
    return res;
  }

  /**
    *
    * @param idSc(int, id du Scénario)
    * @return l'introduction du Scénario passer en paramétre
  */
  public String introScenario(int idSc) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select introSce from SCENARIO where idSc="+idSc);
    rs.next();
    String res = rs.getString("introSce");
    rs.close();
    return res;
  }

  /**
    *
    * @param idSc(int, id du Scénario)
    * @return le temps max du Scénario passer en paramétre
  */
  public Time tpsMaxSc(int idSc) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select tpsMaxSc from SCENARIO where nomSce="+idSc);
    rs.next();
    Time res = rs.getTime("tpsMaxSc");
    rs.close();
    return res;
  }

  /**
    *
    * @param idSc(int, id du Scénario)
    * @return la date de mise en ligne du Scénario passer en paramétre
  */
  public Date dateMiseEnLigne(int idSc) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select dateMiseEnLigne from SCENARIO where idSc="+idSc);
    rs.next();
    Date res = rs.getDate("dateMiseEnLigne");
    rs.close();
    return res;
  }

  /**
    *
    * @param idSc(int, id du Scénario)
    * @return true si l'énigme est un brouillon ou false sinon
  */
  public boolean brouillonSc(int idSc) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select brouillonSc from SCENARIO where idEn="+idSc);
    rs.next();
    String michel = rs.getString("brouillonSc");
    rs.close();
    if (michel == "O"){
      return true;
    }
    else if (michel == "N"){
      return false;
    }
    else{
      throw new SQLException("valeur possible O ou N");
    }
  }

  public byte[] icone(int idSc) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select icone from SCENARIO where idEn="+idSc);
    rs.next();
    byte[] res = rs.getBytes("icone");
    rs.close();
    return res;
  }


  /**
    *
    * @return une liste de dictionnaire, un dictionnaire contient en clés toutes les info de l'égnime
  */
  public List<HashMap<String,String>> getListScenario() throws SQLException{
    List<HashMap<String,String>> liste = new ArrayList<>();
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select * from SCENARIO");
    while(rs.next()){
      HashMap<String,String> sce = new HashMap<>();
      sce.put("idSc", rs.getString("idSc"));
      sce.put("titreSc", rs.getString("titreSc"));
      sce.put("resume", rs.getString("resumeSc"));
      sce.put("tpsMax", rs.getString("tpsMaxSc"));
      sce.put("dateMiseEnLigne", rs.getString("dateMiseEnLigne"));
      sce.put("brouillonSc", rs.getString("brouillonSc"));
      liste.add(sce);
    }
    rs.close();
    return liste;
  }

  public HashMap<String,String> getScenario(int idsc){
    List<HashMap<String,String>> liste;
    try {
      liste = this.getListScenario();
    } catch(Exception e){
      liste = null;
    }
    for (HashMap<String, String> Sce : liste){
      if (Sce.get("idSc") == String.valueOf(idsc)){
        return Sce;
      }
    }
    return null;
  }
}
