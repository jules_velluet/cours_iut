import java.sql.*;
import java.util.*;

public class UtilisateurBD{
    private ConnexionMySQL laConnexion;

    public UtilisateurBD(ConnexionMySQL laConnexion){
        this.laConnexion = laConnexion;
    }

    public String insertUser(Utilisateur user) throws SQLException {
        PreparedStatement pst = this.laConnexion.prepareStatement("insert into UTILISATEUR values(?,?,?,?,?,?)");
        pst.setInt(1, user.getId());
        pst.setString(2, user.getPseudo());
        pst.setString(3, user.getMail());
        pst.setString(4, user.getMotDePasse());
        Blob b = this.laConnexion.createBlob();
        b.setBytes(1, user.getAvatar());
        pst.setBlob(5, b);
        pst.setString(6, user.getRoleU());
        pst.executeUpdate();
        return user.getPseudo();
    }
    public String removeUser(Utilisateur user) throws SQLException {
        PreparedStatement pst = this.laConnexion.prepareStatement("DELETE from UTILISATEUR where idut = ? ,pseudo = ?, mail = ?, motDePasse = ?,avatar = ?, roleU = ?");
        pst.setInt(1, user.getId());
        pst.setString(2, user.getPseudo());
        pst.setString(3, user.getMail());
        pst.setString(4, user.getMotDePasse());
        Blob b = this.laConnexion.createBlob();
        b.setBytes(1, user.getAvatar());
        pst.setBlob(5, b);
        pst.setString(6, user.getRoleU());
        pst.executeUpdate();
        return user.getPseudo();
    }
    public String updateUser(Utilisateur user) throws SQLException {
        PreparedStatement pst = this.laConnexion.prepareStatement("UPDATE UTILISATEUR set pseudo = ?, mail = ?, motDePasse = ?, avatar = ?, roleU = ? where idut = ?");
        pst.setString(1, user.getPseudo());
        pst.setString(2, user.getMail());
        pst.setString(3, user.getMotDePasse());
        Blob b = this.laConnexion.createBlob();
        b.setBytes(1, user.getAvatar());
        pst.setBlob(4, b);
        pst.setString(5, user.getRoleU());
        pst.setInt(6,user.getId());
        pst.executeUpdate();
        return user.getPseudo();
    }
    public boolean UserIsIn(Utilisateur user) throws SQLException {
        Statement st = this.laConnexion.createStatement();
        ResultSet rs = st.executeQuery("Select * from UTILISATEUR where idut = "+ user.getId());
        return (rs.next());
    }

    public int getMaxId() throws SQLException {
        Statement st = this.laConnexion.createStatement();
        ResultSet rs = st.executeQuery("Select ifnull(max(idut),0) as lemax from UTILISATEUR");
        rs.next();
        int res = rs.getInt("lemax");
        rs.close();
        return res;
    }
}
