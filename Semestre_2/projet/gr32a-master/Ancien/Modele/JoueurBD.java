import java.sql.*;
import java.util.*;


public class JoueurBD{
    private ConnexionMySQL laConnexion;

    public JoueurBD(ConnexionMySQL laConnexion){
        this.laConnexion = laConnexion;
    }
    public HashMap<Joueur,List<Integer>> DictJoueurs() throws SQLException {
        
        List<Integer> liste = new ArrayList<>();
        HashMap<Joueur,List<Integer>> Dict = new HashMap<>();
        Statement st = laConnexion.createStatement();
        ResultSet rs = st.executeQuery("Select * from UTILISATEUR where roleU = 'Joueur'");
        Joueur Player = null;
        int nbJ;
        int nbG;
        int prctV;
        Boolean connecte;
        while (rs.next()){
            if (rs.getString("activeut") == "O"){connecte = true;} else {connecte = false;}
            Player = new Joueur(rs.getString("pseudout"),rs.getString("emailut"),rs.getString("mdput"),connecte,rs.getInt("idut"),
            rs.getBytes("avatarut"));
            nbJ = Player.getNbPartiesJouees();
            nbG = Player.getNbPartiesGagnees();
            prctV = Player.getPourcentage();
            liste.add(nbJ);
            liste.add(nbG);
            liste.add(prctV);
            Dict.put(Player, liste);
        }
        return Dict;
    }
}