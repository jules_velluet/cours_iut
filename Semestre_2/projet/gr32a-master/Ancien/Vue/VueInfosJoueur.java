import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;

public class VueInfosJoueur extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    public Scene SceneInit() {
        // Initialisation du bouton retour
        VBox vRetour = new VBox();
        Image img = new Image("http://freevector.co/wp-content/uploads/2014/04/59098-return-arrow-curve-pointing-left.png");
        ImageView view = new ImageView(img);
        view.setFitHeight(20);
        view.setPreserveRatio(true);
        Button bRetour = new Button();
        bRetour.setTranslateX(5);
        bRetour.setTranslateY(5);
        bRetour.setPrefSize(20,20);
        bRetour.setGraphic(view);
        vRetour.getChildren().add(bRetour);
        vRetour.setAlignment(Pos.TOP_LEFT);

        // Titre
        Label titre = new Label("INFORMATIONS");
        titre.setFont(Font.font("Arial", FontWeight.BOLD, 40));
        HBox hbTitre = new HBox();
        hbTitre.getChildren().add(titre);
        hbTitre.setAlignment(Pos.CENTER);

        // Pseudo
        Label pseudo = new Label("Pseudo");
        pseudo.setFont(Font.font("Arial",15));
        HBox hbPseudo = new HBox();
        hbPseudo.getChildren().addAll(pseudo);
        hbPseudo.setAlignment(Pos.CENTER_LEFT);
        hbPseudo.setPadding(new Insets(5,5,5,5));
        hbPseudo.setPrefHeight(50);
        hbPseudo.setPrefWidth(200);
        hbPseudo.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        // Adresse Email
        Label mail = new Label("Adresse Email");
        mail.setFont(Font.font("Arial",15));
        HBox hbMail= new HBox();
        hbMail.getChildren().addAll(mail);
        hbMail.setAlignment(Pos.CENTER_LEFT);
        hbMail.setPadding(new Insets(5,5,5,5));
        hbMail.setPrefHeight(50);
        hbMail.setPrefWidth(200);
        hbMail.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        // Mot de Passe
        Label mdp = new Label("Mot de Passe");
        mdp.setFont(Font.font("Arial",15));
        HBox hbMdp = new HBox();
        hbMdp.getChildren().addAll(mdp);
        hbMdp.setAlignment(Pos.CENTER_LEFT);
        hbMdp.setPadding(new Insets(5,5,5,5));
        hbMdp.setPrefHeight(50);
        hbMdp.setPrefWidth(200);
        hbMdp.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        // Rôle
        Label role = new Label("Rôle");
        role.setFont(Font.font("Arial",15));
        HBox hbRole = new HBox();
        hbRole.getChildren().addAll(role);
        hbRole.setAlignment(Pos.CENTER_LEFT);
        hbRole.setPadding(new Insets(5,5,5,5));
        hbRole.setPrefHeight(50);
        hbRole.setPrefWidth(200);
        hbRole.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        // Colonne Gauche
        VBox colGauche = new VBox();
        colGauche.getChildren().addAll(hbPseudo, hbMail, hbMdp, hbRole);
        colGauche.setSpacing(20);

        // Nombre de Parties Jouées
        Label jouees = new Label("Nb Parties Jouées");
        jouees.setFont(Font.font("Arial",15));
        HBox hbJouees = new HBox();
        hbJouees.getChildren().addAll(jouees);
        hbJouees.setAlignment(Pos.CENTER_LEFT);
        hbJouees.setPadding(new Insets(5,5,5,5));
        hbJouees.setPrefHeight(50);
        hbJouees.setPrefWidth(200);
        hbJouees.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        // Nombre de Parties Gagnées
        Label gagnees = new Label("Nb Parties Gagnées");
        gagnees.setFont(Font.font("Arial",15));
        HBox hbGagnees = new HBox();
        hbGagnees.getChildren().addAll(gagnees);
        hbGagnees.setAlignment(Pos.CENTER_LEFT);
        hbGagnees.setPadding(new Insets(5,5,5,5));
        hbGagnees.setPrefHeight(50);
        hbGagnees.setPrefWidth(200);
        hbGagnees.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        // Nombre de Parties Perdues
        Label perdues = new Label("Nb Parties Perdues");
        perdues.setFont(Font.font("Arial",15));
        HBox hbPerdues = new HBox();
        hbPerdues.getChildren().addAll(perdues);
        hbPerdues.setAlignment(Pos.CENTER_LEFT);
        hbPerdues.setPadding(new Insets(5,5,5,5));
        hbPerdues.setPrefHeight(50);
        hbPerdues.setPrefWidth(200);
        hbPerdues.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        // Pourcentage de Victoire
        Label pourcentage = new Label("% Victoires");
        pourcentage.setFont(Font.font("Arial",15));
        HBox hbPourcentage = new HBox();
        hbPourcentage.getChildren().addAll(pourcentage);
        hbPourcentage.setAlignment(Pos.CENTER_LEFT);
        hbPourcentage.setPadding(new Insets(5,5,5,5));
        hbPourcentage.setPrefHeight(50);
        hbPourcentage.setPrefWidth(200);
        hbPourcentage.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        // Colonne Droite
        VBox colDroite = new VBox();
        colDroite.getChildren().addAll(hbJouees, hbGagnees, hbPerdues, hbPourcentage);
        colDroite.setSpacing(20);

        // Scène
        HBox centre = new HBox();
        centre.getChildren().addAll(colGauche, colDroite);
        centre.setAlignment(Pos.CENTER);
        centre.setSpacing(40);
        VBox scene = new VBox();
        scene.getChildren().addAll(vRetour, hbTitre, centre);
        scene.setAlignment(Pos.TOP_CENTER);
        scene.setSpacing(60);

        return new Scene(scene);
    }

    public void start(Stage stage){
        stage.setTitle("L'Échappée Belle");
        stage.setWidth(600);
        stage.setHeight(600);
        stage.setScene(SceneInit());
        stage.show();
    }
}