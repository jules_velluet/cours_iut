import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import java.util.List;
import java.util.ArrayList;

public class Jeu extends Application {
    Modele mod;
    Stage st;
    VueJeu vueJeu;

    public static void main(String[] args) {
        launch(args);
    }

    void setSceneComplet(Image img, int nbLig, int nbCol){
        mod=new Modele(nbLig,nbCol);
        vueJeu=new VueJeu(mod,img,img.getWidth()*2);

        BorderPane vueComplete = new BorderPane();
        vueComplete.setPadding(new Insets(5));
        vueComplete.setCenter(vueJeu);
        Scene s = new Scene(vueComplete);
        ControleurDeplacement controleur = new ControleurDeplacement(this.mod,this.vueJeu);
        s.setOnKeyPressed(controleur);
        st.setScene(s);
    }

    @Override
    public void start(Stage stage) {
        st=stage;
        Image img=new Image("file:img/tilesMaison.png");
        stage.setTitle("Jeu");
        stage.setWidth(660);
        stage.setHeight(680);
        setSceneComplet(img,(int) img.getHeight()/32, (int) img.getWidth()/32);
        stage.show();
    }
}
