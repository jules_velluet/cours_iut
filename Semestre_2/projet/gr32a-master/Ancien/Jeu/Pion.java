import javafx.scene.image.ImageView;

public class Pion{

  private ImageView iv;
  private int x;
  private int y;

  public Pion(ImageView iv){
      this.iv = iv;
      this.iv.setFitWidth(32);
      this.iv.setFitHeight(32);
      this.x = 5;
      this.y = 9;
  }

  public ImageView getView(){
    return this.iv;
  }

  public int getX(){
    return this.x;
  }

  public int getY(){
    return this.y;
  }

  public void deplacer(String st){
    if(st.equals("UP") && this.y > 0 )
      this.y -= 1;
    else if(st.equals("LEFT") && this.x > 0)
      this.x -= 1;
    else if(st.equals("RIGHT") && this.x < 9)
      this.x +=1;
    else if(st.equals("DOWN") && this.y < 9)
      this.y  += 1;
  }

  public void update(){

  }


}
