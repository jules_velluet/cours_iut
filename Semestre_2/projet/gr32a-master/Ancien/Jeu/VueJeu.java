import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.FontWeight;
import javafx.scene.paint.Color;

public class VueJeu extends VBox {
    private Image image; // image servant de support au taquin
    private Modele mod; // modèle du jeu de taquin
    private double largeurTuile; // largeur en nombre de pixels d'une tuile
    private double hauteurTuile; // hauteur en nombre de pixels d'une tuile
    private double tailleMax; // taille maximum autorisée pour l'affichage
    private Vector<Tuile> lesTuiles; // vecteur contenant les tuile du taquin
    private Pion joueur;
    private GridPane jeu;
    private Enigme enigme;

    public VueJeu(Modele mod, Image img, double tailleMax) {
        super();
        this.image = img;
        this.mod = mod;
        this.tailleMax = tailleMax;
        this.enigme = new Enigme("L'addition","Quel nombre se cache derrière cette addition?","618",new Image("file:img/Enigme_addition.png",200,200,true,true),"Pensez romain mais écrivez arabe!",false);
        double hauteurImage = img.getHeight();
        double largeurImage = img.getWidth();
        this.largeurTuile = largeurImage / mod.getLargeur();
        this.hauteurTuile = hauteurImage / mod.getHauteur();
        double ratio=0;
        if(hauteurImage>largeurImage){
            ratio = tailleMax/hauteurImage;
        } else {
            ratio = tailleMax/largeurImage;
        }
        GridPane grille = new GridPane();
        this.lesTuiles = new Vector<>();
        // creer des tuiles...
        for (int ligne = 0; ligne < mod.getHauteur(); ligne++){
            for(int colonne = 0; colonne < mod.getLargeur(); colonne++){
                Tuile tuile = new Tuile(false, this.largeurTuile, this.largeurTuile, ratio, null, ligne, colonne, image);
                grille.add(tuile,colonne,ligne);
                lesTuiles.add(tuile);
            }
        }
        this.jeu = new GridPane();
        // Maison
        List<List<Integer>> listeTuile = Tuile.stringToList("[[7, 3, 3, 3, 3, 3, 3, 3, 3, 8],[19, 26, 40, 41, 24, 13, 13, 23, 24, 19],[19, 36, 50, 51, 34, 2, 2, 2, 34, 19], [19, 2, 2, 2, 2, 2, 2, 2, 25, 19], [19, 57, 58, 44, 45, 46, 2, 2, 35, 19], [27, 3, 6, 54, 55, 56, 5, 3, 3, 38], [19, 60, 13, 16, 47, 48, 15, 13, 61, 19], [19, 70, 1, 33, 1, 1, 1, 1, 71, 19], [19, 1, 1, 1, 1, 1, 1, 1, 49, 19], [17, 9, 9, 9, 9, 9, 9, 9, 9, 18]]");
        // foret
        //List<List<Integer>> listeTuile = Tuile.stringToList("[[101,101,101,118,119,46,47,112,112,112],[116,117,112,136,137,64,65,116,117,112],[134,135,112,154,155,79,80,134,135,112],[152,153,112,172,173,112,112,152,153,112],[170,171,114,112,112,112,112,170,171,98],[112,112,112,112,115,112,77,72,73,74],[41,99,112,112,168,77,37,90,91,92],[19,22,78,112,77,37,37,108,109,38],[128,146,146,146,146,146,146,146,146,129],[111,111,111,111,111,111,111,111,111,111]]");
        this.joueur = new Pion(new ImageView( new Image("file:img/3.png")));
        for(int i = 0; i < listeTuile.size(); i++){
          for(int y = 0; y <listeTuile.get(i).size(); y++){
            this.jeu.add(this.lesTuiles.get(listeTuile.get(i).get(y)).clone(),y,i);
          }
        }
        this.jeu.add(this.joueur.getView(),5,9);
        this.getChildren().addAll(jeu);
    }

    public void update(String d){
      this.joueur.deplacer(d);
      if(this.joueur.getX() == 5 && this.joueur.getY() == 5){
        if(!this.enigme.getReussi())
          new VueEnigme(this.mod,this.enigme);
      }
      try{
        this.jeu.add(this.joueur.getView(),this.joueur.getX(),this.joueur.getY());
      }catch(Exception e){

      }
    }
}
