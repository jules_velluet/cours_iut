import java.util.List;

public class Scenario{

  private int idSc;
  private String nomScenario;
  private int tempsDisponible;
  private String texteIntro;
  private List<Carte> listeCartes;
  private String texteReussite;
  private String texteEchoue;
  private Blob img;
  private boolean brouillon;

  /**
  *
  * @param idSc numéro d'identification du scénario
  * @param nomScenario nom du scénario
  * @param tempsDisponible temps pour résoudre le scénario
  * @param texteIntro texte d'introduction du scénario
  * @param listeCartes liste de cartes composant le scénario
  * @param texteReussite texte si le joueur réussit le scénario
  * @param texteEchoue texte si le joueur échoue le scénario
  * @param brouillon true si le Scenario est terminé
  */
  public Scenario(int idSc, String nomScenario, int tempsDisponible, String texteIntro, List<Carte> listeCartes, String texteReussite, String texteEchoue, Blob img, boolean brouillon){
    this.idSc = idSc;
    this.nomScenario = nomScenario;
    this.tempsDisponible = tempsDisponible;
    this.texteIntro = texteIntro;
    this.listeCartes = listeCartes;
    this.texteReussite = texteReussite;
    this.texteEchoue = texteEchoue;
    this.img = img;
    this.brouillon = brouillon;
  }

  /**
  *
  * @return la liste de cartes composant le scénario
  */
  public List<Carte> getCarte(){
    return this.listeCartes;
  }

  /**
  *
  * @return le texte d'introduction du scénario
  */
  public String getIntroScenario(){
    return this.texteIntro;
  }

  /**
  *
  * @return le texte si le joueur réussit le scénario
  */
  public String getTexteReussite(){
    return this.texteReussite;
  }

  /**
  *
  * @return le texte si le joueur échoue le scénario
  */
  public String getTexteEchoue(){
    return this.texteEchoue;
  }

}
