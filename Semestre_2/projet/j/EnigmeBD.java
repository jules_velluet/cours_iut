import java.sql.*;
import java.util.List;

public class EnigmeBD{

  private ConnexionMySQL laConnexion;

  public EnigmeBD(ConnexionMySQL laConexion){
    this.laConnexion = laConexion;
  }

  /**
    *
    * @param idEn(int, id de l'enigme)
    * @return le texte de l'egnime passer en parametre
  */
  public String ennoncerEnigme(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select textEn from ENIGME where idEn="+idEn);
    rs.next();
    String res = rs.getString("textEn");
    rs.close();
    return res;
  }

  /**
    *
    * @param idEn(int, id de l'enigme)
    * @return le nom de l'énigme passer en parametre
  */
  public String nomEnigme(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select nomEn from ENIGME where idEn="+idEn);
    rs.next();
    String res = rs.getString("nomEn");
    rs.close();
    return res;
  }

  /**
    *
    * @param idEn(int, id de l'enigme)
    * @return l'aide de l'énigme passer en parametre
  */
  public String aideEnigme(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select aideEn from ENIGME where idEn="+idEn);
    rs.next();
    String res = rs.getString("aideEn");
    rs.close();
    return res;
  }

  /**
    *
    * @param idEn(int, id de l'enigme)
    * @return la réponse de l'énigme passer en parametre
  */
  public String reponseEn(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select reponseEn from ENIGME where idEn="+idEn);
    rs.next();
    String res = rs.getString("reponseEn");
    rs.close();
    return res;
  }

  /**
    *
    * @param idEn(int, id de l'enigme)
    * @return true si l'énigme est un brouillon ou false sinon
  */
  public boolean brouillonEn(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select brouillonEn from ENIGME where idEn="+idEn);
    rs.next();
    String michel = rs.getString("brouillonEn");
    rs.close();
    if (michel == "O"){
      return true;
    }
    else if (michel == "N"){
      return false;
    }
    else{
      throw new SQLException("valeur possible O ou N");
    }
  }

  public byte[] imgEn(int idEn) throws SQLException{
    Statement st;
    st = laConnexion.createStatement();
    ResultSet rs = st.executeQuery("select imgEn from ENIGME where idEn="+idEn);
    rs.next();
    byte[] res = rs.getBytes("imgEn");
    rs.close();
    return res;
  }
}
