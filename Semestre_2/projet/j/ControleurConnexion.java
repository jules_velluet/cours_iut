import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import java.sql.*;

public class ControleurConnexion extends HBox {
    private VueConnexion vueConnexion;
    private VueScenarios vueScenarios;
    private VueInscription vueInscription;
    private VueAccueilAdmin vueAccueilAdmin;

    public ControleurConnexion(VueConnexion vueConnexion) {
        this.vueConnexion = vueConnexion;
        this.vueScenarios = this.vueConnexion.getVueScenarios();
        this.vueInscription = this.vueConnexion.getVueInscription();
        this.vueAccueilAdmin = this.vueConnexion.getVueAccueilAdmin();

        // Initialisation Bouton 1 (CONNECTER)
        Button connecter = new Button("SE CONNECTER");
        connecter.setStyle("-fx-background-color: #6aa84fff;-fx-background-radius: 20px");
        connecter.setPrefWidth(175);
        connecter.setPrefHeight(35);
        connecter.setFont(Font.font("Arial", 15));
        connecter.setTextFill(new Color(1, 1, 1, 1));
        connecter.setOnAction(new ControleurConnecter());

        // Initialisation Bouton 2 (S'INSCRIRE)
        Button inscrire = new Button("S'INSCRIRE");
        inscrire.setStyle("-fx-background-color: #ffffffff; -fx-border-color: grey; -fx-background-radius: 20px;-fx-border-radius: 20px");
        inscrire.setPrefWidth(175);
        inscrire.setPrefHeight(35);
        inscrire.setFont(Font.font("Arial", 15));
        inscrire.setOnAction(new ControleurInscrire());

        // Initialisation HBox
        this.setSpacing(-30);
        this.getChildren().addAll(inscrire, connecter);
        this.setAlignment(Pos.CENTER);

    }
    public class ControleurInscrire implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent actionEvent){
            vueInscription.setScenePageInscription(vueConnexion.getSt());
        }
    }

    public class ControleurConnecter implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent actionEvent){

            String mail = vueConnexion.gettMail().getText();
            String motDePasse = vueConnexion.gettMDP().getText();
            String pseudo = vueConnexion.gettPseudo().getText();
            ConnexionBD connexion = new ConnexionBD(mail, motDePasse, pseudo);
            String role = null;
            try {
                vueConnexion.setUserConnecte(connexion.connecter());
                role = vueConnexion.getUserConnecte().getRoleU();
            } catch (SQLException e){
                System.out.println(e.getMessage());
                Alert al = new Alert(Alert.AlertType.ERROR);
                al.setTitle("Erreur");
                al.setHeaderText("Problème de connexion");
                al.setContentText("Un problème semble persister lors de la tentative de connexion, veuillez prévenir un administrateur.");
                al.showAndWait();
            } catch (NotRegisteredException e){
                Alert al = new Alert(Alert.AlertType.ERROR);
                al.setTitle("Erreur");
                al.setHeaderText("Problème de connexion");
                al.setContentText("Vous n'etes pas inscrit, veuillez vous inscrire");
                al.showAndWait();
            }
            if (role != null && vueConnexion.getTypeCompte().equals("Admin") && role.equals("Admin")){
                vueAccueilAdmin.setScenePageAccueilAdmin(vueConnexion.getSt(), vueConnexion);
            }else if(role != null && vueConnexion.getTypeCompte().equals("Joueur") && role.equals("Joueur")){
                vueScenarios.SceneInit(vueConnexion.getSt());
                vueScenarios.sceneInitSuite();
            }else if(role != null){
                System.out.println(role);
                Alert al = new Alert(Alert.AlertType.ERROR);
                al.setTitle("Erreur");
                al.setHeaderText("Problème de connexion");
                al.setContentText("Nous ne prenons pas en charge l'interface concepteur");
                al.showAndWait();

            }else{
                Alert al = new Alert(Alert.AlertType.ERROR);
                al.setTitle("Erreur");
                al.setHeaderText("Problème de connexion");
                al.setContentText("Role nullpointer");
                al.showAndWait();
            }
        }
    }


}
