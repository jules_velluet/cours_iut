import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import java.sql.SQLException;

public class VueScenarios extends Application {

    private Stage st;
    private VueInfosJoueur vueInfosJoueurs;
    private VueConnexion vueConnexion;
    private int idScenario;
    private ScenarioBD scenarioBD;

    public static void main(String[] args) {
        launch(args);
    }

    public VueScenarios(VueConnexion vueConnexion, VueInfosJoueur vueInfosJoueurs){
        this.vueInfosJoueurs = vueInfosJoueurs;
        this.vueConnexion = vueConnexion;
        this.idScenario = 1;
        try{
            ConnexionMySQL c = new ConnexionMySQL();
            c.connecter("servinfo-mariadb","DBjsoares","jsoares","jsoares");
            this.scenarioBD = new ScenarioBD(c);
        }catch(Exception e){

        }

    }

    public VueInfosJoueur getVue() {
        return this.vueInfosJoueurs;
    }

    public Stage getSt() {
        return st;
    }

    public void setSt(Stage st){
        this.st = st;
    }

    public void sceneInitSuite(){
        this.st.setScene(SceneInit(this.st));
        this.st.show();
    }
    public VueConnexion getVueConnexion(){
        return this.vueConnexion;
    }

    public Scene SceneInit(Stage st){
        this.st = st;

        // Initialisation du bouton déconnexion
        Button bDeco = new Button("Déconnexion");
        bDeco.setTranslateX(5);
        bDeco.setTranslateY(5);
        bDeco.setPrefSize(110,20);
        bDeco.setAlignment(Pos.CENTER);
        ControleurDeconnexion controleurDeconnexion = new ControleurDeconnexion(this.vueConnexion.getVueAccueil());
        bDeco.setOnAction(controleurDeconnexion);

        // Initialisation texte
        Label lScenarios = new Label("Scénarios");
        lScenarios.setFont(Font.font("Arial", FontWeight.BOLD,40));
        lScenarios.setPadding(new Insets(-30,0,0,0));

        // Initialisation Liste de Scénarios
        HBox liste = new HBox();
        liste.setAlignment(Pos.CENTER);
        liste.setSpacing(20);

        BorderPane bScene1 = this.devantScene(this.idScenario);
        Button flecheGauche = this.flecheGauche();
        Button flecheDroite = this.flecheDroite();

        // Initialisation du bouton retour
        VBox vRetour = new VBox();
        Image img = new Image("http://freevector.co/wp-content/uploads/2014/04/59098-return-arrow-curve-pointing-left.png");
        ImageView view = new ImageView(img);
        view.setFitHeight(20);
        view.setPreserveRatio(true);
        Button bRetour = new Button();
        bRetour.setTranslateX(5);
        bRetour.setTranslateY(5);
        bRetour.setPrefSize(20,20);
        bRetour.setGraphic(view);
        vRetour.getChildren().add(bRetour);
        vRetour.setAlignment(Pos.TOP_LEFT);

        // Bouton Informations
        Button infos = new Button("+ INFOS");
        infos.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        infos.setTranslateX(380);
        infos.setTranslateY(5);
        infos.setPrefHeight(30);
        infos.setPrefWidth(100);
        infos.setStyle("-fx-background-border : 10");
        infos.setOnAction(new ControleurScenarios(this));

        // Boutons
        HBox hbBoutons = new HBox();
        hbBoutons.getChildren().addAll(bDeco, infos);


        liste.getChildren().addAll(flecheGauche, bScene1, flecheDroite);


        // Initialisation Bouton Jouer
        Button bJouer = new Button("Jouer");
        bJouer.setStyle("-fx-background-color : #6aa84fff ; -fx-background-radius: 20");
        bJouer.setTextFill(new Color(1,1,1,1));
        bJouer.setFont(Font.font("Arial", FontWeight.BOLD,20));
        bJouer.setPrefHeight(50);
        bJouer.setPrefWidth(150);
        bJouer.setOnAction(new ControleurlancementJeu(this));


        // Initialisation Scène
        VBox scene = new VBox();
        scene.getChildren().addAll(hbBoutons,lScenarios, liste, bJouer);
        scene.setSpacing(20);
        scene.setAlignment(Pos.CENTER);

        return new Scene(scene);
    }

    // Flèches
    public Button flecheGauche() {
        Button flecheGauche = new Button();
        flecheGauche.setPrefHeight(100);
        flecheGauche.setPrefWidth(50);
        flecheGauche.setStyle("-fx-background-color: grey; -fx-shape: 'M 400 250 L 400 550 L 250 400 Z'");
        return flecheGauche;
    }

    public Button flecheDroite() {
        Button flecheDroite = new Button();
        flecheDroite.setPrefHeight(100);
        flecheDroite.setPrefWidth(50);
        flecheDroite.setStyle("-fx-background-color : grey ; -fx-shape : 'M 400 250 L 400 550 L 550 400 Z'");
        return flecheDroite;
    }

    // Méthode Devant Page Scénario
    public BorderPane devantScene(int idScenario) {
        // Initialisation Contenu Scénario
        BorderPane scenario = new BorderPane();
        try{
            Label lTitre = new Label(this.scenarioBD.nomScenario(this.idScenario));
            lTitre.setFont(Font.font("Arial", 30));

            Image image = this.scenarioBD.icone(this.idScenario);
            VBox vbT = new VBox();
            vbT.getChildren().add(lTitre);
            vbT.getChildren().add(new ImageView(image));
            vbT.setAlignment(Pos.CENTER);
            vbT.setPadding(new Insets(50,0,0,0));

            HBox hbT = new HBox();
            hbT.getChildren().add(vbT);
            hbT.setAlignment(Pos.CENTER);
            hbT.setPadding(new Insets(10,0,0,0));

            // Bouton Détails
            Button bDetails = new Button("+ Détails");
            bDetails.setStyle("-fx-background-radius : 30");
            bDetails.setFont(Font.font("Arial", FontWeight.BOLD,15));
            HBox hbD = new HBox();
            hbD.getChildren().add(bDetails);
            hbD.setAlignment(Pos.BOTTOM_RIGHT);
            hbD.setPadding(new Insets(0,10,10,0));


            // Initialisation Scénario
            scenario.setTop(hbT);
            scenario.setBottom(hbD);
            scenario.setPrefHeight(400);
            scenario.setPrefWidth(400);
            scenario.setStyle("-fx-border-color : black ; -fx-border-width : 2px");
        }catch(SQLException e){

        }
        return scenario;
    }

    // Méthode Détails Page Scénario
    public BorderPane detailsScene() {
        // Initialisation Contenu Scénario
        Label lTitre = new Label("Titre");
        lTitre.setFont(Font.font("Arial", 30));
        lTitre.setPadding(new Insets(0,0,20,13));

        Label nbPiece = new Label("Nb de pièce : ");
        nbPiece.setFont(Font.font("Arial", 10));
        Label difficulte = new Label("Difficulté : ");
        difficulte.setFont(Font.font("Arial", 10));
        Label partiesJouees = new Label("Parties jouées : ");
        partiesJouees.setFont(Font.font("Arial", 10));
        Label partiesGagnees = new Label("Parties gagnées : ");
        partiesGagnees.setFont(Font.font("Arial", 10));
        Label partiesPerdues = new Label("Parties perdues : ");
        partiesPerdues.setFont(Font.font("Arial", 10));
        Label pourcentageV = new Label("% de victoires : ");
        pourcentageV.setFont(Font.font("Arial", 10));
        Label tempsMoyen = new Label("Tps Moyen : ");
        tempsMoyen.setFont(Font.font("Arial", 10));

        VBox vb = new VBox();
        vb.getChildren().addAll(nbPiece, difficulte, partiesJouees, partiesGagnees, partiesPerdues, pourcentageV, tempsMoyen);

        // Bouton Retour
        Button retour = new Button("Retour");
        retour.setFont(Font.font("Arial", FontWeight.BOLD,15));
        retour.setUnderline(true);
        retour.setPadding(new Insets(0,0,0,40));

        // Initialisation Scénario
        BorderPane scenario = new BorderPane();
        scenario.setTop(lTitre);
        scenario.setCenter(vb);
        scenario.setBottom(retour);
        scenario.setPrefHeight(300);
        scenario.setPrefWidth(110);

        return scenario;
    }

    public void start(Stage stage){
        this.st = stage;
        this.st.setTitle("L'Échappée Belle");
        this.st.setWidth(600);
        this.st.setHeight(600);
        this.st.setScene(SceneInit(this.st));
        this.st.show();
    }
}
