import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

public class ControleurRetourVueScenario implements EventHandler<ActionEvent> {

    private VueScenarios vueScenarios;
    private Stage stage;


    public ControleurRetourVueScenario(VueScenarios vueScenarios, Stage stage){
        this.vueScenarios = vueScenarios;
        this.stage = stage;
    }

    @Override
    public void handle(ActionEvent actionEvent){
        this.vueScenarios.sceneInitSuite();
        this.stage.close();
    }
}
