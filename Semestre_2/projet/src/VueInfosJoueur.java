import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class VueInfosJoueur extends Application {
    private Stage st;
    private VueScenarios vueScenarios;
    private Joueur joueur;

    public static void main(String[] args) {
        launch(args);
    }

    public void setVueScenarios(VueScenarios vueScenarios){
        this.vueScenarios = vueScenarios;
        this.setJoueur(this.vueScenarios);
    }

    public void setJoueur(VueScenarios vueScenarios) {
        Utilisateur user = vueScenarios.getVueConnexion().getUserConnecte();
        this.joueur = new Joueur(user.getPseudo(),user.getMail(),user.getMotDePasse(),user.estConnecte(),user.getId(),user.getAvatar());
    }

    public VueScenarios getVueScenarios(){
        return this.vueScenarios;
    }

    public void setSt(Stage st){
        this.st = st;
    }

    public Stage getSt(){
        return this.st;
    }

    public void SceneInit(Stage st) {
        this.st = st;
        // Initialisation du bouton retour
        VBox vRetour = new VBox();
        Image img = new Image("http://freevector.co/wp-content/uploads/2014/04/59098-return-arrow-curve-pointing-left.png");
        ImageView view = new ImageView(img);
        view.setFitHeight(20);
        view.setPreserveRatio(true);
        Button bRetour = new Button();
        bRetour.setOnAction(new ControleurRetourInfosJoueurs(this));
        bRetour.setTranslateX(5);
        bRetour.setTranslateY(5);
        bRetour.setPrefSize(20,20);
        bRetour.setGraphic(view);
        vRetour.getChildren().add(bRetour);
        vRetour.setAlignment(Pos.TOP_LEFT);

        // Titre
        Label titre = new Label("INFORMATIONS");
        titre.setFont(Font.font("Arial", FontWeight.BOLD, 40));
        HBox hbTitre = new HBox();
        hbTitre.getChildren().add(titre);
        hbTitre.setAlignment(Pos.CENTER);

        // Pseudo
        Label txtPseudo = new Label("Pseudo :");
        Label pseudo = new Label(this.joueur.getPseudo());
        pseudo.setFont(Font.font("Arial",15));
        HBox hbPseudo = new HBox();
        hbPseudo.getChildren().addAll(pseudo);
        hbPseudo.setAlignment(Pos.CENTER_LEFT);
        hbPseudo.setPadding(new Insets(5,5,5,5));
        hbPseudo.setPrefHeight(50);
        hbPseudo.setPrefWidth(200);
        hbPseudo.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        VBox vPseudo = new VBox();
        vPseudo.getChildren().addAll(txtPseudo, hbPseudo);
        vPseudo.setPrefSize(200, 250);
        vPseudo.setAlignment(Pos.CENTER_LEFT);

        // Adresse Email
        Label txtMail = new Label("Adresse E-mail :");
        Label mail = new Label(this.joueur.getMail());
        mail.setFont(Font.font("Arial",15));
        HBox hbMail= new HBox();
        hbMail.getChildren().addAll(mail);
        hbMail.setAlignment(Pos.CENTER_LEFT);
        hbMail.setPadding(new Insets(5,5,5,5));
        hbMail.setPrefHeight(50);
        hbMail.setPrefWidth(200);
        hbMail.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        VBox vMail = new VBox();
        vMail.getChildren().addAll(txtMail, hbMail);
        vMail.setPrefSize(200, 250);
        vMail.setAlignment(Pos.CENTER_LEFT);

        // Mot de Passe
        Label txtMDP = new Label("Mot de passe :");
        Label mdp = new Label(this.joueur.getMotDePasse());
        mdp.setFont(Font.font("Arial",15));
        HBox hbMdp = new HBox();
        hbMdp.getChildren().addAll(mdp);
        hbMdp.setAlignment(Pos.CENTER_LEFT);
        hbMdp.setPadding(new Insets(5,5,5,5));
        hbMdp.setPrefHeight(50);
        hbMdp.setPrefWidth(200);
        hbMdp.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        VBox vMDP = new VBox();
        vMDP.getChildren().addAll(txtMDP, hbMdp);
        vMDP.setPrefSize(200, 250);
        vMDP.setAlignment(Pos.CENTER_LEFT);

        // Rôle
        Label txtRole = new Label("Rôle :");
        Label role = new Label("Joueur");
        role.setFont(Font.font("Arial",15));
        HBox hbRole = new HBox();
        hbRole.getChildren().addAll(role);
        hbRole.setAlignment(Pos.CENTER_LEFT);
        hbRole.setPadding(new Insets(5,5,5,5));
        hbRole.setPrefHeight(50);
        hbRole.setPrefWidth(200);
        hbRole.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        VBox vRole = new VBox();
        vRole.getChildren().addAll(txtRole, hbRole);
        vRole.setPrefSize(200, 250);
        vRole.setAlignment(Pos.CENTER_LEFT);

        // Colonne Gauche
        VBox colGauche = new VBox();
        colGauche.getChildren().addAll(vPseudo, vMail, vMDP, vRole);
        colGauche.setSpacing(-70);



        // Nombre de Parties Jouées
        Label txtJouees = new Label("Parties jouées :");
        Label jouees = new Label(this.joueur.getNbPartiesJouees()+"");
        jouees.setFont(Font.font("Arial",15));
        HBox hbJouees = new HBox();
        hbJouees.getChildren().addAll(jouees);
        hbJouees.setAlignment(Pos.CENTER_LEFT);
        hbJouees.setPadding(new Insets(5,5,5,5));
        hbJouees.setPrefHeight(50);
        hbJouees.setPrefWidth(200);
        hbJouees.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        VBox vJouees = new VBox();
        vJouees.getChildren().addAll(txtJouees, hbJouees);
        vJouees.setPrefSize(200, 250);
        vJouees.setAlignment(Pos.CENTER_LEFT);

        // Nombre de Parties Gagnées
        Label txtGagnees = new Label("Parties gagnées :");
        Label gagnees = new Label(this.joueur.getNbPartiesGagnees()+"");
        gagnees.setFont(Font.font("Arial",15));
        HBox hbGagnees = new HBox();
        hbGagnees.getChildren().addAll(gagnees);
        hbGagnees.setAlignment(Pos.CENTER_LEFT);
        hbGagnees.setPadding(new Insets(5,5,5,5));
        hbGagnees.setPrefHeight(50);
        hbGagnees.setPrefWidth(200);
        hbGagnees.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        VBox vGagnees = new VBox();
        vGagnees.getChildren().addAll(txtGagnees, hbGagnees);
        vGagnees.setPrefSize(200, 250);
        vGagnees.setAlignment(Pos.CENTER_LEFT);

        // Nombre de Parties Perdues
        Label txtPerdues = new Label("Parties perdues :");
        int partiesPerdues = this.joueur.getNbPartiesJouees()-this.joueur.getNbPartiesGagnees();
        Label perdues = new Label(partiesPerdues+"");
        perdues.setFont(Font.font("Arial",15));
        HBox hbPerdues = new HBox();
        hbPerdues.getChildren().addAll(perdues);
        hbPerdues.setAlignment(Pos.CENTER_LEFT);
        hbPerdues.setPadding(new Insets(5,5,5,5));
        hbPerdues.setPrefHeight(50);
        hbPerdues.setPrefWidth(200);
        hbPerdues.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        VBox vPerdues = new VBox();
        vPerdues.getChildren().addAll(txtPerdues, hbPerdues);
        vPerdues.setPrefSize(200, 250);
        vPerdues.setAlignment(Pos.CENTER_LEFT);

        // Pourcentage de Victoire
        Label txtVictoire = new Label("Pourcentage de victoire :");
        Label pourcentage = new Label(this.joueur.getPourcentage()+"");
        pourcentage.setFont(Font.font("Arial",15));
        HBox hbPourcentage = new HBox();
        hbPourcentage.getChildren().addAll(pourcentage);
        hbPourcentage.setAlignment(Pos.CENTER_LEFT);
        hbPourcentage.setPadding(new Insets(5,5,5,5));
        hbPourcentage.setPrefHeight(50);
        hbPourcentage.setPrefWidth(200);
        hbPourcentage.setStyle("-fx-background-color : lightgrey ; -fx-background-radius : 10");
        VBox vPourcentage = new VBox();
        vPourcentage.getChildren().addAll(txtVictoire, hbPourcentage);
        vPourcentage.setPrefSize(200, 250);
        vPourcentage.setAlignment(Pos.CENTER_LEFT);

        // Colonne Droite
        VBox colDroite = new VBox();
        colDroite.getChildren().addAll(vJouees, vGagnees, vPerdues, vPourcentage);
        colDroite.setSpacing(-70);


        // Scène
        HBox centre = new HBox();
        centre.getChildren().addAll(colGauche, colDroite);
        centre.setAlignment(Pos.CENTER);
        centre.setPadding(new Insets(-80,0,0,0));
        centre.setSpacing(40);
        VBox scene = new VBox();
        scene.getChildren().addAll(vRetour, hbTitre, centre);
        scene.setAlignment(Pos.TOP_CENTER);
        scene.setSpacing(60);

        this.st.setScene(new Scene(scene));
    }

    public void start(Stage stage){
        this.st = stage;
        this.st.setTitle("L'Échappée Belle");
        this.st.setWidth(600);
        this.st.setHeight(600);
        this.SceneInit(this.st);
        this.st.show();
    }
}
