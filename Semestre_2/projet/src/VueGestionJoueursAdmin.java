import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
public class VueGestionJoueursAdmin extends Application {

    private Stage st;
    private TextField tchercheJoueur;
    private VueConnexion vueConnexion;

    public static void main(String[] args) {
        launch(args);
    }

    public void setSt(Stage st){
        this.st = st;
    }

    public void setVueConnexion(VueConnexion vueConnexion) {
        this.vueConnexion = vueConnexion;
    }

    public VueConnexion getVueConnexion() {
        return vueConnexion;
    }

    public Stage getSt(){
        return this.st;
    }

    public void setScenePageGestionJoueursAdmin(){
        // Initialisation du bouton retour
        VBox vRetour = new VBox();
        Image img = new Image("http://freevector.co/wp-content/uploads/2014/04/59098-return-arrow-curve-pointing-left.png");
        ImageView view = new ImageView(img);
        view.setFitHeight(20);
        view.setPreserveRatio(true);
        Button bRetour = new Button();
        bRetour.setOnAction(new ControleurRetourAdminJoueursParties(this));
        bRetour.setTranslateX(5);
        bRetour.setTranslateY(5);
        bRetour.setPrefSize(20,20);
        bRetour.setGraphic(view);
        vRetour.getChildren().add(bRetour);
        vRetour.setAlignment(Pos.TOP_LEFT);
        vRetour.setPadding(new Insets(-30,0,0,0));

        // Initialisation TextFields, textes ...
        Label lGestionJoueur = new Label("Gestion des joueurs");
        lGestionJoueur.setPrefSize(50000,20);
        lGestionJoueur.setAlignment(Pos.CENTER);
        lGestionJoueur.setStyle("-fx-text-fill: #000000; -fx-background-color: #ccccccff");

        Label lrecherche = new Label("Rechercher un joueur");
        lrecherche.setFont(Font.font("Arial",20));
        this.tchercheJoueur = new TextField();
        this.tchercheJoueur.setMaxSize(250,35);

        Label lJoueurs = new Label("Listes des Joueurs :");
        lJoueurs.setFont(Font.font("Arial",FontWeight.BOLD,25));


        // Initialisation vBox et Scene
        BorderPane borderPane = new BorderPane();
        HBox chercherJoueur = new HBox();
        VBox vBoxGestionJoueurs = new VBox();

        chercherJoueur.getChildren().addAll(lrecherche,this.tchercheJoueur);
        chercherJoueur.setAlignment(Pos.CENTER);
        chercherJoueur.setSpacing(25);
        vBoxGestionJoueurs.getChildren().addAll(lGestionJoueur,vRetour,chercherJoueur,lJoueurs);
        vBoxGestionJoueurs.setAlignment(Pos.TOP_CENTER);
        borderPane.setTop(vBoxGestionJoueurs);

        VBox vb = new VBox();
        GridPane gd = new GridPane();
        for (int i = 0; i < 3; i++){
            for (int y = 0; y < 2; y++){
                // Initialisation des labels
                Label pseudo = new Label("Pseudo");
                pseudo.setFont(Font.font("Arial", 15));
                pseudo.setAlignment(Pos.TOP_LEFT);
                Label nbPartiesJouees = new Label("Nb de parties jouées");
                nbPartiesJouees.setFont(Font.font("Arial", 10));
                Label nbPartiesGagnees = new Label("Nb de parties gagnées");
                nbPartiesGagnees.setFont(Font.font("Arial", 10));
                Label pourcentageWin = new Label("Pourcentage de victoire");
                pourcentageWin.setFont(Font.font("Arial", 10));

                // Initialistion scénario
                VBox textesCentraux = new VBox();
                textesCentraux.getChildren().addAll(nbPartiesJouees,nbPartiesGagnees,pourcentageWin);
                textesCentraux.setSpacing(2);
                textesCentraux.setAlignment(Pos.CENTER);
                VBox textePseudo = new VBox();
                textePseudo.getChildren().add(pseudo);
                textePseudo.setAlignment(Pos.TOP_LEFT);
                BorderPane borderPane1 = new BorderPane();
                borderPane1.setTop(textePseudo);
                borderPane1.setCenter(textesCentraux);
                borderPane1.setPadding(new Insets(40));
                borderPane1.setStyle("-fx-border-radius : 10");

                Button bScenario = new Button();
                bScenario.setGraphic(borderPane1);
                bScenario.setStyle("-fx-background-color: white; -fx-border-color: black; -fx-border-width: 1px");
                bScenario.setPadding(new Insets(-15));
                gd.add(bScenario,i,y);
                gd.setHgap(10);
                gd.setVgap(10);
            }
        }
        gd.setAlignment(Pos.BOTTOM_CENTER);
        vBoxGestionJoueurs.getChildren().add(gd);
        vBoxGestionJoueurs.setSpacing(35);

        Scene sceneGestionJoueursAdmin = new Scene(borderPane);
        this.st.setScene(sceneGestionJoueursAdmin);
    }

    public void start(Stage stage) {
        this.st = stage;
        stage.setTitle("L'Échappée Belle - Administrateur");
        stage.setWidth(600);
        stage.setHeight(600);
        this.setScenePageGestionJoueursAdmin();
        stage.show();
    }
}
