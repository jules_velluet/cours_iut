import java.util.ArrayList ;
import java.util.List ;

public class Cave{

  private List<Bouteille> cave;

  public Cave(){
    this.cave = new ArrayList<>();
  }

  public void ajouteBouteille(Bouteille bout){
    this.cave.add(bout);
  }

  public int nbBouteilles(){
    return this.cave.size();
  }

  public int nbBouteillesDeRegion(String region){
    int res=0;
    for (Bouteille bout: this.cave){
      String ville = bout.getRegion();
      if (ville == region){
        res=res+1;
      }
    }
    return res;
  }

  public Bouteille plusVieilleBouteille(){
    int aux = 2021;
    Bouteille res = null;
    for (Bouteille bout: this.cave){
      if (aux == 2021 || aux>bout.getMillesime()){
        aux=bout.getMillesime();
        res=bout;
      }
    }
    return res;
  }
}
