public class ExecutableCave{

  public static void main(String[] args){
      Cave maCave = new Cave();
      maCave.ajouteBouteille(new Bouteille("Bordeaux", "Pomerol", 2007));
      maCave.ajouteBouteille(new Bouteille("Bordeaux", "Pomerol", 2005));
      maCave.ajouteBouteille(new Bouteille("Bourgogne", "Nuits St George", 2001));
      maCave.ajouteBouteille(new Bouteille("Savoie", "Pinot Noir", 2012));
      maCave.ajouteBouteille(new Bouteille("Loire", "Chinon", 2017));

      System.out.println(maCave.nbBouteilles()); // 5
      System.out.println(maCave.nbBouteillesDeRegion("Bordeaux")); // 2
      Bouteille b = maCave.plusVieilleBouteille();
      System.out.println(b.getAppellation());  // Nuits St George
  }
}
