public class ExecutableBouteille{

  public static void main(String[] args){

      Bouteille a = new Bouteille("Bordeaux", "Pomerol", 2007);

      System.out.println(a.getRegion()); // Bordeaux
      System.out.println(a.getAppellation()); // Pomerol
      System.out.println(a.getMillesime());  // 2007
  }
}
