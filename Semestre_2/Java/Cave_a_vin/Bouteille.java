public class Bouteille{
  private String region;
  private String appellation;
  private int annee;

  public Bouteille(String ville, String nom, int anne) {
    this.region=ville;
    this.appellation=nom;
    this.annee=anne;
  }

  public String getRegion(){
    return this.region;
  }

  public String getAppellation(){
    return this.appellation;
  }

  public int getMillesime(){
    return this.annee;
  }
}
