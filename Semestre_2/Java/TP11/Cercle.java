import javafx.scene.shape.Circle;
import javafx.geometry.Pos;
import java.util.List;
import java.util.ArrayList;

public class Cercle extends Circle{

  public static Double RayonMini = 0.1;


  public Cercle(double largeur, double hauteur){
    super(Math.random()*largeur,Math.random()*hauteur, RayonMini);
   }

   public Cercle(List<Cercle> liste, double largeur, double hauteur){
     super(Math.random()*largeur,Math.random()*hauteur,RayonMini);
     this.placerAuHasard(liste, largeur, hauteur);
     this.grossir(liste, largeur, hauteur);
    }

   private boolean intersecte(Cercle c){
     return Math.sqrt(Math.pow(c.getCenterX()-this.getCenterX(),2)+Math.pow(c.getCenterY()-this.getCenterY(),2)) > this.getRadius()+c.getRadius();
   }

   private boolean estDansLeCadre(double largeur, double hauteur){
     return (this.getCenterX()+this.getRadius() < largeur &&  this.getCenterX()-this.getRadius() > 0) && (this.getCenterY()+this.getRadius() < hauteur && this.getCenterY()-this.getRadius() > 0);
   }

   private boolean estValide(List<Cercle> liste, double largeur, double hauteur){
     if (this.estDansLeCadre(largeur,hauteur) == false){
       return false;
     }

     for (Cercle cercle: liste){
       if (this.intersecte(cercle) == false){
         return false;
       }
     }
     return true;
   }

   private void placerAuHasard(List<Cercle> liste, double largeur, double hauteur){
     while (this.estValide(liste,largeur,hauteur) == false){
       this.setCenterX(Math.random()*largeur);
       this.setCenterY(Math.random()*hauteur);
     }
   }

   public void grossir(List<Cercle> liste, double largeur, double hauteur){
     Double rayonMax = Math.min(largeur,hauteur);
     this.setRadius(rayonMax);
     while (estValide(liste, largeur, hauteur) == false){
       rayonMax= rayonMax/2;
       this.setRadius(rayonMax);
     }
   }


}
