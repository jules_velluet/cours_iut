import java.util.List;
import java.util.ArrayList;
import javafx.scene.shape.Circle;
import javafx.geometry.Pos;

public class BibCercle{

  public static Cercle getMin(List<Cercle> liste){
    Cercle michel = null;
    double rad = 0;
    for (Cercle cercle: liste){
      if(rad == 0 || cercle.getRadius()<rad){
        michel = cercle;
        rad = cercle.getRadius();
      }
    }
    return michel;
  }

  public static double surfaceTotale(List<Cercle> liste){
    double res = 0;
    for (Cercle cercle: liste){
      res = res + Math.PI*Math.pow(cercle.getRadius(),2);
    }
    return res;
  }

  public static int nbCercle(List<Cercle> liste){
    return liste.size()+1;
  }
}
