import javafx.geometry.Pos;
import java.util.List;
import java.util.ArrayList;
import javafx.scene.shape.Rectangle;

public class Rectangle1 extends Rectangle{

  public static Double width = 0.1;
  public static Double height = 0.1;


  public Rectangle1(double largeur, double hauteur){
    super(Math.random()*largeur,Math.random()*hauteur, width, height);
   }

   public Rectangle1(List<Rectangle1> liste, double largeur, double hauteur){
     super(Math.random()*largeur,Math.random()*hauteur, width, height);
     this.placerAuHasard(liste, largeur, hauteur);
     this.grossir(liste, largeur, hauteur);
    }

    private boolean intersecte(Rectangle1 r){
       // recouvrement axe horizontal
       boolean horizontal = (this.getX()<r.getX()+r.getWidth()) && (r.getX()<this.getX()+this.getWidth());

       // recouvrement axe vertical
       boolean vertical = (this.getY()<r.getY()+r.getHeight()) && (r.getY()<this.getY()+this.getHeight());

       // recouvrement final
       return horizontal && vertical;
     }

   private boolean estDansLeCadre(double largeur, double hauteur){
     return (this.getX()+this.getWidth() < largeur &&  this.getX() > 0) && (this.getY()+this.getHeight() < hauteur && this.getY() > 0);
   }

   private boolean estValide(List<Rectangle1> liste, double largeur, double hauteur){
     if (this.estDansLeCadre(largeur,hauteur) == false){
       return false;
     }

     for (Rectangle1 cercle: liste){
       if (this.intersecte(cercle) == true){
         return false;
       }
     }
     return true;
   }

   private void placerAuHasard(List<Rectangle1> liste, double largeur, double hauteur){
     while (this.estValide(liste,largeur,hauteur) == false){
       this.setX(Math.random()*largeur);
       this.setY(Math.random()*hauteur);
     }
   }

   public void grossir(List<Rectangle1> liste, double largeur, double hauteur){
     Double widthMax = Math.max(largeur,hauteur);
     Double heightMax = Math.min(largeur,hauteur);
     this.setWidth(widthMax);
     this.setHeight(heightMax);
     while (estValide(liste, largeur, hauteur) == false){
       widthMax = widthMax/2;
       heightMax = heightMax/2;
       this.setWidth(widthMax);
       this.setHeight(heightMax);
     }
   }


}
