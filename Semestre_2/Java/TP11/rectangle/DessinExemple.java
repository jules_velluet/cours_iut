import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.layout.BorderPane;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import java.util.List;
import javafx.scene.shape.Rectangle;
import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.geometry.Pos;

public class DessinExemple extends Application {

    private int largeur = 600;
    private int hauteur = 300;
    private List<Rectangle1> liste;

    // === Début du code à compléter / modifier ===========

    @Override
    public void init(){
        this.liste = new ArrayList<>();
        for (int i=0; i<=15000; i++){
          Rectangle1 r1 = new Rectangle1(liste, largeur,hauteur);
          r1.setFill(new Color(Math.random(), Math.random(), Math.random(), 1.0));
          this.liste.add(r1);
        }

        // Circle c1 = new Circle(50, 50, 50);
        // c1.setFill(new Color(Math.random(), Math.random(), Math.random(), 1.0));
        // this.liste.add(c1);
        // Circle c2 = new Circle(590, 290, 10);
        // c2.setFill(new Color(Math.random(), Math.random(), Math.random(), 1.0));
        // this.liste.add(c2);
        // Circle c3 = new Circle(Math.random()*600, Math.random()*300,30);
        // c3.setFill(new Color(Math.random(), Math.random(), Math.random(), 1.0));
        // this.liste.add(c3);
    }

    // === Fin du code à compléter / modifier ===========

    @Override
    public void start(Stage stage) {
       Group dessinCercles = new Group();
       dessinCercles.getChildren().addAll(this.liste);
       BorderPane root = new BorderPane();
       root.setCenter(dessinCercles);
       VBox infos =this.informations();
       root.setBottom(infos);
       root.setAlignment(infos, Pos.TOP_LEFT);
       Scene scene = new Scene(root, this.largeur, this.hauteur+infos.getHeight()+40);
       stage.setTitle("Formes");
       stage.setScene(scene);
       stage.show();
    }

    private VBox informations(){
        VBox vbox = new VBox();
        String cssLayout = "-fx-border-color: black;\n" + "-fx-border-width: 2;\n";
        vbox.setStyle(cssLayout);
        // Code à compléter à patir de la Q17
        vbox.getChildren().add(new Label("Le Cercle le plus petit est : "));
        vbox.getChildren().add(new Label("La surface totale est : "+BibRectangle.surfaceTotale(this.liste)));
        vbox.getChildren().add(new Label("Il y a "+" cercle ou rectangle "+BibRectangle.nbCercle(this.liste)));
        return vbox;
    }

    public static void main(String args[]){
       launch(args);
   }
}
