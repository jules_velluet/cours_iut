import java.util.List;
import java.util.ArrayList;
import javafx.scene.shape.Rectangle;
import javafx.geometry.Pos;

public class BibRectangle{

  // public static Rectangle getMin(List<Cercle> liste){
  //   Cercle michel = null;
  //   double rad = 0;
  //   for (Cercle cercle: liste){
  //     if(rad == 0 || cercle.getRadius()<rad){
  //       michel = cercle;
  //       rad = cercle.getRadius();
  //     }
  //   }
  //   return michel;
  // }

  public static double surfaceTotale(List<Rectangle1> liste){
    double res = 0;
    for (Rectangle1 cercle: liste){
      res = res + cercle.getX()*cercle.getY();
    }
    return res;
  }

  public static int nbCercle(List<Rectangle1> liste){
    return liste.size()+1;
  }
}
