public class Atout implements Carte{

  private int valeur;

  public Atout(int val) {
    this.valeur = val;
  }

  public String getType(){
    return "Atout";
  }

  public int getValeurInt(){
    return this.valeur;
  }

  public String getValeur(){
    return ""+this.valeur;
  }
}
