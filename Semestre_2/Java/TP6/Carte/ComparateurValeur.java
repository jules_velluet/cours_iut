import java.util.Comparator;

public class ComparateurValeur implements Comparator<Carte>{

  @Override
  public int compare(Carte c1, Carte c2){
    int carte1 = c1.getValeurInt();
    int carte2 = c2.getValeurInt();

    return carte1-carte2;
  }
}
