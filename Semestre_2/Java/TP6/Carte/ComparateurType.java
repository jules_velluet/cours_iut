import java.util.Comparator;

public class ComparateurType implements Comparator<Carte>{

  @Override
  public int compare(Carte c1, Carte c2){
    String carte1 = c1.getType();
    String carte2 = c2.getType();
    return carte1.compareToIgnoreCase(carte2);
  }
}
