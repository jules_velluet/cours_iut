import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;

public class Main {
   private List<Carte> main;

   public Main(){
     this.main = new ArrayList<>();
   }

   public List<Carte> getMesCartes(){
       return this.main;
   }

   public void addrtrt(Carte c){
     this.main.add(c);
   }

   public int nombreDeCartes(){
       return this.main.size();
   }

   @Override
   public String toString(){
     return ""+this.main;
   }

   /**
    * Trie dans l'ordre "Atout" < "Trèfle" < "Carreau"< "Coeur" < "Pique"
    */
   public void triParType(){
     Comparator<Carte> comp = new ComparateurType();
     Collections.sort(this.main, comp);
   }
   public void triParValeur() {
     Comparator<Carte> c = new ComparateurValeur();
     Collections.sort(this.main, c);
   }
}
