import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class CarteCouleur  implements Carte{
   /* "Coeur", "Carreau", "Pique", "Trèfle"
   */
   private String couleur;

   /* 1, 2, 3, ..., 9, 10, 11, 12, 13 (11 représente le Valet,..)
   */
   private int valeur;

   public static List<String> carte = Arrays.asList("As","2","3","4","5","6","7","8","9","10","Valet","Dame","Roi");

   public CarteCouleur(int valeur, String couleur){
      this.valeur = valeur;
      this.couleur = couleur;
   }

   public String getCouleur(){
        return this.couleur;
    }

   /* @return "As", "2", ..., "Dame" ou "Roi"
      la chaîne de caractère représentant une carte
    */
   public String getValeur(){
     return CarteCouleur.carte.get(this.valeur - 1);
   }

   @Override
    public String getType(){
        return this.couleur;
    }

   /* @return 1, 2, 3, ...
    */
   public int getValeurInt(){
       return this.valeur;
   }

   @Override
   public String toString(){
       return getValeur()+ " de "+this.couleur;
   }
}
