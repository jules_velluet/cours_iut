import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;

public class Executable{

  public static void main(String[] args) {
      List<Chat> liste = new ArrayList<>();
      liste.add(new Chat("michel",0));
      liste.add(new Chat("patrick",10));
      liste.add(new Chat("julien",5));
      liste.add(new Chat("grosLard",8));

      System.out.println(liste);

      //trie par nom
      ComparatorNom nom = new ComparatorNom();
      Collections.sort(liste, nom);
      System.out.println(liste);

      //trie par bavard
      ComparatorChat comp = new ComparatorChat();
      Collections.sort(liste, comp);
      System.out.println(liste);

      Comparatoralpha alpha = new Comparatoralpha();
      Collections.sort(liste, alpha);
      System.out.println(liste);
  }
}
