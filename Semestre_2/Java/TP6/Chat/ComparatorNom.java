import java.util.Comparator;

public class ComparatorNom implements Comparator<Chat> {

  public int compare(Chat c1, Chat c2){
    return c1.getNom().length() - c2.getNom().length();
  }
}
