import java.util.Comparator;

public class Comparatoralpha implements Comparator<Chat> {

  public int compare(Chat c1, Chat c2){
    return c1.getNom().compareTo(c2.getNom());
  }
}
