public class Chat{

    private String nom;
    private int bavard;

    public Chat(String nomDuChat){
        this.nom = nomDuChat;
        this.bavard = 1;
    }

    public Chat(String nomDuChat, int bavard){
        this.nom = nomDuChat;
        this.bavard = bavard;
    }

    // Accesseurs ===================
    public String getNom(){
        return this.nom;
    }

    public int getBavardage(){
      return this.bavard;
    }

    // Mutateurs ===================
    public void setNom(String nouveauNom){
        this.nom=nouveauNom;
    }

    public void devientMuet(){
        this.bavard = 0;
    }
    // Autres méthodes ===================

    public void miaule(){
        for(int i=0; i<this.bavard; i++){
            System.out.println("Miaou !!!!");
        }
    }

    public boolean estEndormi(double heure){
        return (heure<=3 || heure>4);
    }

    @Override
    public String toString(){
      return "nom: "+this.nom+" bavard: "+this.bavard;
    }
}
