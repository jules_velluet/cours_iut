import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class executable{

  public static void main(String [] args){
     List<Integer> liste1 = Arrays.asList(1, 3, 6, 7, 9, 10);
     List<Integer> liste2 = Arrays.asList(3, 5, 6, 8, 11);
     System.out.println(BibCollections.intersection2(liste1, liste2));
     List<Integer> liste3 = Arrays.asList(0, 10, 12, 13);
     List<Integer> liste4 = Arrays.asList(2, 3, 4);
     System.out.println(BibCollections.intersection2(liste3, liste4));
     List<Integer> liste5 = new ArrayList<>();
     List<Integer> liste6 = new ArrayList<>();
     for (int i=0; i<100000; i++){
       liste5.add(i);
       liste6.add(i);
     }
     System.out.println(BibCollections.intersection2(liste5, liste6));
}
}
