import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class BibCollections{

  public static List<Integer> intersection(List<Integer> l1, List<Integer> l2){
    List<Integer> res = new ArrayList<>();
    for (Integer elem: l1){
      if (l2.contains(elem)){
        res.add(elem);
      }
    }
    return res;
  }

  public static List<Integer> intersection2(List<Integer> l1, List<Integer> l2){
    List<Integer> res = new ArrayList<>();
    int i = 0;
    int j = 0;
    while (i < l1.size() && j < l2.size()){
      if (l1.get(i) > l2.get(j)){
        j++;
      }
      else if (l1.get(i) < l2.get(j)){
        i++;
      }
      else {
        res.add(l1.get(i));
        i++;
        j++;
      }
    }
    return res;
  }
}
