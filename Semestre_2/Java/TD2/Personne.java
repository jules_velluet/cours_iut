public class Personne{

  private String nom;
  private int age;

  public Personne(String name, int age){
    this.nom = name;
    this.age = age;
  }

  public String getNom(){
    return this.nom;
  }

  public int getAge(){
    return this.age;
  }
}
