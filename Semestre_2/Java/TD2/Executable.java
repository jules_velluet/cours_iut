public class Executable {
   public static void main(String [] args) {
    Table table = new Table();
    table.ajouteConvive(new Personne("Papa", 25));
    table.ajouteConvive(new Personne("GrandPapy", 85));
    table.ajouteConvive(new Personne("Mamie", 63));
    table.ajouteConvive(new Personne("Junior", 2));
    table.ajouteConvive(new Personne("Maman", 22));
    table.ajouteConvive(new Personne("Tonton", 47));
    System.out.println(table.sontACote("GrandPapy","Mamie"));
    System.out.println(table.sontACote("Mamie","GrandPapy"));
    System.out.println(table.sontACote("Junior","Tonton"));
    System.out.println(table.doyen());
    System.out.println(table.estTrie());
   }
}
