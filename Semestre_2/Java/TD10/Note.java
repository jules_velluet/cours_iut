public class Note implements Interfacegrelingrelin{

  private int frequence;
  private int duree;

  public Note(int frequence, int duree){
    this.frequence = frequence;
    this.duree = duree;
  }

  public int getDuree(){
    return this.duree;
  }

  public int getFrequence(){
    return this.frequence;
  }

  public void jouer(){
    System.out.println("frequence: "+this.frequence+", duree: "+this.duree);
  }

  public int hashCode(){
      return this.frequence*1+this.duree*1;
  }
}
