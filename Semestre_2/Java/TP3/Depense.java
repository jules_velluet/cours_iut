public class Depense{

  private Personne personne;
  private double montant;
  private String produit;

  public Depense(Personne pers, double montant, String produit){
    this.personne = pers;
    this.montant = montant;
    this.produit = produit;
  }

  public Personne getPersonne(){
    return this.personne;
  }

  public double getMontant(){
    return this.montant;
  }

  public String getProduit(){
    return this.produit;
  }
 }
