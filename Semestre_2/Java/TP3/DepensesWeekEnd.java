import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.lang.*;

public class DepensesWeekEnd{

  private List<Depense> depense;
  private List<Personne> personne;

  public DepensesWeekEnd(){
    this.depense = new ArrayList<>();
    this.personne = new ArrayList<>();
  }

  public void ajoutePersonne(Personne pers){
    this.personne.add(pers);
  }

  public void ajouteDepense(Depense depense){
    this.depense.add(depense);
  }

  public double totalDepenses(Personne p){
    double res = 0;
    for (Depense dep: depense){
      if (dep.getPersonne() == p){
        res = res + dep.getMontant();
      }
    }
    return res;
  }

  public double moyenneDepense(){
    double montanttotal = 0;
    for (Depense dep: depense){
      montanttotal = montanttotal + dep.getMontant();
    }
    double res = montanttotal/personne.size();
    return res;
  }

  public double totalDepensesProduit(String produit){
    double res = 0;
    for (Depense dep: depense){
      if (dep.getProduit() == produit){
        res = res + dep.getMontant();
      }
    }
    return res;
  }

  public HashMap<String,Double> depenseparpers(){
    HashMap<String, Double> res = new HashMap<>();
    for (Depense dep: depense){
      String nom = dep.getPersonne().getNom();
      if (res.containsKey(nom)){
        double a= res.get(nom);
        res.put(nom, a + dep.getMontant());
      }
      else {
        res.put(nom, dep.getMontant());
      }
    }
    return res;
  }

  public HashMap<String,Double> argent(){
    HashMap<String, Double> res = new HashMap<>();
    double depensemoy = moyenneDepense();
    HashMap<String, Double> n = depenseparpers();
    for (String pers: n.keySet()){
      Double renboursement = Math.abs(n.get(pers) - depensemoy);
      res.put(pers, renboursement);
    }
    return res;
  }
}
//envoi par mail
