import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class Executable{
public static void main(String[] args) {
List liste1, liste2, liste3;

liste1 = new ArrayList<>();
liste1.add(2);
liste1.add(3);
liste1.add(3);
liste1.add(5);

liste2 = Arrays.asList(2,3,3,5);
liste3 = Arrays.asList(2,3,5);

assert BibliothequeListes.identiques(liste1, liste2);
assert !BibliothequeListes.identiques(liste1, liste3);
assert !BibliothequeListes.identiques(liste3, liste2);
}
}
