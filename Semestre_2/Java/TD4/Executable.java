import java.util.ArrayList;
import java.util.List;

public class Executable
{
  public static void main(String[] args)
  {
     Integer x = 3;
     Integer y = new Integer(3);
     System.out.println( x == y);                        //(1)
     System.out.println( x.equals(y));                   //(2)
     System.out.println( x.toString() );                 //(3)
     System.out.println( x );                            //(4)
     System.out.println( x.hashCode() == y.hashCode() ); //(5)

     List<Object> tableau  = new ArrayList<Object>();
     List<Object> tab2 = new ArrayList<Object>();
     tableau.add(new Integer(5));
     tableau.add(new Double(5.));
     tableau.add(new Boolean(true));
     tableau.add(tab2);
     System.out.println(tab2);                           //(6)

     tableau = new ArrayList<Object>();
     tab2 = new ArrayList<Object>();
     tableau.add(new Integer(4));
     tab2.add(new Integer(4));
     System.out.println(tab2 == tableau);                //(7)
     System.out.println(tab2.equals(tableau));           //(8)
  }
}
