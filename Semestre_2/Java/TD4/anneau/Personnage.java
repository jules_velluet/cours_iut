public class Personnage{

  private String nom;
  private int barbe;
  private int oreilles;

  public Personnage(String nom, int barbe, int oreilles){
    this.nom = nom;
    this.barbe = barbe;
    this.oreilles = oreilles;
  }

  @Override
  public String toString(){
    return "("+this.nom+", "+"b="+this.barbe+", "+"o="+this.oreilles+")";
  }

  @Override
  public boolean equals(Object other){
    if (other == null){
      return false;
    }
    if (other instanceof Personnage){
      Personnage autrepers = (Personnage) other;
      return this.nom == autrepers.nom && this.barbe == autrepers.barbe && this.oreilles == autrepers.oreilles;
    }
    return false;
  }

  @Override
  public int hashCode(){
    return this.nom.hashCode() + this.barbe + this.oreilles;
  }
}
