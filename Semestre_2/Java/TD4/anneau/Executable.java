import java.util.List;
import java.util.ArrayList;

public class Executable {
  public static void main(String [] args) {
      // Gimli a une barbe de 65cm et ses oreilles mesurent 15cm
      Personnage gimli = new Personnage("Gimli",65,15);
      System.out.println(gimli);
      // (Gimli, b=65, o=15)

      List<Personnage> fraternite = new ArrayList<>();
      fraternite.add(gimli);
      fraternite.add(new Personnage("Legolas",0,35));
      fraternite.add(new Personnage("GrandPas",20,8));
      System.out.println(fraternite);
      // [(gimli, b=65, o=15), (Legolas, b=0, o=35), (GrandPas, b=20, o=8)]

      System.out.println(fraternite.contains(new Personnage("Gimli",65,15)));
  }
}
