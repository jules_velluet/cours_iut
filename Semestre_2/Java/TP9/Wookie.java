public class Wookie implements Personnage {

  private String nom;
  private String muniDe;
  private double taille;

  public Wookie(String nom, double taille, String muniDe){
    this.nom = nom;
    this.taille = taille;
    this.muniDe = muniDe;
  }

  public String getRace(){
    return "Wookie";
  }

  public double getTaille(){
    return this.taille;
  }

  public String getCaracteristique(){
    return this.muniDe;
  }

  public String getNom(){
    return this.nom;
  }

  public String toString(){
     return "Je suis un Ewok, je m'appelle "+this.nom+", je mesure "+this.taille+" metre(s) et porte "+this.muniDe;
  }
}
