public class Ewok implements Personnage {

  private String nom;
  private String role;
  private double taille;

  public Ewok (String nom, double taille, String role){
    this.nom = nom;
    this.role = role;
    this.taille = taille;
  }

  public String getRace(){
    return "Ewok";
  }

  public double getTaille(){
    return this.taille;
  }

  public String getCaracteristique(){
    return this.role;
  }

  public String getNom(){
    return this.nom;
  }

  public String toString(){
     return "Je suis un Ewok, je m'appelle "+this.nom+", je mesure "+this.taille+" metre(s) et je suis "+this.role;
  }
}
