import java.util.ArrayList;
import java.util.List;
import java.util.Comparator;
import java.util.Collections;

public class RencontreStarWars {

  private List<Personnage> liste;

  public RencontreStarWars(){
    this.liste = new ArrayList<>();
  }
  public List<Personnage> getMesPersonnages(){
    return this.liste;
  }

  public void add(Personnage pers){
    this.liste.add(pers);
  }

  public int nombreDePersonnages(){
    return this.liste.size();
  }

  public void trierParTaille(){
    Comparator<Personnage> comp = new ComparateurTaille();
    Collections.sort(this.liste, comp);
  }

  public void trierParRace(){
    Comparator<Personnage> comp = new ComparateurRace();
    Collections.sort(this.liste, comp);
  }

  public String toString(){
    return ""+this.liste;
  }
}
