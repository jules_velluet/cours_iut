import java.util.Comparator;

public class ComparateurTaille implements Comparator<Personnage>{

  public int compare(Personnage p1, Personnage p2){
    int res;
    if (p1.getTaille() < p2.getTaille()){
      res = -1;
    }
    else if (p1.getTaille() >  p2.getTaille()) {
      res = 1;
    }
    else {
      res =0;
    }
    return res;
  }
}
