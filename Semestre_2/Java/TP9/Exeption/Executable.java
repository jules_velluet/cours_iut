import java.util.Collections;
import java.util.NoSuchElementException;

public class Executable {

  public static void main(String[] args) {
    Tableau t1 = new Tableau();
    t1.remplir();
    System.out.println(t1);
    try {
      System.out.println(Collections.min(t1.getTableau()));
    }
    catch(NoSuchElementException expt){
      System.out.println("il n'y a pas de minimum: la liste est vide");
    }
    try {
      System.out.println(t1.get(1));
    }
    catch (NoSuchElementException expt){
      System.out.println("indice trop grand");
    }
    try {
      System.out.println(t1.getMax());
    }
    catch (NoSuchElementException expt){
      System.out.println("il n'y a pas de maximum: la liste est vide");
    }
    try {
      System.out.println(t1.getMin());
    }
    catch (PasdeTelElementException expt){
      System.out.println("il n'y a pas de minimum: la liste est vide");
    }
  }
}
