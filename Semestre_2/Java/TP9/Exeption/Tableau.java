import java.util.List;
import java.util.ArrayList;
import java.lang.Math;
import java.util.NoSuchElementException;
import java.util.Collections;
public class Tableau {
   private List<Integer> tab;
   public Tableau () {
       tab = new ArrayList<Integer>();
   }
   public void remplir() {
       int nb = (int)(Math.random()* 10);
       for(int i = 0; i< nb; ++i)
           tab.add((int)(Math.random()* 50));

   }
   @Override
   public String toString() {return tab.toString();}
   public List<Integer> getTableau() {return this.tab;}

   public Integer get(int indice) throws NoSuchElementException{
     if (this.tab.size() > indice){
       return this.tab.get(indice);
     }
     else {
       throw new NoSuchElementException();
     }
   }

   public Integer getMax() throws NoSuchElementException{
     return Collections.max(this.tab);
   }

   public Integer getMin() throws PasdeTelElementException{
     if (this.tab.size() == 0){
       throw new PasdeTelElementException();
     }
     else {
       return Collections.max(this.tab);
     }
   }
}
