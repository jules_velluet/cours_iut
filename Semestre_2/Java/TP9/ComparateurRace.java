import java.util.Comparator;

public class ComparateurRace implements Comparator<Personnage>{

  public int compare(Personnage p1, Personnage p2){
    String michel = p1.getRace();
    String patrick = p2.getRace();
    return michel.compareToIgnoreCase(patrick);
  }
}
