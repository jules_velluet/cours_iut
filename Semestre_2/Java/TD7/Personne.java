import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Collections;

public class Personne {
    private String nom;
    private int age;

    public Personne(String nom, int age) {
        this.nom = nom;
        this.age = age;
    }

    public String getNom() {
      return this.nom;
    }

    public int getAge() {
      return this.age;
    }

    public String toString(){
        return this.getNom()+" qui a "+this.getAge()+" ans";
    }

    public static int ecartAgeMini(List<Personne> listep){
      if (listep.size()< 2){
        return -1;
      }

      List<Personne> copie1 = new  ArrayList<>(listep);
      Comparator<Personne> comp = new Comparateur();
      Collections.sort(copie1, comp);
      Integer res = 0;
      for (int i=1 ; i<copie1.size(); i++){
        if (res == 0 || copie1.get(i).getAge() - copie1.get(i-1).getAge() < res){
          res = copie1.get(i).getAge() - copie1.get(i-1).getAge();
        }
      }
      return res;
        //Comparer les éléments adjacents
        //Retourner le minimum
    }
}
