import java.util.Comparator;
public class CompLong implements Comparator<Personne> {
    public int compare(Personne p1, Personne p2) {
        return p1.getNom().length() - p2.getNom().length();
    }
}
