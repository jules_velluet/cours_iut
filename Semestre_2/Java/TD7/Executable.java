import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Collections;

public class Executable{

  public static void main(String [] args){

    Personne jaouad = new Personne("Jaouad", 29);
    Personne l = new Personne("Louis", 22);
    Personne j = new Personne("Jack", 54);
    Personne li = new Personne("Line", 61);
    Personne je = new Personne("Jeannine", 65);

    List<Personne> liste = Arrays.asList(jaouad,l,j,li,je);
    System.out.println(liste);

    Comparator<Personne> comp = new Comparateur();
    Collections.sort(liste, comp);
    System.out.println(liste);

    Comparator<Personne> cn = new CompLong();
    Collections.sort(liste, cn);
    System.out.println(liste);
    int a = Personne.ecartAgeMini(liste);
    System.out.println(a);
  }
}
