import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;

public class Chenil{

  private List<Chien> listechien;

  public Chenil(){
    this.listechien = new ArrayList<>();
  }

  public void deposer(Chien chien){
    this.listechien.add(chien);
  }

  public List<Chien> trier(){
    Comparator<Chien> comp = new ComparateurPoil();
    Collections.sort(this.listechien, comp);
    return this.listechien;
  }

  @Override
  public String toString(){
    String res = "Chenil : [";
    for (Chien chien: this.listechien){
      res = res + chien+", ";
    }
    res = res + " ]";
    return res;
  }
}
