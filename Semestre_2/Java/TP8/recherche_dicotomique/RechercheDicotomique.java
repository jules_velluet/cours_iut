import java.util.ArrayList;
import java.util.List;

public class RechercheDicotomique{

  public static boolean recherche(List<Integer> list, int nb){
    int bas = 0;
    int haut = list.size()-1;

    while (bas<haut){
      int milieu = (bas+haut)/2;
      if (list.get(milieu)<nb){
        bas = milieu+1;
      }
      else {
        haut = milieu;
      }
    }
    return bas<list.size() && nb == list.get(bas);
  }
}
