public class Executable {
   public static void main(String [] args) {
       Chenil chenil = new Chenil();
       chenil.deposer(new Chien("Médor", 15, "brune"));
       chenil.deposer(new Chien("Toutou", 10, "auburn"));
       chenil.deposer(new Chien("Milou", 17, "acajou"));
       System.out.println(chenil);
       System.out.println(chenil.trier());
   }
}
