public class Chien{

  private String nom;
  private String couleur;
  private int taillePoils;

  public Chien(String nom, int poils,String couleur ){
    this.nom = nom;
    this.couleur = couleur;
    this.taillePoils = poils;
  }

  public String getNom(){
    return this.nom;
  }

  public String getCouleur(){
    return this.couleur;
  }

  public int getTaillePoil(){
    return this.taillePoils;
  }

  @Override
  public String toString(){
    return this.nom+" de couleur "+this.couleur+" des poils : "+this.taillePoils+" cm";
  }
}
