import java.util.Comparator;

public class ComparateurPoil implements Comparator<Chien>{

  @Override
  public int compare(Chien c1, Chien c2){
    return c1.getTaillePoil()-c2.getTaillePoil();
  }
}
