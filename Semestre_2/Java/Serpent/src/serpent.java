public class serpent {
	
	private String nom;
	private double taille;
	private int danger;
	
	public serpent(String nom, double taille, int danger) {
		this.nom=nom;
		this.taille=taille;
		this.danger=danger;
	}
	public String getNom() {
		return this.nom;
	}
	
	public double getTaille() {
		return this.taille;
	}
	
	public int getDanger() {
		return this.danger;
	}
	public String serpentToStr() {
		return "Nom: "+this.nom+
				"\nTaille: "+this.taille+
				"\nDanger: "+this.danger;
	}
}
