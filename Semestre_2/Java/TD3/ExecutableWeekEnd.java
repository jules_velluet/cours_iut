class ExecutableWeekEnd {

	public static void main (String [] args){
	
		// Pierre a acheté du pain pour 12 euros
		Personne pierre = new Personne("Pierre");
		Depense pain = new Depense(pierre,12,"pain");
		
		// Paul a dépensé 100 euros pour les pizzas
		Personne paul = new Personne("Paul");
                Depense pizza = new Depense(paul,100,"pizza");
		
		// Pierre a payé l’essence et en a eu pour 70 euros
		Depense essence = new Depense(pierre,70,"essence");		
		// Marie a acheté du vin pour 15 euros
		Personne marie = new Personne("Marie");
                Depense vin = new Depense(marie,15,"vin");
		
		// Paul a aussi acheté du vin et en a eu pour 10 euros
		Depense vin2 = new Depense(paul,10,"vin");
		
		// Anna n’a quant à elle rien acheté.	
		Personne anna = new Personne("Anna");
	
		// Combien a depensé Paul au total
		DepensesWeekEnd lesdepenses = new DepensesWeekEnd();
		lesdepenses.ajouteDepense(pain);
		lesdepenses.ajouteDepense(pizza);
		lesdepenses.ajouteDepense(essence);
		lesdepenses.ajouteDepense(vin);
		lesdepenses.ajouteDepense(vin2);

		lesdepenses.ajoutePersonne(pierre);
		lesdepenses.ajoutePersonne(paul);
		lesdepenses.ajoutePersonne(marie);
		lesdepenses.ajoutePersonne(anna);

		System.out.println("Paul a dépensé au total "+lesdepenses.totalDepenses(paul)+"€");
		// On s'attend a avoir 110€

		// Quel est le montant moyen des depenses
		System.out.println("montant moyen des depenses :"+lesdepenses.moyenneDepense());
		// On s'attend a avoir 41,4€ 


		// Quel est le total des dépenses pour le vin
		System.out.println("montant des depenses pour le vin : "+lesdepenses.totalDepensesProduit("vin");
		// On s'attend à avoir 25€
	}

}
