import java.util.HashMap;
import java.util.List;

public class RepertoireMap implements Repertoire {

  private HashMap<String, List<String>> repertoire;

  public RepertoireMap(){
    this.repertoire = new HashMap<>();
  }

  public void ajouteContact(String nom, String numero){
    if (this.repertoire.containskey(nom)){
      this.repertoire.get(nom).add(numero);
    }
    else{
      List<String> patrick = new ArrayList<>();
      patrick.add(numero);
      this.repertoire.put(nom,patrick);
    }
  }

  public void supprimeContact(String nom) throws PasDeContactException{
    if (this.repertoire.containskey(nom)){
      this.repertoire.remove(nom);
    }
    else{
      throw new PasDeContactException;
    }
  }

  public List<String> getNoms(){
    List<String> Serge = new ArrayList<>();
    Set<String> michel = new Set<>();
    michel = this.repertoire.keySet();
    for (String bernard: michel){
      Serge.add(bernard);
    }
    return Serge;
  }

  public List<String> getNoms(int typeDeTri){

  }

  public List<String> getNumeros(String nom){
    List<String> hoho = new ArrayList<>();
    List<String> jeanmichelle = new Arraylist<>();
    jean-michelle.add(this.repertoire.get(nom));
    for (String youpi:jean-michelle){
      hoho.add(youpi);
    }
    return hoho;
  }

  public List<String> rechercheNumero(String numero)  throws PasDeContactException {
    if (this.repertoire.containsValue(numero) == false){
      throw new PasDeContactException;
    }

  }

  public void initRepertoire();
}
