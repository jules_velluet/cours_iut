import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;


public class Executable{
  public static void main(String[] args) {
    List<Integer> liste1  = new ArrayList<>();
    liste1.add(1);
    liste1.add(5);
    liste1.add(2);
    liste1.add(4);

    List<Integer> liste2 = Arrays.asList(12,3,7,9);

    System.out.println(Bibliotheque.elementEnCommun(liste1, liste2));

    List<Integer> liste3 = Arrays.asList(1,45,3,2);
    List<Integer> liste4 = Arrays.asList(6,5,1,12);

    System.out.println(Bibliotheque.elementEnCommun(liste3, liste4));
  }
}
