

public class Executable{
  public static void main(String[] args) {

    Personnage ironman = new Personnage("ironman");
    Personnage thor = new Personnage("thor", 11312);

    ListePersonnages liste = new ListePersonnages();
    liste.ajoute(ironman);
    liste.ajoute(thor);
    System.out.println(liste);
    System.out.println(liste.maxPointsVie());

    DicoPersonnages dico = new DicoPersonnages();
    dico.ajoute(ironman.getNom(), ironman.getPoints());
    dico.ajoute(thor.getNom(), thor.getPoints());
    System.out.println(dico);
    System.out.println(dico.maxPointsVie());
  }
}
