public class Personnage {

   private String nom;
   private int pointsVie;

   public Personnage (String nom) {
     this.nom = nom;
     this.pointsVie = 5;
   } // 5 points de vie par défaut

   public Personnage (String nom, int points) {
     this.nom = nom;
     this.pointsVie = points;
   }

   public String getNom() {
     return this.nom;
   }

   public int getPoints() {
     return this.pointsVie;
   }

   @Override
   public String toString() {
     return this.nom + ": " + this.pointsVie;
   }
}
