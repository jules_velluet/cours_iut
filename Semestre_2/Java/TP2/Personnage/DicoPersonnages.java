import java.util.Map;
import java.util.HashMap;

public class DicoPersonnages {

 private Map<String, Integer> personnages;

   public DicoPersonnages() {
     this.personnages = new HashMap<>();
   }

   public void ajoute(String nom, int points) {
     this.personnages.put(nom, points);
   }

   @Override
   public String toString() {
       return "Mes personnages : "+this.personnages;
   }

   public String maxPointsVie() {
     int aux =  0;
     String res = null;
     for (String personnage : personnages.keySet()){
       if (aux < personnages.get(personnage)){
         aux = personnages.get(personnage);
         res = personnage;
       }
     }
     return res;
   }
}
