import java.util.ArrayList;
import java.util.List;

public class ListePersonnages {

   private List<Personnage> personnages;

   public ListePersonnages() {
     this.personnages = new ArrayList<>();
   }

   public void ajoute(Personnage personne) {
     this.personnages.add(personne);
   }

   @Override
   public String toString() {
       return "Liste de mes personnages : " + this.personnages;
   }
   /**
    renvoie le nom du personnage qui a le plus grand nombre de points de vie
   */
   public String maxPointsVie() {
     int aux = 0;
     String res= null;
     for (Personnage personne: personnages){
       if (aux < personne.getPoints()){
         aux = personne.getPoints();
         res = personne.getNom();
       }
     }
     return res;
   }
}
