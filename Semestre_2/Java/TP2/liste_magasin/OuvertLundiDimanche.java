public class OuvertLundiDimanche {
   private boolean lundi;
   private boolean dimanche;
   public OuvertLundiDimanche (boolean lundi, boolean dimanche) {
     this.lundi=lundi;
     this.dimanche=dimanche;
   }
   // les getters

   public boolean getOuvertLundi(){
     return this.lundi;
   }

   public boolean getOuvertDimanche(){
     return this.dimanche;
   }

   @Override
   public String toString() {
     return "("+this.lundi+", "+this.dimanche+")";
   }
}
