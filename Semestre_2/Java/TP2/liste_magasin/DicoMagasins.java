import java.util.HashMap ;
import java.util.Map ;
import java.util.ArrayList;
import java.util.List;

public class DicoMagasins {

   private Map<String, OuvertLundiDimanche> magasins;

   public DicoMagasins() {
     this.magasins = new HashMap<>();
   }

   public void ajoute(String nom, boolean lundi, boolean dimanche) {
     this.magasins.put(nom, new OuvertLundiDimanche(lundi,dimanche));
   }
   public List<String> ouvertsLeLundi() {
     ArrayList<String> res = new ArrayList<>();
     for (String magasin : magasins.keySet()){
       OuvertLundiDimanche a = magasins.get(magasin);
       if (a.getOuvertLundi() == true){
         res.add(magasin);
       }
     }
     return res;
   }
   public String toString() {
     return this.magasins.toString();
   }
}
//liste oersonnage + dico personnage
