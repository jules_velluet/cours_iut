public class Personne{

  private String prenom,nom;
  private int jour,mois,annee;
  private double taille;

  public Personne(String nom, String prenom, int jour, int mois, int annee, double taille){
    this.prenom=prenom;
    this.nom=nom;
    this.jour=jour;
    this.mois=mois;
    this.annee=annee;
    this.taille=taille;
  }
  public String getPrenom(){
    return this.prenom;
  }

  public String getNom(){
    return this.nom;
  }

  public int getAnnee(){
    return this.annee;
  }

  public void setAnneeNaissance(int annee){
    this.annee=annee;
  }

  public String signeAstrologique(){
    return "Belier";
  }
  public double getTaille(){
    return this.taille;
  }
}
