public class Executable2{
  public static void main(String[] args) {
    Lettre t = new Lettre('t');
    System.out.println(t.toAscii());
    System.out.println("En morse, "+t+"donne "+t.toMorse());
    String alphabet = "abcdefghijqlmnopkrstuvwxyz ";

    for (int i = 0 ; i<alphabet.length(); i++){
      char c = alphabet.charAt(i);
      Lettre lettre = new Lettre(c);
      system.out.println(lettre.toString()+": "+lettre.toMorse());
    }
  }
}
