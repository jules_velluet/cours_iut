public class Executable{

  public static void main(String[] args) {
    Lettre t = new Lettre('t', "===");
    System.out.println(t.toAscii());
    System.out.println("En morse, "+t+"donne "+t.toMorse());
    System.out.println(t.toChar());
  }
}
