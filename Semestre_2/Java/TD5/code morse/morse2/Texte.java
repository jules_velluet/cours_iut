import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class Texte{
  private List<Lettre> texte;

  public Texte(String chaine){
    this.texte = new ArrayList<>();
    for (char c : chaine.toCharArray()) {
      this.texte.add(new Lettre(c));
    }
  }

  public String toMorse(){
    String res = "";
    for (int i=0; i<this.texte.size(); i++){
      if (this.texte.get(i).equals(Lettre.morse[26])){
        res = res + Lettre.morse[26];
    }
    else {
      res = res + this.texte.get(i).toMorse() + "___";
    }
    }
    return res;
  }

  @Override
  public String toString(){
    String res = "";
    for (Lettre lettre: this.texte){
      res = res+lettre.toString();
    }
    return res;
  }

  public boolean contient(Lettre lettre){
    return this.texte.contains(lettre);
  }

  public static String decode(String texte) {
        List<String> res = new ArrayList<>();
        String[] splitted1 = texte.split("_______");
        for (String part : splitted1) {
            String[] splitted2 = part.split("___");

            res.addAll(Arrays.asList(splitted2));
            res.add("_______");
        }


        String res2 = "";
        for (String part : res) {
          int index = 0;
          int i = 0;
          for (String elem: Lettre.morse){
            if (elem.equals(part)){
              index = i;
            }
            i++;
          }
          res2 += Lettre.alphabet.charAt(index)+"";
        }
        return res2;
    }
}
