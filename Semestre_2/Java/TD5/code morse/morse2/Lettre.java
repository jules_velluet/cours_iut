public class Lettre{

  private char lettre;
  private String code;
  public static String [] morse = new String[] {
    "=_===",
      "===_=_=_=", "===_=_===_=", "===_=_=", "=",
      "=_=_===_=", "===_===_=", "=_=_=_=", "=_=",
      "=_===_===_===", "===_=_===", "=_===_=_=",
      "===_===", "===_=", "===_===_===", "=_===_===_=",
      "===_===_=_===", "=_===_=", "=_=_=", "===",
      "=_=_===", "=_=_=_===", "=_===_===", "===_=_=_===",
      "===_=_==_===", "===_===_=_=", "_______"
  };
  public static String alphabet = "abcdefghijqlmnopkrstuvwxyz ";

  public Lettre(char argument){
    this.lettre = argument;
  }

  public Lettre(String codemorse){
    int index = 0;
    int i = 0;
    for (String elem: Lettre.morse){
      if (elem.equals(codemorse)){
        index = i;
      }
      i++;
    }
    this.lettre = Lettre.alphabet.charAt(index);
  }

  public int toAscii(){
    if (this.lettre != ' '){
      return (int) this.lettre - 97;
    }
    else{
      return 26;
    }
  }

  public String toMorse(){
    return Lettre.morse[this.toAscii()];
  }

  @Override
  public String toString(){
    return this.lettre+" ";
  }

  public boolean equals(Object other){
    if (other == null){
      return false;
    }
    if (other instanceof Lettre){
      Lettre l = (Lettre) other;
      return this.lettre == l.lettre;
    }
    return false;
  }
}
