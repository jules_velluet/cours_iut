public class Executable2{
  public static void main(String[] args) {
    Lettre t = new Lettre('t');
    System.out.println(t.toAscii());
    System.out.println("En morse, "+t+"donne "+t.toMorse());

    for (int i = 0 ; i<Lettre.alphabet.length(); i++){
      char c = Lettre.alphabet.charAt(i);
      Lettre lettre = new Lettre(c);
      System.out.println(lettre.toString()+": "+lettre.toMorse());
    }
    for (String codeMorse: Lettre.morse){
      Lettre lettre = new Lettre(codeMorse);
      System.out.println(lettre.toString()+": "+lettre.toMorse());
    }
    Texte chaine = new Texte("ga bu");
    System.out.println(chaine.toMorse());

    Texte h = new Texte("ab");
    System.out.println(h.toString());

    System.out.println(h.contient(new Lettre('a')));

    System.out.println(Texte.decode("===___===_===_===_______==="));

  }
}
