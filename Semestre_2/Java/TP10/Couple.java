public class Couple implements Contenant<Integer>{

  private int x;
  private int y;

  public Couple(int x, int y){
    this.x = x;
    this.y = y;
  }

  @Override
  public boolean contient(Integer truc){
    return truc.equals(x) || truc.equals(y);
  }
}
