import java.util.HashSet;

public class Ensemble implements Contenant<Integer>{

  private Set<Integer> ensemble;

  public Ensemble(){
    this.ensemble = new HashSet<>();
  }

  public void add(Integer elem){
    this.ensemble.add(elem);
  }
}
