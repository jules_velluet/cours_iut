import java.util.List;

public class GestionContenants{

  public static boolean contiennentTous(List<Contenant> liste, T valeur){
    for (Contenant<T> elem: liste){
      if (!elem.contient(valeur)){
        return false;
      }
    }
    return true;
  }
}
