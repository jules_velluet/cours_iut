public class Vecteur3f{
  private double x,y,z;

  public Vecteur3f(double x, double y, double z){
    this.x=x;
    this.y=y;
    this.z=z;
  }
  public double getX(){
    return this.x;
  }
  public double getY(){
    return this.y;
  }
  public double getZ(){
    return this.z;
  }
  public void modif(double nombre, int indice){
    if(indice==1){
      this.x=nombre;
    }
    if (indice==2){
      this.y=nombre;
    }
    if (indice==3){
      this.z=nombre;
    }
  }

  public double norme(){
    return Math.sqrt(this.x*this.y*this.z);
  }
  public String toString(){
    return "Vecteur3f : <"+this.x+" "+this.y+" "+this.z+" > de norme : "+norme(); }
}
