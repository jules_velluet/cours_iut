public class ExecutableCoupleEntiers {
   public static void main(String [] args) {
       CoupleEntiers couple = new CoupleEntiers(-16,-4);
       CoupleEntiers couple2 = new CoupleEntiers(4,4);
       couple.setPrem(-16);
       couple.setSec(-4);
       System.out.println(couple); // (1)
       System.out.println(couple.fraction()); // (2)
       couple.setSec(2);
       couple.setPrem(2);
       System.out.println(couple.getPrem()+" "+couple.getSec());
       System.out.println(couple.somme()); // affiche -22
       System.out.println(couple.plus(couple2));
       System.out.println(couple.getPrem()+" "+couple.getSec());
   }
}
