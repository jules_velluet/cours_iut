public class CoupleEntiers {
   private int premier, second;
   public CoupleEntiers() {
       this.premier = 0;
       this.second = 2;
   }
   public CoupleEntiers(int premier, int second) {
       this.premier = premier;
       this.second = second;
     }
   public void setPrem(int premier) {this.premier = premier;}
   public void setSec(int second) {this.second = second;}
   public int getPrem() {return this.premier;}
   public int getSec() {return this.second;}
   public void permute() {
       int aux;
       aux = this.premier;
       this.premier = this.second;
       this.second = aux;
   }
   public int fraction() {
       return this.premier / this.second;
   }
   public int somme(){
     return this.premier+this.second;
   }
   public CoupleEntiers plus(CoupleEntiers a){
     return new CoupleEntiers(this.premier+a.premier,this.second+a.second);
   }
}
