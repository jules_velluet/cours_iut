public class Complexe {
  private int reel;
  private int imag;

  public Complexe(int reel, int imag) {
    this.reel=reel;
    this.imag=imag;
  }
  public int getReel(){
    return  this.reel;
  }
  public int getImag(){
    return this.imag;
  }
  public void afficheCartesien(){
    System.out.println("("+this.reel+", "+this.imag+")");
  }
  public void afficheComplexe(){
    System.out.println(this.reel+" +i "+this.imag);
  }
  public Complexe somme(Complexe a){
    return new Complexe(this.reel + a.reel, this.imag + a.imag);
  }
  public boolean egal(Complexe a, Complexe b){
    if (a.reel==b.reel & a.imag==b.imag){
      return true;
    }
    else {
      return false;

    }
  }
}
